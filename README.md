# su DSP conntroller

## Описание
Пульт диспетчера для мультиплеера в Trainz Simulator 2012 для системы [сигнализации sU](https://github.com/TRam1990/sU-core)
Версия скриптов соответствует не вышедшей в релиза версии 3.2

## Дальнейшее развитие
Данная версия пульта выложена в ознакомительных целях и не планируется к дальнейшему развитию. 
Для будущих версий Trainz разработана отдельная версия пульта, написаная с нуля, в связи с глобальными изменениями во внутренней структуре игры.