//=============================================================================
// File: zx_dsp_xtriggercore.gs
// Desc: Реализация Х-триггера
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_xtriggercore.gs"
include "zx_dsp_core.gs"
include "zx_dsp_ui.gs"

//=============================================================================
// Name: zx_DSP_MenuItem
// Desc: Базовый класс для Х-триггера  
//=============================================================================
final class zx_DSP_XTrigger isclass zx_DSP_XTriggerCore, zx_DSP_UI
{

  zx_DSP_JunctionInfo _forwardjncinfo;            //Стрелка по прямой
  zx_DSP_JunctionInfo _backwardjncinfo;           //Стрелка за спиной
  int _forwardjncinfodirection            = -1;   //Направление стрелки по прямой
  int _backwardjncinfodirection           = -1;   //Направление стрелка за спиной

  final thread void UpdateViewName(bool sleep)
  {
    if (sleep) me.Sleep(0.1);
    me.SetFXNameText("leftname", me.GetLocalisedName());
    me.SetFXNameText("rightname", me.GetLocalisedName());
  }

  final int ConvertDirection(int infodir)
  {
    if (infodir == zx_DSP_JunctionInfo.JNC_BACK) return Junction.DIRECTION_BACKWARD;
    else if (infodir == zx_DSP_JunctionInfo.JNC_RIGHT) return Junction.DIRECTION_RIGHT;
    else if (infodir == zx_DSP_JunctionInfo.JNC_LEFT) return Junction.DIRECTION_LEFT;
    return Junction.DIRECTION_NONE;
  }
  
  public int GetStatus(bool group)
  {
    if(!group){
      if(!_trainlock){
        if (
			_forwardjncinfo
			and (
				_forwardjncinfo.state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL
				or (
					_forwardjncinfo.state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT | zx_DSP_JunctionInfo.STATE_TRAIN | zx_DSP_JunctionInfo.STATE_LOCK)
					and (
						_isxjunction
						or _forwardjncinfodirection == zx_DSP_JunctionInfo.JNC_BACK
						or _forwardjncinfo.jnc.GetDirection() == me.ConvertDirection(_forwardjncinfodirection)
					)
				)
			)
		) {
			return STATUS_PATH;
		}
        if (
			_backwardjncinfo
			and (
				_backwardjncinfo.state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL
				or (
					_backwardjncinfo.state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT | zx_DSP_JunctionInfo.STATE_TRAIN | zx_DSP_JunctionInfo.STATE_LOCK)
					and (
						_isxjunction
						or _backwardjncinfodirection == zx_DSP_JunctionInfo.JNC_BACK
						or _backwardjncinfo.jnc.GetDirection() == me.ConvertDirection(_backwardjncinfodirection)
					)
				)
			)
		) {
			return STATUS_PATH;
		}
      }else return STATUS_TRAIN;
    } else if(_neighbor and _neighbor == Router.GetGameObject(_neighborid)) return _neighbor.GetStatus(false);
    return STATUS_NONE;
  }

  final Junction FindFirstJunction(bool forward)
  {
    GSTrackSearch searcher = BeginTrackSearch(forward);
    MapObject mapobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
    while(mapobject and !mapobject.isclass(Junction)){
      if(distance >= 5000.0 and mapobject.isclass(Trackside)) searcher = (cast<Trackside>mapobject).BeginTrackSearch(searcher.GetFacingRelativeToSearchDirection()); //Заглушка от потери поиска (наследие TRS2012. Возможно, больше не нужна)
      mapobject = searcher.SearchNext();
      distance = searcher.GetDistance();
    }
    return cast<Junction>mapobject;
  }

  //Определение направления с которого пришёл поиск к стрелке
  //jnc - Стрелка для которой выполняется проверка
  final int GetJunctionArrivalDirection(Junction jnc)
  {
    int i, retvalue;
    for(i = 0; i < 3; i++){
      GSTrackSearch searcher;
      if(i == 2){
        searcher = jnc.BeginTrackSearch(Junction.DIRECTION_RIGHT);
        retvalue = zx_DSP_JunctionInfo.JNC_RIGHT;
      }else if(i == 1){
        searcher = jnc.BeginTrackSearch(Junction.DIRECTION_LEFT);
        retvalue = zx_DSP_JunctionInfo.JNC_LEFT;
      }else{
        searcher = jnc.BeginTrackSearch(Junction.DIRECTION_BACKWARD);
        retvalue = zx_DSP_JunctionInfo.JNC_BACK;
      }
      MapObject mapobject = searcher.SearchNext();
      float distance = searcher.GetDistance();
      while(mapobject and !mapobject.isclass(Junction) and mapobject != me) { 
        if(distance >= 5000.0 and mapobject.isclass(Trackside)) searcher = (cast<Trackside>mapobject).BeginTrackSearch(searcher.GetFacingRelativeToSearchDirection()); //Заглушка от потери поиска (наследие TRS2012. Возможно, больше не нужна)
        mapobject = searcher.SearchNext();
        distance = searcher.GetDistance();
      }
      if(mapobject == me) return retvalue;
    }
    return -1;
  }

  void OnModuleInit(Message msg)
  {
    if(World.GetCurrentModule() == World.DRIVER_MODULE){
      zx_DSP_Core controller = cast<zx_DSP_Core>_library.GetController();
      if(controller) {
        Junction forwardjnc = FindFirstJunction(true); //Ищем стрелки перед собой
        Junction backwardjnc = FindFirstJunction(false); //Ищем стрелки за собой
        if(forwardjnc){
          _forwardjncinfo = controller.FindJunctionInfo(forwardjnc, false, true);
          _forwardjncinfodirection = GetJunctionArrivalDirection(forwardjnc);
        }
        if(backwardjnc){
          _backwardjncinfo = controller.FindJunctionInfo(backwardjnc, false, true);
          _backwardjncinfodirection = GetJunctionArrivalDirection(backwardjnc);
        }
      }
    }
  }
  
  final string ModeToString(StringTable strTable)
  {
    if(_isxjunction) return strTable.GetString("xjunction");
    return strTable.GetString("xtrack");
  }

  string GetPropertyType(string propertyID)
  {
    if(propertyID == "mode" or propertyID == "resettrigger") return "link";
    else if(propertyID == "selecttrigger") return "map-object,TO";
    return inherited(propertyID);
  }
  
  bool FilterPropertyElementList(string propertyID, GSObject[] listObjects, string[] listNames)
  {
    if(propertyID == "selecttrigger") {
      int index = 0, removeindex = -1, removecount = 0;
      while(index < listObjects.size()) {
        zx_DSP_XTriggerCore xtrigger = cast<zx_DSP_XTriggerCore>Router.GetGameObject(listObjects[index]);
        bool valid = xtrigger and xtrigger != me;
        if(valid and removeindex >= 0) {
          listObjects[removeindex, removeindex + removecount] = null;
          listNames[removeindex, removeindex + removecount] = null;
          index = removeindex;
          removeindex = -1;
          removecount = 0;
          continue; 
        } else if (!valid) {
          if(removeindex == -1) removeindex = index;
          ++removecount;
        }
        ++index; 
      }
      if(removeindex >= 0) {
        listObjects[removeindex, ] = null;
        listNames[removeindex, ] = null;
      } 
      return true;
    }
    return inherited(propertyID, listObjects, listNames);
  }

  string GetDescriptionHTML(void)
  {
    StringTable strtable = GetAsset().GetStringTable();
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    string triggername = "";
    if(_neighbor and _neighbor == Router.GetGameObject(_neighborid))
      triggername = _neighbor.GetLocalisedName();

    buffer.Print("<font size=10 color=#" + GetUIPropertyValue("labelcolor-title") +"><b>");
    buffer.Print(strtable.GetString("title"));
    buffer.Print("</b></font><br><br>");

    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("general")), "colspan=5"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("trigger") + ":"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(triggername, GetUIPropertyValue("labelcolor-value"))));
        buffer.Print(HTMLWindow.StartCell());
          MakeActionButton(buffer, "live://property/selecttrigger", "...", strtable.GetString("tooltip-trigger-select"), true, true, GetUIPropertyValue("color-action-select"));
          MakeActionButton(buffer, "live://property/resettrigger", "X", strtable.GetString("tooltip-trigger-reset"), triggername != "", false, GetUIPropertyValue("color-action-reset"));
        buffer.Print(HTMLWindow.EndCell());
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("mode") + ":"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/mode", ModeToString(strtable), strtable.GetString("tooltip-mode"))));
      buffer.Print(HTMLWindow.EndRow());
    buffer.Print(HTMLWindow.EndTable());
    buffer.Print(HTMLWindow.StartFontColor(GetUIPropertyValue("labelcolor-description")));    
    buffer.Print("<br /><br />");
    buffer.Print(strtable.GetString("description"));
    buffer.Print("<br /><br />");
    buffer.Print(HTMLWindow.EndFontColor());    
    return buffer.AsString();
  }

  void SetPropertyValue(string propertyID, GSObject value, string readableName) 
  {
    if(propertyID == "selecttrigger") {
      zx_DSP_XTriggerCore xtrigger = cast<zx_DSP_XTriggerCore>Router.GetGameObject(value);
      if(xtrigger) SetMeNeighbor(xtrigger);
    } else inherited(propertyID, value, readableName);
  }

  void LinkPropertyValue(string propertyID)
  {
    if(propertyID == "mode") SetMeXJunction(!_isxjunction);
    else if(propertyID == "resettrigger") ResetMeNeighbor();
    else inherited(propertyID);
  }

  public void SetProperties(Soup soup)
  {
    inherited(soup);
    UpdateViewName(false);
  }

  public void Init(Asset asset)
  {
    inherited(asset);
    LoadUIProperties(asset);
    UpdateViewName(true);
  }

};