include "zx_dsp_junction_link_marker_base.gs"
include "zx_dsp_ui.gs"

class zx_DSP_JunctionLinkMarker isclass zx_DSP_JunctionLinkMarkerBase, zx_DSP_UI 
{

  string GetDescriptionHTML(void) {
    StringTable strtable = GetAsset().GetStringTable();
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    buffer.Print("<font size=10 color=#" + GetUIPropertyValue("labelcolor-title") +"><b>");
    buffer.Print(strtable.GetString("title"));
    buffer.Print("</b></font><br><br>");
    buffer.Print(HTMLWindow.StartFontColor(GetUIPropertyValue("labelcolor-description")));    
    buffer.Print("<br /><br />");
    buffer.Print(strtable.GetString("description"));
    buffer.Print("<br /><br />");
    return buffer.AsString();
  }


  void Init(Asset asset) 
  {
    inherited(asset);
    LoadUIProperties(asset);
  }

};
