﻿
kuid                                    <kuid:151055:60082>
kind                                    "scenery"
trackside                               0
trainz-build                            4.5
username                                "TDT sU DSP Span Marker"
category-class                          "WX"
category-keyword						"sudspcontroller;dspcontroller;su;trainzdevteam"
surveyor-only                           1
autoname                                1
license									"freeware"
script                                  "zx_dsp_spanmarker.gs"
class                                   "zx_DSP_SpanMarker"
author                                  "Алексей 'Эрендир' Зверев; Камиль Ахметзянов aka kemal"
contact-email                           "erendir@trainzdevteam.ru"
contact-website                         "trainzdevteam.ru"
organisation                            "Trainz Dev Team"
info-url								"http://wiki.trainzdevteam.ru/ru/dsp-controller/dsp-span-marker"

mesh-table
{
  default
  {
    mesh                                "marker.trainzmesh"
    auto-create                         1
    light                               0
    does-cast-shadows                   0
  }

  type-ab
  {
    mesh                                "typeab.trainzmesh"
    auto-create                         1
    light                               0
    does-cast-shadows                   0
  }

  type-pab
  {
    mesh                                "typepab.trainzmesh"
    auto-create                         0
    light                               0
    does-cast-shadows                   0
  }

  arrow-forward
  {
    mesh                                "arrowf.trainzmesh"
    auto-create                         1
    light                               0
    does-cast-shadows                   0
  }

  arrow-backward
  {
    mesh                                "arrowb.trainzmesh"
    auto-create                         1
    light                               0
    does-cast-shadows                   0
  }

}

script-include-table
{
  dsp-library                           <kuid2:151055:60027:10>
}

string-table
{
  title									"Настройки перегона"
  settings								"Настройки"
  spantype								"Тип перегона"
  spantype-autolock						"автоблокировка (АБ)"
  spantype-semiautolock					"полуавтоматическая (ПАБ)"
  direction								"Доступные направления"
  direction-forward						"по маркеру"
  direction-backward					"против маркера"
  direction-auto						"авто выбор"
  direction-both						"оба"
  initdirection							"Начальное направление"
  turnonebutton							"Разворот одной кнопкой"
  autoarrival							"Авто прибытие"
  tooltip-spantype						"Щёлкните здесь для изменения типа перегона."
  tooltip-direction						"Щёлкните здесь для выбора доступного навправления перегона."
  tooltip-initdirection					"Щёлкните здесь для выбора начального направления перегона."
  tooltip-turnonebutton					"Щёлкните здесь для включения/отключения возможности разворота перегона нажатием одной кнопки."
  tooltip-autoarrival					"Щёлкните здесь для включения/отключения автоматического подтверждения прибытия поезда с перегона."
  description							"Позволяет настроить перегон между станциями. Устанавливается в любое место на перегоне в одном экземпляре на каждый путь перегона.<br /><b>Внимание!</b> Все настройки учитываются только при расчёте станций."
  description-autolock					"<b>Автоблокировка (АБ)</b><br />   • <b>Доступные направления</b> — Направления, в которых возможно движение поездов по перегону относительно направления маркера.<br />   • <b>Начальное направление</b> — Направление перегона при старте сессии относительно направления маркера.<br />   • <b>Разворот одной кнопкой</b> — Если опция включена, перегон будет разворачиваться при постройке выходного маршрута или при нажатии кнопки «развернуть перегон» на вкладке управления перегонами. В противном случае, для разворота перегона будет требоваться разрешения соседней станции."
  description-semiautolock				"<b>Полуавтоматическая блокировка (ПАБ)</b><br />   • <b>Доступные направления</b> — Направления, в которых возможно движение поездов по перегону относительно направления маркера.<br />   • <b>Авто прибытие</b> — Если опция включена, то прибытие поезда на станцию с перегона будет фиксироваться автоматически по числу осей (вагонов) в составе. В противном случае, дежурному станции необходимо будет нажать кнопку фиксации прибытия во вкладке управления перегонами."
}

extensions
{
  ui-properties
  {
    0
    {
      build						4.5
	  labelcolor-title			"7ac1ff"
	  labelcolor-disabled		"596167"
	  labelcolor-description	"748a94"
    }
    1
    {
      build						4.6
	  labelcolor-title			"145eb9"
	  labelcolor-disabled		"7f7f7f"
	  labelcolor-description	"535353"
    }
  }
}


thumbnails
{
  0
  {
    image                               "thumbnail.jpg"
    width                               240
    height                              180
  }
}

kuid-table
{
  dsp-library                           <kuid2:151055:60027:10>
}
