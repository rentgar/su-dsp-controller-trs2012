//=============================================================================
// File: zx_dsp_spanmarker.gs
// Desc: Реализация маркера перегона
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_spaninfo.gs"
include "zx_dsp_ui.gs"

//=============================================================================
// Name: zx_DSP_SpanMarker
// Desc: Класс, реализующий маркер перегона
//=============================================================================
final class zx_DSP_SpanMarker isclass zx_DSP_SpanInfo, zx_DSP_UI
{
 
  string Type2String(StringTable strtable)
  { 
    if (_type == TYPE_SEMIAUTOLOCK) return strtable.GetString("spantype-semiautolock");
    return strtable.GetString("spantype-autolock");
  }

  string Direction2String(StringTable strtable, int direction)
  {
    if (direction == DIRECT_BOTH) return strtable.GetString("direction-both");
    else if (direction == DIRECT_BACKWARD) return strtable.GetString("direction-backward");
    else if (direction == DIRECT_FROWARD) return strtable.GetString("direction-forward");
    return strtable.GetString("direction-auto");
  }

  string GetPropertyType(string propertyID)
  {
    if (propertyID == "type" or propertyID[0, 9] == "direction" or propertyID[0, 13] == "initdirection" or propertyID[0, 11] == "bitproperty") return "link";
    return inherited(propertyID);
  }

  string GetDescriptionHTML(void) {
    StringTable strtable = GetAsset().GetStringTable();
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    int type = GetSpanType(), i;
    int direction = GetSpanProperty(PROP_DIRECTION); 
    int initdirection = GetSpanProperty(PROP_INITDIRECTION);
    string disabledcolor = GetUIPropertyValue("labelcolor-disabled");  
    buffer.Print("<font size=10 color=#" + GetUIPropertyValue("labelcolor-title") +"><b>");
    buffer.Print(strtable.GetString("title"));
    buffer.Print("</b></font><br><br>");
    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("settings")), "colspan=3"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("spantype") + ":"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/type", Type2String(strtable), strtable.GetString("tooltip-spantype"))));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("direction") + ":"));
        buffer.Print(HTMLWindow.StartCell());
          for (i = 1; i <= 3; ++i) MakeMultiActionItem(buffer, "live://property/direction/" + i, Direction2String(strtable, i), strtable.GetString("tooltip-direction"), true, direction == i, i == 1, disabledcolor);    
        buffer.Print(HTMLWindow.EndCell());
      buffer.Print(HTMLWindow.EndRow());
      if (type == TYPE_AUTOLOCK) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("initdirection") + ":"));
          buffer.Print(HTMLWindow.StartCell());
            for (i = 1; i <= 3; ++i) {
              MakeMultiActionItem(buffer, "live://property/initdirection/" + i, Direction2String(strtable, i), strtable.GetString("tooltip-initdirection"), /*i == 0 or direction & i*/ direction == DIRECT_BOTH,  initdirection == i, i == 1, disabledcolor);
              if (i == 2) i = -1;
              else if (i == 0) break;    
            } 
          buffer.Print(HTMLWindow.EndCell());
        buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("turnonebutton") + ":"));
          buffer.Print(HTMLWindow.MakeCell(MakeCheckBox("bitproperty." + PROP_TURNONEBUTTON, GetSpanProperty(PROP_TURNONEBUTTON), strtable.GetString("tooltip-turnonebutton"))));
        buffer.Print(HTMLWindow.EndRow());
      }
      if (type == TYPE_SEMIAUTOLOCK) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("autoarrival") + ":"));
          buffer.Print(HTMLWindow.MakeCell(MakeCheckBox("bitproperty." + PROP_AUTOARRIVAL, GetSpanProperty(PROP_AUTOARRIVAL), strtable.GetString("tooltip-autoarrival"))));
        buffer.Print(HTMLWindow.EndRow());
      }   
    buffer.Print(HTMLWindow.EndTable());
    buffer.Print(HTMLWindow.StartFontColor(GetUIPropertyValue("labelcolor-description")));    
    buffer.Print("<br /><br />");
    buffer.Print(strtable.GetString("description"));
    buffer.Print("<br /><br />");
    if (type == TYPE_AUTOLOCK) buffer.Print(strtable.GetString("description-autolock"));
    else if (type == TYPE_SEMIAUTOLOCK) buffer.Print(strtable.GetString("description-semiautolock"));
    buffer.Print("<br /><br />");
    buffer.Print(HTMLWindow.EndFontColor());    
    return buffer.AsString();
  }

  void LinkPropertyValue(string propertyID)
  {
    if (propertyID == "type") {
      int ctype = GetSpanType();
      if(ctype == TYPE_SEMIAUTOLOCK) ctype = TYPE_AUTOLOCK;
      else ++ctype; 
      SetSpanType(ctype);
    } else if (propertyID[0, 9] == "direction") {
      int direction = Str.ToInt(propertyID[10,]);
      SetSpanProperty(PROP_DIRECTION, direction);
    } else if (propertyID[0, 13] == "initdirection") {
      int direction = Str.ToInt(propertyID[14,]);
      SetSpanProperty(PROP_INITDIRECTION, direction);
    } else if (propertyID[0, 11] == "bitproperty") {
      int property = Str.ToInt(propertyID[12,]);
      int value = GetSpanProperty(property) + 1;
      if (value > 1) value = 0;
      SetSpanProperty(property, value);
    } else inherited(propertyID);
  }
  
  void OnSpanTypeChanged(void) 
  {
    SetMeshVisible("type-ab", _type == TYPE_AUTOLOCK, 0.0);
    SetMeshVisible("type-pab", _type == TYPE_SEMIAUTOLOCK, 0.0);
  }
  
  void OnSpanPropertyChanged(int property) 
  {
    if (property == PROP_DIRECTION) {
      int value = GetSpanProperty(property);
      SetMeshVisible("arrow-forward", value & DIRECT_FROWARD, 0.0);
      SetMeshVisible("arrow-backward", value & DIRECT_BACKWARD, 0.0);
    }
  }

  
  void Init(Asset asset) 
  {
    inherited(asset);
    LoadUIProperties(asset);
  }

};
