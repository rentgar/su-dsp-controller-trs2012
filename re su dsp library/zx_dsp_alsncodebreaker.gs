//=============================================================================
// File: zx_dsp_alsncodebreaker.gs
// Desc: Базовый класс маркера прерывания АЛСН кода
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "trackside.gs"

//=============================================================================
// Name: zx_DSP_ALSNCodeBreaker
// Desc: Базовый класс прерывания кодирования ALSN 
//=============================================================================
class zx_DSP_ALSNCodeBreaker isclass Trackside
{

  //=============================================================================
  // Name: BreakOnlyOneSide
  // Desc: Возвращает значение указывающее что маркер прерывает код АЛСН только
  //       в одном направлении (прямом).
  // Retn: Значение true, если код АЛСН прерывается только в одном направлении;  
  //       в противном случае — значение false. 
  //=============================================================================
  public final bool BreakOnlyOneSide(void);
  
  //=============================================================================
  // Name: SetBreakOnlyOneSide
  // Desc: Устанавливает значение, указывающее, должно ли выполнятся прерывание  
  //       кода АЛСН только в одном направлении или в обоих направлениях. 
  // Parm: onlyOneSide — Значение true, если прерывание кода АЛСН должно 
  //       выполняться только в одном направлении; противном случае — 
  //       значение false. 
  //=============================================================================
  final void SetBreakOnlyOneSide(bool onlyOneSide);
  
  //=============================================================================
  // Name: OnBreakOnlyOneSideChnaged
  // Desc: Вызывается автоматически при изменении значения свойства
  //       BreakOnlyOneSide. 
  // Note: Для реализации должно быть переопределно в классе наследнике.
  //=============================================================================
  void OnBreakOnlyOneSideChnaged() {}
  
	//
	// РЕАЛИЗАЦИЯ
	//

  bool _onlyoneside = false;  

  public final bool BreakOnlyOneSide(void)
  {
    return _onlyoneside;
  }

  final void SetBreakOnlyOneSide(bool onlyOneSide)
  {
    if (onlyOneSide != _onlyoneside) {
      _onlyoneside = onlyOneSide;
      OnBreakOnlyOneSideChnaged();
    }
  }

  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    soup.SetNamedTag("OnlyOneSide", _onlyoneside);
    return soup;
  }

  public void SetProperties(Soup soup)
  {
    inherited(soup);
    SetBreakOnlyOneSide(soup.GetNamedTagAsBool("OnlyOneSide"));
  }

};