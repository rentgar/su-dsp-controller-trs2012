//=============================================================================
// File: zx_dsp_spaninfo.gs
// Desc: Базовый класс маркера перегона.
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "trackside.gs"

//=============================================================================
// Name: zx_DSP_SpanInfo
// Desc: Базовый класс маркера перегона
//=============================================================================
class zx_DSP_SpanInfo isclass Trackside
{
  
  //=============================================================================
  // Name: TYPE_*
  // Desc: Доступные типы перегона.
  //=============================================================================
  public define int TYPE_NULL           = -1;                                 //Пустой перегон без светофоров
  public define int TYPE_AUTOLOCK       = 0;                                  //Автоблокировка
  public define int TYPE_SEMIAUTOLOCK   = 1;                                  //Полу автоматическая блокировка
  
  //=============================================================================
  // Name: DIRECT_*
  // Desc: Флаги доступных направлений перегона.
  //=============================================================================
  public define int DIRECT_AUTO         = 0;                                  //Автоматический выбор начального направления перегона. 
  public define int DIRECT_FROWARD      = 1;                                  //Направление перегона по направлению маркера.
  public define int DIRECT_BACKWARD     = 2;                                  //Направление перегона в противоположном от направлении маркера направлении
  public define int DIRECT_BOTH         = DIRECT_FROWARD | DIRECT_BACKWARD;   //Направление перегона в обоих направлениях маркера
  
  //=============================================================================
  // Name: PROP_*
  // Desc: Коды свойств перегона.
  //=============================================================================
  public define int PROP_DIRECTION      = 0;                                  //Свойство направления перегона
  public define int PROP_INITDIRECTION  = 1;                                  //Свойство начального направления перегона
  public define int PROP_TURNONEBUTTON  = 2;                                  //Возможность разворота с одной кнопки
  public define int PROP_AUTOARRIVAL    = 3;                                  //Автоприбытие

  //=============================================================================
  // Name: GetSpanType
  // Desc: Возвращает тип перегона, назначенный в маркере.
  // Retn: Значение одной из констант TYPE_*, определчющаяя тип перегона. 
  //=============================================================================
  public final int GetSpanType(void);
  
  //=============================================================================
  // Name: SetSpanType
  // Desc: Назначает тип перегона в маркере. 
  // Parm: spanType — значение одной из констант TYPE_*, определяющее тип 
  //       перегона
  //=============================================================================
  final void SetSpanType(int spanType);
  
  //=============================================================================
  // Name: GetSpanProperty
  // Desc: Возвращает значение свойства перегона.
  // Retn: Значение одной из констант PROP_*, определяющее свойство перегона, 
  //       значение которого требуется получить. 
  //=============================================================================
  public int GetSpanProperty(int property);
  
  //=============================================================================
  // Name: SetSpanProperty
  // Desc: Устанавливает значение свойства перегона
  // Parm: property — значение одной из констант PROP_*, указывающей свойство 
  //       перегона, значение которого требуется изменить.
  // Parm: value — устанавливаемое значение свойства перегона. 
  // Retn: Значение true, если значение свойства было изменено; в противном 
  //       случае — значение false, если данное значение свойства не допустимо
  //       для этого перегона или данный тип перегона не поддерживает это 
  //       свойство. 
  //=============================================================================
  final bool SetSpanProperty(int property, int value);
  
  //=============================================================================
  // Name: OnSpanTypeChanged
  // Desc: Вызывается автоматически при изменении типа перегона.
  // Note: Для реализации требуется переопределить в наследующем классе. 
  //=============================================================================
  void OnSpanTypeChanged(void) {}
  
  //=============================================================================
  // Name: OnSpanPropertyChanged
  // Desc: Вызывается автоматически при изменении свойств перегона.
  // Parm: property — значение одной из констант PROP_*, указывающей свойство 
  //       перегона, которое было изменено.
  // Note: Для реализации требуется переопределить в наследующем классе. 
  //=============================================================================
  void OnSpanPropertyChanged(int property) {}

	//
	// РЕАЛИЗАЦИЯ
	//

  int _type = TYPE_AUTOLOCK;
  int[] _properties = new int[4];
                 
  //Возвращает тип перегона.
  public final int GetSpanType(void) 
  {
    return _type;
  }
  
  //Установка типа перегона. 
  //spanType - одна из констант TYPE_*, определяющая тип перегона. 
  final void SetSpanType(int spanType)
  {
    if ((spanType == TYPE_AUTOLOCK or spanType == TYPE_SEMIAUTOLOCK) and spanType != _type) {
      _type = spanType;
      if (spanType == TYPE_AUTOLOCK) _properties[PROP_TURNONEBUTTON] = 0;
      else _properties[PROP_AUTOARRIVAL] = 1;
      OnSpanTypeChanged();         
    }
  }
  
  public int GetSpanProperty(int property)
  {
    if (property >= 0 and property < _properties.size()) return _properties[property];
    return 0;
  }

  final bool SetSpanProperty(int property, int value)
  {
    if (property >= 0 and property < _properties.size()) {
      if ((property == PROP_DIRECTION and (value < 1 or value > 3)) or
          (property == PROP_INITDIRECTION and (_type != TYPE_AUTOLOCK or value < 0 or value > 2)) or
          ((property == PROP_TURNONEBUTTON or property == PROP_AUTOARRIVAL) and (value < 0 or value > 1))) return false;
      if (_properties[property] != value) {
        _properties[property] = value;
        if (property == PROP_DIRECTION and !(_properties[property] & _properties[PROP_INITDIRECTION]) and 
        _properties[PROP_INITDIRECTION] != 0) _properties[PROP_INITDIRECTION] = DIRECT_AUTO;  
        OnSpanPropertyChanged(property);
      }
      return true;
    }
    return false;
  }
  
  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    int i, count = _properties.size();
    for (i = 0; i < count; ++i)
      soup.SetNamedTag("Property." + i, _properties[i]);
    soup.SetNamedTag("Type", _type);
    return soup;
  }
  
  public void SetProperties(Soup soup)
  {
    inherited(soup);
    int i, count = _properties.size();
    for (i = 0; i < count; ++i)
      _properties[i] = soup.GetNamedTagAsInt("Property." + i, _properties[i]);
    _type = soup.GetNamedTagAsInt("Type", _type);
    OnSpanTypeChanged();
  }
  
  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    _properties[PROP_DIRECTION] = DIRECT_BOTH; 
    _properties[PROP_TURNONEBUTTON] = 0;
    _properties[PROP_AUTOARRIVAL] = 1; 
  }

};
