//=============================================================================
// File: zx_dsp_stationborder.gs
// Desc: Базовый класс маркера границы станции
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "trackside.gs"

//=============================================================================
// Name: zx_DSP_StationBorder
// Desc: Базовый класс маркера границы станции 
//=============================================================================
class zx_DSP_StationBorder isclass Trackside
{

  //=============================================================================
  // Name: CanPathPassage
  // Desc: Возвращает указатель указывающий на возможность прохода маршрута через
  //       этот маркер.
  // Retn: Значение true, если маршрут может проходить через маркер; в противном
  //       случае — значение false. 
  //=============================================================================
  public final bool CanPathPassage(void);

  //=============================================================================
  // Name: SetCanPathPassage
  // Desc: Устанавливает значение, указывающее, может ли маршрут проходить через
  //       этот маркер. 
  // Parm: can — Значение true, если маршрут может проходить через этот мркер; в
  //       противном случае — значение false. 
  //=============================================================================
  final void SetCanPathPassage(bool can);


	//
	// РЕАЛИЗАЦИЯ
	//
  
  bool _canpathpassage; 

  public final bool CanPathPassage(void)
  {
    return _canpathpassage;
  }

  final void SetCanPathPassage(bool can)
  {                                     
    _canpathpassage = can;
  }

  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    soup.SetNamedTag("CanPathPassage", _canpathpassage);
    return soup;
  }

  public void SetProperties(Soup soup)
  {
    inherited(soup);
    _canpathpassage = soup.GetNamedTagAsInt("CanPathPassage");
  }


};
