﻿
kuid                                    <kuid2:151055:60027:12>
kind                                    "library"
trainz-build                            4.5
username                                "TDT sU DSP Library"
category-class                          "YL"
category-keyword						"sudspcontroller;dspcontroller;su;trainzdevteam"
license									"freeware"
class                                   "zx_DSP_Library"
script                                  "zx_dsp_library"
author                                  "Алексей 'Эрендир' Зверев; Камиль Ахметзянов aka kemal"
contact-email                           "erendir@trainzdevteam.ru"
contact-website                         "trainzdevteam.ru"
organisation                            "Trainz Dev Team"
info-url								"http://wiki.trainzdevteam.ru/ru/dsp-controller"

script-include-table
{
  su-core                               <kuid2:400260:100120:62>
  extended-class-library				<kuid:151055:60076>
}

string-table
{
  showinfo								"Показать блок информации"
  showadmin                             "Администрирование"
  sound-turn-off						"Отключить звуковые уведомления"
  sound-turn-on							"Включить звуковые уведомления"
  trainnumber							"$0/$1"
  tooltip                               "Пульт диспетчера sU"
  
  menu-controller						"Показать пульт диспетчера"
  menu-driverjunctionlist				"Управление стрелками ТЧМ"
  menu-journal							"Журнал событий"
  
}

thumbnails
{
  0
  {
    image                               "thumbnail.jpg"
    width                               240
    height                              180
  }
}


extensions
{
	ui-properties
	{
		0
		{
			build						4.5
			file-numbereditor			"tane-trainnumbereditor.htm"
			file-numberview				"tane-trainnumberview.htm"
			width						183
			height-numbereditor			150
			height-numberview			42
		}
		1
		{
			build						4.6
			file-numbereditor			"trz2019-trainnumbereditor.htm"
			file-numberview				"trz2019-trainnumberview.htm"
			width						183
			height-numbereditor			150
			height-numberview			42
			style-numberview			0
		}
	
	}
}

kuid-table
{
  su-core                               <kuid2:400260:100120:62>
  extended-class-library				<kuid:151055:60076>
  driver-module                         <kuid:-16:10240>
  menuicon                              <kuid2:151055:21028:3>
}

