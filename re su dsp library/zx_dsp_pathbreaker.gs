//=============================================================================
// File: zx_dsp_pathbreaker.gs
// Desc: Базовый класс прерывателя маршрута
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "trackside.gs"

//=============================================================================
// Name: zx_DSP_PathBreaker
// Desc: Базовый класс для прерывателя маршрутов 
//=============================================================================
class zx_DSP_PathBreaker isclass Trackside
{

  //=============================================================================
  // Name: ShuntPathBreaked
  // Desc: Возвращает указатель наличия прерывания маневрового маршрута в 
  //       указанном направлении
  // Parm: direct – Значение true, если проверка по направлению маркера или
  //       значение false, если проверка для проивоположного направлению маркера.
  // Retn: Значение true, если маршрут недопустим; в против случае — значение 
  //       false. 
  //=============================================================================
  public bool ShuntPathBreaked(bool direct);

  //=============================================================================
  // Name: PathBreaked
  // Desc: Возвращает указатель наличия прерывания поездного маршрута в указанном 
  //       направлении.
  // Parm: direct – Значение true, если проверка по направлению маркера или
  //       значение false, если проверка для проивоположного направлению маркера.
  // Retn: Значение true, если маршрут недопустим; в против случае — значение 
  //       false. 
  //=============================================================================
  public bool PathBreaked(bool direct);

  //=============================================================================
  // Name: IsStaionBorder
  // Desc: Возвращает указатель наличия границы станции.
  // Retn: Значение true, если является границей станции; в против случае —
  //       значение false.
  //=============================================================================
  public bool IsStaionBorder();

	//
	// РЕАЛИЗАЦИЯ
	//

  //Типы прерываний
  define int TYPE_FORWARD_SHUNT       = 1 << 0;   //Прерывание маневрового маршрута в прямом направлении
  define int TYPE_BACKWARD_SHUNT      = 1 << 1;   //Прерывание маневрового маршрута в обратном направлении
  define int TYPE_FORWARD_PATH        = 1 << 2;   //Прерывание маршрута в прямом направлении
  define int TYPE_BACKWARD_PATH       = 1 << 3;   //Прерывание маршрута в обратном направлении
  define int TYPE_STATION_BORDER      = 1 << 4;   //Граница станции

  int action; //Прерывание  (одна из констант TYPE_)

  public bool ShuntPathBreaked(bool direct)
  {
    if((direct and action & TYPE_FORWARD_SHUNT) or
    (!direct and action & TYPE_BACKWARD_SHUNT)) return true;
    return false;
  }

  public bool PathBreaked(bool direct)
  {
    if((direct and action & TYPE_FORWARD_PATH) or
    (!direct and action & TYPE_BACKWARD_PATH)) return true;
    return false;
  }

  public bool IsStaionBorder() {
    return (action & TYPE_STATION_BORDER) and true;
  }

  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    soup.SetNamedTag("Action", action);
    return soup;
  }

  public void SetProperties(Soup soup)
  {
    inherited(soup);
    action = soup.GetNamedTagAsInt("Action");
    if (soup.GetIndexForNamedTag("CanPathPassage") >= 0) {
      action = action | TYPE_STATION_BORDER;
      if (soup.GetNamedTagAsBool("CanPathPassage")) {
        action = action & ~(TYPE_FORWARD_PATH | TYPE_BACKWARD_PATH);
      }
      else {
        action = action | TYPE_FORWARD_PATH | TYPE_BACKWARD_PATH;
      }
    }
  }

};