//=============================================================================
// File: zx_dsp_ui.gs
// Desc: Базовый класс для реализации дполнительных инструментов построения
//       пользовательского интерфейса.
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================


include "common.gs"

class zx_DSP_UI {

  Soup _uistyles;
  
  final void LoadUIProperties(Asset asset)
  {
    float gameversion = TrainzScript.GetTrainzVersion();
    float currversion = 0.0;
    Soup uitablesoup = asset.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("ui-properties");
    int i, count = uitablesoup.CountTags();
    for(i = 0; i < count; ++i) {
      Soup uisoup = uitablesoup.GetNamedSoup(uitablesoup.GetIndexedTagName(i));
      float version = uisoup.GetNamedTagAsFloat("build");
      if(version <= gameversion and version > currversion) {
        _uistyles = uisoup;
        currversion = version;
      }
    }
  }
  
  final string GetUIPropertyValue(string propertyName)
  {
    if (!_uistyles) return "";
    return _uistyles.GetNamedTag(propertyName);
  }

  final void MakeMultiActionItem(HTMLBuffer buffer, string actionLink, string title, string tooltip, bool enabled, bool active, bool first, string disableColor) 
  {
    if (!first) buffer.Print("&nbsp;&nbsp;&nbsp;");
    if (!enabled) buffer.Print(HTMLWindow.MakeFontColor(title, disableColor));   
    else if (active) buffer.Print(HTMLWindow.MakeBold(title));
    else buffer.Print(HTMLWindow.MakeLink(actionLink, title, tooltip));
  }
  
  final void MakeActionButton(HTMLBuffer buffer, string actionLink, string buttonChar, string tooltip, bool enabled, bool hasNextBButton, string buttonColor)
  {
    if (enabled) {
      buffer.Print("[");
      buffer.Print(HTMLWindow.StartFontColor(buttonColor));
      buffer.Print(HTMLWindow.MakeLink(actionLink, buttonChar, tooltip));
      buffer.Print(HTMLWindow.EndFontColor());
      buffer.Print("]");
    } else buffer.Print("[" + buttonChar + "]");
    if (hasNextBButton) buffer.Print("&nbsp;"); 
  }  
  
  //Создаёт html код для флажка
  //linkId — идентификатор для ссылки
  //checked — значение true, если флажок установлен, иначе false
  //tooltip — всплывающая подсказка
  public string MakeCheckBox(string linkId, bool checked, string tooltip) 
  {
    KUID imagekuid;
    if (checked) imagekuid = Constructors.GetTrainzAsset().LookupKUIDTable("checkbox-on");
    else imagekuid = Constructors.GetTrainzAsset().LookupKUIDTable("checkbox-off");
    string linkvalue = "<img kuid='" + imagekuid.GetHTMLString() + "' width=16 height=16>"; 
    return HTMLWindow.MakeLink("live://property/" + linkId, linkvalue, tooltip);
  }
  

};