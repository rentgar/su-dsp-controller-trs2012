//=============================================================================
// File: zx_dsp_library.gs
// Desc: Основная библиотека пульта ДСП
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_xtriggercore.gs"
include "multiplayergame.gs"
include "ecl_bitlogic.gs"
include "zx_specs.gs"
include "library.gs"

//=============================================================================
// Name: zx_DSP_MenuItem
// Desc: Описывает элемент добавляемый в меню для расширений 
//=============================================================================
class zx_DSP_MenuItem
{
  public GameObject destObject;
  public string major;
  public string minor;
  public string title;
};


class zx_DSP_Library isclass Library
{

  //=============================================================================
  // Name: SOUND_*
  // Desc: Набор констант с типом звукового сигнала
  //=============================================================================
  public define int SOUND_CLICK       = 1;  //Звук клика при удачном действии
  public define int SOUND_ERROR       = 2;  //Звук ошибки при клике


  //=============================================================================
  // Name: GetController
  // Desc: Возвращает текущий активный контроллер ДСП
  // Retn: Объект, представляющий пульт ДСП или null, если активный контроллер
  //       ДСП отсутствует.
  //=============================================================================
  public final GameObject GetController(void);

  //=============================================================================
  // Name: SetController
  // Desc: Устанавливает текущий активный контроллер
  // Parm: ctrl – Объект, представляющий пульт ДСП или null, если нужно сбросить 
  //       активный контроллер.
  //=============================================================================
  public final void SetController(GameObject ctrl);
  
  //=============================================================================
  // Name: ReportEvent
  // Desc: Проигрывает звук индикации события
  // Parm: blinksecond – Время в секундах мигания кнопки пульта в меню (не 
  //       работает в TRS2019).
  // Parm: sound – Код звукового уведомления 
  //=============================================================================
  public final void ReportEvent(float blinksecond, int sound);
  
  //=============================================================================
  // Name: AddMenuItem
  // Desc: Регистрирует пункт меню
  // Parm: destObject – Объект, которому будет передано сообщение.
  // Parm: title – Отображаемая строка в меню 
  //=============================================================================
  public void AddMenuItem(GameObject destObject, string major, string minor, string title);
  
  //=============================================================================
  // Name: PlaySoundEvent
  // Desc: Проигрывает звуковой сигнал
  // Parm: sound – Одна из констант SOUND_*, определяющая проигрываемый звук
  //=============================================================================
  public void PlaySoundEvent(int sound);

	//
	// РЕАЛИЗАЦИЯ
	//
  
  define int BROWSER_NUMBEREDITOR     = 1 << 0;                           //Браузер худа установки номера поезда
  define int BROWSER_NUMBERVIEW       = 1 << 1;                           //Браузер худа отображения номера поезда

  StringTable _strtable;                                                  //Таблица текста

  GameObject _controller;                                                 //Активный контроллер ДСП
  GameObjectID _controllerid;                                             //Идентификатор контроллера   

  bool _nosoundnotification           = false;                            //Указатель, что звуковые уведомление отключены

  zx_DSP_MenuItem[] _menuitems        = new zx_DSP_MenuItem[0];           //Элементы меню 


  DriverCharacter _currdriver;                                            //Текущий машинист              
  Train _currtrain;                                                       //Текущий поезд
  Browser _browserne;                                                     //Браузер ввода номера поезда                        
  Browser _browsernv;                                                     //Браузер отображения номера поезда
  
  int _browservisibled;                                                   //Состояние отображения браузера
  
  
  //Стили браузера
  string _trainnumberedithudfile;                                         //Имя файла с худом для изменения 
  string _trainnumberviewhudfile;                                         //Имя файла с худом для изменения 
  int _trainnumberhudwidth;                                               //Ширина худа для номера поезда
  int _trainnumberedithudheight;                                          //Высота худа для редактирования номера поезда
  int _trainnumberviewhudheight;                                          //Высота худа для отображения номера поезда
  int _trainnumberedithudstyle;                                           //Стиль браузера худа для редактирования номера поезда
  int _trainnumberviewhudstyle;                                           //Стиль браузера худа для отображения номера поезда
  
  
  void LoadBrowserUIProperties(void)
  {
    float gameversion = TrainzScript.GetTrainzVersion();                                                                          //Определение текущей версии игры
    float currversion = 0.0;                                                                                                      //Здесь будет текущая версия, для конфига
    Soup uitablesoup = GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("ui-properties");                       //Таблица настроек интерфейса в конфиге
    int i, count = uitablesoup.CountTags();
    for(i = 0; i < count; ++i) {                                                                                                  //Перебираем все имеющиеся конфигурации
      Soup uisoup = uitablesoup.GetNamedSoup(uitablesoup.GetIndexedTagName(i));                                                   //Тут настройки интерфейса
      float version = uisoup.GetNamedTagAsFloat("build");                                                                       //Определение версии для которой выполнена конфигурация
      if(version <= gameversion and version > currversion) {                                                                      //Проверяем подходимость конфигурации
        _trainnumberedithudfile = uisoup.GetNamedTag("file-numbereditor");
        _trainnumberviewhudfile = uisoup.GetNamedTag("file-numberview");
        _trainnumberedithudstyle = uisoup.GetNamedTagAsInt("style-numbereditor", Browser.STYLE_HUD_FRAME);
        _trainnumberviewhudstyle = uisoup.GetNamedTagAsInt("style-numberview", Browser.STYLE_HUD_FRAME);
        _trainnumberhudwidth = uisoup.GetNamedTagAsInt("width", 183);
        _trainnumberedithudheight = uisoup.GetNamedTagAsInt("height-numbereditor");
        _trainnumberviewhudheight = uisoup.GetNamedTagAsInt("height-numberview");
        currversion = version;
      }
    }
  } 
  
  








  public final GameObject GetController(void)
  {
    if(_controller and _controller != Router.GetGameObject(_controllerid)) {
      _controller = null;
      _controllerid = null;
    }
    return _controller;
  }
  
  public final void SetController(GameObject ctrl)
  {
    _controller = ctrl;
    if(_controller) _controllerid = _controller.GetGameObjectID();
  }

  public final void ReportEvent(float blinksecond, int sound)
  {
    me.BlinkSystemMenuIcon(blinksecond);  //Не работает а TRS2019
    if(!_nosoundnotification){
      if(sound == 1) World.Play2DSound(me.GetAsset(), "bell.wav");
      else if(sound == 2) World.Play2DSound(me.GetAsset(), "tone.wav");
    }
  }
  
  public void PlaySoundEvent(int sound)
  {
    switch(sound) {
      case SOUND_CLICK:       World.Play2DSound(GetAsset(), "click.wav");
                              break;
      case SOUND_ERROR:       World.Play2DSound(GetAsset(), "error.wav");
                              break;
      default:                Exception("zx_DSP_Library.PlaySoundEvent> Argument 'sound' has invalid value");                                                    
                              break;
    }
  }

 
  
  
  
  public void AddMenuItem(GameObject destObject, string major, string minor, string title)
  {
    zx_DSP_MenuItem nitem = new zx_DSP_MenuItem();
    nitem.destObject = destObject;
    nitem.major = major;
    nitem.minor = minor;
    nitem.title = title;
    _menuitems[_menuitems.size()] = nitem;
  }


  public bool IsSkipUpdateNotification(Asset sessionAsset, int version)
  {
    OnlineAccess onlineaccess = GetOnlineAccess();
    KUID seesionid = sessionAsset.GetKUID();
    Soup skipinfo = Constructors.NewSoup();
    return onlineaccess.GetLocalData("DSPController", skipinfo) == OnlineAccess.RESULT_OK and skipinfo.GetNamedTagAsInt(seesionid.GetLogString()) == version;
  }
  
  public void SetSkipUpdateNotification(Asset sessionAsset, int version)
  {
    OnlineAccess onlineaccess = GetOnlineAccess();
    KUID seesionid = sessionAsset.GetKUID();
    Soup skipinfo = Constructors.NewSoup();
    onlineaccess.GetLocalData("DSPController", skipinfo);
    skipinfo.SetNamedTag(seesionid.GetLogString(), version);
    int result = onlineaccess.SetLocalData("DSPController", skipinfo);
  }
  
  

  //-------------------Браузер для ввода номера поезда------------------------

  final void SyncTrainPriorityNumber(Vehicle veh, int number)
  {
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("Object", "TrainPriorityNumber");
    soup.SetNamedTag("DriverVehicle", veh.GetGameObjectID());
    soup.SetNamedTag("Priority", number);
    MultiplayerGame.SendGameplayMessageToServer("ZXDSP_Sync", "SyncObject", soup);
  }

  public final string ConvertTrainNumber(string trainnumber)
  {
    int number = Str.ToInt(trainnumber);
    if(number > 0){
      if(number % 2 == 0) return _strtable.GetString2("trainnumber", number - 1, number);
      else return _strtable.GetString2("trainnumber", number, number + 1);
    }
    return "";
  }
  
  final void CreateBrowsers() 
  {
    int width = Interface.GetDisplayWidth();
    if(!_browserne) {
      _browserne = Constructors.NewBrowser();
      _browserne.SetCloseEnabled(false);
			_browserne.SetWindowStyle(_trainnumberedithudstyle);
			_browserne.SetWindowRect(width - _trainnumberhudwidth, 0, width, _trainnumberedithudheight);
      _browserne.LoadHTMLFile(GetAsset(), "ui/" + _trainnumberedithudfile);
      _browserne.SetElementProperty("applynumber", "value", 1);
      _browserne.SetWindowVisible(false);
    }
    if(!_browsernv) {
      _browsernv = Constructors.NewBrowser();
      _browsernv.SetCloseEnabled(false);
			_browsernv.SetWindowStyle(_trainnumberviewhudstyle);
			_browsernv.SetWindowRect(width - _trainnumberhudwidth, 0, width, _trainnumberviewhudheight);
      _browsernv.LoadHTMLFile(GetAsset(), "ui/" + _trainnumberviewhudfile);
      _browsernv.SetWindowVisible(false);
    }
  }

  final void HudBrowserVisible(Browser browser, bool visible)
  {
    Library drivermodule = World.GetLibrary(me.GetAsset().LookupKUIDTable("driver-module"));
    GSObject[] objectparam = new GSObject[1];
		objectparam[0] = browser;
    if(visible) drivermodule.LibraryCall("add-hud", null, objectparam);
    else drivermodule.LibraryCall("remove-hud", null, objectparam);
    browser.SetWindowVisible(visible);
  }
  
  final void SetTrainNumberHudBrowser(int viewBrowser)
  {
    if(viewBrowser == BROWSER_NUMBEREDITOR != (_browservisibled & BROWSER_NUMBEREDITOR)) {
      HudBrowserVisible(_browserne, viewBrowser == BROWSER_NUMBEREDITOR);      
      _browservisibled = ECLLogic.SetBitMask(_browservisibled, BROWSER_NUMBEREDITOR, viewBrowser == BROWSER_NUMBEREDITOR);
    }
    if(viewBrowser == BROWSER_NUMBERVIEW != (_browservisibled & BROWSER_NUMBERVIEW)) {
      HudBrowserVisible(_browsernv, viewBrowser == BROWSER_NUMBERVIEW);      
      _browservisibled = ECLLogic.SetBitMask(_browservisibled, BROWSER_NUMBERVIEW, viewBrowser == BROWSER_NUMBERVIEW);
    }
    
  }
  
  final bool FindOutOrRouteSignal(Vehicle veh, int direct) 
  {
    GSTrackSearch searcher = veh.BeginTrackSearch((direct and veh.GetDirectionRelativeToTrain()) or (!direct and !veh.GetDirectionRelativeToTrain()));
    MapObject mapobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
    while(mapobject and distance < 5000.0 and !mapobject.isclass(Junction) and (!mapobject.isclass(zxSignal) or 
    !((cast<zxSignal>mapobject).Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER | zxSignal.ST_SHUNT)))) {
      mapobject = searcher.SearchNext();
      distance = searcher.GetDistance();
    }
    return mapobject and searcher.GetFacingRelativeToSearchDirection() and mapobject.isclass(zxSignal) and (cast<zxSignal>mapobject).Type & (zxSignal.ST_OUT | zxSignal.ST_ROUTER);  
  }
  
  final bool CheckTrainInStation(Train train) 
  {
    Vehicle[] vehs = train.GetVehicles();
    return FindOutOrRouteSignal(vehs[0], true) or FindOutOrRouteSignal(vehs[vehs.size() - 1], false); 
  }
  

  final void ChangeTrainInfoHandler(Message msg)
  {
    if (!_browserne or !_browsernv) {
      return;
    }
    if(msg.major == "Camera" or msg.major == "Train" or (msg.major == "DriverCharacter" and (msg.src == _currdriver or (_currtrain and _currtrain.GetActiveDriver() == msg.src)))){
      string localuser = me.GetOnlineAccess().GetUsername(), ownername = "";
      DriverCharacter driver = null;
      Train currtrain = _currtrain;
      if(msg.major == "Camera"){
        Vehicle veh = cast<Vehicle> msg.src;
        if(veh) currtrain = veh.GetMyTrain();
        else currtrain = null;
      }
      if(currtrain) driver = currtrain.GetActiveDriver();
      if(driver) ownername = driver.GetOwnerPlayer();
      Str.ToLower(ownername);
      Str.ToLower(localuser);
      
      bool canviewhud = (!MultiplayerGame.IsActive() or localuser == ownername) and currtrain and driver;
      bool cansetnumber, canresetnumber;
      string trainnumber;
      if(canviewhud) {
        Vehicle drvveh = cast<Vehicle>driver.GetLocation();
        cansetnumber = currtrain.IsStopped() /*Math.Fabs(drvveh.GetVelocity()) < 0.2 */and CheckTrainInStation(drvveh.GetMyTrain()); 
        canresetnumber = cansetnumber and drvveh.GetRunningNumber() != "";
        if(drvveh.GetRunningNumber() != "") trainnumber = ConvertTrainNumber(drvveh.GetRunningNumber());
        else trainnumber = "——/——";  
      }
      if(_currtrain and _currtrain != currtrain){
        me.AddHandler(_currtrain, "Train", "StartedMoving", "");
        me.AddHandler(_currtrain, "Train", "StoppedMoving", "");
        me.AddHandler(_currtrain, "Train", "ConsistChanged", "");
      }
      if(currtrain and currtrain != _currtrain){
        me.AddHandler(currtrain, "Train", "StartedMoving", "ChangeTrainInfoHandler");
        me.AddHandler(currtrain, "Train", "StoppedMoving", "ChangeTrainInfoHandler");
        me.AddHandler(currtrain, "Train", "ConsistChanged", "ChangeTrainInfoHandler");
      }
      if(canviewhud) _currdriver = driver;
      else _currdriver = null; 
      _currtrain = currtrain;
      
      int visiblebrowser = 0;
      if(canviewhud) {
        _browsernv.SetTrainzText("number", trainnumber);
        _browserne.SetTrainzText("number", trainnumber);
        if(cansetnumber) visiblebrowser = BROWSER_NUMBEREDITOR; 
        else visiblebrowser = BROWSER_NUMBERVIEW; 
        if(canresetnumber) _browserne.SetElementProperty("destroy", "value", 1);
        else _browserne.SetElementProperty("destroy", "value", 0);
      }
      SetTrainNumberHudBrowser(visiblebrowser);
      if(_controller) PostMessage(_controller, "ZXDSP", "UserVehicleStatusUpdated", 0.1);
    }
  }

  final void BrowserURLHandler(Message msg)
  {
    if(_browserne and msg.src == _browserne and _browservisibled & BROWSER_NUMBEREDITOR){
      if(msg.minor == "live://ZXDSP/apply" and _browserne.GetElementProperty("applynumber", "value") == "true"){
        int number = Str.ToInt(_browserne.GetElementProperty("newnumber", "text"));
        if(number > 0) {
          Vehicle drvveh = cast<Vehicle>_currdriver.GetLocation(); 
          drvveh.SetRunningNumber(number);
          _browserne.SetTrainzText("number", me.ConvertTrainNumber(drvveh.GetRunningNumber()));
          _browserne.SetElementProperty("destroy", "value", 1);
          _browserne.SetElementProperty("newnumber", "text", "");
          World.Play2DSound(me.GetAsset(), "click.wav");
          int priority = 2;
          if((number >= 1 and number <= 999) or (number >= 6000 and number <= 7999)) priority = 1;
          drvveh.GetMyTrain().SetTrainPriorityNumber(priority);
          if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()) me.SyncTrainPriorityNumber(drvveh, priority);
        } else World.Play2DSound(GetAsset(), "error.wav");
      }else if(msg.minor == "live://ZXDSP/destroy" and _browserne.GetElementProperty("destroy", "value") == "true"){
        Vehicle drvveh = cast<Vehicle>_currdriver.GetLocation(); 
        drvveh.SetRunningNumber("");
        _browserne.SetTrainzText("number", "-- --");
        _browserne.SetElementProperty("destroy", "value", 0);
        World.Play2DSound(GetAsset(), "click.wav");
        drvveh.GetMyTrain().SetTrainPriorityNumber(3);
        if(MultiplayerGame.IsActive() and !MultiplayerGame.IsServer()) me.SyncTrainPriorityNumber(drvveh, 3);
      }
    }
  }

  //-----------------------------------------------------------------------

  final thread void WaitMultiplayerActive(void)
  {
    while(!MultiplayerGame.IsActive())me.Sleep(1.0);
    me.PostMessage(_controller, "ZXDSP", "MultiplayerActive", 0.2);
  }

	final void ModuleInitHandler(Message msg)
	{
	  if(World.GetCurrentModule() == World.DRIVER_MODULE and _controller){
		  AddSystemMenuIcon(GetAsset().FindAsset("menuicon"), _strtable.GetString("tooltip"), "");
      WaitMultiplayerActive();
      CreateBrowsers();
      AddHandler(me, "Camera", "Target-Changed", "ChangeTrainInfoHandler");
      AddHandler(me, "DriverCharacter", "OwnerChanged", "ChangeTrainInfoHandler");
      AddHandler(me, "DriverCharacter", "BoardedTrain", "ChangeTrainInfoHandler");
      AddHandler(me, "DriverCharacter", "LeftTrain", "ChangeTrainInfoHandler");
    }
	}

  final void SoundNotificationHandler(Message msg)
  {
    _nosoundnotification = !_nosoundnotification;
  }

  final void OnClickSystemButton(Message msg)
  {
    Menu iconmenu = Constructors.NewMenu();
    iconmenu.AddItem(_strtable.GetString("menu-controller"), _controller, "ZXDSP", "OpenControllerForm");
    iconmenu.AddItem(_strtable.GetString("showinfo"), _controller, "ZXDSP", "OpenInformationForm");
    iconmenu.AddSeperator();
    iconmenu.AddItem(_strtable.GetString("menu-driverjunctionlist"), _controller, "ZXDSP", "OpenDriverJunctionListForm");
    if(_menuitems.size() > 0){
      iconmenu.AddSeperator();
      int i, count = _menuitems.size();
      for(i = 0; i < count; i++)
        iconmenu.AddItem(_menuitems[i].title, _menuitems[i].destObject, _menuitems[i].major, _menuitems[i].minor);
    }
    iconmenu.AddSeperator();
    if(_nosoundnotification) iconmenu.AddItem(_strtable.GetString("sound-turn-on"), me, "ZXDSP", "SoundNotification");
    else iconmenu.AddItem(_strtable.GetString("sound-turn-off"), me, "ZXDSP", "SoundNotification");

    iconmenu.AddSeperator();
    iconmenu.AddItem(_strtable.GetString("menu-journal"), _controller, "ZXDSP", "OpenJournalForm");
    if(MultiplayerGame.IsActive() and MultiplayerGame.IsServer()) 
      iconmenu.AddItem(_strtable.GetString("showadmin"), _controller, "ZXDSP", "OpenPermissionForm");
    ShowSystemMenuIconMenu(iconmenu);
	}

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    LoadBrowserUIProperties();
		RemoveSystemMenuIcon();
   	AddHandler(me, "World", "ModuleInit","ModuleInitHandler");
   	AddHandler(me, "Interface", "ClickSystemButton", "OnClickSystemButton");
    AddHandler(me, "Browser-URL", "", "BrowserURLHandler");
		AddHandler(me, "ZXDSP", "SoundNotification", "SoundNotificationHandler");
    _strtable = myAsset.GetStringTable();
  }


};