//=============================================================================
// File: zx_dsp_xtriggercore.gs
// Desc: Основная библиотека пульта ДСП
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_library.gs"
include "trigger.gs"

//=============================================================================
// Name: zx_DSP_MenuItem
// Desc: Базовый класс для Х-триггера  
//=============================================================================
class zx_DSP_XTriggerCore isclass Trackside
{

  //=============================================================================
  // Name: STATUS_*
  // Desc: Описывает состояние триггера или группы триггера 
  //=============================================================================
  public define int STATUS_NONE    = 0; //Нет блокировки
  public define int STATUS_PATH    = 1; //Присутствует перекрёстный маршрут
  public define int STATUS_TRAIN   = 2; //Пересечение занято поездом

  //=============================================================================
  // Name: GetStatus
  // Desc: Получение состояния триггера
  // Parm: group – значение true, если нужно получить состояние группы триггеров
  //       или значение false, если нужно получить состояние только самого 
  //       триггера 
  // Retn: Значение одной из констант STATUS_*, описывающее состояние.    
  // Note: Для реализации требуется переопределение в наследуемом классе.
  //=============================================================================
  public int GetStatus(bool group);

  //=============================================================================
  // Name: ResetNeighbor
  // Desc: Сбрасывает привязку к парному триггеру
  //=============================================================================
  public final void ResetNeighbor(void);

  //=============================================================================
  // Name: SetNeighbor
  // Desc: Устанавливает привязку к парному триггеру
  // Parm: trigger – триггер, к которому нужно выполнить привязку.
  // Retn: Значение true, если парный триггер имеет настройку для перекрсёстной 
  //       стрелки, иначе — значение false;   
  //=============================================================================
  public final bool SetNeighbor(zx_DSP_XTriggerCore trigger);
  
  //=============================================================================
  // Name: SetXJunction
  // Desc: Устанавливает для триггера состояние установки на перексрёстную 
  //       стрелку
  // Parm: xjunction – значение true, если стрелка установлена на перексрёстную
  //       стрелку; в противном случае — значенеи false.
  //=============================================================================
  public final void SetXJunction(bool xjunction);
  
	//
	// РЕАЛИЗАЦИЯ
	//

  zx_DSP_Library _library;                        //Основная библиотека
  zx_DSP_XTriggerCore _neighbor;                  //Соседний триггер
  GameObjectID _neighborid;                       //Идентификатор соседнего триггера
  bool _isxjunction;                              //Указатель, что триггер установлен на перекрёстной стрелке
  bool _trainlock;                                //Пересечение занято поездом

  public int GetStatus(bool group) 
  {
    return STATUS_NONE;
  }

  public final void ResetNeighbor(void)
  {
    _neighbor = null;
    _neighborid = null;
  }

  public final bool SetNeighbor(zx_DSP_XTriggerCore trigger)
  {
    if(_neighbor != trigger){ //Если сосед изменяется
      if(_neighbor and _neighbor == Router.GetGameObject(_neighborid)) //Если сосед существует, то
        _neighbor.ResetNeighbor(); //Вызываем на нём сброс соседа
      _neighbor = trigger;
      _neighborid = trigger.GetGameObjectID(); 
    }
    return _isxjunction;
  }

  public final void SetXJunction(bool xjunction)
  {
    _isxjunction = xjunction;
  }

  final void SetMeNeighbor(zx_DSP_XTriggerCore trigger)
  {
    me.SetNeighbor(trigger);
    _isxjunction = trigger.SetNeighbor(me);
  }

  final void SetMeXJunction(bool xjunction)
  {
    if(_neighbor and _neighbor == Router.GetGameObject(_neighborid))
      _neighbor.SetXJunction(xjunction);
    _isxjunction = xjunction;
  }

  final void ResetMeNeighbor(void)
  {
    if(_neighbor and _neighbor == Router.GetGameObject(_neighborid))
      _neighbor.ResetNeighbor();
    _neighbor = null;
    _neighborid = null;
  }

  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    if(_neighbor and _neighbor == Router.GetGameObject(_neighborid))
      soup.SetNamedTag("Neighbor", _neighbor.GetGameObjectID());
    soup.SetNamedTag("XJunction", _isxjunction);
    return soup;
  }

  public void SetProperties(Soup soup)
  {
    inherited(soup);
    GameObjectID triggerid = soup.GetNamedTagAsGameObjectID("Neighbor"); 
    _isxjunction = soup.GetNamedTagAsBool("XJunction");
    if(triggerid){
      zx_DSP_XTriggerCore trigger = cast<zx_DSP_XTriggerCore>Router.GetGameObject(triggerid);
      if(trigger) {
        me.SetNeighbor(trigger);
        bool st = trigger.SetNeighbor(me);
        if((st and !_isxjunction) or (!st and _isxjunction))
          trigger.SetXJunction(_isxjunction);
      }
    } else me.ResetMeNeighbor();
  }

  final void TriggerLockHandler(Message msg)
  {
    if(msg.dst == me and msg.minor == "Enter") _trainlock = true;
    else if(msg.dst == me and msg.minor == "Leave") _trainlock = false;
  }

  void OnModuleInit(Message msg) { }

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    KUID librarykuid = me.GetAsset().LookupKUIDTable("dsp-library");
    _library = cast<zx_DSP_Library>World.GetLibrary(librarykuid);
    me.AddHandler(me, "Object", "Enter", "TriggerLockHandler");
    me.AddHandler(me, "Object", "Leave", "TriggerLockHandler");
    me.AddHandler(me, "World", "ModuleInit", "OnModuleInit");
  }

};