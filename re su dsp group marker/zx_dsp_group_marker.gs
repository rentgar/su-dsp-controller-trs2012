//=============================================================================
// File: zx_dsp_group_marker.gs
// Desc: Реализация маркера группового светофора
// Auth: Алексей 'Эрендир' Зверев 2024 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_group_marker_base.gs"
include "zx_dsp_ui.gs"

//=============================================================================
// Name: zx_DSP_GroupMarker
// Desc: Класс, реализующий маркер группового светофора
//=============================================================================
final class zx_DSP_GroupMarker isclass zx_DSP_GroupMarkerBase, zx_DSP_UI
{
    string GetDescriptionHTML(void) {
        StringTable strtable = GetAsset().GetStringTable();
        HTMLBuffer buffer = HTMLBufferStatic.Construct();

        buffer.Print("<font size=10 color=#" + GetUIPropertyValue("labelcolor-title") +"><b>");
        buffer.Print(strtable.GetString("title"));
        buffer.Print("</b></font><br><br>");

        buffer.Print(HTMLWindow.StartFontColor(GetUIPropertyValue("labelcolor-description")));
        buffer.Print("<br /><br />");
        buffer.Print(strtable.GetString("description"));
        buffer.Print("<br /><br />");
        buffer.Print(HTMLWindow.EndFontColor());

        return buffer.AsString();
    }

    void Init(Asset asset) {
        inherited(asset);
        LoadUIProperties(asset);
    }
};
