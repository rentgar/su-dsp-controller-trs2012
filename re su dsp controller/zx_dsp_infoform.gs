//=============================================================================
// File: zx_dsp_infoform.gs
// Desc: Класс формы для информации обучатсках приближения и журнале  
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_classes.gs"
include "zx_dsp_core.gs"
include "htmlbuffer.gs"
include "browser.gs"


//=============================================================================
// Name: zd_DSP_InfoForm
// Desc: Форма вывода информации об участках приближения и журнала.
//=============================================================================
final class zd_DSP_InfoForm isclass GameObject
{

  //=============================================================================
  // Name: Init
  // Desc: Инициализирует новое окно с информацией об участках приблиджения и
  //       журналом.
  // Parm: core – Объект zx_DSP_Core, представляющий ядро маршрутизации. 
  // Parm: strTable – Таблица строк для интерфейса браузера. 
  // Retn: Инициалищированный объект zd_DSP_InfoForm.
  //=============================================================================
  public zd_DSP_InfoForm Init(zx_DSP_Core core, StringTable strTable);

  //=============================================================================
  // Name: Update
  // Desc: Обновляет окно управления правами пользователей.
  // Parm: fullUpdate – значение true, если нужно обновить всё, включая список 
  //       станций; значение false, если нужно обновить только отображаемую
  //       информацию. 
  //=============================================================================
  public void Update(bool fullUpdate);

  //=============================================================================
  // Name: BringToFront
  // Desc: Восстанавливает окно, если оно было свёрнуто, и перемещает его на
  //       передний план, поверх всех окон. 
  //=============================================================================
  public void BringToFront(void);

  //=============================================================================
  // Name: Dispose
  // Desc: Освобождает все ресурсы для этого объекта. 
  //=============================================================================
  public void Dispose(void);

  //=============================================================================
  // Name: MustInformationUpdate
  // Desc: Проверяет, требуется ли обновление блока информации при залданых
  //       требованиях. 
  // Parm: stationId – Идентификатор станции для которой выполняется проверка.
  // Parm: tab – Одна из констант zx_DSP_Station.INFOTAB_*, определяющая
  //       требуемую для обновления вкладку.
  //=============================================================================
  public bool MustInformationUpdate(int stationId, int tab);                                      

  //=============================================================================
  // Name: UpdateInformation
  // Desc: Обновляет блок информации.
  //=============================================================================
  void UpdateInformation(void); 

  //=============================================================================
  // Name: OnLinkClick
  // Desc: Вызывается при нажатии на ссылке в браузере 
  // Parm: action — Действие, которое необходимо выполнить
  //=============================================================================
  void OnLinkClick(string action, string argument);

	//
	// РЕАЛИЗАЦИЯ
	//

  zx_DSP_Core _core;                          //Тут ссылка на ядро
  Browser _browser;                           //Собственно само окно браузера
  StringTable _strtable;                      //Таблица строк
  int _stationviewed = -1;                    //Текущая выбраная станция
  int _stationviewedid = -1;                  //Идентификатор текущей выбранной станции

  public zd_DSP_InfoForm Init(zx_DSP_Core core, StringTable strTable)
  {
    if(_browser) return me; //Если инициализация уже выполнена, то выходим.

    _core = core;
    _strtable = strTable;

    _browser = Constructors.NewBrowser();
    _browser.SetWindowStyle(Browser.STYLE_DEFAULT); 
    _browser.SetScrollEnabled(false);        
    _browser.SetCloseEnabled(true);                                        
    _browser.SetWindowPriority(Browser.BP_Window);
   // _browser.SetButtonOverlayStyle(Browser.BS_None, Browser.BS_OK);
    _browser.SetWindowTitle(strTable.GetString("infoform-caption"));
    _browser.SetWindowCentered(700, 550);
    _browser.LoadHTMLFile(_core.GetAsset(), _core.StyleFormInformations);
    
    _browser.SetElementProperty("stationlist", "html", "");
    _browser.SetElementProperty("stationlist", "background-style", "1");
    
    _browser.SetElementProperty("information", "html", "");
    _browser.SetElementProperty("information", "background-style", "1");

    AddHandler(_browser, "Browser", "Closed", "BrowserClosedHandler");
    AddHandler(_browser, "Browser-URL", "", "BrowserURLHandler");

    Update(true);
       
    return me;
  }

  public void Update(bool fullUpdate)
  {
    if(fullUpdate) {
       if(_core.stations and _core.stations.size()) {
        string lnktablestart = HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%");
        string lnktableend = HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable();
        HTMLBuffer buff = HTMLBufferStatic.Construct();
        int i, count = _core.stations.size();
        buff.Print("<html><body>");
        buff.Print(HTMLWindow.StartTable("width=100%"));
        bool hasstation = false;
        for(i = 0; i < count; ++i){
          zx_DSP_Station station = _core.stations[i];
          buff.Print(HTMLWindow.StartRow());
          if(i == _stationviewed){
            buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleSelectorBackColor));
            hasstation = true;
          } else buff.Print(HTMLWindow.StartCell("width=100%"));
          buff.Print(HTMLWindow.MakeLink("live://viewstation/" + i, lnktablestart + station.name + lnktableend, _strtable.GetString("tooltip-selectstation")));
          buff.Print(HTMLWindow.EndCell() + HTMLWindow.EndRow());
        }
        if(!hasstation) {
          _stationviewed = -1;
          _stationviewedid = -1;
        }
        buff.Print(HTMLWindow.EndTable());
        buff.Print("</body></html>");
        _browser.SetElementProperty("stationlist", "html", buff.AsString());
      }
    }
    UpdateInformation();    
  } 
  
  public void BringToFront(void)
  {
    if(_browser) {
      _browser.RestoreWindow();
      _browser.BringToFront();    
    }
  }
  
  public void Dispose(void)
  {
    if(_browser) {
        _browser.CloseWindow();
        AddHandler(_browser, "Browser", "Closed", null);
        AddHandler(_browser, "Browser-URL", "", null);
    }
    _core = null;
    _browser = null;
    _strtable = null;
    _stationviewed = -1;
    _stationviewedid = -1;
  }

  public bool MustInformationUpdate(int stationId, int tab)
  {
    return _core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _stationviewedid == stationId and _core.stations[_stationviewed].informationFormTab == tab;
  }

  string GetDriverName(DriverCharacter driver)
  {
    if(driver.GetOwnerPlayer() != "") return driver.GetOwnerPlayer();
    return driver.GetLocalisedName();
  }

  string GetTargetStationInfo(zxSignal sign)
  {
    if(sign.privateName.size() == 2) return sign.stationName;
    return sign.privateName + "@" + sign.stationName;
  }

  void CreateApproachInfo(HTMLBuffer buff)
  {
    int i, count = _core.spantrains.size(), found;
    buff.Print(HTMLWindow.StartTable("width=100%"));
    for(i = 0; i < count; ++i){
      if(_core.spantrains[i].stationId == _stationviewedid and Router.DoesGameObjectStillExist(_core.spantrains[i].train) and _core.spantrains[i].train.GetActiveDriver()){
        zx_DSP_SpanTrainInfo info = _core.spantrains[i];
        DriverCharacter driver = info.train.GetActiveDriver();
        buff.Print(HTMLWindow.StartRow());
          buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
            buff.Print(HTMLWindow.StartTable("width=100%"));
              buff.Print(HTMLWindow.StartRow());
                buff.Print(HTMLWindow.StartCell());
                  if(info.blockSectionCount <= 2 and info.hasInPath) buff.Print(_core.MakeHTMLImage("st_re", 32));
                  else if(info.blockSectionCount <= 2) buff.Print(_core.MakeHTMLImage("st_r", 32));
                  else if(info.blockSectionCount <= 4 and !info.hasInPath) buff.Print(_core.MakeHTMLImage("st_y", 32));
                  else buff.Print(_core.MakeHTMLImage("st_g", 32));
                buff.Print(HTMLWindow.EndCell());
                buff.Print(HTMLWindow.StartCell("width=100%"));
                  buff.Print(HTMLWindow.StartTable("width=100%"));
                    buff.Print(HTMLWindow.StartRow());
                      buff.Print(HTMLWindow.StartCell("width=100%"));
                        buff.Print(HTMLWindow.StartTable("width=100%"));
                          buff.Print(HTMLWindow.StartRow());
                            buff.Print(HTMLWindow.MakeCell(info.trainnumber, "width=50%"));
                            buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("driver", 16), "width=16"));
                            buff.Print(HTMLWindow.MakeCell(GetDriverName(driver), "width=50%"));
                            if(info.hasAheadTrain) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("train", 16), "width=16"));
                            else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("empty", 16), "width=16"));
                          buff.Print(HTMLWindow.EndRow());
                        buff.Print(HTMLWindow.EndTable());
                      buff.Print(HTMLWindow.EndCell());
                    buff.Print(HTMLWindow.EndRow());
                    buff.Print(HTMLWindow.StartRow());
                      buff.Print(HTMLWindow.StartCell("width=100%"));
                        buff.Print(HTMLWindow.StartTable("width=100%"));
                          buff.Print(HTMLWindow.StartRow());
                            buff.Print(HTMLWindow.StartCell());
                              buff.Print("<nowrap>");
                                if (_core.spans) {
                                  zx_DSP_SignalInfo signalInfo = _core.GetSignalInfo(info.targetSignal);
                                  if (signalInfo and signalInfo.span) {
                                    buff.Print(signalInfo.span.inverse.station.name);
                                  }
                                  else {
                                    buff.Print("???");
                                  }
                                }
                                else {
                                  if(info.sourceStation != "") buff.Print(info.sourceStation);
                                  else buff.Print(_strtable.GetString("train-unknown"));
                                }
                              buff.Print("</nowrap>");
                            buff.Print(HTMLWindow.EndCell());
                            buff.Print(HTMLWindow.StartCell("width=48"));
                              buff.Print(_core.MakeHTMLImage("empty", 8, 16));
                              buff.Print(_core.MakeHTMLImage("rightarrow", 32, 16));
                              buff.Print(_core.MakeHTMLImage("empty", 8, 16));
                            buff.Print(HTMLWindow.EndCell());
                            buff.Print(HTMLWindow.StartCell());
                              buff.Print("<nowrap>");
                                buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + info.targetSignal.GetGameObjectID().SerialiseToString(), info.targetSignal.privateName, _strtable.GetString("tooltip-cameratosignal")));
                                zx_DSP_SignalInfo signalInfo = _core.GetSignalInfo(info.targetSignal);
                                if (signalInfo and signalInfo.pathsFrom and signalInfo.pathsFrom.size()) {
                                  buff.Print("&nbsp;");
                                  buff.Print(HTMLWindow.StartFontColor(_core.StyleActionText));
                                  buff.Print(HTMLWindow.MakeLink("live://selectsignal/" + info.targetSignal.GetGameObjectID().SerialiseToString(), "⮞", _strtable.GetString("tooltip-topathlistforsignal")));
                                  buff.Print(HTMLWindow.EndFontColor());
                                }
                              buff.Print("</nowrap>");
                            buff.Print(HTMLWindow.EndCell());
                            buff.Print(HTMLWindow.MakeCell("", "width=100%"));
                            buff.Print(HTMLWindow.StartCell());
                              buff.Print("<nowrap>");
                              buff.Print(_strtable.GetString1("train-blocksection", info.blockSectionCount));
                              buff.Print("</nowrap>");
                            buff.Print(HTMLWindow.EndCell());
                          buff.Print(HTMLWindow.EndRow());
                        buff.Print(HTMLWindow.EndTable());
                      buff.Print(HTMLWindow.EndCell());
                    buff.Print(HTMLWindow.EndRow());
                  buff.Print(HTMLWindow.EndTable());
                buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.EndRow());
            buff.Print(HTMLWindow.EndTable());
          buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.EndRow());
        ++found;
      }
    }
    if(found == 0) buff.Print("<tr><td align=center>" + _strtable.GetString("message-nospantrain") + "</td></tr>");
    buff.Print(HTMLWindow.EndTable());
  }

  void CreateJurnalInfo(HTMLBuffer buff, zx_DSP_Station station)
  {
    int i, count = station.jurnal.size();
    buff.Print(HTMLWindow.StartTable("width=100%"));
    if(count){
      for(i = count - 1; i >= 0; --i){
        zx_DSP_JurnalItem jitem = station.jurnal[i];
        buff.Print(HTMLWindow.StartRow());
          buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
            buff.Print(HTMLWindow.StartTable("width=100%"));
              buff.Print(HTMLWindow.StartRow());
                buff.Print(HTMLWindow.StartCell("width=100%"));
                  buff.Print(HTMLWindow.StartTable("width=100%"));
                    buff.Print(HTMLWindow.StartRow());
                      buff.Print(HTMLWindow.StartCell());
                        buff.Print("<nowrap>");
                          buff.Print(_strtable.GetString1("train-number", jitem.trainnumber));
                        buff.Print("</nowrap>");
                      buff.Print(HTMLWindow.EndCell());
                      buff.Print(HTMLWindow.StartCell("width=48"));
                        buff.Print(_core.MakeHTMLImage("empty", 8, 16));
                        if(jitem.isOutPath) buff.Print(_core.MakeHTMLImage("leftarrow", 32, 16));
                        else buff.Print(_core.MakeHTMLImage("rightarrow", 32, 16));
                        buff.Print(_core.MakeHTMLImage("empty", 8, 16));
                      buff.Print(HTMLWindow.EndCell());
                      buff.Print(HTMLWindow.StartCell());
                        buff.Print("<nowrap>");
                          buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + jitem.signal.GetGameObjectID().SerialiseToString(), jitem.signal.privateName, _strtable.GetString("tooltip-cameratosignal")));
                        buff.Print("</nowrap>");
                      buff.Print(HTMLWindow.EndCell());
                      if(jitem.isOutPath) {
                        buff.Print(HTMLWindow.MakeCell("", "width=50%"));
                        buff.Print(HTMLWindow.StartCell());
                          buff.Print("<nowrap>");
                          buff.Print(GetTargetStationInfo(jitem.targetSignal));
                          buff.Print("</nowrap>");
                        buff.Print(HTMLWindow.EndCell());
                        buff.Print(HTMLWindow.MakeCell("", "width=50%"));
                      } else buff.Print(HTMLWindow.MakeCell("", "width=100%"));
                      buff.Print(HTMLWindow.StartCell());
                        buff.Print("<nowrap>");
                        buff.Print(HTMLWindow.PadZerosOnFront(HTMLWindow.GetHoursFromFloat(jitem.time), 2) + ":" + HTMLWindow.PadZerosOnFront(HTMLWindow.GetMinutesFromFloat(jitem.time), 2) + ":" +
                                   HTMLWindow.PadZerosOnFront(HTMLWindow.GetSecondsFromFloat(jitem.time), 2));
                        buff.Print("</nowrap>");
                      buff.Print(HTMLWindow.EndCell());
                    buff.Print(HTMLWindow.EndRow());
                  buff.Print(HTMLWindow.EndTable());
                buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.EndRow());
              buff.Print(HTMLWindow.StartRow());
                buff.Print(HTMLWindow.StartCell("width=100%"));
                  buff.Print(HTMLWindow.StartTable("width=100%"));
                    buff.Print(HTMLWindow.StartRow());
                      buff.Print(HTMLWindow.StartCell("width=16"));
                        buff.Print(_core.MakeHTMLImage("train", 16));
                      buff.Print(HTMLWindow.EndCell());
                      buff.Print(HTMLWindow.StartCell("width=50%"));
                        buff.Print("<nowrap>");
                          buff.Print(HTMLWindow.MakeLink("live://cameratolocomotive/" + jitem.locomotiveId.SerialiseToString(), jitem.loconame, _strtable.GetString("tooltip-cameratolocomotive")));
                        buff.Print("</nowrap>");
                      buff.Print(HTMLWindow.EndCell());
                      buff.Print(HTMLWindow.StartCell("width=16"));
                        buff.Print(_core.MakeHTMLImage("driver", 16));
                      buff.Print(HTMLWindow.EndCell());
                      buff.Print(HTMLWindow.StartCell("width=50%"));
                        buff.Print("<nowrap>");
                        buff.Print(jitem.drivername);
                        buff.Print("</nowrap>");
                      buff.Print(HTMLWindow.EndCell());
                    buff.Print(HTMLWindow.EndRow());
                  buff.Print(HTMLWindow.EndTable());
                buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.EndRow());
            buff.Print(HTMLWindow.EndTable());
          buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.EndRow());
      }
    } else buff.Print("<tr><td align=center>" + _strtable.GetString("message-nojurnalitems") + "</td></tr>");
    buff.Print(HTMLWindow.EndTable());
  }

  void UpdateInformation(void)
  {
    HTMLBuffer buff = HTMLBufferStatic.Construct();
    buff.Print("<html><body>");
    if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and (!MultiplayerGame.IsActive() or MultiplayerGame.IsServer() or _core.stations[_stationviewed].availableControl)) {
      zx_DSP_Station station = _core.stations[_stationviewed];
      if(station.informationFormTab == zx_DSP_Station.INFOTAB_JOURNAL) CreateJurnalInfo(buff, station);
      else CreateApproachInfo(buff);      
    }else if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and MultiplayerGame.IsActive() and !MultiplayerGame.IsServer() and !_core.stations[_stationviewed].availableControl){
      buff.Print(HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() +
                 HTMLWindow.StartRow() + HTMLWindow.StartCell("bgcolor=#D99694 align=center") + HTMLWindow.MakeFontColor(_strtable.GetString("error-accessdenied"), "632523") +
                 HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable());
    }else if(_core.stations and (_stationviewed < 0 or _stationviewed >= _core.stations.size())){
      buff.Print(HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() +
                 HTMLWindow.StartRow() + HTMLWindow.StartCell("align=center") + _strtable.GetString("message-selectstation-info") + HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable());
    } else {
      buff.Print(HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() +
                 HTMLWindow.StartRow() + HTMLWindow.StartCell("bgcolor=#D99694 align=center") + HTMLWindow.MakeFontColor(_strtable.GetString("error-nocalced"), "632523") +
                 HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable());
    }
    buff.Print("</body></html>");
    _browser.SetElementProperty("information", "html", buff.AsString());
  } 

  void OnLinkClick(string action, string argument)
  { 
    if(action == "viewstation") {
      int index = Str.ToInt(argument);
      if(_core.stations and _stationviewed != index and index >= 0 and index < _core.stations.size()) {
        _stationviewed = index;
        _stationviewedid = _core.stations[_stationviewed].id;
        Update(true);
      }
    } else if(action == "showspantrain" or action == "showjurnal") {
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        if(action == "showjurnal")  _core.stations[_stationviewed].informationFormTab = zx_DSP_Station.INFOTAB_JOURNAL;
        else _core.stations[_stationviewed].informationFormTab = zx_DSP_Station.INFOTAB_APPROACHAREA;   
        Update(false);
      }
    } else if((action == "cameratosignal" or action == "cameratolocomotive") and argument != null) {
      GameObjectID objectid = Router.SerialiseGameObjectIDFromString(argument);
      if (objectid) {
        MapObject mapobj = cast<MapObject>Router.GetGameObject(objectid);
        if(mapobj) {
          World.SetCamera(mapobj);
          _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
          return;          
        } 
      } 
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } 
    else if(action == "selectsignal") {
      GameObjectID id = Router.SerialiseGameObjectIDFromString(argument);
      zxSignal signal;
      if (id and (signal = cast<zxSignal>Router.GetGameObject(id)) and _core.GoToPathListForSignal(signal)) {
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      }
      else {
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
      }
    }
  }

  void BrowserURLHandler(Message msg)
  {
    if(msg.src == _browser and /* msg.src == msg.dst and сообщение шлётся бродкастом */   
    msg.minor[0, 7] == "live://") {
      string[] args = Str.Tokens(msg.minor[7,], "/");
      if(args.size() == 2) OnLinkClick(args[0], args[1]);
      else OnLinkClick(args[0], null);
    } 
  }  

  void BrowserClosedHandler(Message msg)
  {
    if(msg.src == _browser and msg.src == msg.dst) {
      PostMessage(_core, "ZXDSP", "InformationFormClosed", 0.0);
      Dispose();
    }
  }

}; 
