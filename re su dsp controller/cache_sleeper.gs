include "gs.gs"
include "cache_mutex.gs"

class CacheSleeper
{
    CacheMutex _mutex;
    int _counter = 0;
    bool _canceled = true;

    public CacheSleeper Init(CacheMutex mutex) {
        _mutex = mutex;
        _canceled = false;
        return me;
    }

    public bool IsCanceled() {
        if (_canceled) {
            return true;
        }
        if (++_counter > 500) {
            Router.GetCurrentThreadGameObject().Sleep(0.001);
            if (_canceled) {
                return true;
            }
            _counter = 0;
        }

        return false;
    }

    public void Cancel() {
        _canceled = true;
    }

    public void Unlock() {
        _mutex.Unlock(me);
    }
};
