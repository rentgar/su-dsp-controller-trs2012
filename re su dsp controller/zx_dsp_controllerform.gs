//=============================================================================
// File: zx_dsp_controllerform.gs
// Desc: Класс формы для контроолера управления  
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_classes.gs"
include "zx_dsp_core.gs"
include "htmlbuffer.gs"
include "browser.gs"

//=============================================================================
// Name: zd_DSP_ControllerForm
// Desc: Форма управления ДСП
//=============================================================================
final class zd_DSP_ControllerForm isclass GameObject
{

  //=============================================================================
  // Name: Init
  // Desc: Инициализирует новое окно управления ДСП
  // Parm: core – Объект zx_DSP_Core, представляющий ядро маршрутизации. 
  // Parm: strTable – Таблица строк для интерфейса браузера. 
  // Retn: Инициалищированный объект zd_DSP_ControllerForm.
  //=============================================================================
  public zd_DSP_ControllerForm Init(zx_DSP_Core core, StringTable strTable);

  //=============================================================================
  // Name: Update
  // Desc: Обновляет окно управления ДСП.
  //=============================================================================
  public void Update();
  
  //=============================================================================
  // Name: Update
  // Desc: Обновляет окно управления ДСП с учётом заданых критериев.
  // Parm: stationIndex – индекс станции, для которой нужно обновить окно.
  // Parm: tabMask – побитовая сумма начений констант CONTROLTAB_*, определяюшая
  //       вкладки, для которых необходимо выполнить обновление.
  //=============================================================================
  public void Update(int stationIndex, int tabMask);
  
  //=============================================================================
  // Name: UpdateSpanTab
  // Desc: Обновляет вкладку управления перегонами, если она содержит перегон.
  // Parm: spanIndex – индекс перегона, при наличии которого требуется обновить
  //       вкладку управления перегонами.
  //=============================================================================
  public void UpdateSpanTab(int spanIndex);

  //=============================================================================
  // Name: BringToFront
  // Desc: Восстанавливает окно, если оно было свёрнуто, и перемещает его на
  //       передний план, поверх всех окон. 
  //=============================================================================
  public void BringToFront(void);

  //=============================================================================
  // Name: Dispose
  // Desc: Освобождает все ресурсы для этого объекта. 
  //=============================================================================
  public void Dispose(void);
  
  //=============================================================================
  // Name: SelectedStationIndex
  // Desc: Возвращает индекс выбраной станции.
  // Retn: Отсчитываемый от 0 индекс выбранной станции; или значение -1, если
  //       станция не выбрана.
  //=============================================================================
  public int SelectedStationIndex(void);

  //=============================================================================
  // Name: UpdateControlPanel
  // Desc: Выполняет обновление панели управления
  //=============================================================================
  void UpdateControlPanel(void);
  
  //=============================================================================
  // Name: BuildPathControl
  // Desc: Создаёт панель управления маршрутами
  // Parm: buff – буфер, в который будет генерироваться htnl код панеои 
  //       управления. 
  //=============================================================================
  void BuildPathControl(HTMLBuffer buff);

  //=============================================================================
  // Name: BuildJunctionControl
  // Desc: Создаёт панель управления стрелками
  // Parm: buff – буфер, в который будет генерироваться htnl код панеои 
  //       управления. 
  //=============================================================================
  void BuildJunctionControl(HTMLBuffer buff);

  //=============================================================================
  // Name: BuildSignalControl
  // Desc: Создаёт панель управления светофорами
  // Parm: buff – буфер, в который будет генерироваться htnl код панеои 
  //       управления. 
  //=============================================================================
  void BuildSignalControl(HTMLBuffer buff);

  //=============================================================================
  // Name: BuildSpanControl
  // Desc: Создаёт панель управления перегонами
  // Parm: buff – буфер, в который будет генерироваться htnl код панеои 
  //       управления. 
  //=============================================================================
  void BuildSpanControl(HTMLBuffer buff);

  //=============================================================================
  // Name: BuildShuntControl
  // Desc: Создаёт панель управления маневрами
  // Parm: buff – буфер, в который будет генерироваться htnl код панеои 
  //       управления. 
  //=============================================================================
  void BuildShuntControl(HTMLBuffer buff);

  //=============================================================================
  // Name: BuildQueueControl
  // Desc: Создаёт панель управления очередью маршрутов
  // Parm: buff – буфер, в который будет генерироваться htnl код панеои 
  //       управления. 
  //=============================================================================
  void BuildQueueControl(HTMLBuffer buff);

  //=============================================================================
  // Name: OnLinkClick
  // Desc: Вызывается при нажатии на ссылке в браузере 
  // Parm: action — Действие, которое необходимо выполнить
  //=============================================================================
  void OnLinkClick(string action, string argumentX, string argumentY, string argumentZ);
  
  //=============================================================================
  // Name: StartResponsibleAction
  // Desc: Вызывается при нажатии на ссылке в браузере 
  // Parm: action — Действие, которое необходимо выполнить
  //=============================================================================
  void StartResponsibleAction(int itemIndex, int action);



	//
	// РЕАЛИЗАЦИЯ
	//
  
  define int RESPMODE_NONE                = 0;      //Ответственое действие не выполняется
  define int RESPMODE_WAIT                = 1;      //Ожидает доступности для ответственого действия
  define int RESPMODE_WAITCONFITM         = 2;      //Ожидает действия от диспетчера

  define int RESPACTION_FORCEPLUS         = 3;      //Спомогательный перевод в плюс
  define int RESPACTION_FORCEMINUS        = 4;      //Вспомогательный перевд в минус
  define int RESPACTION_INVITATION        = 5;      //Включение ПС
  define int RESPACTION_FORCEPATHCANCEL   = 6;      //Включение ПС
  define int RESPACTION_SPAN              = 7;      //Ответственные действия для перегонов

  zx_DSP_Core _core;                                //Тут ссылка на ядро
  Browser _browser;                                 //Собственно само окно браузера
  StringTable _strtable;                            //Таблица строк
  int _stationviewed = -1;                          //Текущая выбраная станция
  
  int _resp_mode = RESPMODE_NONE;                   //Текущий режим ответственого действия
  int _resp_stationindex;                           //Индекс станции для ответственого действия
  int _resp_itemindex;                              //Индекс объекта для ответственого действия
  int _resp_object;                                 //Объект для ответсвеного действия (соответствет коду вкладки)
  int _resp_action;                                 //Ответственое действие
  int _resp_id;                                     //Идентификатор действия
  string _resp_spanaction;                          //Данные действия для перегонов при ответственных действиях  


  public zd_DSP_ControllerForm Init(zx_DSP_Core core, StringTable strTable, int stationIndex)
  {
    if(_browser) return me; //Если инициализация уже выполнена, то выходим.

    _core = core;
    _strtable = strTable;
    _stationviewed = stationIndex;

    _browser = Constructors.NewBrowser();
    _browser.SetWindowStyle(Browser.STYLE_DEFAULT); 
    _browser.SetScrollEnabled(false);        
    _browser.SetCloseEnabled(true);                                        
    _browser.SetWindowPriority(Browser.BP_Window);
    _browser.SetWindowTitle(strTable.GetString("controller-caption"));
    _browser.SetWindowCentered(800, 600);
    _browser.LoadHTMLFile(_core.GetAsset(), _core.StyleFormController);
    
    _browser.SetElementProperty("stationlist", "html", "");
    _browser.SetElementProperty("stationlist", "background-style", "1");
    
    _browser.SetElementProperty("controlpanel", "html", "");
    _browser.SetElementProperty("controlpanel", "background-style", "1");

    AddHandler(_browser, "Browser", "Closed", "BrowserClosedHandler");
    AddHandler(_browser, "Browser-URL", "", "BrowserURLHandler");

    AddHandler(me, "ZXDSP-ResponsibleEnable", null, "ResponsibleTimeoutHandler");
    AddHandler(me, "ZXDSP-ResponsibleTimeout", null, "ResponsibleTimeoutHandler");
    
    Update();
       
    return me;
  }

  public zd_DSP_ControllerForm Init(zx_DSP_Core core, StringTable strTable) {
    return Init(core, strTable, -1);
  }

  public void Update()
  {
    if(_core.stations and _core.stations.size()) {
      string lnktablestart = HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%");
      string lnktableend = HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable();
      HTMLBuffer buff = HTMLBufferStatic.Construct();
      int i, count = _core.stations.size();
      buff.Print("<html><body>");
      buff.Print(HTMLWindow.StartTable("width=100%"));
      bool hasstation = false;
      for(i = 0; i < count; ++i){
        zx_DSP_Station station = _core.stations[i];
        if(!station.CheckMyPermission()) continue;
        buff.Print(HTMLWindow.StartRow());
        if(i == _stationviewed){
          buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleSelectorBackColor));
          hasstation = true;
        } else buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(HTMLWindow.MakeLink("live://viewstation/" + i, lnktablestart + station.name + lnktableend, _strtable.GetString("tooltip-selectstation")));
        buff.Print(HTMLWindow.EndCell() + HTMLWindow.EndRow());
      }
      if(!hasstation) _stationviewed = -1;
      buff.Print(HTMLWindow.EndTable());
      buff.Print("</body></html>");
      _browser.SetElementProperty("stationlist", "html", buff.AsString());
    }
    UpdateControlPanel();
  }
  
  public void Update(int stationIndex, int tabMask)
  {
    if(_core.stations and _stationviewed >= 0 and stationIndex == _stationviewed and _core.stations[_stationviewed].controllerFormTab & tabMask) UpdateControlPanel();
  }
  
  public void UpdateSpanTab(int spanIndex)
  {
    zx_DSP_SpanBase span = _core.spans[spanIndex];
    zx_DSP_Station controlstation = null;   
    int controlstationindex = SelectedStationIndex();
    if (controlstationindex >= 0 and controlstationindex < _core.stations.size()) controlstation = _core.stations[controlstationindex];
    if (controlstation == span.station or controlstation == span.inverse.station) Update(controlstationindex, zx_DSP_Station.CONTROLTAB_SPAN);
  }

  public void BringToFront(void)
  {
    if(_browser) {
      _browser.RestoreWindow();
      _browser.BringToFront();    
    }
  }

  public void Dispose(void)
  {
    if(_browser) {
      _browser.CloseWindow();
      AddHandler(_browser, "Browser", "Closed", null);
      AddHandler(_browser, "Browser-URL", "", null);
      AddHandler(me, "ZXDSP-ResponsibleEnable", null, null);
      AddHandler(me, "ZXDSP-ResponsibleTimeout", null, null);
    }
    _core = null;
    _browser = null;
    _strtable = null;
    _stationviewed = -1;
  }
  
  public int SelectedStationIndex(void)
  {
    return _stationviewed;
  }

  public void GoToStation(int stationIndex) {
    _stationviewed = stationIndex;
    Update();
  }

  void UpdateControlPanel(void)
  {
    HTMLBuffer buff = HTMLBufferStatic.Construct();
    buff.Print("<html><body>");
    buff.Print(HTMLWindow.StartTable("width=100%"));
    if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].CheckMyPermission()) {
      switch(_core.stations[_stationviewed].controllerFormTab) {
        case zx_DSP_Station.CONTROLTAB_PATH:        BuildPathControl(buff);
                                                    break;      
        case zx_DSP_Station.CONTROLTAB_JUNCTION:    BuildJunctionControl(buff);
                                                    break;      
        case zx_DSP_Station.CONTROLTAB_SIGNAL:      BuildSignalControl(buff);
                                                    break;      
        case zx_DSP_Station.CONTROLTAB_SPAN:        BuildSpanControl(buff);
                                                    break;      
        case zx_DSP_Station.CONTROLTAB_SHUNT:       BuildShuntControl(buff);
                                                    break;      
        case zx_DSP_Station.CONTROLTAB_QUEUE:       BuildQueueControl(buff);
                                                    break;
        default:                                    break;                                                          
      }
    } else if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and !_core.stations[_stationviewed].CheckMyPermission()) {
      buff.Print(HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.StartRow() + 
                 HTMLWindow.StartCell("bgcolor=#D99694 align=center") + HTMLWindow.MakeFontColor(_strtable.GetString("error-accessdenied"), "632523") +
                 HTMLWindow.EndCell() + HTMLWindow.EndRow());
    } else if(_core.stations) {
      buff.Print(HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.StartRow() + 
      HTMLWindow.StartCell("align=center") + _strtable.GetString("message-selectstation") + HTMLWindow.EndCell() + HTMLWindow.EndRow());
    } else {
      buff.Print(HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.StartRow() + 
                 HTMLWindow.StartCell("bgcolor=#D99694 align=center") + HTMLWindow.MakeFontColor(_strtable.GetString("error-nocalced"), "632523") +
                 HTMLWindow.EndCell() + HTMLWindow.EndRow());
    }
    buff.Print(HTMLWindow.EndTable());
    buff.Print("</body></html>");
    _browser.SetElementProperty("controlpanel", "html", buff.AsString());
  }
  
  Menu CreateQueueMenu(int pathIndex, int variantIndex)
  {
    Menu menu = Constructors.NewMenu();
    menu.AddItem(_strtable.GetString("menu-mainqueue"), _browser, "Browser-URL", "live://path-queuevariant/" + pathIndex + "/" + variantIndex + "/0");
    menu.AddSeperator();
    menu.AddItem(_strtable.GetString("menu-anywayqueue"), _browser, "Browser-URL", "live://path-queuevariant/" + pathIndex + "/" + variantIndex + "/-1");
    menu.AddSeperator();
    int i;
    for (i = 1; i <= 10; ++i)
      menu.AddItem(_strtable.GetString1("menu-queuegroup", i), _browser, "Browser-URL", "live://path-queuevariant/" + pathIndex + "/" + variantIndex + "/" + i);
    return menu;
  } 




  void BuildPathControl(HTMLBuffer buff)
  {
    zx_DSP_Station station = _core.stations[_stationviewed];
    int i, count = station.paths.size();
    HTMLBuffer pathbuff = null;
    for(i = 0; i < count; ++i) {
      zx_DSP_Path basepath = station.paths[i];
      int status = 0;
      
      while(i < count and station.paths[i].startSign == basepath.startSign) { //Перебираем маршруты, пока они от того же светофора
        zx_DSP_Path path = station.paths[i];
        if(path.startSign == station.selectSign) { //Если начальный светофор совпадает с выбранным
          bool confirmmode = _resp_mode != RESPMODE_NONE and _resp_action == RESPACTION_FORCEPATHCANCEL and _resp_stationindex == _stationviewed and _resp_itemindex == i;
          if(!pathbuff) pathbuff = HTMLBufferStatic.Construct();
          



          pathbuff.Print(HTMLWindow.StartRow());
            pathbuff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
              pathbuff.Print(HTMLWindow.StartTable("width=100%"));
                pathbuff.Print(HTMLWindow.StartRow());
                  pathbuff.Print(HTMLWindow.StartCell());
                    if(path.state & zx_DSP_Path.STATE_BUSY) pathbuff.Print(_core.MakeHTMLImage("st_r", 32));
                    else if(path.state & zx_DSP_Path.STATE_ACTIVE) pathbuff.Print(_core.MakeHTMLImage("st_g", 32));
                    else if(path.state == zx_DSP_Path.STATE_WAIT) pathbuff.Print(_core.MakeHTMLImage("st_b", 32));
                    else if(path.state == zx_DSP_Path.STATE_AUTO) pathbuff.Print(_core.MakeHTMLImage("st_y", 32));
                    else if(path.state == zx_DSP_Path.STATE_X_MODE) pathbuff.Print(_core.MakeHTMLImage("st_w", 32));
                    else pathbuff.Print(_core.MakeHTMLImage("st_gr", 32));
                  pathbuff.Print(HTMLWindow.EndCell());
                  pathbuff.Print(HTMLWindow.StartCell("width=100%"));
                    pathbuff.Print(HTMLWindow.StartTable("width=100%"));
                      pathbuff.Print(HTMLWindow.StartRow());

                        pathbuff.Print(HTMLWindow.StartCell("width=16"));
                          if(path.state & zx_DSP_Path.STATE_ACTIVE){
                            zxSignal sign = path.startSign;
                            if (path.groupSign) {
                              sign = path.groupSign;
                            }
                            if(!sign.shunt_open and sign.GetSignalState() == Signal.GREEN) pathbuff.Print(_core.MakeHTMLImage("st_g", 16));
                            else if(!sign.shunt_open and sign.GetSignalState() == Signal.YELLOW) pathbuff.Print(_core.MakeHTMLImage("st_y", 16));
                            else if(!sign.shunt_open and (sign.train_open or sign.x_mode)) pathbuff.Print(_core.MakeHTMLImage("st_re", 16));
                            else pathbuff.Print(_core.MakeHTMLImage("st_r", 16));
                          } else pathbuff.Print(_core.MakeHTMLImage("st_gr", 16));
                        pathbuff.Print(HTMLWindow.EndCell());
                        pathbuff.Print(HTMLWindow.StartCell());
                          string startSignStr = HTMLWindow.MakeLink("live://cameratosignal/" + path.startSign.GetGameObjectID().SerialiseToString(), path.startSign.privateName, _strtable.GetString("tooltip-cameratosignal"));
                          if (path.groupSign) {
                            startSignStr = HTMLWindow.MakeLink("live://cameratosignal/" + path.groupSign.GetGameObjectID().SerialiseToString(), path.groupSign.privateName, _strtable.GetString("tooltip-cameratosignal")) + "&nbsp;(" + startSignStr + ")";
                          }
                          pathbuff.Print(startSignStr);
                          if (path.backwardEndSign) {
                            pathbuff.Print("&nbsp;");
                            pathbuff.Print(_strtable.GetString("string-pathbehind"));
                            pathbuff.Print("&nbsp;");
                            pathbuff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + path.backwardEndSign.GetGameObjectID().SerialiseToString(), path.backwardEndSign.privateName, _strtable.GetString("tooltip-cameratosignal")));
                          }
                          if (path.endSign) {
                            pathbuff.Print("&nbsp;");
                            pathbuff.Print(_strtable.GetString("string-pathto"));
                            pathbuff.Print("&nbsp;");
                            string endName = path.endSign.privateName;
                            if (path.startSign.stationName != path.endSign.stationName) {
                              endName = endName + "@" + path.endSign.stationName;
                            }
                            pathbuff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + path.endSign.GetGameObjectID().SerialiseToString(), endName, _strtable.GetString("tooltip-cameratosignal")));
                          }
                          if(path.startSign.stationName != path.endSign.stationName) {
                            pathbuff.Print("&nbsp;");
                            pathbuff.Print(_strtable.GetString("string-pathspan"));
                          }
                          zx_DSP_SignalInfo endSignalInfo = null;
                          if ((endSignalInfo = _core.GetSignalInfo(path.endSign)) and endSignalInfo.pathsFrom and endSignalInfo.pathsFrom.size()) {
                            pathbuff.Print("&nbsp;"); 
        			              pathbuff.Print(HTMLWindow.StartFontColor(_core.StyleActionText)); 
                            pathbuff.Print(HTMLWindow.MakeLink("live://selectsignal/" + path.endSign.GetGameObjectID().SerialiseToString() + "/" + endSignalInfo.station.index, "⮞", _strtable.GetString("tooltip-topathlistforsignal")));
        			              pathbuff.Print(HTMLWindow.EndFontColor()); 
                          }
                        pathbuff.Print(HTMLWindow.EndCell());
                        pathbuff.Print(HTMLWindow.StartCell("width=48"));
                          zx_DSP_SignalInfo signalInfo = _core.GetSignalInfo(path.startSign);
                          bool pathprepared = signalInfo and signalInfo.activePathFrom == path;
                          if(path.state & zx_DSP_Path.STATE_ACTIVE) {
			                      if (pathprepared and !path.startSign.train_open and !confirmmode)
                              pathbuff.Print(HTMLWindow.MakeLink("live://path-reopen/" + i, _core.MakeHTMLImage("path-reopensignal-32-16", 32, 16), _strtable.GetString("tooltip-reopenpathsignal")));
			                      else if (pathprepared and !path.startSign.train_open) pathbuff.Print(_core.MakeHTMLImage("path-reopensignal-d-32-16", 32, 16));
                            else  pathbuff.Print(_core.MakeHTMLImage("path-build-d-32-16", 32, 16)); 
                          } else {
                            if (!confirmmode) pathbuff.Print(HTMLWindow.MakeLink("live://path-build/" + i, _core.MakeHTMLImage("path-build-32-16", 32, 16), _strtable.GetString("tooltip-buildpath")));
                            else  pathbuff.Print(_core.MakeHTMLImage("path-build-d-32-16", 32, 16)); 
                          }
                          if(!(path.state & zx_DSP_Path.STATE_ACTIVE) and path.variants.size() > 1 and !confirmmode)
                            pathbuff.Print(HTMLWindow.MakeLink("live://path-buildvariant/" + i, _core.MakeHTMLImage("dropoption-16", 16), _strtable.GetString("tooltip-buildpathvariant")));
                          else pathbuff.Print(_core.MakeHTMLImage("dropoption-d-16", 16));                      
                        pathbuff.Print(HTMLWindow.EndCell());
                        pathbuff.Print(HTMLWindow.StartCell("width=32"));
                          if(path.state & zx_DSP_Path.STATE_ACTIVE and !(path.state & zx_DSP_Path.STATE_BUSY) and !confirmmode)
                            pathbuff.Print(HTMLWindow.MakeLink("live://path-cancel/" + i, _core.MakeHTMLImage("path-cancel-32-16", 32, 16), _strtable.GetString("tooltip-cancelpath")));
                          else pathbuff.Print(_core.MakeHTMLImage("path-cancel-d-32-16", 32, 16));                      
                        pathbuff.Print(HTMLWindow.EndCell());
                        pathbuff.Print(HTMLWindow.StartCell("width=32"));
                          if(path.state & zx_DSP_Path.STATE_ACTIVE and path.state & zx_DSP_Path.STATE_BUSY and !confirmmode)
                            pathbuff.Print(HTMLWindow.MakeLink("live://path-forcecancel/" + i, _core.MakeHTMLImage("path-forcecancel-32-16", 32, 16), _strtable.GetString("tooltip-cancelpath")));
                          else pathbuff.Print(_core.MakeHTMLImage("path-forcecancel-d-32-16", 32, 16));                      
                        pathbuff.Print(HTMLWindow.EndCell());
                      pathbuff.Print(HTMLWindow.EndRow());
                      pathbuff.Print(HTMLWindow.StartRow());
                        pathbuff.Print(HTMLWindow.StartCell("colspan=2"));
                          if(path.state & zx_DSP_Path.STATE_ACTIVE) {
                            if(path.activeVariant > 0) pathbuff.Print(HTMLWindow.MakeFontColor(_strtable.GetString1("string-pathbulded-varinat", path.activeVariant), _core.StyleWarnText));
                            else pathbuff.Print(HTMLWindow.MakeFontColor(_strtable.GetString("string-pathbulded-main"), _core.StyleWarnText));
                          }
                        pathbuff.Print(HTMLWindow.EndCell());
                        
                        
                        pathbuff.Print(HTMLWindow.StartCell("width=48"));
                          if (!confirmmode) {
                            pathbuff.Print(HTMLWindow.MakeLink("live://path-queue/" + i, _core.MakeHTMLImage("path-queue-32-16", 32, 16), _strtable.GetString("tooltip-pathtoqueue")));
                            pathbuff.Print(HTMLWindow.MakeLink("live://path-queuevariant/" + i, _core.MakeHTMLImage("dropoption-16", 16), _strtable.GetString("tooltip-pathtoqueuevariant")));
                          } else  {
                            pathbuff.Print(_core.MakeHTMLImage("path-queue-d-32-16", 32, 16));                      
                            pathbuff.Print(_core.MakeHTMLImage("dropoption-d-16", 16));                      
                          } 
                        pathbuff.Print(HTMLWindow.EndCell());
                        pathbuff.Print(HTMLWindow.StartCell("width=32"));
                          if (path.type & zx_DSP_Path.TYPE_AUTO and path.state & zx_DSP_Path.STATE_AUTO and !confirmmode)
                            pathbuff.Print(HTMLWindow.MakeLink("live://path-autooff/" + i, _core.MakeHTMLImage("path-auto-off-32-16", 32, 16), _strtable.GetString("tooltip-pathautooff")));
                          else if (path.type & zx_DSP_Path.TYPE_AUTO and !confirmmode) pathbuff.Print(HTMLWindow.MakeLink("live://path-autoon/" + i, _core.MakeHTMLImage("path-auto-on-32-16", 32, 16), _strtable.GetString("tooltip-pathautoon")));
                          else pathbuff.Print(_core.MakeHTMLImage("path-auto-disabled-32-16", 32, 16)); 
                        pathbuff.Print(HTMLWindow.EndCell());
                        pathbuff.Print(HTMLWindow.StartCell("width=32"));
                          if(path.type & zx_DSP_Path.TYPE_X_MODE and path.state & zx_DSP_Path.STATE_ACTIVE and !(path.state & zx_DSP_Path.STATE_BUSY) and path.activeVariant == 0) {
                            if(pathprepared and path.state & zx_DSP_Path.STATE_X_MODE and !confirmmode)
                              pathbuff.Print(HTMLWindow.MakeLink("live://path-xmodeoff/" + i, _core.MakeHTMLImage("path-xmode-off-32-16", 32, 16), _strtable.GetString("tooltip-pathxmodeoff")));
                            else if(pathprepared and !confirmmode) pathbuff.Print(HTMLWindow.MakeLink("live://path-xmodeon/" + i, _core.MakeHTMLImage("path-xmode-on-32-16", 32, 16), _strtable.GetString("tooltip-pathxmodeon")));
                            else pathbuff.Print(_core.MakeHTMLImage("path-xmode-disabled-32-16", 32, 16)); 
                          } else pathbuff.Print(_core.MakeHTMLImage("path-xmode-disabled-32-16", 32, 16));
                        pathbuff.Print(HTMLWindow.EndCell());
                      pathbuff.Print(HTMLWindow.EndRow());
                    pathbuff.Print(HTMLWindow.EndTable());
                  pathbuff.Print(HTMLWindow.EndCell());
                pathbuff.Print(HTMLWindow.EndRow());
                if (confirmmode) {
                  pathbuff.Print(HTMLWindow.StartRow());
                    pathbuff.Print(HTMLWindow.StartCell("colspan=2 align=right"));
                      pathbuff.Print("<nowrap>"); 
                        pathbuff.Print(_strtable.GetString("controller-confirm-forcepathcancel"));
                        pathbuff.Print("&nbsp;&nbsp;");
                        if(_resp_mode == RESPMODE_WAITCONFITM) pathbuff.Print(HTMLWindow.MakeLink("live://responsible-accept/" + i, _core.MakeHTMLImage("responsible-accept-16", 16), _strtable.GetString("tooltip-responsible-accept")));
                        else pathbuff.Print(_core.MakeHTMLImage("responsible-accept-d-16", 16));
                        pathbuff.Print("&nbsp;");
                        pathbuff.Print(HTMLWindow.MakeLink("live://responsible-cancel/" + i, _core.MakeHTMLImage("responsible-cancel-16", 16), _strtable.GetString("tooltip-responsible-cancel"))); 
                      pathbuff.Print("</nowrap>"); 
                    pathbuff.Print(HTMLWindow.EndCell());
                  pathbuff.Print(HTMLWindow.EndRow());
                }
                if(path.errorMessage != "" and ((path.errMsgViewedTime >= 0 and path.errMsgViewedTime + 60.0 >= World.GetTimeElapsed()) or path.errMsgViewedTime < 0)) {
                  pathbuff.Print(HTMLWindow.StartRow());
                    pathbuff.Print(HTMLWindow.StartCell("colspan=2"));
                      pathbuff.Print(HTMLWindow.MakeFontColor(path.errorMessage, _core.StyleErrorText));
                    pathbuff.Print(HTMLWindow.EndCell());
                  pathbuff.Print(HTMLWindow.EndRow());
                } else if (path.errorMessage != "" and path.errMsgViewedTime >= 0) {
                  path.errorMessage = null;
                  path.errMsgViewedTime = -1;
                }
              pathbuff.Print(HTMLWindow.EndTable());
            pathbuff.Print(HTMLWindow.EndCell());
          pathbuff.Print(HTMLWindow.EndRow());
        }


      
      
      
        status = status | path.state; //Совмещаем статусы маршрута
        if(path.startSign.stationName != path.endSign.stationName) status = status | (1 << 30); //Если маршрут на перегон, то сохраняем указатель
        else status = status | (1 << 31); //Если маршрут по станции, то сохраняем указатель
        ++i;
      }
      --i;

      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
          buff.Print(HTMLWindow.StartTable("width=100%"));
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.StartCell("width=25"));
                if(basepath.startSign == station.selectSign) 
                  buff.Print(HTMLWindow.MakeLink("live://selectsignal/" + basepath.startSign.GetGameObjectID().SerialiseToString(), _core.MakeHTMLImage("node-close-16", 16), _strtable.GetString("tooltip-unselectpathsignal")));
                else buff.Print(HTMLWindow.MakeLink("live://selectsignal/" + basepath.startSign.GetGameObjectID().SerialiseToString(), _core.MakeHTMLImage("node-open-16", 16), _strtable.GetString("tooltip-selectpathsignal")));
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.StartCell("width=48"));
                if(status & zx_DSP_Path.STATE_BUSY) buff.Print(_core.MakeHTMLImage("st_r", 16));
                else if(status & zx_DSP_Path.STATE_ACTIVE) buff.Print(_core.MakeHTMLImage("st_g", 16));
                else buff.Print(_core.MakeHTMLImage("st_gr", 16));
                if(status & zx_DSP_Path.STATE_WAIT) buff.Print(_core.MakeHTMLImage("st_b", 16));
                else buff.Print(_core.MakeHTMLImage("st_gr", 16));
                if(status & zx_DSP_Path.STATE_AUTO) buff.Print(_core.MakeHTMLImage("st_y", 16));
                else if(status & zx_DSP_Path.STATE_X_MODE) buff.Print(_core.MakeHTMLImage("st_w", 16));
                else buff.Print(_core.MakeHTMLImage("st_gr", 16));
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.StartCell("width=150"));
                zx_DSP_SignalInfo signalInfo = _core.GetSignalInfo(basepath.startSign);
                if (signalInfo and signalInfo.span) {
                  zx_DSP_Station inversStation = signalInfo.span.inverse.station;
                  if (inversStation.CheckMyPermission()) {
                    buff.Print(HTMLWindow.MakeLink("live://viewstation/" + inversStation.index, inversStation.name, _strtable.GetString("tooltip-selectstation")));
                  }
                  else {
                    buff.Print(inversStation.name);
                  }
                  buff.Print("&nbsp;--&gt;&gt;&nbsp;");
                }
                buff.Print(_strtable.GetString("string-pathfrom"));
                buff.Print("&nbsp;");
                buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + basepath.startSign.GetGameObjectID().SerialiseToString(), HTMLWindow.MakeBold(basepath.startSign.privateName), _strtable.GetString("tooltip-cameratosignal")));
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.StartCell());
                if((status & ((1 << 30) | (1 << 31))) == ((1 << 30) | (1 << 31))) buff.Print(_strtable.GetString("pathtypefromsignal-all"));
                else if(status & (1 << 30)) buff.Print(_strtable.GetString("pathtypefromsignal-span"));
                else if(status & (1 << 31)) buff.Print(_strtable.GetString("pathtypefromsignal-station"));
              buff.Print(HTMLWindow.EndCell());
            buff.Print(HTMLWindow.EndRow());
          buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
      if(pathbuff) {
        buff.Print(pathbuff.AsString());
        pathbuff = null;
      }
    }
  }
  
  string GetJunctionStateString(zx_DSP_JunctionInfo jncinf)
  {
		if(jncinf.state & zx_DSP_JunctionInfo.STATE_ERROR) return _strtable.GetString("junctionstate-error");
		else if(jncinf.state & zx_DSP_JunctionInfo.STATE_TRAIN) return _strtable.GetString("junctionstate-busy");
		else if(jncinf.state & zx_DSP_JunctionInfo.STATE_MANUALLOCK) return _strtable.GetString("junctionstate-manuallock");
		else if(jncinf.state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL)  return _strtable.GetString("junctionstate-localcontrol");
		else if(jncinf.state & zx_DSP_JunctionInfo.STATE_LOCK) return _strtable.GetString("junctionstate-lock");
		else if(jncinf.state & zx_DSP_JunctionInfo.STATE_PATH)  return _strtable.GetString1("junctionstate-path", _core.stations[_stationviewed].paths[jncinf.pathIndex].GetNameOrGenerate(_strtable));
		else if(jncinf.state & zx_DSP_JunctionInfo.STATE_SHUNT) return _strtable.GetString1("junctionstate-shunt", _core.stations[_stationviewed].shuntPaths[jncinf.pathIndex].GetNameOrGenerate(_strtable));
		else if(jncinf.state & zx_DSP_JunctionInfo.STATE_MANUAL) return _strtable.GetString("junctionstate-manual");
    return _strtable.GetString("junctionstate-free");
  }

  void BuildJunctionControl(HTMLBuffer buff)
  {
    zx_DSP_Station station = _core.stations[_stationviewed];
    int i, count = station.jncs.size();
    for(i = 0; i < count; ++i) {
      zx_DSP_JunctionInfo jncinf = station.jncs[i]; 
      if(jncinf.masterJunction or jncinf.type & zx_DSP_JunctionInfo.JNC_MANUAL) continue;
      bool confirmmode = _resp_mode != RESPMODE_NONE and (_resp_action == RESPACTION_FORCEPLUS or _resp_action == RESPACTION_FORCEMINUS) and _resp_stationindex == _stationviewed and _resp_itemindex == i;
      bool issynchronized = jncinf.IsSynchronized();
      bool dirisplus = jncinf.IsDirection(true); 
      bool dirisminus = jncinf.IsDirection(false);
      bool allowswicth = jncinf.AllowManualSwitch(false);
      bool allowswicthex = jncinf.AllowManualSwitch(true);
      int state = jncinf.state;
      string title = jncinf.GetFullTitle();
      if(jncinf.slaveJunctions) {
        int j, slavecount = jncinf.slaveJunctions.size();
        for (j = 0; j < slavecount; ++j)
          state = state | jncinf.slaveJunctions[j].state;
      }
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
          buff.Print(HTMLWindow.StartTable("width=100%"));
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.StartCell());
                if(issynchronized and !(jncinf.state & zx_DSP_JunctionInfo.STATE_CUTUP)) {
                  if(jncinf.type & zx_DSP_JunctionInfo.JNC_RIGHT) {
                    if(jncinf.jnc.GetDirection() == JunctionBase.DIRECTION_RIGHT) buff.Print(_core.MakeHTMLImage("jnc-f-r-r-24-16", 24, 16));
                    else buff.Print(_core.MakeHTMLImage("jnc-f-r-f-24-16", 24, 16));
                  } else if(jncinf.type & zx_DSP_JunctionInfo.JNC_LEFT) {
                    if(jncinf.jnc.GetDirection() == JunctionBase.DIRECTION_RIGHT) buff.Print(_core.MakeHTMLImage("jnc-f-l-f-24-16", 24, 16));
                    else buff.Print(_core.MakeHTMLImage("jnc-f-l-l-24-16", 24, 16));
                  } else {
                    if(jncinf.jnc.GetDirection() == JunctionBase.DIRECTION_RIGHT) buff.Print(_core.MakeHTMLImage("jnc-f-s-r-24-16", 24, 16));
                    else buff.Print(_core.MakeHTMLImage("jnc-f-s-l-24-16", 24, 16));
                  } 
                } else {
                  if(jncinf.type & zx_DSP_JunctionInfo.JNC_RIGHT) buff.Print(_core.MakeHTMLImage("jnc-f-r-u-24-16", 24, 16));
                  else if(jncinf.type & zx_DSP_JunctionInfo.JNC_LEFT) buff.Print(_core.MakeHTMLImage("jnc-f-l-u-24-16", 24, 16));
                  else buff.Print(_core.MakeHTMLImage("jnc-f-s-u-24-16", 24, 16));
                }
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if(state & zx_DSP_JunctionInfo.STATE_ERROR) buff.Print(_core.MakeHTMLImage("st_w", 16));
                else if(state & zx_DSP_JunctionInfo.STATE_TRAIN) buff.Print(_core.MakeHTMLImage("st_r", 16));
                else if(state & zx_DSP_JunctionInfo.STATE_MANUALLOCK) buff.Print(_core.MakeHTMLImage("st_gr", 16));
                else if(state & zx_DSP_JunctionInfo.STATE_LOCALCONTROL) buff.Print(_core.MakeHTMLImage("st_be", 16));
                else if(state & zx_DSP_JunctionInfo.STATE_LOCK) buff.Print(_core.MakeHTMLImage("st_be", 16));
                else if(state & zx_DSP_JunctionInfo.STATE_PATH) buff.Print(_core.MakeHTMLImage("st_re", 16));
                else if(state & zx_DSP_JunctionInfo.STATE_SHUNT) buff.Print(_core.MakeHTMLImage("st_b", 16));
                else if(state & zx_DSP_JunctionInfo.STATE_MANUAL) buff.Print(_core.MakeHTMLImage("st_y", 16));
                else buff.Print(_core.MakeHTMLImage("st_g", 16));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell("width=100%"));
                buff.Print("<nowrap>"); 
                  if(title and title.size()) {
                    buff.Print(_strtable.GetString1("junction-number", title));
                    if (!jncinf.slaveJunctions or !jncinf.slaveJunctions.size())
                      buff.Print("&nbsp;&nbsp;&nbsp;[" + HTMLWindow.MakeLink("live://cameratojunction/" + jncinf.jnc.GetGameObjectID().SerialiseToString(), jncinf.jnc.GetLocalisedName(), _strtable.GetString("tooltip-cameratojunction")) + "]");
                  } else buff.Print(HTMLWindow.MakeLink("live://cameratojunction/" + jncinf.jnc.GetGameObjectID().SerialiseToString(), jncinf.jnc.GetLocalisedName(), _strtable.GetString("tooltip-cameratojunction")));
                buff.Print("</nowrap>"); 
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if(allowswicth and !dirisplus and !confirmmode) {
                  if(jncinf.defaultDirection == JunctionBase.DIRECTION_RIGHT) buff.Print(HTMLWindow.MakeLink("live://junction-plus/" + i, _core.MakeHTMLImage("junction-stp-32-16", 32, 16), _strtable.GetString("tooltip-junction-right-plus")));
                  else buff.Print(HTMLWindow.MakeLink("live://junction-plus/" + i, _core.MakeHTMLImage("junction-stp-32-16", 32, 16), _strtable.GetString("tooltip-junction-left-plus")));
                } else buff.Print(_core.MakeHTMLImage("junction-stp-d-32-16", 32, 16));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if(allowswicth and !dirisminus and !confirmmode) {
                  if(jncinf.defaultDirection == JunctionBase.DIRECTION_RIGHT) buff.Print(HTMLWindow.MakeLink("live://junction-minus/" + i, _core.MakeHTMLImage("junction-stm-32-16", 32, 16), _strtable.GetString("tooltip-junction-left-minus")));
                  else buff.Print(HTMLWindow.MakeLink("live://junction-minus/" + i, _core.MakeHTMLImage("junction-stm-32-16", 32, 16), _strtable.GetString("tooltip-junction-right-minus")));
                } else buff.Print(_core.MakeHTMLImage("junction-stm-d-32-16", 32, 16));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if(!(jncinf.state & zx_DSP_JunctionInfo.STATE_MANUAL) and !confirmmode) buff.Print(HTMLWindow.MakeLink("live://junction-lock/" + i, _core.MakeHTMLImage("junction-stb-32-16", 32, 16), _strtable.GetString("tooltip-junction-lock")));
                else buff.Print(_core.MakeHTMLImage("junction-stb-d-32-16", 32, 16));              
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if(jncinf.state & zx_DSP_JunctionInfo.STATE_MANUAL and !confirmmode) buff.Print(HTMLWindow.MakeLink("live://junction-unlock/" + i, _core.MakeHTMLImage("junction-str-32-16", 32, 16), _strtable.GetString("tooltip-junction-unlock")));
                else buff.Print(_core.MakeHTMLImage("junction-str-d-32-16", 32, 16));              
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if(allowswicthex and !dirisplus and !confirmmode) {
                  if(jncinf.defaultDirection == JunctionBase.DIRECTION_RIGHT) buff.Print(HTMLWindow.MakeLink("live://junction-plus-ex/" + i, _core.MakeHTMLImage("junction-stpz-32-16", 32, 16), _strtable.GetString("tooltip-junction-right-plus-ex")));
                  else buff.Print(HTMLWindow.MakeLink("live://junction-plus-ex/" + i, _core.MakeHTMLImage("junction-stpz-32-16", 32, 16), _strtable.GetString("tooltip-junction-left-plus-ex")));
                } else buff.Print(_core.MakeHTMLImage("junction-stpz-d-32-16", 32, 16));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if(allowswicthex and !dirisminus and !confirmmode) {
                  if(jncinf.defaultDirection == JunctionBase.DIRECTION_RIGHT) buff.Print(HTMLWindow.MakeLink("live://junction-minus-ex/" + i, _core.MakeHTMLImage("junction-stmz-32-16", 32, 16), _strtable.GetString("tooltip-junction-left-minus-ex")));
                  else buff.Print(HTMLWindow.MakeLink("live://junction-minus-ex/" + i, _core.MakeHTMLImage("junction-stmz-32-16", 32, 16), _strtable.GetString("tooltip-junction-right-minus-ex")));
                } else buff.Print(_core.MakeHTMLImage("junction-stmz-d-32-16", 32, 16));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                buff.Print(HTMLWindow.MakeLink("live://junction-settings/" + i, _core.MakeHTMLImage("junction-settings-16", 16), _strtable.GetString("tooltip-openjunctionsettings")));
              buff.Print(HTMLWindow.EndCell()); 
            buff.Print(HTMLWindow.EndRow());
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.StartCell("align=center"));
                if(dirisplus) buff.Print(_core.MakeHTMLImage("junction-direction-plus-16", 16));
                else if(dirisminus) buff.Print(_core.MakeHTMLImage("junction-direction-minus-16", 16));
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.StartCell());
                if(jncinf.type & zx_DSP_JunctionInfo.JNC_AUTOREVERT) {
                  if(jncinf.state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT)) buff.Print(_core.MakeHTMLImage("st_gr", 16));
                  else if(dirisplus) buff.Print(_core.MakeHTMLImage("st_w", 16));
                  else buff.Print(_core.MakeHTMLImage("st_r", 16));
                } else buff.Print(_core.MakeHTMLImage("empty", 16));
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.MakeCell(""));
              buff.Print(HTMLWindow.StartCell("colspan=7"));
                if(confirmmode) {
                  buff.Print("<nowrap>"); 
                  if((_resp_action == RESPACTION_FORCEPLUS and allowswicthex and !dirisplus) or (_resp_action == RESPACTION_FORCEMINUS and allowswicthex and !dirisminus)) {
                    if(_resp_action == RESPACTION_FORCEPLUS) buff.Print(_strtable.GetString("controller-confirm-forceplus"));
                    else buff.Print(_strtable.GetString("controller-confirm-forceminus"));
                    buff.Print("&nbsp;&nbsp;");
                    if(_resp_mode == RESPMODE_WAITCONFITM)buff.Print(HTMLWindow.MakeLink("live://responsible-accept/" + i, _core.MakeHTMLImage("responsible-accept-16", 16), _strtable.GetString("tooltip-responsible-accept")));
                    else buff.Print(_core.MakeHTMLImage("responsible-accept-d-16", 16));
                    buff.Print("&nbsp;");
                    buff.Print(HTMLWindow.MakeLink("live://responsible-cancel/" + i, _core.MakeHTMLImage("responsible-cancel-16", 16), _strtable.GetString("tooltip-responsible-cancel"))); 
                  } else _resp_mode = RESPMODE_NONE; 
                  buff.Print("</nowrap>"); 
                }
              buff.Print(HTMLWindow.EndCell());
            buff.Print(HTMLWindow.EndRow());
            buff.Print(HTMLWindow.StartRow());
              if(jncinf.slaveJunctions) {
                buff.Print(HTMLWindow.StartCell("colspan=10"));
                  buff.Print("<nowrap>"); 
                    title = jncinf.GetTitle();
                    if(title and title.size()) {
                      buff.Print(_strtable.GetString1("junction-number", title));
                      buff.Print("&nbsp;[" + HTMLWindow.MakeLink("live://cameratojunction/" + jncinf.jnc.GetGameObjectID().SerialiseToString(), jncinf.jnc.GetLocalisedName(), _strtable.GetString("tooltip-cameratojunction")) + "]");
                    } else buff.Print(HTMLWindow.MakeLink("live://cameratojunction/" + jncinf.jnc.GetGameObjectID().SerialiseToString(), jncinf.jnc.GetLocalisedName(), _strtable.GetString("tooltip-cameratojunction")));
                    buff.Print("&nbsp;&nbsp;&nbsp;»&nbsp;&nbsp;&nbsp;");                                   
                    buff.Print(GetJunctionStateString(jncinf));
                  buff.Print("</nowrap>"); 
                buff.Print(HTMLWindow.EndCell());
              } else buff.Print(HTMLWindow.MakeCell(GetJunctionStateString(jncinf), "colspan=10"));
            buff.Print(HTMLWindow.EndRow());
            if(jncinf.slaveJunctions) {
              int j, slavecount = jncinf.slaveJunctions.size();
              for (j = 0; j < slavecount; ++j) {
                title = jncinf.slaveJunctions[j].GetTitle();
                buff.Print(HTMLWindow.StartRow());
                  buff.Print(HTMLWindow.StartCell("colspan=10"));
                    buff.Print("<nowrap>"); 
                      if(title and title.size()) {
                        buff.Print(_strtable.GetString1("junction-number", title));
                        buff.Print("&nbsp;[" + HTMLWindow.MakeLink("live://cameratojunction/" + jncinf.slaveJunctions[j].jnc.GetGameObjectID().SerialiseToString(), jncinf.slaveJunctions[j].jnc.GetLocalisedName(), _strtable.GetString("tooltip-cameratojunction")) + "]");
                      } else buff.Print(HTMLWindow.MakeLink("live://cameratojunction/" + jncinf.slaveJunctions[j].jnc.GetGameObjectID().SerialiseToString(), jncinf.slaveJunctions[j].jnc.GetLocalisedName(), _strtable.GetString("tooltip-cameratojunction")));
                      buff.Print("&nbsp;&nbsp;&nbsp;»&nbsp;&nbsp;&nbsp;");                                   
                      buff.Print(GetJunctionStateString(jncinf.slaveJunctions[j]));
                    buff.Print("</nowrap>"); 
                  buff.Print(HTMLWindow.EndCell());
                buff.Print(HTMLWindow.EndRow());
              }
            }
          buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
    }
  }

  void BuildSignalControl(HTMLBuffer buff)
  {
    zx_DSP_Station station = _core.stations[_stationviewed];
    int i, count = station.signals.size();
    for(i = 0; i < count; ++i) {
      zxSignal sign = station.signals[i];
      bool isshunt = sign.Type & zxSignal.ST_SHUNT;
      bool availableshunt = !(sign.Type & zxSignal.ST_PERMOPENED) and sign.ex_sgn[zxIndication.STATE_W];
	    bool availableinvitation = sign.ex_sgn[zxIndication.STATE_RWb];
      bool confirmmode = _resp_mode != RESPMODE_NONE and _resp_action == RESPACTION_INVITATION and _resp_stationindex == _stationviewed and _resp_itemindex == i;
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
          buff.Print(HTMLWindow.StartTable("width=100%"));
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.StartCell());
                if(!isshunt and sign.train_open and sign.GetSignalState() == Signal.GREEN) buff.Print(_core.MakeHTMLImage("st_g", 16));
                else if(!isshunt and sign.train_open and sign.GetSignalState() == Signal.YELLOW) buff.Print(_core.MakeHTMLImage("st_y", 16));
                else if(!isshunt and sign.train_open) buff.Print(_core.MakeHTMLImage("st_re", 16));
                else if(!isshunt) buff.Print(_core.MakeHTMLImage("st_r", 16));
                else buff.Print(_core.MakeHTMLImage("empty", 16));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if (!availableshunt) buff.Print(_core.MakeHTMLImage("empty", 16));
                else if(sign.shunt_open) buff.Print(_core.MakeHTMLImage("st_w", 16));
                else if(isshunt) buff.Print(_core.MakeHTMLImage("st_b", 16));
                else buff.Print(_core.MakeHTMLImage("st_be", 16));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if (!availableinvitation) buff.Print(_core.MakeHTMLImage("empty", 16));
                else if(sign.prigl_open) buff.Print(_core.MakeHTMLImage("st_w", 16));
                else buff.Print(_core.MakeHTMLImage("st_gr", 16));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell("width=35%"));
                buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + sign.GetGameObjectID().SerialiseToString(), sign.privateName, _strtable.GetString("tooltip-cameratosignal")));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if (!availableshunt or sign.train_open or sign.prigl_open or confirmmode) buff.Print(_core.MakeHTMLImage("signal-white-disabled-32-16", 32, 16));
                else if(sign.shunt_open) buff.Print(HTMLWindow.MakeLink("live://closeshuntsignal/" + i, _core.MakeHTMLImage("signal-white-off-32-16", 32, 16), _strtable.GetString("tooltip-signalshuntclose"))); 
                else buff.Print(HTMLWindow.MakeLink("live://openshuntsignal/" + i, _core.MakeHTMLImage("signal-white-on-32-16", 32, 16), _strtable.GetString("tooltip-signalshuntopen")));  
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                if (!availableinvitation or sign.train_open or sign.shunt_open or confirmmode) buff.Print(_core.MakeHTMLImage("signal-invitation-disabled-16", 16));
                else if(sign.prigl_open) buff.Print(HTMLWindow.MakeLink("live://closeinvitationsignal/" + i, _core.MakeHTMLImage("signal-invitation-off-16", 16), _strtable.GetString("tooltip-signalinvitationclose")));
                else buff.Print(HTMLWindow.MakeLink("live://openinvitationsignal/" + i, _core.MakeHTMLImage("signal-invitation-on-16", 16), _strtable.GetString("tooltip-signalinvitationopen")));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell("width=65%"));
                if(confirmmode) {
                  if(!sign.train_open and !sign.shunt_open and !sign.prigl_open) {
                    buff.Print("&nbsp;&nbsp;"); 
                    buff.Print(_strtable.GetString("controller-confirm-invitation")); 
                    buff.Print("&nbsp;&nbsp;");
                    if(_resp_mode == RESPMODE_WAITCONFITM)buff.Print(HTMLWindow.MakeLink("live://responsible-accept/" + i, _core.MakeHTMLImage("responsible-accept-16", 16), _strtable.GetString("tooltip-responsible-accept")));
                    else buff.Print(_core.MakeHTMLImage("responsible-accept-d-16", 16));
                    buff.Print("&nbsp;");
                    buff.Print(HTMLWindow.MakeLink("live://responsible-cancel/" + i, _core.MakeHTMLImage("responsible-cancel-16", 16), _strtable.GetString("tooltip-responsible-cancel"))); 
                  } else _resp_mode = RESPMODE_NONE; 
                }
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell());
                buff.Print(HTMLWindow.MakeLink("live://signalsettings/" + i, _core.MakeHTMLImage("signal-settings-16", 16), _strtable.GetString("tooltip-opensignalsettings")));
              buff.Print(HTMLWindow.EndCell()); 
            buff.Print(HTMLWindow.EndRow());
          buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
    }
  }               

  //Устаревший интерфейс управления перегонами для обратной совместимости.
  void BuildSpanControlLegacy(HTMLBuffer buff)
  {
    zx_DSP_Station station = _core.stations[_stationviewed];
    int i, count = station.signals.size();
    for(i = 0; i < count; ++i) {
      zxSignal sign = station.signals[i];
      if(!(sign.Type & zxSignal.ST_IN) or !sign.span_soup or !sign.span_soup.GetNamedTagAsBool("Inited")) continue;   //Если светофор не входной, или на входном не инициализтрован перегон, то пропускаем его
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
          buff.Print(HTMLWindow.StartTable("width=100%"));
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.StartCell());
                if(sign.wrong_dir) buff.Print(_core.MakeHTMLImage("st_g", 32));
                else buff.Print(_core.MakeHTMLImage("st_r", 32));
              buff.Print(HTMLWindow.EndCell()); 
              buff.Print(HTMLWindow.StartCell("width=100%"));
                buff.Print(HTMLWindow.StartTable("width=100%"));
                  buff.Print(HTMLWindow.StartRow());
                    buff.Print(HTMLWindow.StartCell("width=25%"));
                      buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + sign.GetGameObjectID().SerialiseToString(), sign.privateName, _strtable.GetString("tooltip-cameratosignal")));
                    buff.Print(HTMLWindow.EndCell()); 
                    buff.Print(HTMLWindow.StartCell());
                      if(sign.wrong_dir) buff.Print(_core.MakeHTMLImage("rightarrow", 32, 16));
                      else buff.Print(_core.MakeHTMLImage("leftarrow", 32, 16));
                    buff.Print(HTMLWindow.EndCell()); 
                    buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("empty", 48, 16)));
                    buff.Print(HTMLWindow.StartCell("width=75%"));
                      buff.Print(sign.span_soup.GetNamedTag("end_sign_n") + "@" + sign.span_soup.GetNamedTag("end_sign_s"));
                    buff.Print(HTMLWindow.EndCell()); 
                    buff.Print(HTMLWindow.StartCell());
                      buff.Print("<nowrap>");
                      if(!sign.wrong_dir) buff.Print(HTMLWindow.MakeLink("live://span-turn/" + i, _strtable.GetString("string-turnspan"), _strtable.GetString("tooltip-turnspan")));
                      else buff.Print(HTMLWindow.MakeFontColor(_strtable.GetString("string-turnspan"), _core.StyleDisabledLabel)); 
                      buff.Print("</nowrap>");
                    buff.Print(HTMLWindow.EndCell()); 
                  buff.Print(HTMLWindow.EndRow());
                  buff.Print(HTMLWindow.StartRow());
                    buff.Print(HTMLWindow.StartCell("colspan=5"));
                      if(station.spanErrorSignalId and station.spanErrorSignalId.DoesMatch(sign) and station.spanErrorMessage != "" and ((station.spanErrorMessageViewTime >= 0 and
                      station.spanErrorMessageViewTime + 60.0 >= World.GetTimeElapsed()) or station.spanErrorMessageViewTime < 0)){
                        if(station.spanErrorMessageViewTime < 0) station.spanErrorMessageViewTime = World.GetTimeElapsed();
                        buff.Print(HTMLWindow.MakeFontColor(station.spanErrorMessage, _core.StyleErrorText));
                      }else if(station.spanErrorMessage != "" and station.spanErrorMessageViewTime >= 0) {
                        station.spanErrorMessage = ""; 
                        station.spanErrorMessageViewTime = -1;
                        station.spanErrorSignalId = null;
                      }
                    buff.Print(HTMLWindow.EndCell()); 
                  buff.Print(HTMLWindow.EndRow());
                buff.Print(HTMLWindow.EndTable());
              buff.Print(HTMLWindow.EndCell()); 
            buff.Print(HTMLWindow.EndRow());
          buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
    }
  }
  
  void BuildSpanControl(HTMLBuffer buff)
  {
    if (!_core.spans) {                 //Заглужка для обратной совместимости
      BuildSpanControlLegacy(buff);
      return;
    }

    zx_DSP_Station station = _core.stations[_stationviewed];
    zx_DSP_MakeHTMLContext context = new zx_DSP_MakeHTMLContext().Init(_core, _strtable, station);
    if (_resp_object == zx_DSP_Station.CONTROLTAB_SPAN) context.SetResponsible(_resp_itemindex, _resp_mode, _resp_spanaction);
    int i, count = station.spans.size(), viewed;
    for(i = 0; i < count; ++i) {
      zx_DSP_SpanBase span = station.spans[i];
      if (!span.CanMakeUIHTML()) continue;        //Если перегон не имеет UI, переходим к следующуему
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
          span.MakeUIHTML(buff, context);
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
      ++viewed;
    }
    if (!viewed) {
      buff.Print(HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.StartRow() + 
      HTMLWindow.StartCell("align=center") + _strtable.GetString("message-noviewedspan") + HTMLWindow.EndCell() + HTMLWindow.EndRow());
    }
  }
  
  
  void BuildShuntControl(HTMLBuffer buff)
  {
    zx_DSP_Station station = _core.stations[_stationviewed];
    int i, count = station.signals.size();
    buff.Print(HTMLWindow.StartRow());
    buff.Print(HTMLWindow.StartCell("width=180 bgcolor=#" + _core.StyleBlockBackColor));
    buff.Print(HTMLWindow.StartTable("width=100%"));
    bool canstartshuntpath = station.startShuntSignalIndex >= 0 and (station.activeShuntPaths or (!(station.signals[station.startShuntSignalIndex].Type & zxSignal.ST_PERMOPENED) and
                             station.signals[station.startShuntSignalIndex].ex_sgn[zxIndication.STATE_W]));
    for(i = 0; i < count; ++i){
      zxSignal sign = station.signals[i];
      zx_DSP_SignalInfo signalInfo = _core.GetSignalInfo(sign);
      bool isshunt = sign.Type & zxSignal.ST_SHUNT; 
      bool availableshunt = !(sign.Type & zxSignal.ST_PERMOPENED) and sign.ex_sgn[zxIndication.STATE_W];
      if (station.activeShuntPaths) {
        availableshunt = signalInfo and signalInfo.shuntPathsFrom and signalInfo.shuntPathsFrom.size();
      }
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell());
          if(sign.shunt_open) buff.Print(_core.MakeHTMLImage("st_w", 16));
          else if(isshunt) buff.Print(_core.MakeHTMLImage("st_b", 16));
          else if(sign.train_open and sign.GetSignalState() == Signal.GREEN) buff.Print(_core.MakeHTMLImage("st_g", 16));
          else if(sign.train_open and sign.GetSignalState() == Signal.YELLOW) buff.Print(_core.MakeHTMLImage("st_y", 16));
          else if(sign.train_open) buff.Print(_core.MakeHTMLImage("st_re", 16));
          else buff.Print(_core.MakeHTMLImage("st_r", 16));
        buff.Print(HTMLWindow.EndCell());
        if(availableshunt){
          if(i == station.startShuntSignalIndex) buff.Print(HTMLWindow.StartCell("width=50% bgcolor=#" + _core.StyleSelectorBackColor) + sign.privateName + HTMLWindow.EndCell());
          else if(signalInfo and signalInfo.activePathFrom) buff.Print(HTMLWindow.StartCell("width=50%") + HTMLWindow.MakeFontColor(sign.privateName, _core.StyleDisabledLabel) + HTMLWindow.EndCell());
          else buff.Print(HTMLWindow.StartCell("width=50%") + HTMLWindow.MakeLink("live://shunt-selectstart/" + i, sign.privateName, _strtable.GetString("tooltip-selectstartsignal")) + HTMLWindow.EndCell());
        } else buff.Print(HTMLWindow.StartCell("width=50%") + HTMLWindow.MakeFontColor(sign.privateName, _core.StyleDisabledLabel) + HTMLWindow.EndCell()); 
        if(i == station.endShuntSignalIndex) buff.Print(HTMLWindow.StartCell("width=50% bgcolor=#" + _core.StyleSelectorBackColor) + sign.privateName + HTMLWindow.EndCell());
        else if(!canstartshuntpath or station.startShuntSignalIndex == i or (station.activeShuntPaths and (!signalInfo or !((signalInfo.shuntPathsBehind and signalInfo.shuntPathsBehind.size()) or (signalInfo.shuntPathsTo and signalInfo.shuntPathsTo.size())) or signalInfo.activePathBackward or signalInfo.activePathTo))) buff.Print(HTMLWindow.StartCell("width=50%") + HTMLWindow.MakeFontColor(sign.privateName, _core.StyleDisabledLabel) + HTMLWindow.EndCell());
        else buff.Print(HTMLWindow.StartCell("width=50%") + HTMLWindow.MakeLink("live://shunt-selectend/" + i, sign.privateName, _strtable.GetString("tooltip-selectendsignal")) + HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
    }
    buff.Print(HTMLWindow.EndTable());
    buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.StartCell());
    buff.Print(HTMLWindow.StartTable("width=100%"));
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("bgcolor=#" + _core.StyleBlockBackColor));
          buff.Print(HTMLWindow.StartTable("width=100%"));
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.StartCell("width=100%"));
                buff.Print(_strtable.GetString("string-buildshuntpathfrom") + "&nbsp;");
                buff.Print(HTMLWindow.StartFontColor(_core.StyleWarnText));
                  if(canstartshuntpath) buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + station.signals[station.startShuntSignalIndex].GetGameObjectID().SerialiseToString(), 
                                                   station.signals[station.startShuntSignalIndex].privateName, _strtable.GetString("tooltip-cameratosignal"))); 
                  else buff.Print("?");
                buff.Print(HTMLWindow.EndFontColor());
                buff.Print("&nbsp;" + _strtable.GetString("string-buildshuntpathto") + "&nbsp;");
                buff.Print(HTMLWindow.StartFontColor(_core.StyleWarnText));
                  if(canstartshuntpath and station.endShuntSignalIndex >= 0) buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + station.signals[station.endShuntSignalIndex].GetGameObjectID().SerialiseToString(), 
                                                                                        station.signals[station.endShuntSignalIndex].privateName, _strtable.GetString("tooltip-cameratosignal"))); 
                  else buff.Print("?");
                buff.Print(HTMLWindow.EndFontColor());
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.StartCell());
                if(canstartshuntpath and station.endShuntSignalIndex >= 0) buff.Print(HTMLWindow.MakeLink("live://shunt-free", _core.MakeHTMLImage("shunt-free-32-16", 32, 16), _strtable.GetString("tooltip-buildshuntpath-free")));
                else buff.Print(_core.MakeHTMLImage("shunt-free-d-32-16", 32, 16));
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.StartCell());
                if(canstartshuntpath and station.endShuntSignalIndex >= 0) buff.Print(HTMLWindow.MakeLink("live://shunt-train", _core.MakeHTMLImage("shunt-train-32-16", 32, 16), _strtable.GetString("tooltip-buildshuntpath-train")));
                else buff.Print(_core.MakeHTMLImage("shunt-train-d-32-16", 32, 16));
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.StartCell());
                if(canstartshuntpath or station.endShuntSignalIndex >= 0) buff.Print(HTMLWindow.MakeLink("live://shunt-reset", _core.MakeHTMLImage("reset-32-16", 32, 16), _strtable.GetString("tooltip-buildshuntpath-train")));
                else buff.Print(_core.MakeHTMLImage("reset-d-32-16", 32, 16));
              buff.Print(HTMLWindow.EndCell());
            buff.Print(HTMLWindow.EndRow());
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.MakeCell("", "colspan=4"));
            buff.Print(HTMLWindow.EndRow());
            if(station.shuntPathErrorMessage != "" and ((station.shuntPathErrorMessageViewTime >= 0 and
            station.shuntPathErrorMessageViewTime + 20.0 >= World.GetTimeElapsed()) or station.shuntPathErrorMessageViewTime < 0)){
              if(station.shuntPathErrorMessageViewTime < 0) station.shuntPathErrorMessageViewTime = World.GetTimeElapsed();
              buff.Print(HTMLWindow.StartRow() + HTMLWindow.StartCell("colspan=4 align=center") + HTMLWindow.MakeFontColor(station.shuntPathErrorMessage, _core.StyleErrorText) +
                         HTMLWindow.EndCell() + HTMLWindow.EndRow());
            }else if(station.shuntPathErrorMessage != "" and station.shuntPathErrorMessageViewTime >= 0){
              station.shuntPathErrorMessage = ""; 
              station.shuntPathErrorMessageViewTime = -1;
            }
          buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell());
          buff.Print(HTMLWindow.StartTable("width=100%"));
          zx_DSP_ShuntPath[] paths = station.activeShuntPaths;
          if (!paths) {
            paths = station.shuntPaths;
          }
          count = paths.size();
          for(i = 0; i < count; ++i){
            zx_DSP_ShuntPath path = paths[i];
            bool haschain = path.chainname and (path.prevPart or path.nextPart); 
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.StartCell("bgcolor=#" + _core.StyleBlockBackColor));
                buff.Print(HTMLWindow.StartTable("width=100%"));
                  buff.Print(HTMLWindow.StartRow());
                    buff.Print(HTMLWindow.StartCell());
                      if(path.state & zx_DSP_ShuntPath.STATE_BUSY) buff.Print(_core.MakeHTMLImage("st_b", 16));
                      else buff.Print(_core.MakeHTMLImage("st_w", 16));
                    buff.Print(HTMLWindow.EndCell());
                    buff.Print(HTMLWindow.StartCell());
                      buff.Print(_strtable.GetString("string-pathfrom"));
                      buff.Print("&nbsp;");
                      string startSignStr = HTMLWindow.MakeLink("live://cameratosignal/" + path.startSign.GetGameObjectID().SerialiseToString(), path.startSign.privateName, _strtable.GetString("tooltip-cameratosignal"));
                      if (path.groupSign) {
                        startSignStr = HTMLWindow.MakeLink("live://cameratosignal/" + path.groupSign.GetGameObjectID().SerialiseToString(), path.groupSign.privateName, _strtable.GetString("tooltip-cameratosignal")) + "&nbsp;(" + startSignStr + ")";
                      }
                      buff.Print(startSignStr);
                      if (path.backwardEndSign) {
                        buff.Print("&nbsp;");
                        buff.Print(_strtable.GetString("string-pathbehind"));
                        buff.Print("&nbsp;");
                        buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + path.backwardEndSign.GetGameObjectID().SerialiseToString(), path.backwardEndSign.privateName, _strtable.GetString("tooltip-cameratosignal")));
                      }
                      if (path.endSign) {
                        buff.Print("&nbsp;");
                        buff.Print(_strtable.GetString("string-pathto"));
                        buff.Print("&nbsp;");
                        string endName = path.endSign.privateName;
                        if (path.startSign.stationName != path.endSign.stationName) {
                          endName = endName + "@" + path.endSign.stationName;
                        }
                        buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + path.endSign.GetGameObjectID().SerialiseToString(), endName, _strtable.GetString("tooltip-cameratosignal")));
                      }
                    buff.Print(HTMLWindow.EndCell());
                    // if (path.name and path.name.size()) {
                    //   buff.Print(HTMLWindow.MakeCell(path.name, "width=50%"));
                    // }
                    if (haschain) buff.Print(HTMLWindow.MakeCell(path.chainname, "width=50%"));
                    else buff.Print(HTMLWindow.MakeCell("", "width=50%"));
                    string lnk;
                    buff.Print(HTMLWindow.StartCell()); 
                      if (haschain) {
                        if (path.id < 0) {
                          lnk = "live://shunt-cancel-chain/" + path.index;
                        }
                        else {
                          lnk = "live://shund-disassemble-chain/" + path.id;
                        }
                        buff.Print(HTMLWindow.MakeLink(lnk, _core.MakeHTMLImage("shunt-disassemble-chain-16", 16), _strtable.GetString("tooltip-dissasemblyshuntpathchain")));
                      }
                      else buff.Print(_core.MakeHTMLImage("shunt-disassemble-chain-d-16", 16));
                    buff.Print(HTMLWindow.EndCell());
                    buff.Print(HTMLWindow.StartCell()); 
                      if (path.id < 0) {
                        lnk = "live://shunt-cancel/" + path.index;
                      }
                      else {
                        lnk = "live://shund-disassemble/" + path.id;
                      }
                      buff.Print(HTMLWindow.MakeLink(lnk, _core.MakeHTMLImage("shunt-disassemble-16", 16), _strtable.GetString("tooltip-dissasemblyshuntpath")));
                    buff.Print(HTMLWindow.EndCell());
                  buff.Print(HTMLWindow.EndRow());
                buff.Print(HTMLWindow.EndTable());
              buff.Print(HTMLWindow.EndCell());
            buff.Print(HTMLWindow.EndRow());
          }
          buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
    buff.Print(HTMLWindow.EndTable());
    buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());
  }

  void BuildQueueControl(HTMLBuffer buff)
  {
    zx_DSP_Station station = _core.stations[_stationviewed];
    int i, count = station.pathQueue.size();
    if (count) {
      for(i = 0; i < count; ++i) {
        zx_DSP_PathQueueItem item = station.pathQueue[i];
        zx_DSP_Path path = station.paths[item.index];
        bool isfirst = i == 0 or item.group != station.pathQueue[i - 1].group;
        bool islast = i == count - 1 or item.group != station.pathQueue[i + 1].group;   
        if (isfirst) {
          string groupname;
          if (item.group > 0) groupname = _strtable.GetString1("pathqueue-group", item.group);
          else if (item.group == -1) groupname = _strtable.GetString("pathqueue-group-any");
          else groupname = _strtable.GetString("pathqueue-group-main");
          buff.Print(HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.StartRow() + 
          HTMLWindow.StartCell("align=center") + groupname + HTMLWindow.EndCell() + HTMLWindow.EndRow());
        }
        string variantname = item.variant;
        if(item.variant >= 0 and path.variants[item.variant].name != "") variantname = path.variants[item.variant].name;  
        




        buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("bgcolor=#" + _core.StyleBlockBackColor));
        buff.Print(HTMLWindow.StartTable("width=100%"));
          buff.Print(HTMLWindow.StartRow());
            buff.Print(HTMLWindow.MakeCell(_strtable.GetString1("pathqueue-number", item.id), "width=40"));
            buff.Print(HTMLWindow.StartCell(""));
              buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + path.startSign.GetGameObjectID().SerialiseToString(), path.startSign.privateName, _strtable.GetString("tooltip-cameratosignal")));
              if (path.backwardEndSign) {
                buff.Print("&nbsp;");
                buff.Print(_strtable.GetString("string-pathbehind"));
                buff.Print("&nbsp;");
                buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + path.backwardEndSign.GetGameObjectID().SerialiseToString(), path.backwardEndSign.privateName, _strtable.GetString("tooltip-cameratosignal")));
              }
              if (path.endSign) {
                buff.Print("&nbsp;");
                buff.Print(_strtable.GetString("string-pathto"));
                buff.Print("&nbsp;");
                string endName = path.endSign.privateName;
                if (path.startSign.stationName != path.endSign.stationName) {
                  endName = endName + "@" + path.endSign.stationName;
                }
                buff.Print(HTMLWindow.MakeLink("live://cameratosignal/" + path.endSign.GetGameObjectID().SerialiseToString(), endName, _strtable.GetString("tooltip-cameratosignal")));
              }
              if(path.startSign.stationName != path.endSign.stationName) {
                buff.Print("&nbsp;");
                buff.Print(_strtable.GetString("string-pathspan"));
              }
            buff.Print(HTMLWindow.EndCell());
            if(isfirst or item.group == -1) buff.Print(HTMLWindow.MakeCell(_strtable.GetString1("pathqueue-attempt", item.attempt), "width=100"));
            else buff.Print(HTMLWindow.MakeSpacerCell(100));
            buff.Print(HTMLWindow.StartCell("width=0"));
              if (!isfirst) buff.Print(HTMLWindow.MakeLink("live://queue-moveup/" + item.id, _core.MakeHTMLImage("moveup-16", 16), _strtable.GetString("tooltip-queueitemup")));
              else buff.Print(_core.MakeHTMLImage("moveup-d-16", 16));
            buff.Print(HTMLWindow.EndCell());
            buff.Print(HTMLWindow.StartCell("width=36 rowspan=2"));
              buff.Print(HTMLWindow.MakeLink("live://queue-exclude/" + item.id, _core.MakeHTMLImage("delete-36", 36), _strtable.GetString("tooltip-queueitemexclude")));
            buff.Print(HTMLWindow.EndCell());
          buff.Print(HTMLWindow.EndRow());
          buff.Print(HTMLWindow.StartRow());
            buff.Print(HTMLWindow.MakeSpacerCell(40));
            if (item.variant > 0) buff.Print(HTMLWindow.MakeCell(_strtable.GetString1("pathqueue-variant", variantname), "colspan=2"));
            else if (!item.variant) buff.Print(HTMLWindow.MakeCell(_strtable.GetString("pathqueue-mainvariant"), "colspan=2"));
            else buff.Print(HTMLWindow.MakeCell(_strtable.GetString("pathqueue-autovariant"), "colspan=2"));
            buff.Print(HTMLWindow.StartCell("width=0"));
              if (!islast) buff.Print(HTMLWindow.MakeLink("live://queue-movedown/" + item.id, _core.MakeHTMLImage("movedown-16", 16), _strtable.GetString("tooltip-queueitemdown")));
              else buff.Print(_core.MakeHTMLImage("movedown-d-16", 16));
            buff.Print(HTMLWindow.EndCell());
          buff.Print(HTMLWindow.EndRow());
          if ((isfirst or item.group == -1) and item.message and item.message != "") {
            buff.Print(HTMLWindow.StartRow());
              buff.Print(HTMLWindow.StartCell("align=right"));
                buff.Print(_core.MakeHTMLImage("status-level-error-16", 16));
              buff.Print(HTMLWindow.EndCell());
            
              //buff.Print(HTMLWindow.MakeSpacerCell(40));
              buff.Print(HTMLWindow.StartCell("colspan=2"));
                buff.Print(HTMLWindow.MakeFontColor(item.message, _core.StyleErrorText));                               
              buff.Print(HTMLWindow.EndCell());
              buff.Print(HTMLWindow.MakeCell("", "colspan=2 bgcolor=000045"));
            buff.Print(HTMLWindow.EndRow());
          }
        buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.EndRow());
      }
    } else {
      buff.Print(HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%") + HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.StartRow() + 
      HTMLWindow.StartCell("align=center") + _strtable.GetString("message-pathqueueempty") + HTMLWindow.EndCell() + HTMLWindow.EndRow());
    }
  }

  void OnLinkClick(string action, string argumentX, string argumentY, string argumentZ)
  {
    if(action == "viewstation") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed != index and index >= 0 and index < _core.stations.size()) {
        _stationviewed = index;
        Update();
      }
    } else if(action == "selecttab") {
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        int newtab = 1 << Str.ToInt(argumentX);
        if(_core.stations[_stationviewed].controllerFormTab != newtab) {
          _core.stations[_stationviewed].controllerFormTab = newtab;
          UpdateControlPanel();
        }
      }
    } else if((action == "cameratosignal" or action == "cameratojunction") and argumentX != null) {
      GameObjectID objectid = Router.SerialiseGameObjectIDFromString(argumentX);
      if (objectid) {
        MapObject mapobj = cast<MapObject>Router.GetGameObject(objectid);
        if(mapobj) {
          World.SetCamera(mapobj);
          _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
          return;          
        } 
      } 
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
      
    } else if (action == "goro-station-tab") {
      int stationindex = Str.ToInt(argumentX);
      int newtab = Str.ToInt(argumentY);
      if (_core.stations and stationindex >= 0 and stationindex < _core.stations.size()) {
        if(_stationviewed != stationindex or _core.stations[_stationviewed].controllerFormTab != newtab) {
          _stationviewed = stationindex; 
          _core.stations[_stationviewed].controllerFormTab = newtab;
          _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
          Update();
          return;
        }
      }
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "selectsignal") {
      int stationindex = _stationviewed;
      if(argumentY != "") stationindex = Str.ToInt(argumentY);  
      if(_core.stations and stationindex >= 0 and stationindex < _core.stations.size()) {
        GameObjectID objectid = Router.SerialiseGameObjectIDFromString(argumentX);
        if (objectid) {
          zxSignal signal = cast<zxSignal>Router.GetGameObject(objectid);
          if(signal) {
            if(_core.stations[stationindex].selectSign != signal or _stationviewed != stationindex) 
              _core.stations[stationindex].selectSign = signal;
            else _core.stations[stationindex].selectSign = null;
            if(_stationviewed != stationindex) {
              _stationviewed = stationindex;
              _core.stations[_stationviewed].controllerFormTab = zx_DSP_Station.CONTROLTAB_PATH;
              Update();
            } else UpdateControlPanel();
            _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
            return;          
          } 
        }
      } 
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-build") { 
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        _core.BuildPath(_stationviewed, index, -1, true, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-buildvariant" and argumentY) { 
      int index = Str.ToInt(argumentX);
      int varindex = Str.ToInt(argumentY);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size() and (varindex >= 0 and varindex < _core.stations[_stationviewed].paths[index].variants.size() or varindex == -1)) {
        _core.BuildPath(_stationviewed, index, varindex, true, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-reopen") { 
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        _core.ReopenSignalForPath(_stationviewed, index, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-buildvariant" and !argumentY) {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        zx_DSP_Station station = _core.stations[_stationviewed];
        zx_DSP_Path path = station.paths[index];
        Menu menu = Constructors.NewMenu();
	      menu.AddItem(_strtable.GetString("menu-onlymainvariant"), _browser, "Browser-URL", "live://" + action + "/" + index + "/0");
	      int i, count = path.variants.size();
        if(count > 1) {
          menu.AddItem(_strtable.GetString("menu-autoselectvariant"), _browser, "Browser-URL", "live://" + action + "/" + index + "/-1");
          menu.AddSeperator();
          for (i = 0; i < count; ++i) {
            string itemname = "#" + i;
            if (path.variants[i].name.size() and path.variants[i].name != "") itemname = itemname + " — " + path.variants[i].name;
            else if(i == 0) itemname = itemname + " — " + _strtable.GetString("string-variant-main");
            else itemname = itemname + " — " + _strtable.GetString("string-variant");
            menu.AddItem(itemname, _browser, "Browser-URL", "live://" + action + "/" + index + "/" + i);
          }
        }
        _browser.PopupMenu(menu);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-cancel") { 
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        _core.DisassemblePath(_stationviewed, index, false, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-forcecancel") { 
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        StartResponsibleAction(index, RESPACTION_FORCEPATHCANCEL);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-queuevariant" and !argumentY) {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        zx_DSP_Station station = _core.stations[_stationviewed];
        zx_DSP_Path path = station.paths[index];
        if (path.variants.size() > 1) {
          Menu menu = Constructors.NewMenu();
          menu.AddSubmenu(_strtable.GetString("menu-onlymainvariant") + " ➤", CreateQueueMenu(index, 0));
          int i, count = path.variants.size();
          if(count > 1) {
            menu.AddSubmenu(_strtable.GetString("menu-autoselectvariant") + " ➤", CreateQueueMenu(index, -1));
            menu.AddSeperator();
            for (i = 0; i < count; ++i) {
              string itemname = "#" + i;
              if (path.variants[i].name.size() and path.variants[i].name != "") itemname = itemname + " — " + path.variants[i].name;
              else if(i == 0) itemname = itemname + " — " + _strtable.GetString("string-variant-main");
              else itemname = itemname + " — " + _strtable.GetString("string-variant");
              menu.AddSubmenu(itemname + " ➤", CreateQueueMenu(index, i));
            }
          }
          _browser.PopupMenu(menu);
        } else _browser.PopupMenu(CreateQueueMenu(index, 0)); 
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-queue") { 
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        _core.PathAddQueue(_stationviewed, index, -1, 0, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-queuevariant" and argumentY) { 
      int index = Str.ToInt(argumentX);
      int varindex = Str.ToInt(argumentY);
      int group = Str.ToInt(argumentZ);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size() and (varindex >= 0 and varindex < _core.stations[_stationviewed].paths[index].variants.size() or varindex == -1) and
      group >= -1 and group <= 10) {
        _core.PathAddQueue(_stationviewed, index, varindex, group, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-autoon" or action == "path-autooff") { 
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        _core.SetAutoBuildPath(_stationviewed, index, action == "path-autoon", _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "path-xmodeon" or action == "path-xmodeoff") { 
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].paths and 
      index >= 0 and index < _core.stations[_stationviewed].paths.size()) {
        //_core.SetPathXMode(_stationviewed, index, action == "path-xmodeon", _core.GetLocalPlayerName());
        _core.SetPathXMode(_stationviewed, index, action == "path-xmodeon",  _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    
    
    
 


    
    
    
    

                             
                              




    
    
    } else if(action == "junction-plus") {
      int index = Str.ToInt(argumentX);
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].jncs and 
      index >= 0 and index < _core.stations[_stationviewed].jncs.size()) {
        zx_DSP_JunctionInfo jncinfo = _core.stations[_stationviewed].jncs[index]; 
		    _core.ManualJunctionSwitch(jncinfo, true, false, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "junction-minus") {
      int index = Str.ToInt(argumentX);
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].jncs and 
      index >= 0 and index < _core.stations[_stationviewed].jncs.size()) {
        zx_DSP_JunctionInfo jncinfo = _core.stations[_stationviewed].jncs[index]; 
		    _core.ManualJunctionSwitch(jncinfo, false, false, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "junction-plus-ex") {
      int index = Str.ToInt(argumentX);
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].jncs and 
      index >= 0 and index < _core.stations[_stationviewed].jncs.size()) {
        StartResponsibleAction(index, RESPACTION_FORCEPLUS);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "junction-minus-ex") {
      int index = Str.ToInt(argumentX);
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].jncs and 
      index >= 0 and index < _core.stations[_stationviewed].jncs.size()) {
        StartResponsibleAction(index, RESPACTION_FORCEMINUS);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "junction-lock") {
      int index = Str.ToInt(argumentX);
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].jncs and 
      index >= 0 and index < _core.stations[_stationviewed].jncs.size()) {
        zx_DSP_JunctionInfo jncinfo = _core.stations[_stationviewed].jncs[index]; 
		    _core.ManualJunctionLock(jncinfo, true, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "junction-unlock") {
      int index = Str.ToInt(argumentX);
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].jncs and 
      index >= 0 and index < _core.stations[_stationviewed].jncs.size()) {
        zx_DSP_JunctionInfo jncinfo = _core.stations[_stationviewed].jncs[index]; 
		    _core.ManualJunctionLock(jncinfo, false, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "junction-settings") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].jncs and 
      index >= 0 and index < _core.stations[_stationviewed].jncs.size()) {
        zx_DSP_JunctionInfo jncinfo = _core.stations[_stationviewed].jncs[index];
        if (jncinfo.slaveJunctions and jncinfo.slaveJunctions.size()) {
          if (!argumentY) {
            Menu menu = Constructors.NewMenu();
            if (jncinfo.GetTitle().size()) menu.AddItem(_strtable.GetString2("menu-junctionsettings-number", jncinfo.GetTitle(), jncinfo.jnc.GetLocalisedName()), _browser, "Browser-URL", "live://junction-settings/" + index + "/-1");
            else menu.AddItem(_strtable.GetString1("menu-junctionsettings", jncinfo.jnc.GetLocalisedName()), _browser, "Browser-URL", "live://junction-settings/" + index + "/-1");
	          int i, count = jncinfo.slaveJunctions.size();
            for (i = 0; i < count; ++i) {
              zx_DSP_JunctionInfo slvjncinfo = jncinfo.slaveJunctions[i]; 
              if (slvjncinfo.GetTitle().size()) menu.AddItem(_strtable.GetString2("menu-junctionsettings-number", slvjncinfo.GetTitle(), slvjncinfo.jnc.GetLocalisedName()), _browser, "Browser-URL", "live://junction-settings/" + index + "/" + i);
              else menu.AddItem(_strtable.GetString1("menu-junctionsettings", slvjncinfo.jnc.GetLocalisedName()), _browser, "Browser-URL", "live://junction-settings/" + index + "/" + i);
            }
            _browser.PopupMenu(menu);
          } else {
            int slaveindex = Str.ToInt(argumentY);
            if (slaveindex >= 0) jncinfo = jncinfo.slaveJunctions[slaveindex];
            jncinfo.jnc.PostMessage(jncinfo.jnc, "MapObject", "View-Details", 0.0);
          } 
        } else jncinfo.jnc.PostMessage(jncinfo.jnc, "MapObject", "View-Details", 0.0);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
   
  
  
  
  
  
  
  
  
  
  
  
  


      
    } else if(action == "openshuntsignal") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].signals and 
      index >= 0 and index < _core.stations[_stationviewed].signals.size()) {
        zxSignal signal = _core.stations[_stationviewed].signals[index];
        _core.SetSignalState(_stationviewed, index, "ShuntMode.true", _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "closeshuntsignal") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].signals and 
      index >= 0 and index < _core.stations[_stationviewed].signals.size()) {
        zxSignal signal = _core.stations[_stationviewed].signals[index];
        _core.SetSignalState(_stationviewed, index, "ShuntMode.false", _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "openinvitationsignal") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].signals and 
      index >= 0 and index < _core.stations[_stationviewed].signals.size()) {
        StartResponsibleAction(index, RESPACTION_INVITATION);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "closeinvitationsignal") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].signals and 
      index >= 0 and index < _core.stations[_stationviewed].signals.size()) {
        zxSignal signal = _core.stations[_stationviewed].signals[index];
        _core.SetSignalState(_stationviewed, index, "PriglMode.false", _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "signalsettings") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].signals and 
      index >= 0 and index < _core.stations[_stationviewed].signals.size()) {
        zxSignal signal = _core.stations[_stationviewed].signals[index];
        signal.PostMessage(signal, "MapObject", "View-Details", 0.0);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "span-turn") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].signals and 
      index >= 0 and index < _core.stations[_stationviewed].signals.size()) {
        zxSignal signal = _core.stations[_stationviewed].signals[index];
        if(signal.Type & zxSignal.ST_IN and signal.span_soup and signal.span_soup.GetNamedTagAsBool("Inited")) {
          _core.TurnSpan(_stationviewed, index, _core.GetLocalPlayerName());
          _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
        } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "shunt-selectstart") {
      int index = Str.ToInt(argumentX);
      zx_DSP_SignalInfo signalInfo;
      if (
        _core.stations
        and _stationviewed >= 0
        and _stationviewed < _core.stations.size()
        and _core.stations[_stationviewed].signals
        and index >= 0
        and index < _core.stations[_stationviewed].signals.size()
        and (
          (
            !_core.stations[_stationviewed].activeShuntPaths
            and !(_core.stations[_stationviewed].signals[index].Type & zxSignal.ST_PERMOPENED)
            and _core.stations[_stationviewed].signals[index].ex_sgn[zxIndication.STATE_W]
          )
          or (
            (signalInfo = _core.GetSignalInfo(_core.stations[_stationviewed].signals[index]))
            and signalInfo.shuntPathsFrom
            and signalInfo.shuntPathsFrom.size()
          )
        )
      ) {
        _core.stations[_stationviewed].startShuntSignalIndex = index;
        _core.stations[_stationviewed].endShuntSignalIndex = -1;
        UpdateControlPanel();
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "shunt-selectend") {
      int index = Str.ToInt(argumentX);
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _core.stations[_stationviewed].signals and 
      index >= 0 and index < _core.stations[_stationviewed].signals.size() and _core.stations[_stationviewed].startShuntSignalIndex >= 0) {
        _core.stations[_stationviewed].endShuntSignalIndex = index;
        UpdateControlPanel();
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "shunt-free" or action == "shunt-train") {
      zx_DSP_Station station;
      zx_DSP_SignalInfo signalInfo;
      if (
        _core.stations
        and _stationviewed >= 0
        and _stationviewed < _core.stations.size()
        and (station = _core.stations[_stationviewed])
        and station.signals
        and station.startShuntSignalIndex >= 0
        and station.startShuntSignalIndex < station.signals.size()
        and station.endShuntSignalIndex >= 0
        and station.endShuntSignalIndex < station.signals.size()
        and (
          (
            !_core.stations[_stationviewed].activeShuntPaths
            and !(station.signals[station.startShuntSignalIndex].Type & zxSignal.ST_PERMOPENED)
            and station.signals[station.startShuntSignalIndex].ex_sgn[zxIndication.STATE_W]
          )
          or (
            (signalInfo = _core.GetSignalInfo(station.signals[station.startShuntSignalIndex]))
            and signalInfo.shuntPathsFrom
            and signalInfo.shuntPathsFrom.size()
          )
        )
      ) {
        _core.MakeShuntPath(_stationviewed, station.startShuntSignalIndex, station.endShuntSignalIndex, "shunt-free" == action, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "shunt-reset") {
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        _core.stations[_stationviewed].startShuntSignalIndex = -1;
        _core.stations[_stationviewed].endShuntSignalIndex = -1;
        UpdateControlPanel();
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "shund-disassemble") {    
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        _core.DisassembleShuntPath(_stationviewed, Str.ToInt(argumentX), _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "shunt-cancel") {
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        _core.DisassembleShuntPath(_stationviewed, Str.ToInt(argumentX), false, false, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "shund-disassemble-chain") {     
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        _core.DisassembleShuntPathChain(_stationviewed, Str.ToInt(argumentX), _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "shunt-cancel-chain") {
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        _core.DisassembleShuntPath(_stationviewed, Str.ToInt(argumentX), false, true, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "queue-moveup") {
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        _core.PathQueueMove(_stationviewed, Str.ToInt(argumentX), true, false, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "queue-movedown") {
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        _core.PathQueueMove(_stationviewed, Str.ToInt(argumentX), false, false, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
    } else if(action == "queue-exclude") {
      if(_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        _core.PathRemoveFromQueue(_stationviewed, Str.ToInt(argumentX), false, _core.GetLocalPlayerName());
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);


    } else if(action[0, 11] == "span-action") {
      string[] actiontokens = Str.Tokens(action, "|");
      if (actiontokens.size() == 4) {
        _core.ExecuteSpanAction(Str.ToInt(actiontokens[2]), actiontokens[3], argumentX, argumentY, argumentZ, _core.StationIndexOf(Str.ToInt(actiontokens[1])), _core.GetLocalPlayerName());   
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      }
    } else if(action == "span-responsible") {
      int index = Str.ToInt(argumentX);
      if (_core.spans and index >= 0 and index < _core.spans.size()) {
        _resp_spanaction = argumentY; 
        StartResponsibleAction(index, RESPACTION_SPAN);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
      
         //define int RESPACTION_SPAN              = 7;      //Ответственные действия для перегонов   _resp_spanaction

    
    
    } else if(action == "responsible-accept") {
      if(_resp_mode == RESPMODE_WAITCONFITM) {
        ClearMessages("ZXDSP-ResponsibleTimeout", _resp_id);
        _resp_mode = RESPMODE_NONE;
        switch(_resp_action) {
          
          case RESPACTION_FORCEPLUS: {
            zx_DSP_JunctionInfo jncinfo = _core.stations[_stationviewed].jncs[_resp_itemindex]; 
		        _core.ManualJunctionSwitch(jncinfo, true, true, _core.GetLocalPlayerName());
            break;
          }

          case RESPACTION_FORCEMINUS:{
            zx_DSP_JunctionInfo jncinfo = _core.stations[_stationviewed].jncs[_resp_itemindex]; 
		        _core.ManualJunctionSwitch(jncinfo, false, true, _core.GetLocalPlayerName());
            break;
          }

          case RESPACTION_INVITATION:{
            zxSignal signal = _core.stations[_stationviewed].signals[_resp_itemindex];
            _core.SetSignalState(_stationviewed, _resp_itemindex, "PriglMode.true", _core.GetLocalPlayerName());
            break;
          }
            
          case RESPACTION_FORCEPATHCANCEL: 
            _core.DisassemblePath(_stationviewed, _resp_itemindex, true, _core.GetLocalPlayerName());
            break;
            
          case RESPACTION_SPAN: {
              string[] actiontokens = Str.Tokens(_resp_spanaction, "|");
              string argx, argy, argz;
              if (actiontokens.size() > 1) argx = actiontokens[1];   
              if (actiontokens.size() > 2) argy = actiontokens[2];   
              if (actiontokens.size() > 3) argz = actiontokens[3];   
              _core.ExecuteSpanAction(_resp_itemindex, actiontokens[0], argx, argy, argz, _resp_stationindex, _core.GetLocalPlayerName());   
              _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
            }
            break;


          default: break;
        }
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      }
    } else if(action == "responsible-cancel") {
      if(_resp_mode != RESPMODE_NONE) {
        ClearMessages("ZXDSP-ResponsibleEnable", _resp_id);
        ClearMessages("ZXDSP-ResponsibleTimeout", _resp_id);
        _resp_mode = RESPMODE_NONE;
      }
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
      UpdateControlPanel();
    } 
  }

  void StartResponsibleAction(int itemIndex, int action)
  {
    if(_resp_mode != RESPMODE_NONE) {
      ClearMessages("ZXDSP-ResponsibleEnable", _resp_id);
      ClearMessages("ZXDSP-ResponsibleTimeout", _resp_id);
    }
    _resp_mode = RESPMODE_WAIT;
    _resp_stationindex = _stationviewed;
    _resp_itemindex = itemIndex;
    _resp_object = _core.stations[_stationviewed].controllerFormTab;
    _resp_action = action;
    PostMessage(me, "ZXDSP-ResponsibleEnable", ++_resp_id, 5.0);
    UpdateControlPanel();
  }


/*
  int _resp_mode = RESPMODE_NONE;             //Текущий режим ответственого действия
  int _resp_stationindex;                     //Индекс станции для ответственого действия
  int _resp_itemindex;                        //Индекс объекта для ответственого действия
  int _resp_object;                           //Объект для ответсвеного действия (соответствет коду вкладки)
  int _resp_action;                           //Ответственое действие  


  define int RESPMODE_NONE          = 0;      //Ответственое действие не выполняется
  define int RESPMODE_WAIT          = 1;      //Ожидает доступности для ответственого действия
  define int RESPMODE_WAITCONFITM   = 2;      //Ожидает действия от диспетчера
*/
  


  void BrowserURLHandler(Message msg)
  {
    if(msg.src == _browser and /* msg.src == msg.dst and сообщение шлётся бродкастом */   
    msg.minor[0, 7] == "live://") {
      string[] args = Str.Tokens(msg.minor[7,], "/");
      if (args.size() == 4) OnLinkClick(args[0], args[1], args[2], args[3]);
      else if (args.size() == 3) OnLinkClick(args[0], args[1], args[2], null);
      else if (args.size() == 2) OnLinkClick(args[0], args[1], null, null);
      else OnLinkClick(args[0], null, null, null);
    } 
  }  

  void BrowserClosedHandler(Message msg)
  {
    if(msg.src == _browser and msg.src == msg.dst) {
      PostMessage(_core, "ZXDSP", "ControllerFormClosed", 0.0);
      Dispose();
    }
  }
  
  void ResponsibleTimeoutHandler(Message msg)
  {
    if(_resp_mode != RESPMODE_NONE and _resp_id == Str.ToInt(msg.minor)) {
      if(msg.major == "ZXDSP-ResponsibleEnable") {
        _resp_mode = RESPMODE_WAITCONFITM; 
        PostMessage(me, "ZXDSP-ResponsibleTimeout", _resp_id, 10.0);
      } else _resp_mode = RESPMODE_NONE;
      UpdateControlPanel(); //!!!!!!!!!!!!!!!!!!!!Заменить на вызов с условием вкладки 
    }
  }


};