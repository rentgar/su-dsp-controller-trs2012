//=============================================================================
// File: zx_dsp_journalform.gs
// Desc: Класс формы для журнала событий  
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================


include "zx_dsp_classes.gs"
include "zx_dsp_core.gs"
include "htmlbuffer.gs"
include "browser.gs"

//=============================================================================
// Name: zx_DSP_PermissionsForm
// Desc: Форма журнала событий.
//=============================================================================
final class zd_DSP_JournalForm isclass GameObject
{

  //=============================================================================
  // Name: Init
  // Desc: Инициализирует новое окно журнала сообщений.
  // Parm: core – Объект zx_DSP_Core, представляющий ядро маршрутизации. 
  // Parm: strTable – Таблица строк для интерфейса браузера. 
  // Retn: Инициалищированный объект zd_DSP_JournalForm.
  //=============================================================================
  public zd_DSP_JournalForm Init(zx_DSP_Core core, StringTable strTable);

  //=============================================================================
  // Name: Update
  // Desc: Обновляет окно журнала соыбтий.
  // Parm: includeStationList – значение true, если нужно обновить всё, включая 
  //       список станций; значение false, если нужно обновить только 
  //       отображаемую информацию. 
  //=============================================================================
  public void Update(bool includeStationList);

  //=============================================================================
  // Name: BringToFront
  // Desc: Восстанавливает окно, если оно было свёрнуто, и перемещает его на
  //       передний план, поверх всех окон. 
  //=============================================================================
  public void BringToFront(void);

  //=============================================================================
  // Name: Dispose
  // Desc: Освобождает все ресурсы для этого объекта. 
  //=============================================================================
  public void Dispose(void);

  //=============================================================================
  // Name: GenerateJournal
  // Desc: Создаёт журнал
  //=============================================================================
  thread void GenerateJournal(void);  
  
  //=============================================================================
  // Name: OnLinkClick
  // Desc: Вызывается при нажатии на ссылке в браузере 
  // Parm: action — Действие, которое необходимо выполнить
  //=============================================================================
  void OnLinkClick(string action, string argument);

	//
	// РЕАЛИЗАЦИЯ
	//

  zx_DSP_Core _core;                          //Тут ссылка на ядро
  Browser _browser;                           //Собственно само окно браузера
  StringTable _strtable;                      //Таблица строк
  int _stationviewed = -1;                    //Текущая выбраная станция
  int _filter = -1;                           //Текущий фильтр
  int _creater = 0;                           //Индекс текущего создателя журнала

  define int LEVELMASK    = 1879048192;       //Маска для филтров уровня сообщения
  define int SOURCEMASK   = 16777215;         //Маска фильтров источника 
  
  public zd_DSP_JournalForm Init(zx_DSP_Core core, StringTable strTable)
  {
    if(_browser) return me; //Если инициализация уже выполнена, то выходим.

    _core = core;
    _strtable = strTable;

    _browser = Constructors.NewBrowser();
    _browser.SetWindowStyle(Browser.STYLE_DEFAULT); 
    _browser.SetScrollEnabled(false);        
    _browser.SetCloseEnabled(true);                                        
    _browser.SetWindowPriority(Browser.BP_Window);
    _browser.SetWindowTitle(strTable.GetString("journal-caption"));
    _browser.SetWindowCentered(750, 550);
    _browser.LoadHTMLFile(_core.GetAsset(), _core.StyleFormJournal);

    _browser.SetElementProperty("stationlist", "html", "");
    _browser.SetElementProperty("stationlist", "background-style", "1");
    
    _browser.SetElementProperty("journal", "html", "");
    _browser.SetElementProperty("journal", "background-style", "1");

    me.AddHandler(_browser, "Browser", "Closed", "BrowserClosedHandler");
    me.AddHandler(_browser, "Browser-URL", "", "BrowserURLHandler");

    Update(true);
       
    return me;
  }
  
  public void Update(bool includeStationList)
  {
    if(includeStationList) {
       if(_core.stations and _core.stations.size()) {
        string lnktablestart = HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%");
        string lnktableend = HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable();
        HTMLBuffer buff = HTMLBufferStatic.Construct();
        int i, count = _core.stations.size();
        if(_stationviewed >= count) _stationviewed = -1;
        buff.Print("<html><body>");
        buff.Print(HTMLWindow.StartTable("width=100%"));
        buff.Print(HTMLWindow.StartRow());
        if(_stationviewed == -1) buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleSelectorBackColor));
        else buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(HTMLWindow.MakeLink("live://viewstation/-1", lnktablestart + _strtable.GetString("journal-allstation") + lnktableend, _strtable.GetString("tooltip-journal-allstation")));
        buff.Print(HTMLWindow.EndCell() + HTMLWindow.EndRow());
        buff.Print(HTMLWindow.StartRow() + HTMLWindow.MakeCell(_core.MakeHTMLImage("separator-100-8", 100, 8)) + HTMLWindow.EndRow());
        for(i = 0; i < count; ++i){
          zx_DSP_Station station = _core.stations[i];
          buff.Print(HTMLWindow.StartRow());
          if(i == _stationviewed) buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleSelectorBackColor));
          else buff.Print(HTMLWindow.StartCell("width=100%"));
          buff.Print(HTMLWindow.MakeLink("live://viewstation/" + i, lnktablestart + station.name + lnktableend, _strtable.GetString("tooltip-journal-station")));
          buff.Print(HTMLWindow.EndCell() + HTMLWindow.EndRow());
        }
        buff.Print(HTMLWindow.EndTable());
        buff.Print("</body></html>");
        _browser.SetElementProperty("stationlist", "html", buff.AsString());
      }
    }
    _browser.SetElementProperty("filter-level-info", "value", (bool)(_filter & zx_DSP_LogEntry.LEVEL_INFO));
    _browser.SetElementProperty("filter-level-warn", "value", (bool)(_filter & zx_DSP_LogEntry.LEVEL_WARN));
    _browser.SetElementProperty("filter-level-error", "value", (bool)(_filter & zx_DSP_LogEntry.LEVEL_ERROR));
    _browser.SetElementProperty("filter-source-system", "value", (bool)(_filter & zx_DSP_LogEntry.SOURCE_SYSTEM));
    _browser.SetElementProperty("filter-source-station", "value", (bool)(_filter & zx_DSP_LogEntry.SOURCE_STATION));
    _browser.SetElementProperty("filter-source-junction", "value", (bool)(_filter & zx_DSP_LogEntry.SOURCE_JUNCTION));
    _browser.SetElementProperty("filter-source-signal", "value", (bool)(_filter & zx_DSP_LogEntry.SOURCE_SIGNAL));
    _browser.SetElementProperty("filter-source-path", "value", (bool)(_filter & zx_DSP_LogEntry.SOURCE_PATH));
    _browser.SetElementProperty("filter-source-shunpath", "value", (bool)(_filter & zx_DSP_LogEntry.SOURCE_SHUNTPATH));
    _browser.SetElementProperty("filter-source-span", "value", (bool)(_filter & zx_DSP_LogEntry.SOURCE_SPAN));
    _browser.SetElementProperty("journal", "html", "<html><body><table width=100%><tr><td align=center>" + _strtable.GetString("journal-preparing") + "</td></tr></table></body></html>");
    GenerateJournal();
  }

  public void BringToFront(void)
  {
    if(_browser) {
      _browser.RestoreWindow();
      _browser.BringToFront();    
    }
  }
  
  public void Dispose(void)
  {
    if(_browser) {
        _browser.CloseWindow();
        me.AddHandler(_browser, "Browser", "Closed", null);
        me.AddHandler(_browser, "Browser-URL", "", null);
    }
    _core = null;
    _browser = null;
    _strtable = null;
    _stationviewed = -1;
    _filter = -1;
    _creater = 0;
  }

  thread void GenerateJournal(void)
  {
    int creater = ++_creater;
    int sleepcounter = 0, viewcount = 0;
    HTMLBuffer buff = HTMLBufferStatic.Construct();
    buff.Print("<html><body>");
    buff.Print(HTMLWindow.StartTable("width=100%"));
    if(_core.logentries and _core.logentries.size()) {
      int i, count = _core.logentries.size();
      for(i = count - 1; i >= 0; --i) {
        zx_DSP_LogEntry entry = _core.logentries[i]; 
        if((entry.type & _filter & LEVELMASK) and (entry.type & _filter & SOURCEMASK) and (!MultiplayerGame.IsActive() or MultiplayerGame.IsServer() or 
        entry.type & zx_DSP_LogEntry.PERM_USRE) and (_stationviewed == -1 or _stationviewed == entry.stationIndex)){
          int level = entry.type & _filter & LEVELMASK;
          int source = entry.type & _filter & SOURCEMASK;
          string stationname = "";
          buff.Print(HTMLWindow.StartRow());
            buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleBlockBackColor));
              buff.Print(HTMLWindow.StartTable("width=100%"));
                buff.Print(HTMLWindow.StartRow());
                  buff.Print(HTMLWindow.StartCell());
                    if(entry.stationIndex >= 0 and entry.stationIndex < _core.stations.size()) stationname = _core.stations[entry.stationIndex].name; 
                    if(level & zx_DSP_LogEntry.LEVEL_ERROR) buff.Print(_core.MakeHTMLImage("status-level-error-16", 16));  
                    else if(level & zx_DSP_LogEntry.LEVEL_WARN) buff.Print(_core.MakeHTMLImage("status-level-warn-16", 16));  
                    else buff.Print(_core.MakeHTMLImage("status-level-info-16", 16));
                    buff.Print("&nbsp;");
                    if(source & zx_DSP_LogEntry.SOURCE_SPAN) buff.Print(_core.MakeHTMLImage("status-source-span-16", 16));  
                    else if(source & zx_DSP_LogEntry.SOURCE_SHUNTPATH) buff.Print(_core.MakeHTMLImage("status-source-shuntpath-16", 16));  
                    else if(source & zx_DSP_LogEntry.SOURCE_PATH) buff.Print(_core.MakeHTMLImage("status-source-path-16", 16));  
                    else if(source & zx_DSP_LogEntry.SOURCE_JUNCTION) buff.Print(_core.MakeHTMLImage("status-source-junction-16", 16));  
                    else if(source & zx_DSP_LogEntry.SOURCE_SIGNAL) buff.Print(_core.MakeHTMLImage("status-source-signal-16", 16));  
                    else if(source & zx_DSP_LogEntry.SOURCE_STATION) buff.Print(_core.MakeHTMLImage("status-source-station-16", 16));  
                    else buff.Print(_core.MakeHTMLImage("status-source-system-16", 16));  
                  buff.Print(HTMLWindow.EndCell());
                  buff.Print(HTMLWindow.StartCell());
                    buff.Print("<nowrap>");
                    buff.Print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    buff.Print(HTMLWindow.PadZerosOnFront(HTMLWindow.GetHoursFromFloat(entry.time), 2) + ":" + HTMLWindow.PadZerosOnFront(HTMLWindow.GetMinutesFromFloat(entry.time), 2) + ":" +
                               HTMLWindow.PadZerosOnFront(HTMLWindow.GetSecondsFromFloat(entry.time), 2));
                    buff.Print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    buff.Print("</nowrap>");
                  buff.Print(HTMLWindow.EndCell());
                  buff.Print(HTMLWindow.MakeCell(stationname, "width=100%"));
                buff.Print(HTMLWindow.EndRow());
                buff.Print(HTMLWindow.StartRow());
                  if(level & zx_DSP_LogEntry.LEVEL_ERROR) buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(entry.message, _core.StyleErrorText), "colspan=3"));  
                  else if(level & zx_DSP_LogEntry.LEVEL_WARN) buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(entry.message, _core.StyleWarnText), "colspan=3"));  
                  else buff.Print(HTMLWindow.MakeCell(entry.message, "colspan=3"));
                buff.Print(HTMLWindow.EndRow());
              buff.Print(HTMLWindow.EndTable());
            buff.Print(HTMLWindow.EndCell());
          buff.Print(HTMLWindow.EndRow());
          ++viewcount;
          stationname = null;
        }
        if(sleepcounter++ > 1000) {
          sleepcounter = 0;
          Sleep(0.01);
        }
        if(creater != _creater) return;
      }
    }
    if(!viewcount) buff.Print("<tr><td align=center>" + _strtable.GetString("journal-empty") + "</td></tr>"); 
    buff.Print(HTMLWindow.EndTable());
    buff.Print("</body></html>");
    _browser.SetElementProperty("journal", "html", buff.AsString());
  }  


  void OnLinkClick(string action, string argument)
  { 
    if(action == "viewstation") {
      int index = Str.ToInt(argument);
      if(_core.stations and _stationviewed != index and index >= -1 and index < _core.stations.size()) {
        _stationviewed = index;
        Update(true);
      }
    } else if(action == "filter") {
      if(argument == "level-info") _filter = _filter ^ zx_DSP_LogEntry.LEVEL_INFO;
      else if(argument == "level-warn") _filter = _filter ^ zx_DSP_LogEntry.LEVEL_WARN;
      else if(argument == "level-error") _filter = _filter ^ zx_DSP_LogEntry.LEVEL_ERROR;
      else if(argument == "source-system") _filter = _filter ^ zx_DSP_LogEntry.SOURCE_SYSTEM;
      else if(argument == "source-station") _filter = _filter ^ zx_DSP_LogEntry.SOURCE_STATION;
      else if(argument == "source-junction") _filter = _filter ^ zx_DSP_LogEntry.SOURCE_JUNCTION;
      else if(argument == "source-signal") _filter = _filter ^ zx_DSP_LogEntry.SOURCE_SIGNAL;
      else if(argument == "source-path") _filter = _filter ^ zx_DSP_LogEntry.SOURCE_PATH;
      else if(argument == "source-shunpath") _filter = _filter ^ zx_DSP_LogEntry.SOURCE_SHUNTPATH;
      else if(argument == "source-span") _filter = _filter ^ zx_DSP_LogEntry.SOURCE_SPAN;
      Update(false);
    } 
  }

  void BrowserURLHandler(Message msg)
  {
    if(msg.src == _browser and /* msg.src == msg.dst and сообщение шлётся бродкастом */   
    msg.minor[0, 7] == "live://") {
      string[] args = Str.Tokens(msg.minor[7,], "/");
      if(args.size() == 2) OnLinkClick(args[0], args[1]);
      else OnLinkClick(args[0], null);
    } 
  }  
  
  void BrowserClosedHandler(Message msg)
  {
    if(msg.src == _browser and msg.src == msg.dst) {
      PostMessage(_core, "ZXDSP", "JournalFormClosed", 0.0);
      Dispose();
    }
  }
  
};