//=============================================================================
// File: zx_dsp_classes.gs
// Desc: Набор классов, используемых маршрутизацией
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_context.gs"
include "zx_dsp_junction_link_marker_base.gs"
include "zx_dsp_junctioninfo.gs"
include "zx_dsp_span.gs"
include "zx_specs.gs"
include "cache_mutex.gs"

//=============================================================================
// Name: zx_DSP_PathQueueItem
// Desc: Описывает элемент очереди маршрутов.
//=============================================================================
final class zx_DSP_PathQueueItem
{
  public int id;                                                                      //Идентификатор элемента
  public int index;                                                                   //Индекс маршрута
  public int variant                              = -1;                               //Индекс варианта маршрута. (-1 - автовыбор варианта)
  public string client;                                                               //Имя пользователя, добавившего маршрут в очередь
  public int group;                                                                   //Индекс группы очереди, гд -1 - маршруты собираемые по возможности без очердёности, 0 - стандартная группа, от 1 до 10 кастомные группы.
  public string message;                                                              //Сообщение состояния
  public float timeout                            = 0.0;                              //Таймаут для обработки записи. 
  public int attempt;                                                                 //Колличество неудачных попыток.
};

//=============================================================================
// Name: zx_DSP_JunctionsHolder
// Desc: Описывает трассу маршрута.
//=============================================================================
class zx_DSP_JunctionsHolder
{
  public zx_DSP_JunctionInfo[] jncs;                                                  //Список стрелок по маршруту
  public int[] jncDirections;                                                         //Список направлений для сборки маршрута
  public bool[] jncFace;                                                              //Список расположений стрелки (true - противошёрстное, false - пошерстное);
  public float length;                                                                //Длина маршрута в метрах
  public int priorityMultiplier;                                                      //Коэфициент приоритета маршрута
  public zxSignal[] forwardObjects;
  public zxSignal[] backwardObjects;
};

//=============================================================================
// Name: zx_DSP_PathVariant
// Desc: Описывает вариант маршрута по станции.
//=============================================================================
final class zx_DSP_PathVariant isclass zx_DSP_JunctionsHolder
{
  public bool invisible;                                                              //Указатель, что вариант маршрута невидим
  public string name;                                                                 //Наименование варианта
  public int marker;
  public bool alsncoding;                                                             //Указатель наличия кодирования съездов по маршруту (true - съезды кодируются; false - не кодируются)
};

//=============================================================================
// Name: zx_DSP_PathAbstract
// Desc: Описывает абстрактный маршрут.
//=============================================================================
class zx_DSP_PathAbstract
{
  public bool invisible                           = false;                            //Указатель, что маршрут скрыт и не отображается
  public int index;
  public string name;                                                                 //Наименование маршрута
  public zxSignal startSign;                                                          //Светофор начала маршрута
  public zxSignal groupSign;
  public zxSignal endSign;                                                            //Светофор конца маршрута
  public zxSignal backwardEndSign;                                                    //Тылом стоящий светофор, по проходу которого будет сбрасываться маршрут

  public string GenerateName(StringTable stringTable) {
    string startName = startSign.privateName;
    if (groupSign) {
      startName = groupSign.privateName + " (" + startName + ")";
    }
    string ret = stringTable.GetString("string-pathfrom") + " " + startName;
    if (backwardEndSign and backwardEndSign != endSign) {
      ret = ret + " " + stringTable.GetString("string-pathbehind") + " " + backwardEndSign.privateName;
    }
    if (endSign) {
      ret = ret + " " + stringTable.GetString("string-pathto") + " " + endSign.privateName;
      if (startSign.stationName != endSign.stationName) {
        ret = ret + "@" + endSign.stationName + " " + stringTable.GetString("string-pathspan");
      }
    }

    return ret;
  }

  public string GetNameOrGenerate(StringTable stringTable) {
    if (name and name.size()) {
      return name;
    }

    return GenerateName(stringTable);
  }
};

//=============================================================================
// Name: zx_DSP_Path
// Desc: Описывает маршрут по станции.
//=============================================================================
final class zx_DSP_Path isclass zx_DSP_PathAbstract
{
  public define int TYPE_AUTO                     = 1;                                //Доступен автомаршрут
  public define int TYPE_X_MODE                   = 2;                                //Доступен авторежим (кресты)

  public define int STATE_NONE                    = 0;                                //Маршрут не собран и не стоит в очереди
  public define int STATE_WAIT                    = 1;                                //Ожидает в очереди сбора
  public define int STATE_ACTIVE                  = 2;                                //Маршрут замкнут
  public define int STATE_BUSY                    = 4;                                //Маршрут занят
  public define int STATE_AUTO                    = 8;                                //Маршрут, который пересобирает сам себя (автомаршрут)
  public define int STATE_X_MODE                  = 16;                               //Авторежим (крест на светофоре)

  public zx_DSP_PathVariant[] variants            = new zx_DSP_PathVariant[0];        //Набор вариантов маршрута. (индекс 0 - всегда основной)

  public int type                                 = 0;
  public int state                                = STATE_NONE;                       //Состояние маршрута
  public int activeVariant                        = -1;                               //Индекс варианта маршрута, который в данный момент замкнут

  public string errorMessage;                                                         //Сообщение об ошибке
  public float errMsgViewedTime                   = -1;                               //Время отображения сообщения

  public int invisbleVariationCount               = 0;                                //Количество скрытых вариантов маршрута
  public int variationviewindex                   = -1;                               //Индекс отображаемого варианта маршрута
};

//=============================================================================
// Name: zx_DSP_ShuntPath
// Desc: Описывает маневровый маршрут по станции.
//=============================================================================
final class zx_DSP_ShuntPath isclass zx_DSP_JunctionsHolder, zx_DSP_PathAbstract
{
  public define int STATE_NONE                    = 0;
  public define int STATE_ACTIVE                  = 1;
  public define int STATE_BUSY                    = 2;

  public int id = -1;                                                                 //Идентификатор маршрута
  public string chainname;                                                            //Наименование цепочки маршрутов
  public int state                                = STATE_NONE;                       //Указатель, что маневровый маршрут занят поездом
  public zx_DSP_ShuntPath prevPart;                                                   //Предыдущий элементарный маршрут в цепочке
  public zx_DSP_ShuntPath nextPart;                                                   //Следующий элементарный маршрут в цепочке
  public zx_DSP_ShuntPath searchSource;
  public int searchWeight;
};

//=============================================================================
// Name: zx_DSP_JurnalItem
// Desc: Описывает запись в журнале информации.
//=============================================================================
final class zx_DSP_JurnalItem
{
  public bool isOutPath;                                                              //Указатель, что маршрут на выход со станции
  public string trainnumber;                                                          //Номер поезда
  public string loconame;                                                             //Наименование локомотива
  public string drivername;                                                           //Имя машиниста
  public zxSignal signal;                                                             //Имя светофора к которому прибывает или от которого отправляется поезд
  public zxSignal targetSignal;                                                       //Целевой светофор при выходном маршруте
  public GameObjectID locomotiveId;                                                   //Идентификатор локомтива, в котором находится машинист
  public float time;                                                                  //Время, в которое произошло событие
};

//=============================================================================
// Name: zx_DSP_Station
// Desc: Описывает станцию.
//=============================================================================
final class zx_DSP_Station
{

  //=============================================================================
  // Name: INFOTAB_*
  // Desc: Вкладки формы информации
  //=============================================================================  
  public define int INFOTAB_APPROACHAREA          = 0;                                //Участки приближения
  public define int INFOTAB_JOURNAL               = 1;                                //Журнал передвижения

  //=============================================================================
  // Name: CONTROLTAB_*
  // Desc: Вкладки формы управления
  //=============================================================================  
  public define int CONTROLTAB_PATH               = (1 << 0);                         //Вкладка правления маршрутами
  public define int CONTROLTAB_JUNCTION           = (1 << 1);                         //Вкладка правления стрелками
  public define int CONTROLTAB_SIGNAL             = (1 << 2);                         //Вкладка правления светофорами
  public define int CONTROLTAB_SPAN               = (1 << 3);                         //Вкладка правления перегонами
  public define int CONTROLTAB_SHUNT              = (1 << 4);                         //Вкладка правления манёврами
  public define int CONTROLTAB_QUEUE              = (1 << 5);                         //Вкладка правления очередью

  public int id;                                                                      //Идентификатор странции
  public string name                              = "";                               //Наименование станции
  public int index                                = -1;                               //Индекс станции в списке.
  public zx_DSP_JunctionInfo[] jncs               = new zx_DSP_JunctionInfo[0];       //Стрелки относящиеся к станции
  public zx_DSP_Path[] paths                      = new zx_DSP_Path[0];               //Список маршрутов
  public zx_DSP_ShuntPath[] shuntPaths            = new zx_DSP_ShuntPath[0];          //Список маневровых маршрутов
  public zx_DSP_ShuntPath[] activeShuntPaths      = null;                             //Построенные маневровые маршруты. null для легаси режима
  public zxSignal[] signals;                                                          //Список светофоров станции
  public zx_DSP_PathQueueItem[] pathQueue         = new zx_DSP_PathQueueItem[0];      //Очередь маршрутов на построение
  public zx_DSP_JurnalItem[] jurnal               = new zx_DSP_JurnalItem[0];         //Журнал прибытия и отбытия
  public zx_DSP_SpanBase[] spans                  = new zx_DSP_SpanBase[0];           //Список перегонов, примыкающих к станции

  public bool availableControl                    = false;                            //Указатель, что cтанция доступна для управления
  public string[] users                           = new string[0];                    //Список пользователей, которым доступна данная станция

  public int startShuntSignalIndex                = -1;                               //Индекс сигнала с которого будет начат маневровый маршрут
  public int endShuntSignalIndex                  = -1;                               //Индекс сигнала до которого будет построен маневровый маршрут

  public string shuntPathErrorMessage;                                                //Сообщение об ошибке для маневрового маршрута
  public float  shuntPathErrorMessageViewTime     = -1;                               //Время отображения сообщения

  public string spanErrorMessage;                                                     //Сообщение об ошибке при развороте перегона
  public GameObjectID spanErrorSignalId;                                              //Игровой идентификатор входного светофора ограничивающего перегон для которого ошибка
  public float spanErrorMessageViewTime           = -1;                               //Время отображения сообщение об ошибке для перегона

  public bool jview, sview, pview;                                                    //Указатель, что информация раскрыта в настроках
  public int pathviewindex                        = -1;                               //Индекс маршрута, который раскрыт в настройках
  public int tab                                  = 0;                                //Выбраная вкладка
  public zxSignal selectSign;                                                         //Светофор, выбранный в маршрутах


  public int informationFormTab                   = INFOTAB_APPROACHAREA;             //Выбраная вкладка в форме информациию. Одна из констант INFOTAB_*
  public int controllerFormTab                    = CONTROLTAB_PATH;                  //Выбраная влкадка в форме котнроллера ДСП. Одна из констант CONTROLTAB_*


  public int invisiblePathCount                   = 0;                                //Количество скрытых маршрутов

  public zx_DSP_ShuntPath[] shuntPathSearchQueue;
  
  //=============================================================================
  // Name: CheckUserPermission
  // Desc: Проверяет, если ли у пользователя разрешение на управление этой 
  //       станцией.
  // Parm: userName – имя пользователя, для которого выполняется проерка.
  // Retn: Значение true, если пользователь может управлять станций; в противном 
  //       случае — значение false.
  //=============================================================================
  public bool CheckUserPermission(string userName)
  {
    string localplayername = World.GetLocalPlayerName();
    Str.ToLower(localplayername);
    if (userName == localplayername) return !MultiplayerGame.IsActive() or MultiplayerGame.IsServer() or availableControl;
    if(users) {
      int i, count = users.size();
      for(i = 0; i < count; ++i)
        if(users[i] == userName) return true;
    }
    return false;
  }
  
  //=============================================================================
  // Name: CheckMyPermission
  // Desc: Выполняет проверку, иметт ли текущий пользователь разрешение на 
  //       управление станией. 
  // Retn: Значение true, если пользователь может управлять станций; в противном 
  //       случае — значение false.
  //=============================================================================
  public bool CheckMyPermission(void)
  {
    return !MultiplayerGame.IsActive() or MultiplayerGame.IsServer() or availableControl;   
  }
  
  
};

//=============================================================================
// Name: zx_DSP_ShuntPathJunctionItem
// Desc: Описывает стрелку, которая является частью маневрового маршрута.
//=============================================================================
final class zx_DSP_ShuntPathJunctionItem
{
  public zx_DSP_JunctionInfo jncinf;                                                  //Информация о стралке
  public int direction;                                                               //Направление левера
  public int checkdirection;                                                          //Направления, которые были проверенны
  public int face;                                                                    //Стрелка противошёрстная
  public int part;                                                                    //Номер части маршрута (нумерация с 1)/количество светофоров перед элементом
};

//=============================================================================
// Name: zx_DSP_PathExtInfo
// Desc: Представляет дополнительную информацию о маршруте.
//=============================================================================
final class zx_DSP_PathExtInfo
{
  public bool free;                                                                   //Указатель, что маршрут свободен
  public bool cross;                                                                  //Указатель, что есть перекрёстный маршрут
};

//=============================================================================
// Name: zx_DSP_SpanTrainInfo
// Desc: Описывает поезд, который находится на перегоне.
//=============================================================================
final class zx_DSP_SpanTrainInfo
{
  public int id;                                                                      //Идентификатор записи
  public int stationId                            = -1;                               //Идентификатор станции к которой следует поезд
  public Train train;                                                                 //Поезд, которым управляет игрок
  public zxSignal targetSignal;                                                       //Входной светофор станции, к которой выполняется движение
  public string trainnumber;                                                          //Номер поезда
  public string sourceStation;                                                        //Наименование станции, с которой отправился поезд
  public int blockSectionCount;                                                       //Количество блок участков до входного светофора
  public bool hasAheadTrain;                                                          //Указатель наличия впереди поезда (до входного сигнала)
  public bool hasInPath;                                                              //Указатель наличия входного маршрута
};

//=============================================================================
// Name: zx_DSP_LogEntry
// Desc: Описывает запись в журнал событий.
//=============================================================================
final class zx_DSP_LogEntry
{

  //=============================================================================
  // Name: LEVEL_*
  // Desc: Уровень сообщения.
  //=============================================================================  
  public define int LEVEL_INFO                    = (1 << 28);                        //Информационное сообщение
  public define int LEVEL_WARN                    = (1 << 29);                        //Сообщение, на которое необходимо обратить внимание (ответственные действия)
  public define int LEVEL_ERROR                   = (1 << 30);                        //Сообщение об ошибке - что=то сломалось или пошло не так.

  //=============================================================================
  // Name: SOURCE_*
  // Desc: Исочник сообщения
  //=============================================================================  
  public define int SOURCE_SYSTEM                 = (1 << 0);                         //Источник записи система.
  public define int SOURCE_STATION                = (1 << 1);                         //Источник станция
  public define int SOURCE_JUNCTION               = (1 << 2);                         //Источник стрелка
  public define int SOURCE_SIGNAL                 = (1 << 3);                         //Источник светофор
  public define int SOURCE_PATH                   = (1 << 4);                         //Источник маршрут
  public define int SOURCE_SHUNTPATH              = (1 << 5);                         //Источник маневровый маршрут
  public define int SOURCE_SPAN                   = (1 << 6);                         //Источник перегон
 
  //Тут добавить по мере возможности другие источники. Смещение до 27 включительно

  public define int PERM_USRE                     = (1 << 31);                        //Специальный флаг разрешения читать сообщения ДСП
  
  //=============================================================================
  // Name: CODE_*
  // Desc: Код сообщения
  //============================================================================= 
  public define int CODE_ADDPERMISSION            = 1;                                //Добавлено разрешение на станцию
  public define int CODE_REMOVEPERMISSION         = 2;                                //Удалено разрешение на станцию
  public define int CODE_HANDJUNCTIONTOGGLE       = 3;                                //Перевод ручной стрелки
  public define int CODE_JUNCTIONCUTUP            = 4;                                //Взрез стрелки
  public define int CODE_JUNCTIONCUTUPRESTORE     = 5;                                //Восстановение стрелки после взреза
  public define int CODE_JUNCTIONTOGGLE			      = 6;                                //Перевод стрелки с пульта
  public define int CODE_JUNCTIONTOGGLEFORCE	    = 7;                                //Перевод стрелки с пульта (вспомогательный)
  public define int CODE_JUNCTIONLOCK			        = 8;                                //Блокировка стрелки в пульте
  public define int CODE_JUNCTIONUNLOCK			      = 9;                                //Разблокировка стрелки в пульте
  public define int CODE_ASSEMBLYSHUNTPATH	      = 10;                               //Сборка маневрового маршрута
  public define int CODE_DISASSEMBLYSHUNTPATH	    = 11;                               //Разборка маневрового маршрута
  public define int CODE_TURNSPAN	                = 12;                               //Разворот перегона
  public define int CODE_BUILDPATH                = 13;                               //Сборка маршрута
  public define int CODE_REOPENSIGNAL             = 14;                               //Повторное открытие светофора маршрута
  public define int CODE_DISASSEMBLEPATHFORCE     = 15;                               //Принуительный разбор маршрута
  public define int CODE_DISASSEMBLEPATH          = 16;                               //Разбор маршрута
  public define int CODE_XMODEON                  = 17;                               //Включение авторежима
  public define int CODE_XMODEOFF                 = 18;                               //Отключение авторежима
  public define int CODE_AUTHOPATHON              = 19;                               //Включение автомаршрута
  public define int CODE_AUTHOPATHOFF             = 20;                               //Отключение автомаршрута
  public define int CODE_SIGNALINVITATIONON       = 21;                               //Включение пригласительного сигнала
  public define int CODE_SIGNALINVITATIONOFF      = 22;                               //Отключение пригласительного сигнала
  public define int CODE_SPANABTURN               = 23;                               //Разворот перегона
  public define int CODE_SPANABTURNBYPATH         = 24;                               //Разворот перегона при сборке маршрута
  public define int CODE_SPANABCONSENT            = 25;                               //Дача согласия
  public define int CODE_SPANABAUXILARYIN         = 26;                               //Вспомогательный приём
  public define int CODE_SPANABAUXILARYOUT        = 27;                               //Вспомогательное отправление
  public define int CODE_SPANPABCONSENT           = 28;                               //Дача согласия
  public define int CODE_SPANPABCONSENTCANCEL     = 29;                               //Отмена согласия
  public define int CODE_SPANPABARRIVED           = 30;                               //Дача прибытия
  public define int CODE_SPANPABFIXATION          = 31;                               //Искусственная фиксация прбытия
  public define int CODE_SPANPABRESETCOUNTER      = 32;                               //Сброс счётчика ССО

  

  
  public int type;                                                                    //Побитовая сумма флагов  LEVEL_*, SOURCE_* и PERM_USRE, определяющая тип записи
  public int code;                                                                    //Одна из констант CODE_*, определяющая код записи
  public int stationIndex;                                                            //Индекс станции, с которой связана запись
  public int itemIndex;                                                               //Индекс элемента станции, с которым связана запись
  public float time;                                                                  //Время возникнования события
  public string message;                                                              //Отображаемое сообщение
  public string userName;                                                             //Имя пользователя, который создал событие

};

//=============================================================================
// Name: zx_DSP_DriveJunctionItem
// Desc: Описывает стрелку, отображаемую машинисту по его маршруту.
//=============================================================================
final class zx_DSP_DriveJunctionItem
{
  public zx_DSP_JunctionInfo junctionInfo;                                            //Информация о стрелке
  public float distance;                                                              //Растояние до стрелки
  public bool face;                                                                   //Указывает, что  стрелка по маршруту противошёрстная
};

//=============================================================================
// Name: zx_DSP_SignalInfo
// Desc: Данные, записываемые в светофор, для сопоставления его со станцией и
//       перегоном.
//=============================================================================
final class zx_DSP_SignalInfo isclass zxExtraSignalData
{
  public int versionId;
  public zx_DSP_Station station;                                                      //Станция, которой принадлежит светофор или null для проходных светофоров.
  public zx_DSP_SpanBase span;                                                        //Перегон, которому принадлежит проходной светофор или null, если светофор не сопаставлен с перегоном.
  public zx_DSP_Path[] pathsFrom;
  public zx_DSP_Path[] pathsBehind;
  public zx_DSP_Path[] pathsTo;
  public zx_DSP_ShuntPath[] shuntPathsFrom;
  public zx_DSP_ShuntPath[] shuntPathsBehind;
  public zx_DSP_ShuntPath[] shuntPathsTo;
  public zx_DSP_PathAbstract activePathFrom;                                          //todo: заполнять также и в промежуточные светофоры
  public zx_DSP_PathAbstract activePathTo;
  public zx_DSP_PathAbstract activePathBackward;
};

static class zx_DSP_Helper
{
  public GameObject GetGameObjectWait(GameObjectID id) {
    GameObject obj = Router.GetGameObject(id);
    while (!obj and World.NO_MODULE == World.GetCurrentModule()) {
      Router.GetCurrentThreadGameObject().Sleep(0.01);
      obj = Router.GetGameObject(id);
    }

    return obj;
  }
};
