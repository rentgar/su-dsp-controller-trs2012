//=============================================================================
// File: zx_dsp_spaninfo.gs
// Desc: Классы для реализации пернона.
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "alsn_vehicleproperties.gs"
include "zx_dsp_classes.gs"
include "zx_dsp_spaninfo.gs"
include "ecl_bitlogic.gs"



//=============================================================================
// Name: zx_DSP_SpanBase
// Desc: Базовый класс для полуперегона.
//=============================================================================
class zx_DSP_SpanBase isclass GameObject
{

  public define int RESULT_SUCCESS              = 0;      //Можно построить маршрут на перегон. Перегон развёрнут.
  public define int RESULT_ONESIDE              = 1;      //Недопустимое направление для одностороннего перегона.
  public define int RESULT_WRONGWAY             = 2;      //Неверное направление перегона
  public define int RESULT_BUSY                 = 3;      //Перегон занят подвижным составом.
  public define int RESULT_LOCK                 = 4;      //Перегон заблокирован 

  //=============================================================================
  // Name: GetType
  // Desc: Вызывается автоматически системой для получения типа перегона, с 
  //       которым связан реализующий класс. 
  // Retn: Возвращает значение одной из констант zx_DSP_SpanInfo.TYPE_*, 
  //       определяющее тип перегона. 
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public int GetType() { return 0; }
  
  //=============================================================================
  // Name: CanMakeUIHTML
  // Desc: Возвращает значение, указывающее, может ли данный перегон отображать
  //       пользовательский интерфейс состояния и управления генерируемый 
  //       функцией zx_DSP_SpanBase.MakeUIHTML(...).
  // Retn: Возвращает значение true, если пользовательский интерфейс может быть
  //       сгенерирован; в противном случае возвращает значение false. 
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public bool CanMakeUIHTML() { return true; }

  //=============================================================================
  // Name: MakeUIHTML
  // Desc: Вызывается автоматически системой для генерации HTML кода интерфейса
  //       управления перегоном.
  // Parm: buffer – буфер для построение HTML кода пользовательского интерфейса
  //       управления перегоном.
  // Parm: context – констекст, в котором должен быть построен пользовательский
  //       интерфейс.
  //=============================================================================
  public void MakeUIHTML(HTMLBuffer buffer, zx_DSP_MakeHTMLContext context) {}
  
  //=============================================================================
  // Name: DoUIAction
  // Desc: Вызывается автоматически системой при запросе выполнения действия в
  //       пользовательском интерфейсе, сгенерироанном функцией MakeUIHTML(...).
  // Parm: action – сьрока, представляющее действие, которое требуется выполнить.
  // Parm: argumentX – строка, представляющая первый аргумент действия.
  // Parm: argumentY – строка, представляющая второй аргумент действия.
  // Parm: argumentZ – строка, представляющая третий аргумент действия.
  // Parm: context – констекст, в котором выполняется действие.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public void DoUIAction(string action, string argumentX, string argumentY, string argumentZ, zx_DSP_UIActionContext context) {}
  
  //=============================================================================
  // Name: GetSyncProperties
  // Desc: Вызывается автоматически системой во время синхронизации перегона
  //       для получения синхронизируемых данных.
  // Parm: fully – значение true, если требуется полная синхронизация (при 
  //       подключении нового клиента).
  // Retn: Объект Soup содержащий синхронизируемые данные или значение null, если
  //       синхронизация не требуется. 
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public Soup GetSyncProperties(bool fully) { return null; }
  
  //=============================================================================
  // Name: ApplySyncProperties
  // Desc: Вызывается автоматически системой при получении данных синхронизации.
  // Parm: soup – объект soup, содержащий данные синхронизации. 
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public void ApplySyncProperties(Soup soup) {}

  //=============================================================================
  // Name: ResponceArrival
  // Desc: Вызывается автоматически системой при получении ответа на запрос
  //       при инициации действия с перегоном из UI на клиенте.
  // Parm: soup – объект soup, содержащий данные ответа. 
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public mandatory void ResponceArrival(Soup soup);

  //=============================================================================
  // Name: SetErrorMessage
  // Desc: Добавляет сообщение об ошибке. Сообщение отобразится только у того
  //       игрока, который пытался выполнить действие.
  // Parm: message - текст сообщения об ошибке
  // Parm: context – контекст, в котором выполняется действие.
  //=============================================================================
  void SetErrorMessage(string message, zx_DSP_UIActionContext context);

  //=============================================================================
  // Name: SpanInitialization
  // Desc: Вызывается автоматически системой для инициализации перегона.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public void SpanInitialization(void) {}
   
  //=============================================================================
  // Name: RequestBuildPath
  // Desc: Вызывается автоматически системой для проверки возможности построить
  //       маршрут отправления. В зависимости от реализации и настроек перегона
  //       может развернуть перегон.
  // Parm: context – контекст, в котором выполняется действие.
  // Retn: Значение одной из констант RESULT_*, определяющее результат проверки
  //       возможности построить маршрут отправления.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
	public int RequestBuildPath(zx_DSP_UIActionContext context) { return RESULT_LOCK; }
  
  //=============================================================================
  // Name: SyncSignal
  // Desc: Выполняет синхронизацию светофора в перегоне.
  // Parm: signalIndex – отсчитываемый от 0 (ноля) индекс светофора в перегоне.
  //=============================================================================
  final void SyncSignal(int signalIndex);
  
  //=============================================================================
  // Name: RaiseSync
  // Desc: Оповещает систему о необходимости синхронизации перегона.
  // Parm: includeInverse — значение true, если требуется синхронизировать
  //       оба полуперегона: или значение false, если требуется синхронизировать
  //       только иекущмй полуперегон.
  // Note: Функция не выполняет синхронизацию, а лишь оповещает систему о
  //       необходимости синхронизации полуперегона. Синхронизация будет 
  //       выполнена не ранее следующего игрового кадра.
  //=============================================================================
  final void RaiseSync(bool includeInverse);
  
  //=============================================================================
  // Name: RaiseUIUpdate
  // Desc: Оповещает систему о необходимости обновления пользовательского
  //       интерфейса.
  // Parm: includeInverse — значение true, если требуется синхронизировать
  //       оба полуперегона: или значение false, если требуется синхронизировать
  //       только иекущмй полуперегон.
  // Note: Функция не выполняет обновление UI, а лишь оповещает систему о
  //       необходимости обновления UI. Обновление будет выполнено, если окно
  //       управление пультом запущено и не ранее следующего игрового кадра.
  //=============================================================================
  final void RaiseUIUpdate(void);
  
  //=============================================================================
  // Name: OnBuildOutPath
  // Desc: Вызывается автоматически системой после постройки маршрута отправления
  //       на перегон.
  // Parm: context – контекст, в котором выполняется действие.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public void OnBuildOutPath(zx_DSP_UIActionContext context) {}

  //=============================================================================
  // Name: OnCancelOutPath
  // Desc: Вызывается автоматически системой после отмены маршрута отправления
  //       на перегон.
  // Parm: context – контекст, в котором выполняется действие.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
  public void OnCancelOutPath(zx_DSP_UIActionContext context) {}

  //=============================================================================
  // Name: OnBuildInPath
  // Desc: Вызывается автоматически системой после постройки маршрута приёма
  //       с перегона на станцию.
  // Parm: context – контекст, в котором выполняется действие.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
	public void OnBuildInPath(zx_DSP_UIActionContext context) {}

  //=============================================================================
  // Name: OnReleaseInPathFirstSection
  // Desc: Вызывается автоматически системой после разделки первой секции 
  //       маршрута приёма с перегона на станцию.
  // Parm: context – контекст, в котором выполняется действие.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. 
  //=============================================================================
	public void OnReleaseInPathFirstSection(zx_DSP_UIActionContext context) {}

  //=============================================================================
  // Name: GetProperties
  // Desc: Вызывается автоматически системой для сохранения состояния перегона и 
  //       его настроек..
  // Retn: Объект Soup, содержащий сохранённые настройки.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. Обязательно требуется вызов родительской функции
  //       через inherited(). 
  //=============================================================================
  public mandatory Soup GetProperties(void);

  //=============================================================================
  // Name: SetProperties
  // Desc: Вызывается автоматически системой прив восстановлении состояния  
  //       перегона и его настроек..
  // Parm: soup – объект soup, содержащий сохранённые ранее данные о состоянии
  //       перегона и его настройках. 
  // Retn: Значенеи true, если настройки состояние удачно восстановлены; в
  //       противном случае — значенеи false, если данные содержат ошибки.
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. Обязательно требуется вызов родительской функции
  //       через inherited(). 
  //=============================================================================
  public mandatory bool SetProperties(Soup soup);

  //=============================================================================
  // Name: Init
  // Desc: Вызывается автоматически системой при переарсчёте перегонов для
  //       начального определение перегона.
  // Parm: info – объект zx_DSP_SpanInfo, представляющий маркер перегона с 
  //       задагнными пользователем параметрами перегона; или значение NULL, если
  //       на перегоне не установлен маркер.
  // Parm: facing – значение true, если инициализируется полуперегон по 
  //       направлению маркера; или значение false если в противоположную от 
  //       направления маркера сторону. 
  // Note: Для реализации, требуется переопределить в классе реализующем
  //       функии перегона. Обязательно требуется вызов родительской функции
  //       через inherited(). 
  //=============================================================================
  public mandatory void Init(zx_DSP_SpanInfo info, bool facing);

  public void AddHandlers() {}

	//
	// РЕАЛИЗАЦИЯ
	//
  
  public int index;                                       //Индекс перегона в общем списка.
  public zx_DSP_SpanBase inverse;                         //Обратный полуперегон
  public zx_DSP_Station station;                          //Станция, в сторону которой направлен полуперегон и которой он принадлежит
  public zxSignal[] signals;                              //Массив светофоров, 0 - входной, 1 - предвходной, итд
  public bool available;                                  //Возможность движения в направлении этого полуперегона
  public string errorMessage;                             //Сообщение об ошибке
  public float errorMessageViewTime             = -1;     //Время отображения сообщение об ошибке
  
  public mandatory void ResponceArrival(Soup soup)
  {
    string message = soup.GetNamedTag("errorMessage");
    if ("" != message) {
      errorMessage = message;
      errorMessageViewTime = World.GetTimeElapsed();
    }
  }

  void SetErrorMessage(string message, zx_DSP_UIActionContext context)
  {
    if (context.IsRequest()) {
      Soup soup = Constructors.NewSoup();
      soup.SetNamedTag("errorMessage", message);
      context.SendResponce(me, soup);
    } else {
      errorMessage = message;
      errorMessageViewTime = World.GetTimeElapsed();
    }
  }
  
  final void SyncSignal(int signalIndex)
  {
    if (!MultiplayerGame.IsActive() or !MultiplayerGame.IsServer()) return;
    zxSignal signal = signals[signalIndex];
	  Soup datasoup = Constructors.NewSoup();
	  datasoup.SetNamedTag("id", signal.GetName());
	  datasoup.SetNamedTag("state", signal.MainState);
	  datasoup.SetNamedTag("stateALS", signal.MainStateALS);
    datasoup.SetNamedTag("limit", signal.speed_limit);
    datasoup.SetNamedTag("default_state", signal.GetSignalState());
    datasoup.SetNamedTag("train_open", signal.train_open);
    datasoup.SetNamedTag("shunt_open", signal.shunt_open);
    datasoup.SetNamedTag("wrong_dir", signal.wrong_dir);
    datasoup.SetNamedTag("barrier_closed", signal.barrier_closed);
    datasoup.SetNamedTag("prigl_open", signal.prigl_open);
    datasoup.SetNamedTag("x_mode", signal.x_mode);
    datasoup.SetNamedTag("type_msg", "sU_SetSettings");
	  MultiplayerGame.BroadcastGameplayMessage("sU_signals", "mult_client", datasoup);
  } 
  
  final void RaiseSync(bool includeInverse)
  {
    ClearMessages("ZXDSP", "RaiseSpanSync");
    if (includeInverse) {
      ClearMessages("ZXDSP", "RaiseBothSpanSync");
      PostMessage(me, "ZXDSP", "RaiseBothSpanSync", 0.0);
    } else PostMessage(me, "ZXDSP", "RaiseSpanSync", 0.0);
  }
  
  final void RaiseUIUpdate(void)
  {
    ClearMessages("ZXDSP", "RaiseSpanUIUpdate");
    PostMessage(me, "ZXDSP", "RaiseSpanUIUpdate", 0.0);
  }

  public mandatory Soup GetProperties(void)
  {
    Soup soup = Constructors.NewSoup();
    int i, count = signals.size();
    soup.SetNamedTag("SignalsCount", count);
    for (i = 0; i < count; ++i) 
      soup.SetNamedTag("SignalId." + i, signals[i].GetGameObjectID());
    soup.SetNamedTag("Available", available);
    return soup;
  }

  public mandatory bool SetProperties(Soup soup)
  {
    GameObjectID signalId;
    int i, count = soup.GetNamedTagAsInt("SignalsCount");
    signals = new zxSignal[count];
    for (i = 0; i < count; ++i) {
      if (!(signalId = soup.GetNamedTagAsGameObjectID("SignalId." + i))) return false;
      if (!(signals[i] = cast<zxSignal>zx_DSP_Helper.GetGameObjectWait(signalId))) return false;
    }
    available = soup.GetNamedTagAsBool("Available");
    return true;
  }
  
  //Загрузка настроек из маркера
  public mandatory void Init(zx_DSP_SpanInfo info, bool facing) 
  {
    if (info) {
      int dir = info.GetSpanProperty(zx_DSP_SpanInfo.PROP_DIRECTION);
      available = (dir & zx_DSP_SpanInfo.DIRECT_FROWARD and facing)
               or (dir & zx_DSP_SpanInfo.DIRECT_BACKWARD and !facing);
    } else available = true; 
  }
  
};

//=============================================================================
// Name: zx_DSP_SpanAB
// Desc: Реализация перегона с автоматической блокировкой АБ
//=============================================================================
class zx_DSP_SpanAB isclass zx_DSP_SpanBase
{
  
  //Флаги залипших кнопок.
  define int BUTT_CONSENT               = 1 << 0;                       //Кнопка дачи согласия
  define int BUTT_AUXILIARY             = 1 << 1;                       //Кнопка вспомогательного отправления
  
  define float BUTTOMTIMEOUT            = 60.0;                         //Таймаут для залипающих кнопок. По тсьечению они отлипают автоматически.

  bool direction = true;                                                //Установленное направление движения соответствует направлению полуперегона / Станция установлена на приём
  bool oneButton;                                                       //Разворот перегона с одной кнопки (нет дачи согласия)
  int buttons;                                                          //Маска флагов состояния заливания кнопок
  int timeoutid;                                                        //Идентификатор таймаута; 

	int RequestSwitch(bool force, zx_DSP_UIActionContext context);

	public int GetType() 
  {
		return zx_DSP_SpanInfo.TYPE_AUTOLOCK;
	}
  
  final string SignalIcon(Signal signal, string green, string yellow, string red)                                       
  {
    if (signal.GetSignalState() == Signal.GREEN) return green;
    else if (signal.GetSignalState() == Signal.YELLOW) return yellow;
    return red;
  }

  public void MakeUIHTML(HTMLBuffer buffer, zx_DSP_MakeHTMLContext context)
  {
    StringTable strtable = context.GetStringTable();
    zx_DSP_SpanAB inversespan = cast<zx_DSP_SpanAB>inverse;
    bool isresponsible = context.CheckResponsible(me); 
    bool canturn = !isresponsible and inversespan.available and direction and !inversespan.direction; 
    bool canconsent = !isresponsible and available and !direction and inversespan.direction and !oneButton;
    bool canauxiliaryin = !isresponsible and available and (inversespan.direction or !direction);  
    bool canauxiliaryout = !isresponsible and inversespan.available and (!inversespan.direction or direction);
    
    string inverseinsignalname = inverse.signals[0].privateName;
    if (station != inverse.station) inverseinsignalname = inverseinsignalname + "@" + inverse.station.name;     

   
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.StartCell());
          if (inverse.available and !inversespan.direction and direction and inversespan.buttons & BUTT_CONSENT) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_ge", 16, 16), strtable.GetString("tooltip-spanstatus-consent")));  
          else if (inverse.available and inversespan.direction and !direction and inversespan.buttons & BUTT_AUXILIARY) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_ye", 16, 16), strtable.GetString("tooltip-spanstatus-consent")));  
          else if (!direction and (cast<zx_DSP_SpanAB>inverse).direction) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_g", 16, 16), strtable.GetString("tooltip-spanstatus-directout")));
          else if (direction and !(cast<zx_DSP_SpanAB>inverse).direction) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_y", 16, 16), strtable.GetString("tooltip-spanstatus-directin")));
          else buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_gr", 16, 16), strtable.GetString("tooltip-spanstatus-unknown")));
        buffer.Print(HTMLWindow.EndCell()); 
        buffer.Print(HTMLWindow.StartCell("width=100%"));
          buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(signals[0]), signals[0].privateName, strtable.GetString("tooltip-cameratosignal")));
          if (!direction and inversespan.direction)  buffer.Print(HTMLWindow.MakeFontColor("&nbsp;&gt;&gt;&gt;&nbsp;", "00ff00"));                                     //todo: добавить стили цвета
          else if (direction and !inversespan.direction) buffer.Print(HTMLWindow.MakeFontColor("&nbsp;&lt;&lt;&lt;&nbsp;", "ffff00"));
          else if (!direction) buffer.Print("&nbsp;&gt;&gt;&gt;&nbsp;");                                     
          else if (direction) buffer.Print("&nbsp;&lt;&lt;&lt;&nbsp;");
          else buffer.Print("&nbsp;---&nbsp;");
          buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(inverse.signals[0]), inverseinsignalname, strtable.GetString("tooltip-cameratosignal")));
          if (station != inverse.station and (!MultiplayerGame.IsActive() or MultiplayerGame.IsServer() or inverse.station.availableControl)) {
            buffer.Print("&nbsp;"); 
            buffer.Print(HTMLWindow.StartFontColor(context.GetCore().StyleActionText)); 
            buffer.Print(HTMLWindow.MakeLink(context.MakeShowTabURL(inverse.station.index, zx_DSP_Station.CONTROLTAB_SPAN), "⮞", strtable.GetString("tooltip-tospanliststation")));
            buffer.Print(HTMLWindow.EndFontColor()); 
          }
        buffer.Print(HTMLWindow.EndCell()); 
        buffer.Print(HTMLWindow.StartCell());
          if (canturn) buffer.Print(HTMLWindow.MakeLink(context.MakeActionURL(me, "turn"), context.MakeHTMLImage("span-turn-32-16", 32, 16), strtable.GetString("tooltip-turnspan")));
          else buffer.Print(context.MakeHTMLImage("span-turn-disabled-32-16", 32, 16)); 
        buffer.Print(HTMLWindow.EndCell()); 
        buffer.Print(HTMLWindow.StartCell());
          if (canconsent and !(buttons & BUTT_CONSENT)) buffer.Print(HTMLWindow.MakeLink(context.MakeActionURL(me, "consent"), context.MakeHTMLImage("span-consent-on-32-16", 32, 16), strtable.GetString("tooltip-spanconsent")));
          else if (canconsent) buffer.Print(HTMLWindow.MakeLink(context.MakeActionURL(me, "consent-cancel"), context.MakeHTMLImage("span-consent-off-32-16", 32, 16), strtable.GetString("tooltip-spanconsent-cancel")));
          else if (!oneButton) buffer.Print(context.MakeHTMLImage("span-consent-disabled-32-16", 32, 16));
          else buffer.Print(context.MakeHTMLImage("empty", 32, 16));  
        buffer.Print(HTMLWindow.EndCell()); 




        buffer.Print(HTMLWindow.StartCell());
          if (canauxiliaryin) buffer.Print(HTMLWindow.MakeLink(context.MakeResponsibleActionURL(me, "auxiliaryin"), context.MakeHTMLImage("span-auxiliary-in-32-16", 32, 16), strtable.GetString("tooltip-auxiliaryin")));
          else buffer.Print(context.MakeHTMLImage("span-auxiliary-in-disabled-32-16", 32, 16)); 
        buffer.Print(HTMLWindow.EndCell());
        
         
        buffer.Print(HTMLWindow.StartCell()); 
          if (canauxiliaryout and !(buttons & BUTT_AUXILIARY)) buffer.Print(HTMLWindow.MakeLink(context.MakeResponsibleActionURL(me, "auxiliaryout"), context.MakeHTMLImage("span-auxiliary-out-on-32-16", 32, 16), strtable.GetString("tooltip-auxiliaryout")));
          else if (canauxiliaryout) buffer.Print(HTMLWindow.MakeLink(context.MakeActionURL(me, "auxiliaryout", "cancel"), context.MakeHTMLImage("span-auxiliary-out-off-32-16", 32, 16), strtable.GetString("tooltip-auxiliaryout-cancel")));
          else buffer.Print(context.MakeHTMLImage("span-auxiliary-out-disabled-32-16", 32, 16));
        buffer.Print(HTMLWindow.EndCell()); 
      buffer.Print(HTMLWindow.EndRow());
      if (isresponsible) {
        buffer.Print(HTMLWindow.StartRow());
          if (context.ResponsibleAction(me) == "auxiliaryout") buffer.Print(HTMLWindow.MakeCell(context.MakeResponsibleHTML(me, strtable.GetString("controller-confirm-auxiliaryout")), "colspan=6 align=right"));
          else buffer.Print(HTMLWindow.MakeCell(context.MakeResponsibleHTML(me, strtable.GetString("controller-confirm-auxiliaryin")), "colspan=6 align=right"));
        buffer.Print(HTMLWindow.EndRow());
      }
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.StartCell("colspan=6"));
          if (direction and !(cast<zx_DSP_SpanAB>inverse).direction) {
            buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(signals[0]), context.MakeHTMLImage(SignalIcon(signals[0], "st_g", "st_y", "st_r"), 16, 16) , strtable.GetString3("tooltip-cameratosignal-span", signals[0].privateName + "@" + signals[0].stationName, inverse.station.name, station.name)));
            int i, count = signals.size();
            for (i = 1; i < count; ++i)
              buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(signals[i]), context.MakeHTMLImage(SignalIcon(signals[i], "span-signal-green-8-16", "span-signal-yellow-8-16", "span-signal-red-8-16"), 8, 16) , strtable.GetString3("tooltip-cameratosignal-span", signals[i].privateName, inverse.station.name, station.name)));
          } else {
            int i, count = inverse.signals.size();
            for (i = count - 1; i > 0; --i)
              buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(inverse.signals[i]), context.MakeHTMLImage(SignalIcon(inverse.signals[i], "span-signal-green-8-16", "span-signal-yellow-8-16", "span-signal-red-8-16"), 8, 16) , strtable.GetString3("tooltip-cameratosignal-span", inverse.signals[i].privateName, station.name, inverse.station.name)));
            buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(inverse.signals[0]), context.MakeHTMLImage(SignalIcon(inverse.signals[0], "st_g", "st_y", "st_r"), 16, 16) , strtable.GetString3("tooltip-cameratosignal-span", inverse.signals[0].privateName + "@" + inverse.signals[0].stationName, station.name, inverse.station.name)));
          }
        buffer.Print(HTMLWindow.EndCell()); 
      buffer.Print(HTMLWindow.EndRow());
      
      if(errorMessage and errorMessage != "" and ((errorMessageViewTime >= 0 and errorMessageViewTime + 60.0 >= World.GetTimeElapsed()) or errorMessageViewTime < 0)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.StartCell("colspan=6"));
            buffer.Print(HTMLWindow.MakeFontColor(errorMessage, context.GetCore().StyleErrorText));
          buffer.Print(HTMLWindow.EndCell());
        buffer.Print(HTMLWindow.EndRow());
      } else if (errorMessage != "" and errorMessageViewTime >= 0) {
        errorMessage = null;
        errorMessageViewTime = -1;
      }
      
    buffer.Print(HTMLWindow.EndTable());
  }

  public void DoUIAction(string action, string argumentX, string argumentY, string argumentZ, zx_DSP_UIActionContext context) 
  {
    zx_DSP_SpanAB inversespan = cast<zx_DSP_SpanAB>inverse;
    errorMessage = null;
    errorMessageViewTime = -1;
    if ("consent" == action) {                                                                                //Кнопка дачи согласия
      if (available and !direction and inversespan.direction and !(buttons & BUTT_CONSENT)) {
        buttons = BUTT_CONSENT;
        context.Sync(me, false);
        context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_INFO, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANABCONSENT, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanabconsent", context.UserName(), signals[0].privateName), context.UserName(), true);
        PostMessage(me, "ZXDSP-ButtonTimeout", ++timeoutid + "|" + BUTT_CONSENT, BUTTOMTIMEOUT);  
      }
    } else if ("consent-cancel" == action) {                                                                  //Кнопка отмены согласия
      if (buttons & BUTT_CONSENT) {
        buttons = ECLLogic.SetBitMask(buttons, BUTT_CONSENT, false);
        context.Sync(me, false);
      }
    } else if ("turn" == action) {                                                                            //Кнопка разворота перегона
      if (!oneButton and !(inversespan.buttons & BUTT_CONSENT)) SetErrorMessage(context.GetCore().GetStringTable().GetString1("spanstate-noconsent", inversespan.station.name), context); 
      else {
        inversespan.buttons = 0;
        RequestSwitch(false, context);
        context.Sync(me, true);
        context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_INFO, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANABTURN, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanabturn", context.UserName(), signals[0].privateName), context.UserName(), true);
      }
    } else if ("auxiliaryin" == action) {
      if (inversespan.buttons & BUTT_AUXILIARY) {
        inversespan.RequestSwitch(true, context);
        inversespan.buttons = 0;
        context.Sync(me, true);
        context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_WARN, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANABAUXILARYIN, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanabauxilaryin", context.UserName(), signals[0].privateName), context.UserName(), true);
      } else SetErrorMessage(context.GetCore().GetStringTable().GetString1("spanstate-noauxiliaryout", inversespan.station.name), context);
    } else if ("auxiliaryout" == action) {
      if (argumentX != "cancel") {
        buttons = BUTT_AUXILIARY;
        context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_WARN, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANABAUXILARYOUT, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanabauxilaryout", context.UserName(), signals[0].privateName), context.UserName(), true);
        PostMessage(me, "ZXDSP-ButtonTimeout", ++timeoutid + "|" + BUTT_AUXILIARY, BUTTOMTIMEOUT);
      } else buttons = ECLLogic.SetBitMask(buttons, BUTT_AUXILIARY, false); 
      context.Sync(me, false);
    }
  }           

  public Soup GetSyncProperties(bool fully) 
  {
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("direction", direction);
    soup.SetNamedTag("buttons", buttons);
    return soup;
  }

  public void ApplySyncProperties(Soup soup) 
  {
    direction = soup.GetNamedTagAsInt("direction", direction);
    buttons = soup.GetNamedTagAsInt("buttons", buttons);
  }
  
  public void SpanInitialization(void) 
  {
    int i, count = signals.size();
    signals[0].wrong_dir = !direction;
    for (i = 1; i < count; ++i) {
      zxSignal_main sign = cast<zxSignal_main>signals[i];
      sign.predvhod = (1 == i);
      sign.SetPredvhod();
      sign.wrong_dir = !direction;
      sign.x_mode = direction and (sign.Type & zxSignal.ST_FLOAT_BLOCK);
      sign.stationName = signals[0].stationName;
      if (direction) {
        sign.MainState = zxIndication.STATE_R;
        sign.UpdateState(4, -1);
        sign.Sleep(0.001);
      } else {
        sign.MainState = sign.MainStateALS = zxIndication.STATE_Rx;
        sign.SetSignal(true);
      }
    }
    AddHandler(me, "ZXDSP-ButtonTimeout", "", "ButtonTimeoutHandler");
  }

  public int RequestBuildPath(zx_DSP_UIActionContext context) 
  {
    errorMessage = null;
    errorMessageViewTime = -1;
    if (direction) return RESULT_SUCCESS;                      //если пытаться делать неисправности, то правильнее спрашивать у своей половины, то есть (!inverse.direction)
    if (!oneButton) {
      //ошибка о необходимости ДС
      return RESULT_WRONGWAY;
    }
    int result = (cast<zx_DSP_SpanAB>inverse).RequestSwitch(false, context);
    if (RESULT_SUCCESS == result) {
      context.Sync(me, true);
      context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_INFO, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANABTURNBYPATH, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanabturnbypath", context.UserName(), inverse.signals[0].privateName), context.UserName(), true);
    }
    return result;
  }

	public Soup GetProperties(void) 
  {
		Soup soup = inherited();
		soup.SetNamedTag("direction", direction);
		soup.SetNamedTag("oneButton", oneButton);
		return soup;
	}

	public bool SetProperties(Soup soup) {
		bool ret = inherited(soup);
		direction = soup.GetNamedTagAsBool("direction", direction);
		oneButton = soup.GetNamedTagAsBool("oneButton", oneButton);
		return ret;
	}

	public void Init(zx_DSP_SpanInfo info, bool facing) {
		inherited(info, facing);
		if (info and info.GetSpanProperty(zx_DSP_SpanInfo.PROP_INITDIRECTION) == zx_DSP_SpanInfo.DIRECT_FROWARD) direction = facing;
		else if (info and info.GetSpanProperty(zx_DSP_SpanInfo.PROP_INITDIRECTION) == zx_DSP_SpanInfo.DIRECT_BACKWARD) direction = !facing;
		else if (signals.size() > inverse.signals.size()) direction = true;
		else if (signals.size() < inverse.signals.size()) direction = false;
		else direction = facing;
		oneButton = available and (!info or info.GetSpanProperty(zx_DSP_SpanInfo.PROP_TURNONEBUTTON));
	}

  int RequestSwitch(bool force, zx_DSP_UIActionContext context) {
    if (!direction) return RESULT_SUCCESS;                                        //?????? Не стоит ли добавить в проверку inverse.direction?
    if (!inverse.available) {
      SetErrorMessage(context.GetCore().GetStringTable().GetString("spanstate-onside"), context);
      return RESULT_ONESIDE;
    }
    if (!force) {
      if ((cast<zx_DSP_SpanAB>inverse).direction) {
        SetErrorMessage(context.GetCore().GetStringTable().GetString("spanstate-error"), context);
        return RESULT_BUSY;                                                                               //Если состояние не синхронизировано, то только вспомогательный режим
      }
      int i, count = signals.size();
      for (i = 1; i < count; ++i) {
        if (signals[i].MainState <= zxIndication.STATE_RWb) {
          SetErrorMessage(context.GetCore().GetStringTable().GetString("spanstate-busy"), context);
          return RESULT_BUSY;
        }
      } 

      //todo: отключить проверку, если первый проходной находится за обратным входным

      GSTrackSearch searcher = signals[signals.size() - 1].BeginTrackSearch(false);
      while (searcher.SearchNextObject() and !(searcher.GetObject() == inverse.signals[0])) {
        if (cast<Vehicle>searcher.GetObject()) {
          SetErrorMessage(context.GetCore().GetStringTable().GetString("spanstate-busy"), context);
          return RESULT_BUSY;
        }
      } 
    }
    if (context.GetCore().StationHasPathBehindSignal(inverse.station, inverse.signals[0], zx_DSP_JunctionInfo.STATE_PATH)) {
      SetErrorMessage(context.GetCore().GetStringTable().GetString("spanstate-oncomingpath"), context);
      return RESULT_LOCK;
    }

    //Противоположная станция становится на приём
    (cast<zx_DSP_SpanAB>inverse).direction = true;

    //Гаснут проходные
	  int i, count = signals.size();
	  signals[0].wrong_dir = true;
    SyncSignal(0);
	  for (i = 1; i < count; ++i) {
		  zxSignal sign = signals[i];
		  sign.MainState = sign.MainStateALS = zxIndication.STATE_Rx;
		  sign.wrong_dir = true;
		  sign.x_mode = false;
		  sign.SetSignal(true);
      SyncSignal(i);
	  }
	  signals[count - 1].CheckPrevSignals(false);

    //Включаются проходные
	  count = inverse.signals.size();
	  inverse.signals[0].wrong_dir = false;
	  inverse.signals[0].UpdateState(0, -1);                                  
	  for (i = 0; i < count; ++i) {
		  zxSignal sign = inverse.signals[i];
		  sign.MainState = zxIndication.STATE_R;
		  sign.wrong_dir = false;
		  sign.x_mode = sign.Type & zxSignal.ST_FLOAT_BLOCK;
		  sign.UpdateState(0, -1);                                                
	  }

    //Станция становится на отправление
    direction = false;

    return RESULT_SUCCESS;
  }
  
  final void ButtonTimeoutHandler(Message msg) 
  {
    if (msg.src == me) {
      string[] tokens = Str.Tokens(msg.minor, "|");
      if (Str.ToInt(tokens[0]) == timeoutid) {
        buttons = ECLLogic.SetBitMask(buttons, Str.ToInt(tokens[1]), false);
        PostMessage(me, "ZXDSP", "RaiseSpanSync", 0.0);
        PostMessage(me, "ZXDSP", "RaiseSpanUIUpdate", 0.0);
      }
    }
  }

};

//=============================================================================
// Name: zx_DSP_SpanAB
// Desc: Реализация перегона с полуавтоматической блокировкой ПАБ
//=============================================================================
class zx_DSP_SpanPAB isclass zx_DSP_SpanBase
{

  define int STATE_INITIAL      = 0;  //Начальное состояние перегона
  define int STATE_CONSENT      = 1;  //Получено согласие
  define int STATE_BUSY         = 2;  //Перегон занят (поезд отправлен)
  define int STATE_FIXATION     = 3;  //Прибытие зафиксировано 

  public bool sso = false;            //Счётчики осей. Флаг ставится только с той стороны, где стоит оборудование системы счёта
  public int counter = 0;             //счётчик осей
  int state = STATE_INITIAL;          //состояние перегона

  public void OnCounterChanged();

  public int GetType() 
  {
    return zx_DSP_SpanInfo.TYPE_SEMIAUTOLOCK;
  }
  
  final void MakeCounterHTML(HTMLBuffer buffe, zx_DSP_MakeHTMLContext context)
  {
    if (counter > 0) buffe.Print(context.MakeHTMLImage("span-counter-title", 24, 16));
    else if (counter < 0) buffe.Print(context.MakeHTMLImage("span-counter-title-negative", 24, 16));
    else buffe.Print(context.MakeHTMLImage("span-counter-title-null", 24, 16));
    if (counter) { 
      string strcounter = (string)counter, symbol = "";
      int i, count = strcounter.size();
      for (i = 0; i < count; ++i) {
        symbol[0, 1] = strcounter[i, i + 1]; 
        if (symbol == "-") buffe.Print(context.MakeHTMLImage("span-counter-minus", 8, 16));
        else if (counter > 0) buffe.Print(context.MakeHTMLImage("span-counter-num-" + symbol, 8, 16));
        else buffe.Print(context.MakeHTMLImage("span-counter-num-" + symbol + "-n", 8, 16));
      }
    } else buffe.Print(context.MakeHTMLImage("span-counter-num-null", 8, 16));
  }

  public void MakeUIHTML(HTMLBuffer buffer, zx_DSP_MakeHTMLContext context)
  {
    StringTable strtable = context.GetStringTable();
    zx_DSP_SpanPAB inversespan = cast<zx_DSP_SpanPAB>inverse;
    bool isresponsible = context.CheckResponsible(me); 
    
    int colcount = 3;
    bool canarrival = !isresponsible and state == STATE_FIXATION and inversespan.state == STATE_INITIAL;
    bool canconsent = !isresponsible and inversespan.available and inversespan.state == STATE_INITIAL;
    bool canfixation  = !isresponsible and state == STATE_BUSY;  
    bool canreset  = !isresponsible and counter and context.GetCore().IsUserSessionPermission();
    
    string inverseinsignalname = inverse.signals[0].privateName;
    if (station != inverse.station) inverseinsignalname = inverseinsignalname + "@" + inverse.station.name;     
    
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.StartCell("width=16"));
          if (state == STATE_INITIAL and inversespan.state == STATE_INITIAL) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_gr", 16, 16), strtable.GetString("tooltip-spanstatus-initial")));
          else if (state == STATE_CONSENT and inversespan.state == STATE_INITIAL) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_y", 16, 16), strtable.GetString("tooltip-spanstatus-consentin")));
          else if (state == STATE_INITIAL and inversespan.state == STATE_CONSENT) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_g", 16, 16), strtable.GetString("tooltip-spanstatus-consentout")));
          else if (state == STATE_BUSY or state == STATE_FIXATION) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_r", 16, 16), strtable.GetString("tooltip-spanstatus-busyin")));
          else buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_re", 16, 16), strtable.GetString("tooltip-spanstatus-busyout")));
        buffer.Print(HTMLWindow.EndCell()); 
        buffer.Print(HTMLWindow.StartCell());
          buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(signals[0]), signals[0].privateName, strtable.GetString("tooltip-cameratosignal")));
          if (state == STATE_INITIAL and inversespan.state == STATE_CONSENT) buffer.Print(HTMLWindow.MakeFontColor("&nbsp;&gt;&gt;&gt;&nbsp;", "00ff00"));                                     //todo: добавить стили цвета
          else if (state == STATE_INITIAL and (inversespan.state == STATE_BUSY or inversespan.state == STATE_FIXATION)) buffer.Print(HTMLWindow.MakeFontColor("&nbsp;&gt;&gt;&gt;&nbsp;", "ff0000"));                                     //todo: добавить стили цвета
          else if (inversespan.state == STATE_INITIAL and state == STATE_CONSENT) buffer.Print(HTMLWindow.MakeFontColor("&nbsp;&lt;&lt;&lt;&nbsp;", "ffff00"));
          else if (inversespan.state == STATE_INITIAL and (state == STATE_BUSY or state == STATE_FIXATION)) buffer.Print(HTMLWindow.MakeFontColor("&nbsp;&lt;&lt;&lt;&nbsp;", "ff0000"));                                     //todo: добавить стили цвета
          else buffer.Print("&nbsp;---&nbsp;");
          buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(inverse.signals[0]), inverseinsignalname, strtable.GetString("tooltip-cameratosignal")));
          if (station != inverse.station and (!MultiplayerGame.IsActive() or MultiplayerGame.IsServer() or inverse.station.availableControl)) {
            buffer.Print("&nbsp;"); 
            buffer.Print(HTMLWindow.StartFontColor(context.GetCore().StyleActionText)); 
            buffer.Print(HTMLWindow.MakeLink(context.MakeShowTabURL(inverse.station.index, zx_DSP_Station.CONTROLTAB_SPAN), "⮞", strtable.GetString("tooltip-tospanliststation")));
            buffer.Print(HTMLWindow.EndFontColor()); 
          }
        buffer.Print(HTMLWindow.EndCell());
        if (!sso and !inversespan.sso) {
          buffer.Print(HTMLWindow.StartCell("width=8"));
            if (state == STATE_FIXATION and inversespan.state == STATE_INITIAL) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("span-ind-fixation-on-8-16", 8, 16), strtable.GetString("tooltip-spanstatus-fixation-on"))); 
            else buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("span-ind-fixation-off-8-16", 8, 16), strtable.GetString("tooltip-spanstatus-fixation-off"))); 
          buffer.Print(HTMLWindow.EndCell()); 
          buffer.Print(HTMLWindow.StartCell("width=32"));
            if (canarrival) buffer.Print(HTMLWindow.MakeLink(context.MakeActionURL(me, "arrived"), context.MakeHTMLImage("span-arrival-32-16", 32, 16), strtable.GetString("tooltip-spanarrival")));
            else buffer.Print(context.MakeHTMLImage("span-arrival-disabled-32-16", 32, 16)); 
          buffer.Print(HTMLWindow.EndCell());
          colcount = 6; 
        } else if (sso) {
          buffer.Print(HTMLWindow.StartCell("width=" + (24 + ((string)counter).size() * 8)));
            buffer.Print("<nowrpa>");
            MakeCounterHTML(buffer, context);
            buffer.Print("</nowrpa>");
          buffer.Print(HTMLWindow.EndCell());
          buffer.Print(HTMLWindow.MakeCell(context.MakeHTMLImage("empty", 32, 16), "width=32"));        
          colcount = 6; 
        }
        buffer.Print(HTMLWindow.StartCell("width=32"));
          if (canconsent and state == STATE_INITIAL) buffer.Print(HTMLWindow.MakeLink(context.MakeActionURL(me, "consent"), context.MakeHTMLImage("span-consent-on-32-16", 32, 16), strtable.GetString("tooltip-spanconsent-semi")));
          else if (canconsent and state == STATE_CONSENT) buffer.Print(HTMLWindow.MakeLink(context.MakeActionURL(me, "consent", "cancel"), context.MakeHTMLImage("span-consent-off-32-16", 32, 16), strtable.GetString("tooltip-spanconsent-semi-cancel")));
          else buffer.Print(context.MakeHTMLImage("span-consent-disabled-32-16", 32, 16));
        buffer.Print(HTMLWindow.EndCell()); 
        if (!sso and !inversespan.sso) {
          buffer.Print(HTMLWindow.StartCell("width=32"));
            if (canfixation) buffer.Print(HTMLWindow.MakeLink(context.MakeResponsibleActionURL(me, "fixation"), context.MakeHTMLImage("span-fixation-32-16", 32, 16), strtable.GetString("tooltip-spanfixation")));
            else buffer.Print(context.MakeHTMLImage("span-fixation-disabled-32-16", 32, 16)); 
          buffer.Print(HTMLWindow.EndCell()); 
        } else if (sso) {
          buffer.Print(HTMLWindow.StartCell("width=32"));
            if (canreset) buffer.Print(HTMLWindow.MakeLink(context.MakeResponsibleActionURL(me, "resetcounter"), context.MakeHTMLImage("span-resetcounter-32-16", 32, 16), strtable.GetString("tooltip-spanresetcounter")));
            else buffer.Print(context.MakeHTMLImage("span-resetcounter-disabled-32-16", 32, 16)); 
          buffer.Print(HTMLWindow.EndCell()); 
        } 
      buffer.Print(HTMLWindow.EndRow());
      if (isresponsible) {
        buffer.Print(HTMLWindow.StartRow());
          if (context.ResponsibleAction(me) == "fixation") buffer.Print(HTMLWindow.MakeCell(context.MakeResponsibleHTML(me, strtable.GetString("controller-confirm-fixation")), "colspan=" + colcount + " align=right"));
          else buffer.Print(HTMLWindow.MakeCell(context.MakeResponsibleHTML(me, strtable.GetString("controller-confirm-resetcounter")), "colspan=" + colcount + " align=right"));
        buffer.Print(HTMLWindow.EndRow());
      }
      if(errorMessage and errorMessage != "" and ((errorMessageViewTime >= 0 and errorMessageViewTime + 60.0 >= World.GetTimeElapsed()) or errorMessageViewTime < 0)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.StartCell("colspan=" + colcount));
            buffer.Print(HTMLWindow.MakeFontColor(errorMessage, context.GetCore().StyleErrorText));
          buffer.Print(HTMLWindow.EndCell());
        buffer.Print(HTMLWindow.EndRow());
      } else if (errorMessage != "" and errorMessageViewTime >= 0) {
        errorMessage = null;
        errorMessageViewTime = -1;
      }
    buffer.Print(HTMLWindow.EndTable());
  }

  public void DoUIAction(string action, string argumentX, string argumentY, string argumentZ, zx_DSP_UIActionContext context)
  {
    zx_DSP_SpanPAB inversespan = cast<zx_DSP_SpanPAB>inverse;
    if ("consent" == action) {
      if ("cancel" == argumentX) {
        if (STATE_CONSENT == state) {
          signals[0].wrong_dir = true;              
          state = STATE_INITIAL;
          context.Sync(me, false);
          context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_INFO, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANPABCONSENTCANCEL, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanpabconsentcancel", context.UserName(), signals[0].privateName), context.UserName(), true);
        }
      } else {
        if (STATE_INITIAL == state and STATE_INITIAL == inversespan.state) {
          signals[0].wrong_dir = false;             
          state = STATE_CONSENT;
          context.Sync(me, false);
          context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_INFO, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANPABCONSENT, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanpabconsent", context.UserName(), signals[0].privateName), context.UserName(), true);
        }
      }
    } else if ("arrived" == action) {
      if (STATE_FIXATION == state) {
        if (!inverse.available) state = STATE_CONSENT;
        else state = STATE_INITIAL;
        signals[0].wrong_dir = state == STATE_INITIAL;                
        context.Sync(me, false);
        context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_INFO, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANPABARRIVED, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanpabarrived", context.UserName(), signals[0].privateName), context.UserName(), true);
      }
    } else if ("fixation" == action) {
      if (STATE_BUSY == state) {
        state = STATE_FIXATION;
        context.Sync(me, false);
        context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_WARN, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANPABFIXATION, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanpabfixation", context.UserName(), signals[0].privateName), context.UserName(), true);
      }
    } else if ("resetcounter" == action) {
      if (sso and counter != 0 and context.GetCore().IsUserSessionPermission()) {
        counter = 0;
        OnCounterChanged();
        context.GetCore().AddLogEntry(zx_DSP_LogEntry.LEVEL_WARN, zx_DSP_LogEntry.SOURCE_SPAN, zx_DSP_LogEntry.CODE_SPANPABRESETCOUNTER, context.GetStation().index, index, context.GetCore().GetStringTable().GetString2("journal-spanpabresetcounter", context.UserName(), signals[0].privateName), context.UserName(), true);
      }
    }
  }

  public Soup GetSyncProperties(bool fully) 
  {
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("state", state);
    soup.SetNamedTag("counter", counter);
    return soup;
  }

  public void ApplySyncProperties(Soup soup) 
  {
    state = soup.GetNamedTagAsInt("state", state);
    counter = soup.GetNamedTagAsInt("counter", counter);
    signals[0].wrong_dir = state == STATE_INITIAL;              
  }

  public void SpanInitialization(void) 
  {
    if (available and !inverse.available) state = STATE_CONSENT;                      //Если перегон односторонний, то согласие есть всегда
    signals[0].wrong_dir = state == STATE_INITIAL;  
  }

  public int RequestBuildPath(zx_DSP_UIActionContext context) 
  {
    if (STATE_CONSENT == state) return RESULT_SUCCESS;
    return RESULT_LOCK;
  }

  public void OnBuildOutPath(zx_DSP_UIActionContext context) 
  {
    if (STATE_CONSENT == state) {
      signals[0].wrong_dir = false;                                                 
      state = STATE_BUSY;
      context.Sync(me, false);
    }
  }

  public void OnReleaseInPathFirstSection(zx_DSP_UIActionContext context) 
  {
    if (STATE_BUSY == state and !sso and !(cast<zx_DSP_SpanPAB>inverse).sso) {                                                               //todo: проверить предмаршрутный участок
      state = STATE_FIXATION;
      context.Sync(me, false);
      RaiseUIUpdate();                                                                          //!!!!!По хорошему, обновление при zx_DSP_UIActionContext должен обеспечить вызывающий код, так что это временное решение.
    }
  }

  public void OnCounterChanged() {
    zx_DSP_SpanPAB inversespan = cast<zx_DSP_SpanPAB>inverse;
    if (0 == counter and (STATE_BUSY == state or STATE_FIXATION == state or STATE_BUSY == inversespan.state or STATE_FIXATION == inversespan.state)) {
      if (!inversespan.available) state = STATE_CONSENT;
      else state = STATE_INITIAL;
      if (!available) inversespan.state = STATE_CONSENT;
      else inversespan.state = STATE_INITIAL;
      signals[0].wrong_dir = state == STATE_INITIAL;                
      inversespan.signals[0].wrong_dir = inversespan.state == STATE_INITIAL;                
      RaiseSync(true);
    }
  }

  public Soup GetProperties(void) 
  {
    Soup soup = inherited();
    soup.SetNamedTag("sso", sso);
    return soup;
  }

  public bool SetProperties(Soup soup) 
  {
    bool ret = inherited(soup);
    sso = soup.GetNamedTagAsBool("sso", sso);
    return ret;
  }
  
  public void Init(zx_DSP_SpanInfo info, bool facing) 
  {
    inherited(info, facing);
    sso = info.GetSpanProperty(zx_DSP_SpanInfo.PROP_AUTOARRIVAL) and !facing;
  }

  //todo: вынести в хелпер
  //Поиск подвижной еденицы в районе светофора
  //sign - сигнал, для которого производится проверка
  //возвращает 1 - ПС перед светофором, 2 - ПС за светофором, 0 - не найден
  public final int CheckTrainNearSignal(zxSignal sign, Train train)
  {
    int i, ret;
    for (i = 0; i < 2; ++i) {
      GSTrackSearch searcher = sign.BeginTrackSearch((bool)i);
      MapObject mapobject = searcher.SearchNext();
      float distance = searcher.GetDistance();
      while (mapobject and distance < 50 and !mapobject.isclass(Vehicle)) {
        mapobject = searcher.SearchNext();
        distance = searcher.GetDistance();
      }
      if (mapobject and mapobject.isclass(Vehicle)) {
        Train foundtrain = (cast<Vehicle>mapobject).GetMyTrain();
        if (foundtrain == train) return i + 1; 
      } 
    }
    return 0;
  }

  void OnSignalLeave(Message msg) {
    if (!MultiplayerGame.IsActive() or MultiplayerGame.IsServer()) {
      zx_DSP_SpanPAB inversespan = cast<zx_DSP_SpanPAB>inverse;
      zxSignal sign = cast<zxSignal>msg.dst;
      Train train = cast<Train>msg.src;
      int side = CheckTrainNearSignal(sign, train);
      if (side) {
        Vehicle[] vehs = train.GetVehicles();
        int axelcount, i, count = vehs.size();
        for (i = 0; i < count; ++i)
          axelcount = axelcount + ALSN_VehicleProperties.PullAxleCount(vehs[i]);
        if (counter >= 0) {
          if (side == 1) counter = counter + axelcount;
          else if (side == 2) counter = counter - axelcount;
        }
        OnCounterChanged();
        PostMessage(me, "ZXDSP", "RaiseSpanSync", 0.0);
        PostMessage(me, "ZXDSP", "RaiseSpanUIUpdate", 0.0);
      }
    }
  }

  public void AddHandlers() {
    if (sso) {
      AddHandler(signals[0], "Object", "Leave", "OnSignalLeave");
      AddHandler(inverse.signals[0], "Object", "Leave", "OnSignalLeave");
    }
  }

};

//=============================================================================
// Name: zx_DSP_SpanNull
// Desc: Реализация пустого перегона (без проходных светофоров)
//=============================================================================
final class zx_DSP_SpanNull isclass zx_DSP_SpanBase
{
  public int GetType() 
  {
    return zx_DSP_SpanInfo.TYPE_NULL;
  }

  public void MakeUIHTML(HTMLBuffer buffer, zx_DSP_MakeHTMLContext context)
  {
    StringTable strtable = context.GetStringTable();
    zx_DSP_SpanNull inversespan = cast<zx_DSP_SpanNull>inverse;

    string inverseinsignalname = inverse.signals[0].privateName;
    if (station != inverse.station) inverseinsignalname = inverseinsignalname + "@" + inverse.station.name;

    bool inPath = context.GetCore().StationHasPathBehindSignal(inverse.station, inverse.signals[0], zx_DSP_JunctionInfo.STATE_PATH);
    bool outPath = context.GetCore().StationHasPathBehindSignal(station, signals[0], zx_DSP_JunctionInfo.STATE_PATH);

    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.StartCell("width=16"));
          if (inPath) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_y", 16, 16), strtable.GetString("tooltip-spanstatus-oncoming")));
          else if (outPath) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_g", 16, 16), strtable.GetString("tooltip-spanstatus-along")));
          else buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_gr", 16, 16), strtable.GetString("tooltip-spanstatus-free")));
        /*buffer.Print(HTMLWindow.EndCell());
        buffer.Print(HTMLWindow.StartCell("width=16"));*/
          if (zxIndication.STATE_RWb == inverse.signals[0].MainState) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_we", 16, 16), strtable.GetString("tooltip-nextinsignalstatus")));
          else if (Signal.GREEN == inverse.signals[0].GetSignalState()) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_g", 16, 16), strtable.GetString("tooltip-nextinsignalstatus")));
          else if (Signal.YELLOW == inverse.signals[0].GetSignalState()) buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_y", 16, 16), strtable.GetString("tooltip-nextinsignalstatus")));
          else buffer.Print(HTMLWindow.MakeLink("", context.MakeHTMLImage("st_r", 16, 16), strtable.GetString("tooltip-nextinsignalstatus")));
        buffer.Print(HTMLWindow.EndCell());
        buffer.Print(HTMLWindow.StartCell());
          buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(signals[0]), signals[0].privateName, strtable.GetString("tooltip-cameratosignal")));
          if (inPath) buffer.Print(HTMLWindow.MakeFontColor("&nbsp;&lt;&lt;&lt;&nbsp;", "ffff00"));
          else if (outPath) buffer.Print(HTMLWindow.MakeFontColor("&nbsp;&gt;&gt;&gt;&nbsp;", "00ff00"));
          else buffer.Print("&nbsp;---&nbsp;");
          buffer.Print(HTMLWindow.MakeLink(context.MakeCameraToSignalURL(inverse.signals[0]), inverseinsignalname, strtable.GetString("tooltip-cameratosignal")));
          if (station != inverse.station and inverse.station.CheckMyPermission()) {
            buffer.Print("&nbsp;"); 
            buffer.Print(HTMLWindow.StartFontColor(context.GetCore().StyleActionText)); 
            buffer.Print(HTMLWindow.MakeLink(context.MakeShowTabURL(inverse.station.index, zx_DSP_Station.CONTROLTAB_SPAN), "⮞", strtable.GetString("tooltip-tospanliststation")));
            buffer.Print(HTMLWindow.EndFontColor()); 
          }
        buffer.Print(HTMLWindow.EndCell());  
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(strtable.GetString("string-span-autocontrol"), context.GetCore().StyleDisabledLabel), "align=right"));
      buffer.Print(HTMLWindow.EndRow());
      if (errorMessage and errorMessage != "" and ((errorMessageViewTime >= 0 and errorMessageViewTime + 60.0 >= World.GetTimeElapsed()) or errorMessageViewTime < 0)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.StartCell("colspan=3"));
            buffer.Print(HTMLWindow.MakeFontColor(errorMessage, context.GetCore().StyleErrorText));
          buffer.Print(HTMLWindow.EndCell());
        buffer.Print(HTMLWindow.EndRow());
      } else if (errorMessage != "" and errorMessageViewTime >= 0) {
        errorMessage = null;
        errorMessageViewTime = -1;
      }
    buffer.Print(HTMLWindow.EndTable());
  }

  public Soup GetSyncProperties(bool fully)
  {
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("wrong_dir", signals[0].wrong_dir);

    return soup;
  }

  public void ApplySyncProperties(Soup soup)
  {
    signals[0].wrong_dir = soup.GetNamedTagAsBool("wrong_dir");
  }

  public void SpanInitialization()
  {
    signals[0].wrong_dir = true;
  }

  public int RequestBuildPath(zx_DSP_UIActionContext context) 
  {
    if (!available) {
      SetErrorMessage(context.GetCore().GetStringTable().GetString("spanstate-onside"), context);
      return RESULT_ONESIDE;
    }
    if (context.GetCore().StationHasPathBehindSignal(station, signals[0], zx_DSP_JunctionInfo.STATE_PATH)) {
      SetErrorMessage(context.GetCore().GetStringTable().GetString("spanstate-oncomingpath"), context);
      return RESULT_LOCK;
    }
    signals[0].wrong_dir = false;

    return RESULT_SUCCESS;
  }

  public void OnBuildOutPath(zx_DSP_UIActionContext context) 
  {
    signals[0].wrong_dir = false;
    context.Sync(me, false);
  }

  public void OnCancelOutPath(zx_DSP_UIActionContext context)
  {
    signals[0].wrong_dir = true;
    context.Sync(me, false);
  }

  public void OnReleaseInPathFirstSection(zx_DSP_UIActionContext context) 
  {
    signals[0].wrong_dir = true;
  }
};
