//=============================================================================
// File: zx_dsp_junctionforms.gs
// Desc: Классы форм для свойства стрелки и списка стрелок для ТЧМ
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_classes.gs"
include "zx_dsp_core.gs"
include "htmlbuffer.gs"
include "browser.gs"

//=============================================================================
// Name: zx_DSP_JunctionSettingsForm
// Desc: Представляет окно свойств стрелочного перевода
//=============================================================================
final class zx_DSP_JunctionSettingsForm isclass GameObject
{
  zx_DSP_Core _core;                    //Тут ссылка на ядро
  Browser _browser;                     //Собственно само окно браузера
  StringTable _strtable;                //Таблица строк
  zx_DSP_JunctionInfo _junction;        //Информация о стрелке, для которой выводится информация
  
  
  
  //=============================================================================
  // Name: Init
  // Desc: Инициализирует браузер для свойств стрелки 
  // Parm: juncInfo – Объект zx_DSP_JunctionInfo, содержащий информацию о 
  //       стрелочном переводе.                                         
  // Parm: core – Объект zx_DSP_Core, представляющий ядро маршрутизации. 
  // Parm: strTable – Таблица строк для интерфейса браузера. 
  // Retn: Инициалищированный объект zx_DSP_JunctionSettingsBrowser.
  //=============================================================================
  public zx_DSP_JunctionSettingsForm Init(zx_DSP_JunctionInfo juncInfo, zx_DSP_Core core, StringTable strTable);
  
  //=============================================================================
  // Name: Update
  // Desc: Обновляет браузер свойст стрелки. 
  //=============================================================================
  public void Update(void); 
  
  //=============================================================================
  // Name: BringToFront
  // Desc: Восстанавливает окно, если оно было свёрнуто, и перемещает его на
  //       передний план, поверх всех окон. 
  //=============================================================================
  public void BringToFront(void);

  //=============================================================================
  // Name: Dispose
  // Desc: Освобождает все ресурсы для этого объекта 
  //=============================================================================
  public void Dispose(void);
  
  //=============================================================================
  // Name: OnLinkClick
  // Desc: Вызывается при нажатии на ссылке в браузере 
  // Parm: action — Действие, которое необходимо выполнить
  //=============================================================================
  void OnLinkClick(string action);

	//
	// РЕАЛИЗАЦИЯ
	//
  
  //Возвращает номер стрелки или ей имя
  //union — если true, то возвращается номер стрелки
  //        вместе с именем. Если false, то только
  //        номер стрелки. В обоих случаях, если номер
  //        стрелке не на назначен, то возвращается
  //        только имя.
  string GetJunctionNumberOrName(bool union)
  {
    if(_junction.number != 0) {
      if(union) return _strtable.GetString2("junction-number-name", _junction.number, _junction.jnc.GetLocalisedName());
      else return _strtable.GetString1("junction-number", _junction.number);  
    }  
    return _junction.jnc.GetLocalisedName();
  }
  

  public zx_DSP_JunctionSettingsForm Init(zx_DSP_JunctionInfo juncInfo, zx_DSP_Core core, StringTable strTable)
  {
    if(_browser) return me;
    int height = Interface.GetDisplayHeight();
    int width = Interface.GetDisplayWidth();
    
    _core = core;
    _strtable = strTable;
    _junction = juncInfo;
    
    _browser = Constructors.NewBrowser(); 
    _browser.SetWindowStyle(Browser.STYLE_DEFAULT);
    _browser.SetScrollEnabled(false);
    _browser.SetCloseEnabled(true);
    _browser.SetWindowPriority(Browser.BP_Window);
    _browser.SetButtonOverlayStyle(Browser.BS_None, Browser.BS_OK);
    _browser.SetWindowTitle(_strtable.GetString1("junctionsettings-caption", GetJunctionNumberOrName(false)));
    _browser.SetWindowRect(width / 2 - 160, height / 2 - 90, width / 2 + 160, height / 2 + 90);
    
    me.AddHandler(_browser, "Browser", "Closed", "BrowserClosedHandler");
    me.AddHandler(_browser, "Browser", "button-ok", "BrowserClickOkHandler");
    me.AddHandler(_browser, "Browser-URL", "", "BrowserURLHandler");
    
    
    
    Update();
    return me;
  }    


  public void Update(void)
  {
	  if (!_browser) {
		  return;
	  }
    int currdirrect = _junction.jnc.GetDirection();
    HTMLBuffer buff = HTMLBufferStatic.Construct();
    buff.Print(HTMLWindow.StartTable());
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-name") + ":"));
        buff.Print(HTMLWindow.StartCell());
          buff.Print(HTMLWindow.MakeLink("live://cameratojunction", GetJunctionNumberOrName(true), _strtable.GetString("tooltip-cameratojunction")));
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-station") + ":"));
        if(_junction.station) buff.Print(HTMLWindow.MakeCell(_junction.station.name));
        else buff.Print(HTMLWindow.MakeCell(""));
      buff.Print(HTMLWindow.EndRow());
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-type") + ":"));
        if(_junction.type & zx_DSP_JunctionInfo.JNC_LEFT) buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-type-left")));
        else if(_junction.type & zx_DSP_JunctionInfo.JNC_RIGHT) buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-type-right")));
        else buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-type-symm")));
      buff.Print(HTMLWindow.EndRow());
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-toggle") + ":"));
        if(_junction.type & zx_DSP_JunctionInfo.JNC_MANUAL) buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-toggle-manual")));
        else buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-toggle-auto")));
      buff.Print(HTMLWindow.EndRow());
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-direction") + ":"));
        buff.Print(HTMLWindow.StartCell());
          if(!(_junction.state & zx_DSP_JunctionInfo.STATE_CUTUP) and _junction.IsSynchronized()) {
            if(currdirrect == JunctionBase.DIRECTION_LEFT) {
              if(_junction.type & zx_DSP_JunctionInfo.JNC_LEFT) buff.Print(_core.MakeHTMLImage("jnc-f-l-l-24-16", 24, 16));
              else if(_junction.type & zx_DSP_JunctionInfo.JNC_RIGHT) buff.Print(_core.MakeHTMLImage("jnc-f-r-f-24-16", 24, 16));
              else buff.Print(_core.MakeHTMLImage("jnc-f-s-l-24-16", 24, 16));
              buff.Print("&nbsp;");
              if(currdirrect == _junction.defaultDirection) buff.Print(_strtable.GetString("junction-direction-plus-left"));
              else buff.Print(_strtable.GetString("junction-direction-minus-left")); 
            } else if(currdirrect == JunctionBase.DIRECTION_RIGHT){
              if(_junction.type & zx_DSP_JunctionInfo.JNC_LEFT) buff.Print(_core.MakeHTMLImage("jnc-f-l-f-24-16", 24, 16));
              else if(_junction.type & zx_DSP_JunctionInfo.JNC_RIGHT) buff.Print(_core.MakeHTMLImage("jnc-f-r-r-24-16", 24, 16));
              else buff.Print(_core.MakeHTMLImage("jnc-f-s-r-24-16", 24, 16));
              buff.Print("&nbsp;"); 
              if(currdirrect == _junction.defaultDirection) buff.Print(_strtable.GetString("junction-direction-plus-right"));
              else buff.Print(_strtable.GetString("junction-direction-minus-right")); 
            }
          } else {
            if(_junction.type & zx_DSP_JunctionInfo.JNC_LEFT) buff.Print(_core.MakeHTMLImage("jnc-f-l-u-24-16", 24, 16));
            else if(_junction.type & zx_DSP_JunctionInfo.JNC_RIGHT) buff.Print(_core.MakeHTMLImage("jnc-f-r-u-24-16", 24, 16));
            else buff.Print(_core.MakeHTMLImage("jnc-f-s-u-24-16", 24, 16));
            buff.Print("&nbsp;");
            buff.Print(_strtable.GetString("junction-direction-unknown"));
          }
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
      if(_junction.type & zx_DSP_JunctionInfo.JNC_MANUAL and !(_junction.state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT | zx_DSP_JunctionInfo.STATE_TRAIN | 
      zx_DSP_JunctionInfo.STATE_LOCK | zx_DSP_JunctionInfo.STATE_MANUALLOCK | zx_DSP_JunctionInfo.STATE_ERROR | zx_DSP_JunctionInfo.STATE_LOCALCONTROL))) {
        buff.Print(HTMLWindow.StartRow());
          buff.Print(HTMLWindow.MakeCell(""));
          buff.Print(HTMLWindow.StartCell());
            if(currdirrect == JunctionBase.DIRECTION_LEFT) {
              if(_junction.type & zx_DSP_JunctionInfo.JNC_LEFT) buff.Print(HTMLWindow.StartLink("live://toggle", _strtable.GetString("tooltip-jnc-setdir-forward")));
              else if(_junction.type & zx_DSP_JunctionInfo.JNC_RIGHT) buff.Print(HTMLWindow.StartLink("live://toggle", _strtable.GetString("tooltip-jnc-setdir-rejection")));
              else buff.Print(HTMLWindow.StartLink("live://toggle", _strtable.GetString("tooltip-jnc-setdir-right")));
            } else if(currdirrect == JunctionBase.DIRECTION_RIGHT){
              if(_junction.type & zx_DSP_JunctionInfo.JNC_LEFT) buff.Print(HTMLWindow.StartLink("live://toggle", _strtable.GetString("tooltip-jnc-setdir-rejection")));
              else if(_junction.type & zx_DSP_JunctionInfo.JNC_RIGHT) buff.Print(HTMLWindow.StartLink("live://toggle", _strtable.GetString("tooltip-jnc-setdir-forward")));
              else buff.Print(HTMLWindow.StartLink("live://toggle", _strtable.GetString("ttooltip-jnc-setdir-left")));
            }
            if(currdirrect == _junction.defaultDirection) buff.Print(_core.MakeHTMLImage("direction-minus-16", 16));
            else buff.Print(_core.MakeHTMLImage("direction-plus-16", 16)); 
            buff.Print("&nbsp;"); 
            buff.Print(_strtable.GetString("junctionsettings-setdirection"));
          buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.EndRow());
      }
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-cutup") + ":"));
        if(_junction.state & zx_DSP_JunctionInfo.STATE_CUTUP) buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(_strtable.GetString("junctionsettings-cutup-yes"), _core.StyleErrorLabel)));
        else buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(_strtable.GetString("junctionsettings-cutup-no"), _core.StyleSuccessLabel)));
      buff.Print(HTMLWindow.EndRow());
      if(_junction.state & zx_DSP_JunctionInfo.STATE_CUTUP and _core.IsUserSessionPermission()) {
        buff.Print(HTMLWindow.StartRow());
          buff.Print(HTMLWindow.MakeCell(""));
          buff.Print(HTMLWindow.StartCell());
            buff.Print(HTMLWindow.MakeLink("live://cutup-reset", _strtable.GetString("junctionsettings-cutup-reset"), _strtable.GetString("tooltip-jnc-reset-cutup")));
          buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.EndRow());
      }
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.MakeCell(_strtable.GetString("junctionsettings-state") + ":"));
        if(_junction.state & zx_DSP_JunctionInfo.STATE_TRAIN) buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(_strtable.GetString("junctionsettings-state-trainz"), _core.StyleErrorLabel)));
        else if(_junction.state & (zx_DSP_JunctionInfo.STATE_TRAIN | zx_DSP_JunctionInfo.STATE_TRAIN)) buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(_strtable.GetString("junctionsettings-state-path"), _core.StyleErrorLabel)));
        else if(_junction.state & (zx_DSP_JunctionInfo.STATE_LOCK | zx_DSP_JunctionInfo.STATE_MANUALLOCK | zx_DSP_JunctionInfo.STATE_ERROR | zx_DSP_JunctionInfo.STATE_LOCALCONTROL)) buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(_strtable.GetString("junctionsettings-state-lock"), _core.StyleErrorLabel)));
        else buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(_strtable.GetString("junctionsettings-state-free"), _core.StyleSuccessLabel)));
      buff.Print(HTMLWindow.EndRow());
    buff.Print(HTMLWindow.EndTable());
    _browser.LoadHTMLString(_core.GetAsset(), buff.AsString());
  }
  
  public void BringToFront(void) 
  {
    _browser.RestoreWindow();
    _browser.BringToFront();
  }
  
  public void Dispose(void)
  {
    if(_browser) {
        _browser.CloseWindow();
        me.AddHandler(_browser, "Browser", "Closed", null);
        me.AddHandler(_browser, "Browser", "button-ok", null);
    }
    _core = null;
    _browser = null;
    _strtable = null;
    _junction = null;
  }

  void OnLinkClick(string action)
  {
    if(action == "cameratojunction") {        //Перевод камеры к стрелке
        World.SetCamera(_junction.jnc);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
    } else if(action == "toggle") {           //Переключение стрелки
      if (JunctionBase.DIRECTION_RIGHT != _junction.jnc.GetDirection()) 
        _core.HandJunctionSetDirection(_junction, JunctionBase.DIRECTION_RIGHT, _core.GetLocalPlayerName());
      else _core.HandJunctionSetDirection(_junction, JunctionBase.DIRECTION_LEFT, _core.GetLocalPlayerName());
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
    } else if(action == "cutup-reset") {          //Восставноление стрелки после вреза
		  _core.JunctionCutupReset(_junction);
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
    }
  }

  void BrowserURLHandler(Message msg)
  {
    if(msg.src == _browser and /* msg.src == msg.dst and сообщение шлётся бродкастом */ 
    msg.minor[0, 7] == "live://") OnLinkClick(msg.minor[7,]);
  }  
  
  void BrowserClosedHandler(Message msg)
  {
    if(msg.src == _browser and msg.src == msg.dst) {
      _junction.settingsBrowser = null;
      Dispose();
    }
  }
  
  void BrowserClickOkHandler(Message msg){ BrowserClosedHandler(msg); }
  
};



//=============================================================================
// Name: zx_DSP_JunctionSettingsForm
// Desc: Представляет окно со списком стрелак рядом с поездом игрока
//=============================================================================
final class zx_DSP_DriverJunctionListForm isclass GameObject
{

  //=============================================================================
  // Name: UPDATE_*
  // Desc: Определяет действие при обновлении окна
  //=============================================================================  
  public define int UPDATE_NORMAL       = 1;    //Обновление окна
  public define int UPDATE_NOVEHICLE    = 2;    //Обновление окна с выводом сообщения, что пользователь не в поезде
  public define int UPDATE_DOESCONTROL  = 3;    //Обновление окна с выводом сообщения, что пользователь не управляет поездом
  public define int UPDATE_STOPING      = 4;    //Обновление окна с выводом сообщения, что необходимо остановиться 

  //=============================================================================
  // Name: Init
  // Desc: Инициализирует новое окно с информацией об участках приблиджения и
  //       журналом.
  // Parm: core – Объект zx_DSP_Core, представляющий ядро маршрутизации. 
  // Parm: strTable – Таблица строк для интерфейса браузера. 
  // Retn: Инициалищированный объект zd_DSP_InfoForm.
  //=============================================================================
  public zx_DSP_DriverJunctionListForm Init(zx_DSP_Core core, StringTable strTable);
  
  //=============================================================================
  // Name: Update
  // Desc: Обновляет содержимое формы.
  // Parm: frontList – список стрелок перед поездом 
  // Parm: rearList – список стрелока за поездом 
  //=============================================================================
  public void Update(zx_DSP_DriveJunctionItem[] frontList, zx_DSP_DriveJunctionItem[] rearList);
  
  //=============================================================================
  // Name: Update
  // Desc: Обновляет содержимое формы.
  // Parm: action – значение одной из констант UPDATE_*, определяющих действие 
  //       при обновлении окна.
  //=============================================================================
  public void Update(int action);

  //=============================================================================
  // Name: BringToFront
  // Desc: Восстанавливает окно, если оно было свёрнуто, и перемещает его на
  //       передний план, поверх всех окон. 
  //=============================================================================
  public void BringToFront(void);

  //=============================================================================
  // Name: Dispose
  // Desc: Освобождает все ресурсы для этого объекта. 
  //=============================================================================
  public void Dispose(void);
  
  //=============================================================================
  // Name: CheckHasJunctionInList
  // Desc: Проверянт, имеется ли указаная стрелка в отображаемых списках.
  // Parm: junction – Объект Junctino, представляющий стрелку, наличие которой
  //       необходимо проверить.
  // Retn: Значение true, если такая стрелка присутствует; в противном случае —
  //       значение false.
  //=============================================================================
  public bool CheckHasJunctionInList(Junction junction);

  //=============================================================================
  // Name: BuildJunctionList
  // Desc: Генерирует отображаемый список стрелок.
  // Parm: buff – Буфер, в котором будет формироваться HTML код отображаемого 
  //       списка.
  // Parm: list – Массив записей о стрелках, которые необходимо отобразить. 
  //=============================================================================
  void BuildJunctionList(HTMLBuffer buff, zx_DSP_DriveJunctionItem[] list);

  //=============================================================================
  // Name: OnLinkClick
  // Desc: Вызывается при нажатии на ссылке в браузере 
  // Parm: action — Действие, которое необходимо выполнить
  //=============================================================================
  void OnLinkClick(string action, string argument);

	//
	// РЕАЛИЗАЦИЯ
	//

  zx_DSP_Core _core;                          //Тут ссылка на ядро
  Browser _browser;                           //Собственно само окно браузера
  StringTable _strtable;                      //Таблица строк
  zx_DSP_DriveJunctionItem[] _frontlist;      //Список стрелок перед составом
  zx_DSP_DriveJunctionItem[] _rearlist;       //Список стрелок за составом


  public zx_DSP_DriverJunctionListForm Init(zx_DSP_Core core, StringTable strTable)
  {
    if(_browser) return me; //Если инициализация уже выполнена, то выходим.

    _core = core;
    _strtable = strTable;
    
    _browser = Constructors.NewBrowser();
    _browser.SetWindowStyle(Browser.STYLE_DEFAULT); 
    _browser.SetScrollEnabled(true);        
    _browser.SetCloseEnabled(true);                                        
    _browser.SetWindowPriority(Browser.BP_HUD);
    _browser.SetWindowTitle(strTable.GetString("driverjunctionlist-caption"));
    _browser.SetWindowSize(230, 250);
    _browser.SetWindowPosition(30, 0);

    AddHandler(_browser, "Browser", "Closed", "BrowserClosedHandler");
    AddHandler(_browser, "Browser-URL", "", "BrowserURLHandler");
    
    Update(true);
    
    PostMessage(_core, "ZXDSP", "DriverJunctionListFormCreated", 0.1);    
    return me;    
  }

  public void Update(zx_DSP_DriveJunctionItem[] frontList, zx_DSP_DriveJunctionItem[] rearList)
  {
    _frontlist = frontList;
    _rearlist = rearList;
    Update(UPDATE_NORMAL);
  }
  
  public void Update(int action)
  {                                         
    if(action != UPDATE_NORMAL) {
      _frontlist = null;
      _rearlist = null;
    }
    HTMLBuffer buff = HTMLBufferStatic.Construct();
    buff.Print("<html><body>");
    buff.Print(HTMLWindow.StartTable("width=100%"));
    if(action == UPDATE_NORMAL) {
      if(_frontlist and _rearlist) {
        buff.Print("<tr><td colspan=5>" + _strtable.GetString("driverjunctionlist-frontlist") + "</td></tr>");
        BuildJunctionList(buff, _frontlist);
        buff.Print("<tr><td colspan=5>" + _strtable.GetString("driverjunctionlist-rearlist") + "</td></tr>");
        BuildJunctionList(buff, _rearlist);
      } else buff.Print("<tr><td align=center>" + _strtable.GetString("driverjunctionlist-error") + "</td></tr>"); 
    } 
    else if(action == UPDATE_NOVEHICLE) buff.Print("<tr><td align=center>" + _strtable.GetString("driverjunctionlist-trainunselect") + "</td></tr>"); 
    else if(action == UPDATE_DOESCONTROL) buff.Print("<tr><td align=center>" + _strtable.GetString("driverjunctionlist-doesnotcontrol") + "</td></tr>"); 
    else if(action == UPDATE_STOPING) buff.Print("<tr><td align=center>" + _strtable.GetString("driverjunctionlist-muststop") + "</td></tr>"); 
    buff.Print(HTMLWindow.EndTable());
    buff.Print("</body></html>");
    _browser.LoadHTMLString(_core.GetAsset(), buff.AsString());
  }

  public void BringToFront(void)
  {
    if(_browser) {
      _browser.RestoreWindow();
      _browser.BringToFront();    
    }
  }

  public void Dispose(void)
  {
    if(_browser) {
        _browser.CloseWindow();
        AddHandler(_browser, "Browser", "Closed", null);
        AddHandler(_browser, "Browser-URL", "", null);
    }
    _core = null;
    _browser = null;
    _frontlist = null;
    _rearlist = null;
  }

  public bool CheckHasJunctionInList(Junction junction)
  {
    if(_frontlist and _rearlist) {
      int i, count = _frontlist.size();
      for(i = 0; i < count; ++i)
        if(_frontlist[i].junctionInfo.jnc == junction) return true;
      count = _rearlist.size();
      for(i = 0; i < count; ++i)
        if(_rearlist[i].junctionInfo.jnc == junction) return true;
    }
    return false;
  }

  void BuildJunctionList(HTMLBuffer buff, zx_DSP_DriveJunctionItem[] list)
  {
    int i, count = list.size();
    for(i = 0; i < count; ++i) {
      zx_DSP_DriveJunctionItem item = list[i];
      if(item.distance > 0.0){
        buff.Print(HTMLWindow.StartRow());
          if(!(item.junctionInfo.state & zx_DSP_JunctionInfo.STATE_CUTUP) and item.junctionInfo.IsSynchronized()) {
            int direction = item.junctionInfo.jnc.GetDirection(); 
            if(item.junctionInfo.type & zx_DSP_JunctionInfo.JNC_LEFT) {
              if(item.face){
                if(direction == JunctionBase.DIRECTION_LEFT) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-l-l-24-16", 24, 16)));
                else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-l-f-24-16", 24, 16)));
              } else {
                if(direction == JunctionBase.DIRECTION_LEFT) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-l-l-24-16", 24, 16)));
                else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-l-f-24-16", 24, 16)));
              }
            } else if(item.junctionInfo.type & zx_DSP_JunctionInfo.JNC_RIGHT) {
              if(item.face){
                if(direction == JunctionBase.DIRECTION_LEFT) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-r-f-24-16", 24, 16)));
                else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-r-r-24-16", 24, 16)));
              } else {
                if(direction == JunctionBase.DIRECTION_LEFT) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-r-f-24-16", 24, 16)));
                else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-r-r-24-16", 24, 16)));
              }
            } else {
              if(item.face){
                if(direction == JunctionBase.DIRECTION_LEFT) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-s-l-24-16", 24, 16)));
                else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-s-r-24-16", 24, 16)));
              } else {
                if(direction == JunctionBase.DIRECTION_LEFT) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-s-l-24-16", 24, 16)));
                else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-s-r-24-16", 24, 16)));
              }
            }
          } else {
            if(item.junctionInfo.type & zx_DSP_JunctionInfo.JNC_LEFT){
              if(item.face) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-l-u-24-16", 24, 16)));
              else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-l-u-24-16", 24, 16)));
            } else if(item.junctionInfo.type & zx_DSP_JunctionInfo.JNC_RIGHT){
              if(item.face) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-r-u-24-16", 24, 16)));
              else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-r-u-24-16", 24, 16)));
            } else {
              if(item.face) buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-f-s-u-24-16", 24, 16)));
              else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("jnc-r-s-u-24-16", 24, 16)));
            } 
          }
          string junctionname = item.junctionInfo.jnc.GetLocalisedName();
          if(item.junctionInfo.number > 0) junctionname = _strtable.GetString1("train-number", item.junctionInfo.number);
          buff.Print(HTMLWindow.MakeCell(junctionname, "width=100%"));
          buff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + _strtable.GetString1("string-distance", (int)item.distance) + "&nbsp;</nowrap>"));
          if(item.junctionInfo.type & zx_DSP_JunctionInfo.JNC_MANUAL and !(item.junctionInfo.state & (zx_DSP_JunctionInfo.STATE_PATH | zx_DSP_JunctionInfo.STATE_SHUNT | zx_DSP_JunctionInfo.STATE_TRAIN | 
          zx_DSP_JunctionInfo.STATE_LOCK | zx_DSP_JunctionInfo.STATE_MANUALLOCK | zx_DSP_JunctionInfo.STATE_ERROR | zx_DSP_JunctionInfo.STATE_LOCALCONTROL | zx_DSP_JunctionInfo.STATE_CUTUP))) 
            buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://toggle/" + item.junctionInfo.index, _core.MakeHTMLImage("handjunction-toggle-16", 16), _strtable.GetString("tooltip-junctiontoggle"))));
          else buff.Print(HTMLWindow.MakeCell(_core.MakeHTMLImage("handjunction-toggle-d-16", 16)));
          buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://settings/" + item.junctionInfo.index, _core.MakeHTMLImage("junction-settings-16", 16), _strtable.GetString("tooltip-openjunctionsettings"))));
          junctionname = null;
        buff.Print(HTMLWindow.EndRow());
      }
    }
  }

  void OnLinkClick(string action, string argument)
  { 
    if(action == "settings") { 
      zx_DSP_JunctionInfo jncinfo = _core.jncs[Str.ToInt(argument)];
      if(jncinfo) {
        if(!jncinfo.settingsBrowser) {
          jncinfo.settingsBrowser = new zx_DSP_JunctionSettingsForm().Init(jncinfo, _core, _strtable);
        } else jncinfo.settingsBrowser.BringToFront();
      }
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);      
    } if(action == "toggle") {
      zx_DSP_JunctionInfo jncinfo = _core.jncs[Str.ToInt(argument)];
      if (JunctionBase.DIRECTION_RIGHT != jncinfo.jnc.GetDirection())
        _core.HandJunctionSetDirection(jncinfo, JunctionBase.DIRECTION_RIGHT, _core.GetLocalPlayerName());
      else _core.HandJunctionSetDirection(jncinfo, JunctionBase.DIRECTION_LEFT, _core.GetLocalPlayerName());
      _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
    } 
  }

  void BrowserURLHandler(Message msg)
  {
    if(msg.src == _browser and /* msg.src == msg.dst and сообщение шлётся бродкастом */   
    msg.minor[0, 7] == "live://") {
      string[] args = Str.Tokens(msg.minor[7,], "/");
      if(args.size() == 2) OnLinkClick(args[0], args[1]);
      else OnLinkClick(args[0], null);
    } 
  }  

  void BrowserClosedHandler(Message msg)
  {
    if(msg.src == _browser and msg.src == msg.dst) {
      PostMessage(_core, "ZXDSP", "DriverJunctionListFormClosed", 0.0);
      Dispose();
    }
  }


};




