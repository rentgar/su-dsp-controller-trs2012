include "cache_sleeper.gs"

class CacheMutex
{
    CacheSleeper _sleeper = null;

    public bool IsLocked() {
        return _sleeper and true;
    }

    public CacheSleeper TryLock() {
        if (_sleeper) {
            return null;
        }
        _sleeper = new CacheSleeper().Init(me);

        return _sleeper;
    }

    public CacheSleeper Lock() {
        while (_sleeper) {
            Router.GetCurrentThreadGameObject().Sleep(0.01);
        }
        _sleeper = new CacheSleeper().Init(me);

        return _sleeper;
    }

    public void Cancel() {
        if (_sleeper) {
            _sleeper.Cancel();
            _sleeper = null;
        }
    }

    public CacheSleeper CancelAndLock() {
        Cancel();

        return Lock();
    }

    public void Unlock(CacheSleeper sleeper) {
        if (sleeper == _sleeper) {
            _sleeper = null;
        }
    }
};
