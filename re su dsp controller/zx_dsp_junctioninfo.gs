//=============================================================================
// File: zx_dsp_junctioninfo.gs
// Desc: Классы, связанные со стрелкой
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_junctionforms.gs"
include "junction.gs"
include "zx_dsp_classes.gs"

//Описывает ссылку на соседнюю стрелку
final class zx_DSP_JunctionNeighbor
{
	public int junctionIndex;                                                           //Индекс стрелки
	public int direction;                                                               //Направление стрелки, по которому соседство
};

//Описывает стрелку
final class zx_DSP_JunctionInfo
{
	public define int STATE_FREE                    = 0;                                //Стрелка свободна
	public define int STATE_MANUAL                  = 1;                                //Стрелка установлена с пульта
	public define int STATE_PATH                    = 2;                                //Стрелка занята собраным поездным маршрутом
	public define int STATE_SHUNT                   = 4;                                //Стрелка занята собраным маневровым маршрутом
	public define int STATE_TRAIN                   = 8;                                //Стрелка занята поездом
	public define int STATE_LOCK                    = 16;                               //Блокированна системой (автоматический рассчёт маршрутов)
	public define int STATE_PATHLOCK                = 32;                               //Стрелка заблокирована для постройки маршрута
	public define int STATE_SHUNTLOCK               = 64;                               //Стрелка заблокирована для постройки маневрового маршрута
	public define int STATE_MANUALLOCK              = 128;                              //Стрелка заблокирована для ручного управления
	public define int STATE_ERROR                   = 256;                              //Ошибка работы стрелки
	public define int STATE_LOCALCONTROL            = 512;                              //Местное управление (маневровая колонка)
	public define int STATE_CUTUP					          = 1024;								              //Взрез
	public define int STATE_DIRECTION_MINUS			    = 2048;	  				                  //Стрелку перевели в минус
  
  public define int ERRORSTATEMASK                = STATE_CUTUP;                      //Маска флагов состояния, описывающих ошибку стрелки  


	public define int JNC_LEFT                      = 1;                                //Стрелка влево
	public define int JNC_RIGHT                     = 2;                                //Стрелка вправо                                    ы
	public define int JNC_BACK                      = 4;                                //Стрелка назад
	public define int JNC_MANUAL                    = 8;                                //Ручная стрелка
	public define int JNC_AUTOREVERT                = 16;                               //Стрелка с самовозвратом (не может быть совместно с флагом JNC_MANUAL)
	public define int JNC_USED						= 32;								//В стеке поиска

	public define int DIRECT_UNKNOWN                = 0;                                //Направление маршрута по стрелке не известно
	public define int DIRECT_FORWARD                = 1;                                //Противошёрстное направление
	public define int DIRECT_BACKWARD               = 2;                                //Пошёрстное направление

	public int index;                                                                   //Индекс стрелки
	public zx_DSP_JunctionInfo masterJunction;                                          //Ведущая стрелка
	public zx_DSP_JunctionInfo[] slaveJunctions;                                        //Ведомые стрелки
	public string name;                                                                 //Имя стрелки
	public int number;                                                                  //Номер стрелки на станции
	public string title;                                                                //Отображаемое обозначение стрелки
	public Junction jnc;                                                                //Ссылка на стрелку
	public int defaultDirection;                                                        //Исходное направление стрелки
	public int state                                = STATE_FREE;                       //Состояние стрелки
	public int pathIndex                            = -1;                               //Индекс маршрута, который занял стрелку
	public int stationId                            = -1;                               //Идентификатор станции, которой принадлежит стрелка
	public zx_DSP_Station station                   = null;                             //Станция, которой принадлежит стрелка
	public int indexInPath                          = -1;                               //Индекс стрелки в маршруте
	public int type;                                                                    //Тип стрелки
	public int pathdirection;                                                           //Направление маршрута по стрелке
	public int multiplierLeft;                                                          //Множитель для отклонения влево
	public int multiplierRight;                                                         //Множитель для отклонения вправо

	public float locktime                           = -1;                               //Время, когда стрелка была заблокирована (зарезервировано)

	public zx_DSP_JunctionNeighbor neighborLeft;                                        //Соседная стрелка слева
	public zx_DSP_JunctionNeighbor neighborRight;                                       //Соседная стрелка справа
	public zx_DSP_JunctionNeighbor neighborBack;                                        //Соседная стрелка сзади

	public int searchDirState                       = 0;                                //Состояние стрелки в поиске
  
  public zx_DSP_JunctionSettingsForm settingsBrowser;                                 //Открытое окно со свойствами стрелки. Если окно закрыто, то значение null.
  
  
	/******************************************************************************
	* Name: ResetSelfReturn
	* Desc: Отменяет состояние самовозврата стрелки
	******************************************************************************/
  public void ResetSelfReturn()
  {
		if (masterJunction) {
			masterJunction.ResetSelfReturn();
			return;
		}
    jnc.ClearMessages("ZXDSP", "SelfReturn");
  }

	/******************************************************************************
	* Name: IsSynchronized
	* Desc: Проверяет, что все стрелки группы находятся в одинаковом положении
	* Rets: true, если все стрелки в одинаковом положении, иначе false
	******************************************************************************/
	public bool IsSynchronized() {
		if (masterJunction) return masterJunction.IsSynchronized();
		bool mydir = (jnc.GetDirection() == defaultDirection);
		if (mydir != !(state & STATE_DIRECTION_MINUS)) return false;
		if (slaveJunctions) {
			int i, count	= slaveJunctions.size();
			for (i = 0; i < count; ++i) {
				bool slavedirection = (slaveJunctions[i].jnc.GetDirection() == slaveJunctions[i].defaultDirection);
				//if (slavedirection != !(slaveJunctions[i].state & STATE_DIRECTION_MINUS)) return false;
				if (slavedirection != !(state & STATE_DIRECTION_MINUS)) return false;
				if (slavedirection != mydir) return false;
			}
		}
		return true;
	}

	/******************************************************************************
	* Name: IsDirection
	* Desc: Проверяет, с учётом спаренных, что стрелка находятся в указанном положении
	* Parm: direction - направление стрелки, true для перевода в положение по
	* 			умолчанию (+), false для перевода в противоположное положение (-).
	* Rets: true, если стрелка в указанном положении
	******************************************************************************/
	public bool IsDirection(bool direction) {
		//return !(state & STATE_DIRECTION_MINUS) == direction and IsSynchronized();
    return ((masterJunction and !(masterJunction.state & STATE_DIRECTION_MINUS)) or (!masterJunction and !(state & STATE_DIRECTION_MINUS))) == direction and IsSynchronized();
	}

	/******************************************************************************
	* Name: IsDirection
	* Desc: Проверяет, с учётом спаренных, что стрелка находятся в указанном положении
	* Parm: direction - направление стрелки, одна из констант класса JunctionBase.
	* Rets: true, если стрелка в указанном положении
	******************************************************************************/
	public bool IsDirection(int direction) {
		return jnc.GetDirection() == direction and IsSynchronized();
	}

	/******************************************************************************
	* Name: AllowSwitch
	* Desc: Проверяет допустимость перевода стрелки через маршрутный набор.
	* Rets: true, если стрелку можно перевести, иначе false.
	******************************************************************************/
	public bool AllowSwitch() {
		if (masterJunction) return masterJunction.AllowSwitch();
		int flags = STATE_MANUAL | STATE_PATH | STATE_SHUNT | STATE_TRAIN | STATE_ERROR | STATE_LOCALCONTROL;
		if (state & flags) return false;
		if (slaveJunctions) {
			int i, count	= slaveJunctions.size();
			for (i = 0; i < count; ++i) 
				if (slaveJunctions[i].state & flags) return false;
		}
		return true;
	}

	/******************************************************************************
	* Name: AllowManualSwitch
	* Desc: Проверяет допустимость перевода стрелки через индивидуальный перевод.
	* Parm: skipTrain - Пропустить проверку свободности от подвижного состава
	*			(вспомогательный перевод стрелки).
	* Rets: true, если стрелку можно перевести, иначе false.
	******************************************************************************/
	public bool AllowManualSwitch(bool skipTrain) {
		if (masterJunction) return masterJunction.AllowManualSwitch(skipTrain);
		int flags = STATE_MANUAL | STATE_PATH | STATE_SHUNT | STATE_LOCK | STATE_MANUALLOCK | STATE_LOCALCONTROL;
		if (state & flags or (state & STATE_ERROR and state & ERRORSTATEMASK & ~STATE_CUTUP)) return false;
		if (!skipTrain and (state & STATE_TRAIN)) return false;
		if (slaveJunctions) {
			int i, count	= slaveJunctions.size();
			for (i = 0; i < count; ++i) {
				if (slaveJunctions[i].state & flags) return false;
				if (!skipTrain and (slaveJunctions[i].state & STATE_TRAIN)) return false;
			}
		}
		return true;
	}

	/******************************************************************************
	* Name: SetDirection
	* Desc: Переводит стрелку в заданное положение.
	* Parm: token - токен с правом "junction-dir"
	* Parm: direction - направление стрелки, true для перевода в положение по
	* 			умолчанию (+), false для перевода в противоположное положение (-).
	******************************************************************************/
	public void SetDirection(SecurityToken token, bool direction) {
		if (masterJunction) {
			masterJunction.SetDirection(token, direction);
			return;
		}
    ResetSelfReturn();
		if (direction) {
			jnc.SetDirection(token, defaultDirection);
			state = state & ~STATE_DIRECTION_MINUS;
		} else if (defaultDirection == Junction.DIRECTION_RIGHT) {
			jnc.SetDirection(token, Junction.DIRECTION_LEFT);
			state = state | STATE_DIRECTION_MINUS;
		} else {
			jnc.SetDirection(token, Junction.DIRECTION_RIGHT);
			state = state | STATE_DIRECTION_MINUS;
		}
		if (slaveJunctions) {
			int i, count = slaveJunctions.size();
			for (i = 0; i < count; ++i) {
				zx_DSP_JunctionInfo slave = slaveJunctions[i];
				if (direction) {
					slave.jnc.SetDirection(token, slave.defaultDirection);
					//slave.state = slave.state & ~STATE_DIRECTION_MINUS;
				} else if (slave.defaultDirection == Junction.DIRECTION_RIGHT) {
					slave.jnc.SetDirection(token, Junction.DIRECTION_LEFT);
					//slave.state = slave.state | STATE_DIRECTION_MINUS;
				} else {
					slave.jnc.SetDirection(token, Junction.DIRECTION_RIGHT);
					//slave.state = slave.state | STATE_DIRECTION_MINUS;
				}
			}
		}
	}

	/******************************************************************************
	* Name: SetDirection
	* Desc: Переводит стрелку в заданное положение.
	* Parm: token - токен с правом "junction-dir"
	* Parm: direction - направление стрелки, одна из констант класса JunctionBase.
	******************************************************************************/
	public void SetDirection(SecurityToken token, int direction) {
		SetDirection(token, direction == defaultDirection);
	}

	/******************************************************************************
	* Name: AllowUseToPath
	* Desc: Проверяет допустимость использования стрелки в поездном маршруте.
	* Rets: true, если стрелку можно использовать, иначе false.
	******************************************************************************/
	public bool AllowUseToPath() {
		return !(state & (STATE_PATH | STATE_SHUNT | STATE_TRAIN | STATE_LOCK | STATE_PATHLOCK | STATE_ERROR | STATE_LOCALCONTROL | STATE_CUTUP)) and !(type & JNC_MANUAL);
	}

	/******************************************************************************
	* Name: AllowUseToShunt
	* Desc: Проверяет допустимость использования стрелки в маневровом маршруте.
	* Rets: true, если стрелку можно использовать, иначе false.
	******************************************************************************/
	public bool AllowUseToShunt() {
		return !(state & (STATE_PATH | STATE_SHUNT | STATE_TRAIN | STATE_LOCK | STATE_SHUNTLOCK | STATE_ERROR | STATE_LOCALCONTROL | STATE_CUTUP)) and !(type & JNC_MANUAL);
	}

	/******************************************************************************
	* Name: AllowUseToExternal
	* Desc: Проверяет допустимость использования стрелки внешней системой.
	* Rets: true, если стрелку можно использовать, иначе false.
	******************************************************************************/
	public bool AllowUseToExternal() {
		if (masterJunction) return masterJunction.AllowUseToExternal();
		int flags = STATE_PATH | STATE_SHUNT | STATE_TRAIN | STATE_LOCK | STATE_ERROR | STATE_LOCALCONTROL;
		if (state & flags) return false;
		if (slaveJunctions) {
			int i, count = slaveJunctions.size();
			for (i = 0; i < count; ++i) 
				if (slaveJunctions[i].state & flags) return false;
		}
		return true;
	}
  
  
	/******************************************************************************
	* Name: TrySelfReturn
	* Desc: Пытается запустить процесс перевода стрелки в положение по умолчанию
  *       (+), если это возможно.
	* Parm: longTime - значение true если требуется длинная (180с) выдержка
  *       времени перед перевожом; значение false, если короткая вадержка (60с).
	******************************************************************************/
  public final void TrySelfReturn(bool longTime) 
  {
		if (masterJunction) {
      masterJunction.TrySelfReturn(longTime);
      return; 
    }
		int flags = STATE_PATH | STATE_SHUNT, timeout = 60;
		if (!(type & JNC_AUTOREVERT) or state & flags) return;
		if (slaveJunctions) {
			int i, count	= slaveJunctions.size();
			for (i = 0; i < count; ++i) 
				if (slaveJunctions[i].state & flags) return;
		}
    if (longTime) timeout = 180;
    jnc.PostMessage(jnc, "ZXDSP", "SelfReturn", timeout);
  }


	/******************************************************************************
	* Name: GetTitle
	* Desc: Возвращает обозначение стрелки, если задано явно, или номер стрелки.
	* Rets: Обозначение стрелки.
	******************************************************************************/
	public string GetTitle() {
		if (title and title.size()) return title;
		if (number) return (string)number;
		return "";
	}

	/******************************************************************************
	* Name: GetFullTitle
	* Desc: Собирает обозначение стрелок, входящих в группу и формирует из них
	*			обозначение съезда.
	* Rets: Обозначение съезда.
	******************************************************************************/
	public string GetFullTitle() {
		if (masterJunction) return masterJunction.GetFullTitle();
		string ret = GetTitle();
		if (!slaveJunctions) return ret;
		int i, count = slaveJunctions.size();
		for (i = 0; i < count; ++i) {
			string title = slaveJunctions[i].GetTitle();
			if (!title or !title.size()) continue;
			if (ret != "") ret = ret + "/";
			ret = ret + title;
		}
		return ret;
	}
  
	/******************************************************************************
	* Name: UpdateSettigsForm
	* Desc: Выполняет окно свойств стелки, если оно открыто
	******************************************************************************/
  public void UpdateSettigsForm(void)
  {
		if (settingsBrowser) settingsBrowser.Update();
  }

	/******************************************************************************
	* Name: UpdateSettigsForm
	* Desc: Выполняет окно свойств стелки, если оно открыто
	* Parm: includeSlave - значение true, если нужно так же обновить окно свойств стрелок, включённых в группу 
	******************************************************************************/
  public void UpdateSettigsForm(bool includeSlave)
  {
		if (includeSlave and masterJunction) {
			masterJunction.UpdateSettigsForm(true);
			return;
		}
    UpdateSettigsForm();
    if(includeSlave and slaveJunctions) {
			int i, count	= slaveJunctions.size();
			for (i = 0; i < count; ++i)
				slaveJunctions[i].UpdateSettigsForm();
    }
  }
  
};
