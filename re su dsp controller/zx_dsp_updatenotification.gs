//=============================================================================
// File: zx_dsp_updatenotification.gs
// Desc: Класс системы оповещения о новой версии пульта. 
// Auth: Алексей 'Эрендир' Зверев 2023 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_core.gs"
include "htmlbuffer.gs"
include "browser.gs"

//=============================================================================
// Name: zx_DSP_UpdateInfo
// Desc: Описывает информацию об обновлении.
//=============================================================================
class zx_DSP_UpdateInfo isclass GSObject
{
  public bool critical;
  public bool mustrecalc;
  public string version;
  public string[] notes;
};

//=============================================================================
// Name: zx_DSP_UpdateNotificationForm
// Desc: Форма уведомления о новой версии пульта.
//=============================================================================
class zx_DSP_UpdateNotificationForm isclass GameObject
{

  
  public zx_DSP_UpdateNotificationForm Init(zx_DSP_Core core, StringTable strTable, zx_DSP_UpdateInfo info);

  //=============================================================================
  // Name: Dispose
  // Desc: Освобождает все ресурсы для этого объекта. 
  //=============================================================================
  public void Dispose(void);

	//
	// РЕАЛИЗАЦИЯ
	//
  
  zx_DSP_Core _core;                          //Тут ссылка на ядро
  Browser _browser;                           //Собственно само окно браузера
  StringTable _strtable;                      //Таблица строк
  zx_DSP_UpdateInfo _info;                    //Информация об обновлении.

  public zx_DSP_UpdateNotificationForm Init(zx_DSP_Core core, StringTable strTable, zx_DSP_UpdateInfo info)
  {
    if(_browser) return me; //Если инициализация уже выполнена, то выходим.
    
    _core = core;
    _strtable = strTable;
    _info = info;
                                         
    _browser = Constructors.NewBrowser();
    _browser.SetWindowStyle(Browser.STYLE_DEFAULT); 
    _browser.SetScrollEnabled(false);        
    _browser.SetCloseEnabled(false);                                        
    _browser.SetWindowPriority(Browser.BP_Window);
    _browser.SetButtonOverlayStyle(Browser.BS_None, Browser.BS_OK);

    _browser.SetWindowTitle(strTable.GetString("updatenotification-caption-new"));
    _browser.SetWindowCentered(500, 450);
    if (info.critical or info.mustrecalc) _browser.LoadHTMLFile(_core.GetAsset(), "newversionrecalc.html");
    else _browser.LoadHTMLFile(_core.GetAsset(), "newversion.html");

    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    buffer.Print("<html><body>");
    buffer.Print(HTMLWindow.StartTable("width=100%"));
    int i, count = info.notes.size(); 
    for (i = 0; i < count; ++i) {
      Str.TrimLeft(info.notes[i], " ");
      Str.TrimRight(info.notes[i], " ");
      string note = strTable.GetString("note-" + info.notes[i]);
      if (note and note.size()) {
        buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("•"));
        buffer.Print(HTMLWindow.MakeCell(note, "width=100%"));
        buffer.Print(HTMLWindow.EndRow());
      }
    }
    buffer.Print(HTMLWindow.EndTable());
    buffer.Print("</body></html>");

    _browser.SetElementProperty("note", "html", buffer.AsString());
    _browser.SetElementProperty("note", "background-style", "1");
                       
    _browser.SetTrainzText("currentversion", _core.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("version-notes").GetNamedTag("version"));
    if (info.version and info.version.size()) _browser.SetTrainzText("oldversion", info.version);
    
    if (info.critical) _browser.SetTrainzText("warning", strTable.GetString("updatenotification-critical"));
    else if (info.mustrecalc) _browser.SetTrainzText("warning", strTable.GetString("updatenotification-mustrecalc"));
    
    AddHandler(_browser, "Browser", "Closed", "BrowserClosedHandler");
    AddHandler(_browser, "Browser", "button-ok", "BrowserClickOkHandler");

    return me;
  }

  public void Dispose(void)
  {
    if (_browser) {
        _browser.CloseWindow();
        AddHandler(_browser, "Browser", "Closed", null);
        AddHandler(_browser, "Browser", "button-ok", null);
    }
    _core = null;
    _browser = null;
    _strtable = null;
  }
  
  void BrowserClosedHandler(Message msg)
  {
    if (msg.src == _browser and msg.src == msg.dst) {
      PostMessage(_core, "ZXDSP", "UpdateNotificationFormClosed", 0.0);
      Dispose();
    }
  }
  
  void BrowserClickOkHandler(Message msg){ BrowserClosedHandler(msg); }  

  public zx_DSP_UpdateInfo Info()
  {
    return _info;
  }  

};