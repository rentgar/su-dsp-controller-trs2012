//=============================================================================
// File: zx_dsp_context.gs
// Desc: Классы для реализации контекстов.
//       На данный момент работают только с перегонами (zx_DSP_SpanBase).
//       В дальнейшем предполагается создать базовый класс zx_DSP_Entity для
//       всех типов объектов
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_core.gs"



//=============================================================================
// Name: zx_DSP_MakeHTMLContext
// Desc: Класс, представляющий контекст для генерации HTML кода сущности в
//       браузере
//=============================================================================
final class zx_DSP_MakeHTMLContext isclass GSObject
{

  //=============================================================================
  // Name: GetCore
  // Desc: Возвращает ссылку на ядро пульта. 
  // Retn: Объект zx_DSP_Core представляющий ядро пульта. 
  //=============================================================================
  public zx_DSP_Core GetCore();
  
  //=============================================================================
  // Name: MakeHTMLImage
  // Desc: Возращает тэг <img> с изображением с учётом текущего стиля.
  // Parm: name – имя изображения без пути и расширеения. 
  // Parm: width – ширина изображения. 
  // Parm: height – высота изображения. 
  //=============================================================================
  public string MakeHTMLImage(string name, int width, int height);
  
  //=============================================================================
  // Name: MakeHTMLImage
  // Desc: Возращает тэг <img> с изображением с учётом текущего стиля.
  // Parm: name – имя изображения без пути и расширеения. 
  // Parm: bothSize – ширина и высота изображения. 
  //=============================================================================
  public string MakeHTMLImage(string name, int bothSize);
  
  //=============================================================================
  // Name: MakeCameraToSignalURL
  // Desc: Генерирует ссылку, для перевода камеры к светофору.
  // Parm: sigmal – объект zxSignal, представляющий светофор для которого нужно
  //       сгенирировать ссылку переноса камеры. 
  // Retn: URL для перевода камеры к светофору sigmal. 
  //=============================================================================
  public string MakeCameraToSignalURL(zxSignal sigmal);
  
  //=============================================================================
  // Name: MakeActionURL
  // Desc: Генерирует ссылку для выполнения действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: action – действие, которое необходимо выполнить.
  // Parm: argument1 – первый передаваемый аргумент действия.
  // Parm: argument2 – второй передаваемый аргумент действия.
  // Parm: argument3 – третий передаваемый аргумент действия.
  // Retn: URL для выполнения действия. 
  // Note: Для выполнения действия будет вызвана функция указанной сущности
  //       zx_DSP_SpanBase.DoUIAction(...). 
  //=============================================================================
  public string MakeActionURL(zx_DSP_SpanBase entity, string action, string argument1, string argument2, string argument3);
  
  //=============================================================================
  // Name: MakeActionURL
  // Desc: Генерирует ссылку для выполнения действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: action – действие, которое необходимо выполнить.
  // Parm: argument1 – первый передаваемый аргумент действия.
  // Parm: argument2 – второй передаваемый аргумент действия.
  // Retn: URL для выполнения действия. 
  // Note: Для выполнения действия будет вызвана функция указанной сущности
  //       zx_DSP_SpanBase.DoUIAction(...). 
  //=============================================================================
  public string MakeActionURL(zx_DSP_SpanBase entity, string action, string argument1, string argument2);

  //=============================================================================
  // Name: MakeActionURL
  // Desc: Генерирует ссылку для выполнения действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: action – действие, которое необходимо выполнить.
  // Parm: argument1 – передаваемый аргумент действия.
  // Retn: URL для выполнения действия. 
  // Note: Для выполнения действия будет вызвана функция указанной сущности
  //       zx_DSP_SpanBase.DoUIAction(...). 
  //=============================================================================
  public string MakeActionURL(zx_DSP_SpanBase entity, string action, string argument1);

  //=============================================================================
  // Name: MakeActionURL
  // Desc: Генерирует ссылку для выполнения действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: action – действие, которое необходимо выполнить.
  // Retn: URL для выполнения действия. 
  // Note: Для выполнения действия будет вызвана функция указанной сущности
  //       zx_DSP_SpanBase.DoUIAction(...). 
  //=============================================================================
  public string MakeActionURL(zx_DSP_SpanBase entity, string action);

  //=============================================================================
  // Name: MakeResponsibleActionURL
  // Desc: Генерирует ссылку для инициализации ответственного действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: action – действие, которое необходимо выполнить.
  // Parm: argumentX – первый передаваемый аргумент действия.
  // Parm: argumentY – второй передаваемый аргумент действия.
  // Parm: argumentZ – третий передаваемый аргумент действия.
  // Retn: URL для выполнения действия. 
  // Note: Для выполнения действия будет вызвана функция указанной сущности
  //       zx_DSP_SpanBase.DoUIAction(...). 
  //=============================================================================
  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity,string action, string argumentX, string argumentY, string argumentZ);

  //=============================================================================
  // Name: MakeResponsibleActionURL
  // Desc: Генерирует ссылку для инициализации ответственного действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: action – действие, которое необходимо выполнить.
  // Parm: argumentX – первый передаваемый аргумент действия.
  // Parm: argumentY – второй передаваемый аргумент действия.
  // Retn: URL для выполнения действия. 
  // Note: Для выполнения действия будет вызвана функция указанной сущности
  //       zx_DSP_SpanBase.DoUIAction(...). 
  //=============================================================================
  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity, string action, string argumentX, string argumentY);
  
  //=============================================================================
  // Name: MakeResponsibleActionURL
  // Desc: Генерирует ссылку для инициализации ответственного действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: action – действие, которое необходимо выполнить.
  // Parm: argumentX – первый передаваемый аргумент действия.
  // Retn: URL для выполнения действия. 
  // Note: Для выполнения действия будет вызвана функция указанной сущности
  //       zx_DSP_SpanBase.DoUIAction(...). 
  //=============================================================================
  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity, string action, string argumentX);

  //=============================================================================
  // Name: MakeResponsibleActionURL
  // Desc: Генерирует ссылку для инициализации ответственного действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: action – действие, которое необходимо выполнить.
  // Retn: URL для выполнения действия. 
  // Note: Для выполнения действия будет вызвана функция указанной сущности
  //       zx_DSP_SpanBase.DoUIAction(...). 
  //=============================================================================
  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity, string action);
  
  //=============================================================================
  // Name: MakeResponsibleHTML
  // Desc: Генерирует HTML код для подтверждения ответственного действия.
  // Parm: entity - сущность, для которой выполняется действие
  // Parm: questionLabel - отображаемая строка с вопросом о подтверждении
  //       ответственного действия.
  // Retn: HTML код для подтверждения ответственного действия; или пустая строка,
  //       если нет ответственного действия связаного с этой сущьностью. 
  //=============================================================================
  public string MakeResponsibleHTML(zx_DSP_SpanBase entity, string questionLabel);
  
  //=============================================================================
  // Name: MakeShowTabURL
  // Desc: Генерирует ссылку для перехода к указанной вкладке на указанной
  //       станции.
  // Parm: stationIndex — отсчитываемый от 0 (ноля) индекс станции, к которой
  //       нужно перейти.
  // Parm: tab — значение одной из констант zx_DSP_Station.CONTROLTAB_*,
  //       определяющее какую вкладку нужно открыть.  
  // Retn: URL для перехода к указанной вкладки указанной станции. 
  //=============================================================================
  public string MakeShowTabURL(int stationIndex, int tab);
  
  //=============================================================================
  // Name: GetStringTable
  // Desc: Возвращает ссылку на таблицу строкю
  // Retn: Объект StringTable представляющий таблицу строк. 
  //=============================================================================
  public StringTable GetStringTable();
  
  //=============================================================================
  // Name: GetStation
  // Desc: Возвращает ссылку на станцию, для которой нужно построить HTML
  // Retn: Объект zx_DSP_Station представляющий станцию. 
  //=============================================================================
  public zx_DSP_Station GetStation();

  //=============================================================================
  // Name: CheckResponsible
  // Desc: Проверяет, имеется ли в текущем контексте запущенное ответственное 
  //       действие для указанного полуперегона.
  // Parm: entity - сущность, для которой выполняется проверка наличия 
  //       ответственного действия.
  // Retn: Значение true, если имеется ответственное действие; в противном
  //       случае — значение false. 
  //=============================================================================
  public bool CheckResponsible(zx_DSP_SpanBase entity);
  
  //=============================================================================
  // Name: ResponsibleAction
  // Desc: Возвращает ответственное действие для указанного перегона.
  // Parm: entity - сущность, для которой нужно получить действие 
  // Retn: Строка, представляющая ответственное действие; или значение null,
  //       если нет ответственных действий, связанных с указанным полуперегоном. 
  //=============================================================================
  public string ResponsibleAction(zx_DSP_SpanBase entity);
  
  //=============================================================================
  // Name: ResponsibleArgumentX
  // Desc: Возвращает X-аргумент ответственное действия для указанного перегона.
  // Parm: entity - сущность, для которой нужно получить аргумент
  // Retn: Строка, представляющая X-аргумент; или значение null, если нет 
  //       ответственных действий, связанных с указанным полуперегоном. 
  // Note: Если урумент действия не задан, то так же будет возвращать null.
  //       Проверку наличия связаного ответственного действия выполнять через
  //       CheckResponsible(...).
  //=============================================================================
  public string ResponsibleArgumentX(zx_DSP_SpanBase entity); 

  //=============================================================================
  // Name: ResponsibleArgumentY
  // Desc: Возвращает Y-аргумент ответственное действия для указанного перегона.
  // Parm: entity - сущность, для которой нужно получить аргумент
  // Retn: Строка, представляющая Y-аргумент; или значение null, если нет 
  //       ответственных действий, связанных с указанным полуперегоном. 
  // Note: Если урумент действия не задан, то так же будет возвращать null.
  //       Проверку наличия связаного ответственного действия выполнять через
  //       CheckResponsible(...).
  //=============================================================================
  public string ResponsibleArgumentY(zx_DSP_SpanBase entity); 

  //=============================================================================
  // Name: ResponsibleArgumentZ
  // Desc: Возвращает Z-аргумент ответственное действия для указанного перегона.
  // Parm: entity - сущность, для которой нужно получить аргумент
  // Retn: Строка, представляющая Z-аргумент; или значение null, если нет 
  //       ответственных действий, связанных с указанным полуперегоном. 
  // Note: Если урумент действия не задан, то так же будет возвращать null.
  //       Проверку наличия связаного ответственного действия выполнять через
  //       CheckResponsible(...).
  //=============================================================================
  public string ResponsibleArgumentZ(zx_DSP_SpanBase entity); 
  
  
	//
	// РЕАЛИЗАЦИЯ
	//
  
  define int RESPMODE_NONE                = 0;                  //Ответственое действие не выполняется
  define int RESPMODE_WAIT                = 1;                  //Ожидает доступности для ответственого действия
  define int RESPMODE_WAITCONFITM         = 2;                  //Ожидает действия от диспетчера

  zx_DSP_Core _core;
  StringTable _strtable;
  zx_DSP_Station _station;
  int _respmode                           = RESPMODE_NONE;      //Текущий режим ответственного действия
  int _respitemindex;                                           //Индекс перегона, с которым связано ответственное действие.
  string _respaction;
  string _respargx;
  string _respargy;
  string _respargz;
      
  
  
  public zx_DSP_MakeHTMLContext Init(zx_DSP_Core core, StringTable strTable, zx_DSP_Station station)
  {
    _core = core;
    _strtable = strTable;
    _station = station;
    return me;
  }
  
  public void SetResponsible(int index, int mode, string action)
  {
    string[] actiontokens = Str.Tokens(action, "|");
    _respitemindex = index;  
    _respmode = mode;
    _respaction = actiontokens[0]; 
    if (actiontokens.size() > 1) _respargx = actiontokens[1];
    else _respargx = null;   
    if (actiontokens.size() > 2) _respargy = actiontokens[2];   
    else _respargy = null;   
    if (actiontokens.size() > 3) _respargz = actiontokens[3];   
    else _respargz = null;   
  }
  
  public string MakeHTMLImage(string name, int width, int height)
  {
    return _core.MakeHTMLImage(name, width, height);
  }
  
  public string MakeHTMLImage(string name, int bothSize)
  {
    return _core.MakeHTMLImage(name, bothSize, bothSize);
  }
  
  public string MakeCameraToSignalURL(zxSignal sigmal)
  {
    return "live://cameratosignal/" + sigmal.GetGameObjectID().SerialiseToString();
  }
  
  public string MakeActionURL(zx_DSP_SpanBase entity, string action, string argument1, string argument2, string argument3)
  {
    return "live://span-action|" + _station.id + "|" + entity.index + "|" + action + "/" + argument1 + "/" + argument2 + "/" + argument3;
  }
  
  public string MakeActionURL(zx_DSP_SpanBase entity, string action, string argument1, string argument2)
  {
    return "live://span-action|" + _station.id + "|" + entity.index + "|" + action + "/" + argument1 + "/" + argument2;
  }

  public string MakeActionURL(zx_DSP_SpanBase entity, string action, string argument1)
  {
    return "live://span-action|" + _station.id + "|" + entity.index + "|" + action + "/" + argument1;
  }

  public string MakeActionURL(zx_DSP_SpanBase entity, string action)
  {
    return "live://span-action|" + _station.id + "|" + entity.index + "|" + action + "/";
  }

  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity, int actionCode)
  {
    return "live://span-responsible/" + entity.index + "/" + actionCode;
  }

  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity,string action, string argumentX, string argumentY, string argumentZ)
  {
    return "live://span-responsible/" + entity.index + "/" + action + "|" + argumentX + "|" + argumentY + "|" + argumentZ;
  }

  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity, string action, string argumentX, string argumentY)
  {
    return "live://span-responsible/" + entity.index + "/" + action + "|" + argumentX + "|" + argumentY;
  }
  
  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity, string action, string argumentX)
  {
    return "live://span-responsible/" + entity.index + "/" + action + "|" + argumentX;
  }

  public string MakeResponsibleActionURL(zx_DSP_SpanBase entity, string action)
  {
    return "live://span-responsible/" + entity.index + "/" + action;
  }
  
  public string MakeResponsibleHTML(zx_DSP_SpanBase entity, string questionLabel)
  {
    if (_respmode != RESPMODE_NONE and _respitemindex == entity.index) {
      HTMLBuffer buff = HTMLBufferStatic.Construct();
      buff.Print(questionLabel);
      buff.Print("&nbsp;&nbsp;");
      if(_respmode == RESPMODE_WAITCONFITM) buff.Print(HTMLWindow.MakeLink("live://responsible-accept/" + entity.index, _core.MakeHTMLImage("responsible-accept-16", 16), _strtable.GetString("tooltip-responsible-accept")));
      else buff.Print(_core.MakeHTMLImage("responsible-accept-d-16", 16));
      buff.Print("&nbsp;");
      buff.Print(HTMLWindow.MakeLink("live://responsible-cancel/" + entity.index, _core.MakeHTMLImage("responsible-cancel-16", 16), _strtable.GetString("tooltip-responsible-cancel")));
      return buff.AsString(); 
    }
    return "";
  }

  public string MakeShowTabURL(int stationIndex, int tab)
  {
    return "live://goro-station-tab/" + stationIndex + "/" + tab;
  }

  public bool CheckResponsible(zx_DSP_SpanBase entity)
  {
    return _respmode != RESPMODE_NONE and _respitemindex == entity.index;
  }

  public StringTable GetStringTable()
  {
    return _strtable;
  }
  
  public zx_DSP_Station GetStation()
  {
    return _station;
  }
  
  public string ResponsibleAction(zx_DSP_SpanBase entity)
  {
    if (CheckResponsible(entity)) return _respaction;
    return (string)null; 
  }
  
  public string ResponsibleArgumentX(zx_DSP_SpanBase entity) 
  {
    if (CheckResponsible(entity)) return _respargx;
    return (string)null; 
  }

  public string ResponsibleArgumentY(zx_DSP_SpanBase entity) 
  {
    if (CheckResponsible(entity)) return _respargy;
    return (string)null; 
  }

  public string ResponsibleArgumentZ(zx_DSP_SpanBase entity) 
  {
    if (CheckResponsible(entity)) return _respargz;
    return (string)null; 
  }
  

  public zx_DSP_Core GetCore() 
  {
    return _core;  
  }

};



//=============================================================================
// Name: zx_DSP_UIActionContext
// Desc: Класс, представляющий контекст для выполнения действий вызванных из
//       пользовательского интерфейса.
//=============================================================================
final class zx_DSP_UIActionContext isclass GSObject
{

  //=============================================================================
  // Name: GetStation
  // Desc: Возвращает ссылку на станцию, которая выхвала действие.
  // Retn: Объект zx_DSP_Station представляющий станцию. 
  //=============================================================================
  public zx_DSP_Station GetStation();

  //=============================================================================
  // Name: GetCore
  // Desc: Возвращает ссылку на ядро пульта. 
  // Retn: Объект zx_DSP_Core представляющий ядро пульта. 
  //=============================================================================
  public zx_DSP_Core GetCore();
  
  //=============================================================================
  // Name: UserName
  // Desc: Возвращает имя пользователя, инициировашего действие.
  // Retn: Имя пользователя. 
  //=============================================================================
  public string UserName();
  
  //=============================================================================
  // Name: IsRequest
  // Desc: Возвращает значение, указывающее, было ли действие вызвано запросом
  //       от клиента.
  // Retn: Значение true, если действие было вызвано запрсом от клиента; в
  //       в противном случае, если действие вызвано на сервере — значение false. 
  //=============================================================================
  public bool IsRequest();
  
  //=============================================================================
  // Name: Sync
  // Desc: Генерирует синхронизацию сущности с клиентами
  // Parm: entity — сущность, которую требуется синхронизировать
  // Parm: includeInverse — значение true, если требуется синхронизировать
  //       оба полуперегона: или значение false, если требуется синхронизировать
  //       только указанный полуперегон.
  //=============================================================================
  public void Sync(zx_DSP_SpanBase entity, bool includeInverse);
  
  //=============================================================================
  // Name: Sync
  // Desc: Генерирует синхронизацию сущности с клиентами
  // Parm: entity - сущность, которую требуется синхронизировать
  // Parm: soup – объект Soup содержащий данные для синхронизации. 
  //=============================================================================
  public void Sync(zx_DSP_SpanBase entity, Soup soup); 
  
  //=============================================================================
  // Name: SendResponce
  // Desc: Отправка ответа на запрос действия при IsRequest() == true
  // Parm: entity - сущность, которую требуется синхронизировать
  // Parm: soup – объект Soup содержащий данные с ответом. 
  //=============================================================================
  public void SendResponce(zx_DSP_SpanBase entity, Soup soup);

  //=============================================================================
  // Name: UpdateUI
  // Desc: Выполняет обновление пользовательского интерфейса для сущности
  // Parm: entity - сущность, для которой требуется обновление интерфейса
  //=============================================================================
  public void UpdateUI(zx_DSP_SpanBase entity);

	//
	// РЕАЛИЗАЦИЯ
	//

  zx_DSP_Core _core;
  zx_DSP_Station _station;
//  zd_DSP_ControllerForm _controllerform;
  string _username;
  
  public zx_DSP_UIActionContext Init(zx_DSP_Core core, zx_DSP_Station station, string userName)
  {
    _core = core;
    _station = station;
    _username = userName;
    return me;
  }

  public zx_DSP_Station GetStation()
  {
    return _station;
  }

  public zx_DSP_Core GetCore()
  {
    return _core;
  }
  
  public string UserName()
  {
    return _username;
  }
  
  public bool IsRequest()
  {
    return _username and _core.GetLocalPlayerName() != _username;
  }
  
  public void Sync(zx_DSP_SpanBase entity, bool includeInverse)
  {
    _core.SyncSpan(entity.index, includeInverse);
  }
  
  public void Sync(zx_DSP_SpanBase entity, Soup syncSoup)
  {
    if (!MultiplayerGame.IsActive() or !MultiplayerGame.IsServer() or !syncSoup) return;
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("Object", "Span");
    soup.SetNamedTag("Index", entity.index);
    soup.SetNamedSoup("Data", syncSoup); 
    MultiplayerGame.BroadcastGameplayMessage("ZXDSP_Sync", "SyncObject", soup);
  } 
  
  public void SendResponce(zx_DSP_SpanBase entity, Soup syncSoup)
  {
    if (!MultiplayerGame.IsActive() or !MultiplayerGame.IsServer() or !syncSoup) return;
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("Object", "Span-Responce");
    soup.SetNamedTag("Index", entity.index);
    soup.SetNamedSoup("Data", syncSoup); 
    MultiplayerGame.SendGameplayMessageToClient(_username, "ZXDSP_Sync", "SyncObject", soup);
  }

  public void UpdateUI(zx_DSP_SpanBase entity)
  {
    //todo: отметка о необходимости обновления UI для заданной сущности
  }
};