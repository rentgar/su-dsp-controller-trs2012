//=============================================================================
// File: zx_dsp_permissionform.gs
// Desc: Класс формы управления правами пользователей для управления станциями. 
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_classes.gs"
include "zx_dsp_core.gs"
include "htmlbuffer.gs"
include "browser.gs"



//=============================================================================
// Name: zx_DSP_PermissionsForm
// Desc: Форма управления правами польщователей по управлению станцией.
//=============================================================================
final class zx_DSP_PermissionsForm isclass GameObject
{

  //=============================================================================
  // Name: Init
  // Desc: Инициализирует новое окно управления правми пользователей на станции.
  // Parm: core – Объект zx_DSP_Core, представляющий ядро маршрутизации. 
  // Parm: strTable – Таблица строк для интерфейса браузера. 
  // Retn: Инициалищированный объект zx_DSP_PermissionsForm.
  //=============================================================================
  public zx_DSP_PermissionsForm Init(zx_DSP_Core core, StringTable strTable);
  
  //=============================================================================
  // Name: Update
  // Desc: Обновляет окно управления правами пользователей.
  //=============================================================================
  public void Update(void); 
  
  //=============================================================================
  // Name: BringToFront
  // Desc: Восстанавливает окно, если оно было свёрнуто, и перемещает его на
  //       передний план, поверх всех окон. 
  //=============================================================================
  public void BringToFront(void);

  //=============================================================================
  // Name: Dispose
  // Desc: Освобождает все ресурсы для этого объекта. 
  //=============================================================================
  public void Dispose(void);

  //=============================================================================
  // Name: UpdateAllowedUserList
  // Desc: Обновляет список пользователй, которым рахрешён доступ к выбранной
  //       станции. 
  //=============================================================================
  void UpdateAllowedUserList(void);
  
  //=============================================================================
  // Name: UpdateUserList
  // Desc: Обновляет список пользователей в сессии. 
  //=============================================================================
  void UpdateUserList(void);
  
  //=============================================================================
  // Name: OnLinkClick
  // Desc: Вызывается при нажатии на ссылке в браузере 
  // Parm: action — Действие, которое необходимо выполнить
  //=============================================================================
  void OnLinkClick(string action, string argument);

	//
	// РЕАЛИЗАЦИЯ
	//

  zx_DSP_Core _core;                          //Тут ссылка на ядро
  Browser _browser;                           //Собственно само окно браузера
  StringTable _strtable;                      //Таблица строк
  MultiplayerSessionManager _sessionmanager;  //Менеджер мультиплеерной сессией
  int _stationviewed = -1;                    //Текущая выбраная станция
  string _selectstationuser;                  //Имя текущего выбраного пользователя станции
  string _selectuser;                         //Имя выбраного пользователя
  
  
  public zx_DSP_PermissionsForm Init(zx_DSP_Core core, StringTable strTable)
  {
    if(_browser) return me; //Если инициализация уже выполнена, то выходим.
    
    _core = core;
    _strtable = strTable;
    _sessionmanager = cast<MultiplayerSessionManager>TrainzScript.GetLibrary(_core.GetAsset().LookupKUIDTable("multiplayer-core"));
                                         
    _browser = Constructors.NewBrowser();
    _browser.SetWindowStyle(Browser.STYLE_DEFAULT); 
    _browser.SetScrollEnabled(false);        
    _browser.SetCloseEnabled(true);                                        
    _browser.SetWindowPriority(Browser.BP_Window);
    _browser.SetButtonOverlayStyle(Browser.BS_None, Browser.BS_OK);
    _browser.SetWindowTitle(strTable.GetString("permissionsform-caption"));
    _browser.SetWindowCentered(570, 302);
    _browser.LoadHTMLFile(_core.GetAsset(), _core.StyleFormPermissions);
    
    _browser.SetElementProperty("stationlist", "html", "");
    _browser.SetElementProperty("stationlist", "background-style", "1");
    
    _browser.SetElementProperty("alloweduserlist", "html", "");
    _browser.SetElementProperty("alloweduserlist", "background-style", "1");
                              
    _browser.SetElementProperty("userlist", "html", "");
    _browser.SetElementProperty("userlist", "background-style", "1");

    _browser.SetElementProperty("username", "max-chars", "32");

    Sniff(_sessionmanager, "MultiplayerSession", "UsersChange", true);
    AddHandler(me, "MultiplayerSession", "UsersChange", "MultiplayerUsersChange");
 
    AddHandler(_browser, "Browser", "Closed", "BrowserClosedHandler");
    AddHandler(_browser, "Browser", "button-ok", "BrowserClickOkHandler");
    AddHandler(_browser, "Browser-URL", "", "BrowserURLHandler");
        
    Update(); 
    
    return me;
  }

  public void Update(void)
  {
    if(_core.stations and _core.stations.size()){
      string lnktablestart = HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%");
      string lnktableend = HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable();
      HTMLBuffer buff = HTMLBufferStatic.Construct();
      int i, count = _core.stations.size();
      buff.Print("<html><body>");
      buff.Print(HTMLWindow.StartTable("width=100%"));
      for (i = 0; i < count; ++i) {
        zx_DSP_Station station = _core.stations[i];
        buff.Print(HTMLWindow.StartRow());
        if (i == _stationviewed) buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleSelectorBackColor));
        else buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(HTMLWindow.MakeLink("live://viewstation/" + i, lnktablestart + station.name + lnktableend, _strtable.GetString("tooltip-selectstation")));
        buff.Print(HTMLWindow.EndCell() + HTMLWindow.EndRow());
      }
      buff.Print(HTMLWindow.EndTable());
      buff.Print("</body></html>");
      _browser.SetElementProperty("stationlist", "html", buff.AsString());
    }
    _browser.SetElementProperty("adduserbutton", "value", (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()));
    UpdateAllowedUserList();
  } 

  public void BringToFront(void)
  {
    if (_browser) {
      _browser.RestoreWindow();
      _browser.BringToFront();    
    }
  }
  
  public void Dispose(void)
  {
    if (_browser) {
        _browser.CloseWindow();
        AddHandler(_browser, "Browser", "Closed", null);
        AddHandler(_browser, "Browser", "button-ok", null);
        AddHandler(_browser, "Browser-URL", "", null);
    }
    if (_sessionmanager) Sniff(_sessionmanager, "MultiplayerSession", "UsersChange", false);
    AddHandler(me, "MultiplayerSession", "UsersChange", null);
    _core = null;
    _browser = null;
    _strtable = null;
    _sessionmanager = null;
    _stationviewed = -1;
    _selectstationuser = null;
    _selectuser = null;
  }
  
  void UpdateAllowedUserList(void)
  {
    HTMLBuffer buff = HTMLBufferStatic.Construct();
    buff.Print("<html><body>");
    if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
      zx_DSP_Station station = _core.stations[_stationviewed];
      string lnktablestart = HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%");
      string lnktableend = HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable();
      buff.Print(HTMLWindow.StartTable("width=100%"));
      int i, count = station.users.size();
      for (i = 0; i < count; ++i) {
        string username = station.users[i];
        buff.Print(HTMLWindow.StartRow());
        if (username == _selectstationuser) buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleSelectorBackColor));
        else buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(HTMLWindow.MakeLink("live://selectstationuser/" + username, lnktablestart + username + lnktableend, _strtable.GetString("tooltip-selectuser")));
        buff.Print(HTMLWindow.EndCell() + HTMLWindow.EndRow());
      }
      buff.Print(HTMLWindow.EndTable());
    }
    buff.Print("</body></html>");
    _browser.SetElementProperty("alloweduserlist", "html", buff.AsString());
    _browser.SetElementProperty("excludeuserbutton", "value", (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _selectstationuser != ""));
    UpdateUserList();
  }
  
  bool CanUserVisible(string username)
  {
    if (_core.GetLocalPlayerName() == username) return false;
    if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
      zx_DSP_Station station = _core.stations[_stationviewed];
      int i, count = station.users.size();
      for (i = 0; i < count; ++i)
        if (station.users[i] == username) return false; 
    }
    return true;
  }  

  void UpdateUserList(void)
  {
    /*string[] users = new string[6];
    users[0] = "Erendir";
    users[1] = "Zima";
    users[2] = "DSemen";
    users[3] = "Tram__";
    users[4] = "alekseymesh";
    users[5] = "Proton2";*/
  
    string lnktablestart = HTMLWindow.StartTable("width=100%") + HTMLWindow.StartRow() + HTMLWindow.StartCell("width=100%");
    string lnktableend = HTMLWindow.EndCell() + HTMLWindow.EndRow() + HTMLWindow.EndTable();
    HTMLBuffer buff = HTMLBufferStatic.Construct();
    buff.Print("<html><body>");
    buff.Print(HTMLWindow.StartTable("width=100%"));
    int i, count = _sessionmanager.CountUsers(); //users.size();
    bool hasselectuser = false;
    for (i = 0; i < count; ++i) {
      string username = _sessionmanager.GetIndexedUsername(i); //users[i]; 
      Str.ToLower(username);
      if (CanUserVisible(username)) {
        buff.Print(HTMLWindow.StartRow());
        if (username == _selectuser) {
          buff.Print(HTMLWindow.StartCell("width=100% bgcolor=#" + _core.StyleSelectorBackColor));
          hasselectuser = true;
        } else buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(HTMLWindow.MakeLink("live://selectuser/" + username, lnktablestart + username + lnktableend, _strtable.GetString("tooltip-selectuser")));
        buff.Print(HTMLWindow.EndCell() + HTMLWindow.EndRow());
      }  
    }
    buff.Print(HTMLWindow.EndTable());
    buff.Print("</body></html>");
    if (!hasselectuser) _selectuser = null;
    _browser.SetElementProperty("userlist", "html", buff.AsString());
    _browser.SetElementProperty("includeuserbutton", "value", (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _selectuser != ""));
  }

  void OnLinkClick(string action, string argument)
  {
    if(action == "viewstation") {
      int index = Str.ToInt(argument);
      if (_core.stations and _stationviewed != index and index >= 0 and index < _core.stations.size()) {
        _stationviewed = index;
        _selectstationuser = null;
        Update();
      }          
    } else if (action == "adduser") {
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size()) {
        string username = _browser.GetElementProperty("username", "text");
        Str.TrimLeft(username, " ");
        Str.TrimRight(username, " ");
        Str.ToLower(username);
        if (username.size() and _core.AddPermissionUser(_stationviewed, username)) {
          _selectstationuser = username;
          UpdateAllowedUserList();
          _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
        } else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
      }
      _browser.SetElementProperty("username", "text", "");
    } else if (action == "selectstationuser") {
      _selectstationuser = argument;
      UpdateAllowedUserList();
    } else if (action == "excludeuser") {
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _selectstationuser != ""){
        _core.RemovePermissionUser(_stationviewed, _selectstationuser);
        _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
        _selectstationuser = null;
        UpdateAllowedUserList(); 
      }
    } else if (action == "selectuser") {
      _selectuser = argument;
      UpdateUserList();
    } else if (action == "includeuser") {
      if (_core.stations and _stationviewed >= 0 and _stationviewed < _core.stations.size() and _selectuser != "") {
        if (_core.AddPermissionUser(_stationviewed, _selectuser)) _core.PlaySoundEvent(zx_DSP_Library.SOUND_CLICK);
        else _core.PlaySoundEvent(zx_DSP_Library.SOUND_ERROR);
        _selectstationuser = _selectuser;
        UpdateAllowedUserList(); 
      }
    }      
  }

  void BrowserURLHandler(Message msg)
  {
    if(msg.src == _browser and /* msg.src == msg.dst and сообщение шлётся бродкастом */   
    msg.minor[0, 7] == "live://") {
      string[] args = Str.Tokens(msg.minor[7,], "/");
      if (args.size() == 2) OnLinkClick(args[0], args[1]);
      else OnLinkClick(args[0], null);
    } 
  }  

  void BrowserClosedHandler(Message msg)
  {
    if (msg.src == _browser and msg.src == msg.dst) {
      PostMessage(_core, "ZXDSP", "PermissionFormClosed", 0.0);
      Dispose();
    }
  }
  
  void BrowserClickOkHandler(Message msg){ BrowserClosedHandler(msg); }  
  
  void MultiplayerUsersChange(Message msg) { UpdateUserList(); }

};