//=============================================================================
// File: zx_dsp_controller.gs
// Desc: Содержит класс пульта ДСП
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_group_marker_base.gs"
include "zx_dsp_alsncodebreaker.gs"
include "zx_dsp_stationborder.gs"
include "ct_advancedjunction.gs"
include "zx_dsp_spaninfo.gs"
include "zx_dsp_core.gs"
include "string.gs"


//=============================================================================
// Name: zx_DSP_Controller
// Desc: Реализация класса контроллера ДСП
//=============================================================================
class zx_DSP_Controller isclass zx_DSP_Core
{
  int _calcprocess = -1, _calcmaxprogress;
  string _lasterrormessage; 
  string _calcstationname;
  
  int _updateindicatortimeout;
  string _workindicator = "";

  bool _viewinvisibled = false;                                                        //Указатель, что необходимо показывать скрытые маршруты  
  
  int _selectedstation        = -1;                       //Выбранная станция
  int _objectid               = -1;                       //Индекс выбраного объекта станции
  int _objecttype;                                        //Тип выбраного объекта (0 - маршруты, 1 - стрелки, 2 - светофоры, 3 - перегоны, 4 - маневровые маршруты)
  int _subobjectid            = -1;                       //Индекс выбранного подобъекта
  
  Signal _selectedsignal;                                 //Светофор, от которого раскрыт список маршрутов
  
  Browser _propertybrowser;
  
  
  final void CheckMustSleepEx(void)
  {
    if (World.GetSeconds() >= _updateindicatortimeout) {
      if (_workindicator.size() == 5) _workindicator = "";
      else _workindicator = _workindicator + ".";
      PropertyBrowserRefresh(_propertybrowser);
      _updateindicatortimeout = World.GetSeconds() + 2.0;
    }
    CheckMustSleep();
  }  
  

  //Создаёт html код для флажка
  //linkId — идентификатор для ссылки
  //checked — значение true, если флажок установлен, иначе false
  //tooltip — всплывающая подсказка
  public final string MakeCheckBox(string linkId, bool checked, string tooltip) 
  {
    KUID imagekuid;
    if (checked) imagekuid = Constructors.GetTrainzAsset().LookupKUIDTable("checkbox-on");
    else imagekuid = Constructors.GetTrainzAsset().LookupKUIDTable("checkbox-off");
    string linkvalue = "<img kuid='" + imagekuid.GetHTMLString() + "' width=16 height=16>"; 
    return HTMLWindow.MakeLink("live://property/" + linkId, linkvalue, tooltip);
  }

  //Создаёт html код для флажка
  //checked — значение true, если флажок установлен, иначе false
  public final string MakeCheckBoxUnlink(bool checked) 
  {
    KUID imagekuid;
    if (checked) imagekuid = Constructors.GetTrainzAsset().LookupKUIDTable("checkbox-on");
    else imagekuid = Constructors.GetTrainzAsset().LookupKUIDTable("checkbox-off");
    return "<img kuid='" + imagekuid.GetHTMLString() + "' width=16 height=16>"; 
  }

  //Разбиение строки индексов на отдельные индексы
  public final int[] GetIndexes(string si)
  {
    string[] sIndexes = Str.Tokens(si, ".");
    int[] ret = new int[2];
    ret[0] = Str.UnpackInt(sIndexes[0]);
    ret[1] = Str.UnpackInt(sIndexes[1]);
    if(sIndexes.size() > 2) ret[2] = Str.UnpackInt(sIndexes[2]);
    return ret;
  }

  //Сверка двух светофоров для сортировки
  final int SignalCompare(zxSignal one, zxSignal two, bool inversStation)
  {
    int result = 0;
    int oneDir = DetermDirection(one.privateName);
    int twoDir = DetermDirection(two.privateName);
    if (two.Type & zxSignal.ST_IN and !(one.Type & zxSignal.ST_IN)) result = 1;
    else if (!(two.Type & zxSignal.ST_IN) and one.Type & zxSignal.ST_IN) result = -1;
    else if (two.Type & zxSignal.ST_IN and one.Type & zxSignal.ST_IN) {
      if (oneDir == twoDir) {
        if (two.stationName < one.stationName) {
          result = 1;
        }
        else if (two.stationName > one.stationName) {
          result = -1;
        }
      }

      if (!result) {
        zx_DSP_SpanBase oneSpan = null;
        zx_DSP_SpanBase twoSpan = null;

        zx_DSP_SignalInfo signalInfo = GetSignalInfo(one);
        if (signalInfo and signalInfo.span) {
          oneSpan = signalInfo.span.inverse;
        }
        signalInfo = GetSignalInfo(two);
        if (signalInfo and signalInfo.span) {
          twoSpan = signalInfo.span.inverse;
        }

        if (twoSpan and !oneSpan) {
          result = 1;
        }
        else if (!twoSpan and oneSpan) {
          result = -1;
        }
        else if (twoSpan and oneSpan and (oneDir == twoDir)) {
          if (twoSpan.signals[0].stationName < oneSpan.signals[0].stationName) {
            result = 1;
          }
          else if (twoSpan.signals[0].stationName > oneSpan.signals[0].stationName) {
            result = -1;
          }
        }
      }
    }
    else if (two.Type & zxSignal.ST_SHUNT and !(one.Type & zxSignal.ST_SHUNT)) result = -1;
    else if (!(two.Type & zxSignal.ST_SHUNT) and one.Type & zxSignal.ST_SHUNT) result = 1;
    else if (two.Type & zxSignal.ST_SHUNT and one.Type & zxSignal.ST_SHUNT) result = 0;
    else if (two.Type & zxSignal.ST_ROUTER and !(one.Type & zxSignal.ST_ROUTER)) result = 1;
    else if (!(two.Type & zxSignal.ST_ROUTER) and one.Type & zxSignal.ST_ROUTER) result = -1;
    else if (two.Type & zxSignal.ST_ROUTER and one.Type & zxSignal.ST_ROUTER) result = 0;
    else if (two.Type & zxSignal.ST_OUT and !(one.Type & zxSignal.ST_OUT)) result = 1;
    else if (!(two.Type & zxSignal.ST_OUT) and one.Type & zxSignal.ST_OUT) result = -1;
    else if (two.Type & zxSignal.ST_OUT and one.Type & zxSignal.ST_OUT) result = 0;
    if (!result and oneDir != twoDir) {
      if (0 == twoDir) {
        result = 1;
      }
      else if (0 == oneDir) {
        result = -1;
      }
      else if (oneDir < twoDir) {
        result = 1;
      }
      else {
        result = -1;
      }
    }
    if (!result) {
      string twoStr = "" + two.privateName;
      int twoNum = 0;
      int i = 0;
      while (i < twoStr.size()) {
        if (twoStr[i] >= '0' and twoStr[i] <= '9') {
          twoNum = 10 * twoNum + (twoStr[i] - '0');
          twoStr[i, i+1] = null;
        }
        else {
          ++i;
        }
      }
      string oneStr = "" + one.privateName;
      int oneNum = 0;
      i = 0;
      while (i < oneStr.size()) {
        if (oneStr[i] >= '0' and oneStr[i] <= '9') {
          oneNum = 10 * oneNum + (oneStr[i] - '0');
          oneStr[i, i+1] = null;
        }
        else {
          ++i;
        }
      }
      if (twoStr < oneStr) {
        result = 1;
      }
      else if (twoStr > oneStr) {
        result = -1;
      }
      else if (twoNum < oneNum) {
        result = 1;
      }
      else if (twoNum > oneNum) {
        result = -1;
      }
      else if (two.privateName < one.privateName) {         //НМ1 - 1НМ
        result = 1;
      }
      else if (two.privateName > one.privateName) {
        result = -1;
      }
    }
    if (0 == result and two.GetId() < one.GetId()) result = 1;
    else if (0 == result and two.GetId() > one.GetId()) result = -1;
    return result;
  }

  //Сверка двху перегонов для сортировки
  //one — первый сравниваемый перегон
  //two — второй сравниваемый перегон
  //station — Станция, для которой выполняется сортировка перегонов
  final int SpanCompare(zx_DSP_SpanBase one, zx_DSP_SpanBase two, zx_DSP_Station station)
  {
    one = one.inverse;
    two = two.inverse;
    zxSignal sig1 = one.signals[0];
    zxSignal sig2 = two.signals[0];
    int dir1 = DetermDirection(sig1.privateName);
    int dir2 = DetermDirection(sig2.privateName);

    if (dir1 < dir2) return 1;
    if (dir1 > dir2) return -1;
    if (sig1.stationName > sig2.stationName) return 1;
    if (sig1.stationName < sig2.stationName) return -1;
    if (sig1.privateName > sig2.privateName) return 1;
    if (sig1.privateName < sig2.privateName) return -1;

    return 0;
  }

  //Выполняет сортировку массива стрелок
  //junctions — массив стрелок, который необходимо отсортировать
	final void SortJunction(zx_DSP_JunctionInfo[] junctions) 
  {
    int i, j, count = junctions.size();
    for (i = 0; i < count - 1; ++i) {
      int minindex = i;
      for (j = i + 1; j < count; ++j) {
        int minnumber = junctions[minindex].number;
        int number = junctions[j].number;
        int result = 0;
        if (minnumber == 0 and number > 0) result = 1;
        else if (minnumber > 0 and number == 0) result = -1;
        else if (minnumber < number) result = -1;
        else if (minnumber > number) result = 1;
        if (!result) {
          string minTitle = junctions[minindex].GetTitle();
          string title = junctions[j].GetTitle();
          if (minTitle < title) {
            result = -1;
          }
          else if (minTitle > title) {
            result = 1;
          }
        }
        if (!result) {
          string minName = junctions[minindex].name;
          string name = junctions[j].name;
          if (minName < name) {
            result = -1;
          }
          else if (minName > name) {
            result = 1;
          }
        }
        if (result > 0) {
          minindex = j;
        }
        CheckMustSleep();
      }
      if (minindex != i) {
        zx_DSP_JunctionInfo jncinfo = junctions[i];
        junctions[i] = junctions[minindex];
        junctions[minindex] = jncinfo;  
      }
    }
	}

	//Сортиировка списка стрелок для станции
  //station — станция, стрелки которой необходимо отсортировать
	final void StationJunctionSort(zx_DSP_Station station) 
  {
		SortJunction(station.jncs);
	} 

  //Выполняет ассинхронную сортировку стрелок станции
  //station — станция, стрелки которой необходимо отсортировать
  final thread void SortStationJunctionAsync(zx_DSP_Station station, CacheSleeper sleeper)
  {
    SortJunction(station.jncs);
	  GenerateCache(sleeper);
  }
  
  //Выполняет сортировку светофоров станции
  //station — станция, чьи светофоры нужно отсортировать 
  final void StationSignalSort(zx_DSP_Station station)
  {
    int i, j, count = station.signals.size();
    for (i = 0; i < count - 1; ++i) {
      int minindex = i;
      for (j = i + 1; j < count; ++j) {
        if (SignalCompare(station.signals[minindex], station.signals[j], true) == 1) minindex = j;
        CheckMustSleep();
      }
      if (minindex != i) {
        zxSignal sign = station.signals[i];
        station.signals[i] = station.signals[minindex];
        station.signals[minindex] = sign;
      }
    }
  }
   
  //Выполняет сортировку перегонов станции
  //station — станция, чьи перегоны нужно отсортировать 
  final void StationSpanSort(zx_DSP_Station station)
  {
    int i, j, count = station.spans.size();
    for (i = 0; i < count - 1; ++i) {
      int minindex = i;
      for (j = i + 1; j < count; ++j) {
        if (SpanCompare(station.spans[minindex], station.spans[j], station) == 1) minindex = j;
        CheckMustSleep();
      }
      if (minindex != i) {
        zx_DSP_SpanBase span = station.spans[i];
        station.spans[i] = station.spans[minindex];
        station.spans[minindex] = span;
      }
    }
  }
  

  //Сортировка вариантов маршрута
  //path — маршрут для которого нужно отсортировать ваиантов
  //cansleep — значение true, если допускается усыпление потока
  final void SortPathVariant(zx_DSP_Path path, bool cansleep)
  {
    int i, j, count = path.variants.size();
    for (i = 0; i < count - 1; ++i) {
      int minindex = i;
      for (j = i + 1; j < count; ++j) {
        if (path.variants[j].priorityMultiplier < path.variants[minindex].priorityMultiplier) minindex = j;
		    else if (path.variants[j].priorityMultiplier == path.variants[minindex].priorityMultiplier and path.variants[j].jncs.size() < path.variants[minindex].jncs.size()) minindex = j;
        if (cansleep) CheckMustSleep();
      }
      if (minindex != i) {
        zx_DSP_PathVariant variant = path.variants[i];
        path.variants[i] = path.variants[minindex];
        path.variants[minindex] = variant;
      }
    }
    if (count > 0 and !(path.variants[0].marker & zxMarker.MRFORBIDXMODE) and	(path.startSign.Type & zxSignal.ST_FLOAT_BLOCK)) {
      path.type = path.type | zx_DSP_Path.TYPE_X_MODE;
    }
    else {
      path.type = path.type & ~zx_DSP_Path.TYPE_X_MODE;
    }
  }

  //Сортировка списка маршрутов для станции
  //station — станция на которой нужно отсортировать маршруты
  final void PathSort(zx_DSP_Station station)
  {
    int i, j, count = station.paths.size();
    for (i = 0; i < count; ++i) {
      int minindex = i;
      for (j = i + 1; j < count; ++j) {
        zx_DSP_Path one = station.paths[minindex];
        zx_DSP_Path two = station.paths[j];
        int result = SignalCompare(one.startSign, two.startSign, true);
        zxSignal endsignalone = one.backwardEndSign;
        zxSignal endsignaltwo = two.backwardEndSign;
        if (result == 0) {
          if (!endsignalone or (endsignalone.Type & zxSignal.ST_IN) or (one.endSign and endsignalone.privateName.size() >= 3 and endsignalone.privateName[1, 2] == ss[1, 2])) {
            endsignalone = one.endSign;
          }
          if (!endsignaltwo or (endsignaltwo.Type & zxSignal.ST_IN) or (two.endSign and endsignaltwo.privateName.size() >= 3 and endsignaltwo.privateName[1, 2] == ss[1, 2])) {
            endsignaltwo = two.endSign;
          }
          result = SignalCompare(endsignalone, endsignaltwo, false);
        }
        if (result > 0) {
          minindex = j;
        }
        CheckMustSleep();
      }
      if(minindex != i){
        zx_DSP_Path mpath = station.paths[i];
        station.paths[i] = station.paths[minindex];
        station.paths[minindex] = mpath;
      }
      station.paths[i].index = i;
      SortPathVariant(station.paths[i], true);
    }
  }

  //Сортировка списка маневровых маршрутов для станции
  //station - станция на которой нужно отсортировать маневровые маршруты
  final void ShuntPathSort(zx_DSP_Station station) {
    int i, j;
    int count = station.shuntPaths.size();
    for (i = 0; i < count; ++i) {
      int minindex = i;
      for (j = i + 1; j < count; ++j) {
        zx_DSP_ShuntPath one = station.shuntPaths[minindex];
        zx_DSP_ShuntPath two = station.shuntPaths[j];
        int result = SignalCompare(one.startSign, two.startSign, false);
        if (!result) {
          if (one.priorityMultiplier > two.priorityMultiplier) {
            result = 1;
          }
          else if (one.priorityMultiplier < two.priorityMultiplier) {
            result = -1;
          }
        }
        CheckMustSleep();
        if (!result) {
          zxSignal sigOne = one.backwardEndSign;
          if (!sigOne) {
            sigOne = one.endSign;
          }
          zxSignal sigTwo = two.backwardEndSign;
          if (!sigTwo) {
            sigTwo = two.endSign;
          }
          result = SignalCompare(sigOne, sigTwo, false);
        }
        if (result > 0) {
          minindex = j;
        }
        CheckMustSleep();
      }
      if (minindex != i) {
        zx_DSP_ShuntPath mpath = station.shuntPaths[i];
        station.shuntPaths[i] = station.shuntPaths[minindex];
        station.shuntPaths[minindex] = mpath;
      }
      station.shuntPaths[i].index = i;
    }
  }

  //Сортировка списка станций
  final void StationSort(void)
  {
    int i, j, count = stations.size();
    for (i = 0; i < count; ++i) {
      int minindex = i;
      for (j = i + 1; j < count; ++j) {
        if (stations[j].name < stations[minindex].name) minindex = j;
        CheckMustSleep();
      }
      if(minindex != i) {
        zx_DSP_Station station = stations[i];
        stations[i] = stations[minindex];
        stations[minindex] = station;
      }
      stations[i].index = i;
    }
  }
  
  //Получение масива о пошёрстности стрелки
  //jncstatcode — массив состояния стрелок п маршруту
  final bool[] GetJunctionsFace(int[] jncstatcode)
  {
    bool[] ret = new bool[jncstatcode.size()];
    int i, count = jncstatcode.size();
    for(i = 0; i < count; ++i)
      ret[i] = jncstatcode[i] & 4;
    return ret;
  }

  //=============================================================================
  // Name: CheckCorrectJunction
  // Desc: Выполняет проверку приавильности установки левера стрелки
  // Parm: junc – Левер стрелки для которого выполняется проверка
  // Retn: Значение true, если левер установлен правильно, в противном случае —
  //       значение false.
  //=============================================================================
  final bool CheckCorrectJunction(Junction junc)
  {
    int i;
    GSTrackSearch searcher;
    Trackside[] tsobjects = new Trackside[3];
    for (i = 0; i <= 2; ++i) {
      switch(i) {
        case 2:   searcher = junc.BeginTrackSearch(Junction.DIRECTION_RIGHT); 
                  break;
        case 1:   searcher = junc.BeginTrackSearch(Junction.DIRECTION_LEFT); 
                  break;
        default:  searcher = junc.BeginTrackSearch(Junction.DIRECTION_BACKWARD); 
                  break;
      }
      if (!searcher) {
        _lasterrormessage = _strtable.GetString1("error-junction-wrongplace", junc.GetLocalisedName());
        return false;
      } 
      MapObject mobject = searcher.SearchNext();
      while(mobject and !mobject.isclass(Trackside)) mobject = searcher.SearchNext();
      tsobjects[i] = cast<Trackside>mobject;
    }
    if (!tsobjects[0] or !tsobjects[1] or !tsobjects[2]) { //Если хоть в одном направлении нет trackside
      if (!tsobjects[0] and !tsobjects[1] and !tsobjects[2]) _lasterrormessage = _strtable.GetString1("error-junction-nullneighbor-allboth", junc.GetLocalisedName());
      else if (!tsobjects[0] and !tsobjects[1]) _lasterrormessage = _strtable.GetString1("error-junction-nullneighbor-backleft", junc.GetLocalisedName());
      else if (!tsobjects[0] and !tsobjects[2]) _lasterrormessage = _strtable.GetString1("error-junction-nullneighbor-backright", junc.GetLocalisedName());
      else if (!tsobjects[1] and !tsobjects[2]) _lasterrormessage = _strtable.GetString1("error-junction-nullneighbor-leftright", junc.GetLocalisedName());
      else if (!tsobjects[0]) _lasterrormessage = _strtable.GetString1("error-junction-nullneighbor-back", junc.GetLocalisedName());
      else if (!tsobjects[1]) _lasterrormessage = _strtable.GetString1("error-junction-nullneighbor-left", junc.GetLocalisedName());
      else _lasterrormessage = _strtable.GetString1("error-junction-nullneighbor-right", junc.GetLocalisedName());
      return false;
    } else if(tsobjects[0] != tsobjects[1] and tsobjects[1] == tsobjects[2] and tsobjects[2].isclass(Junction)) { //Если в противошёрстном направлении одна и та же стрелка
      _lasterrormessage = _strtable.GetString2("error-junction-neighborseparator", junc.GetLocalisedName(), (cast<Junction>tsobjects[2]).GetLocalisedName());
      return false;
    } else if(tsobjects[0] == tsobjects[1]) { //Если объекты для обратного и левого направления совпадают
      _lasterrormessage = _strtable.GetString1("error-junction-coincidence-backleft", junc.GetLocalisedName());
      return false;
    } else if(tsobjects[0] == tsobjects[2]) { //Если объекты для обратного и правого направления совпадают
      _lasterrormessage = _strtable.GetString1("error-junction-coincidence-backright", junc.GetLocalisedName());
      return false;
    } else if(tsobjects[1] == tsobjects[2]) { //Если объекты для левого и правого направления совпадают
      _lasterrormessage = _strtable.GetString1("error-junction-coincidence-leftright", junc.GetLocalisedName());
      return false;
    }
    return true;
  }
  
  //=============================================================================
  // Name: AddJunctionToList
  // Desc: Добавляет стрелку в список стрелок
  // Parm: junction – Левер стрелки которую необходимо добавить
  // Parm: insertindex – Отсчитываемый от 0 индекс в списке, по которому нужно
  //       вставить информацию о стрелке; или значение -1, если нужно добавить
  //       запись в конец списка. 
  // Retn: Объект zx_DSP_JunctionInfo, представляющий информацию о добавленной
  //       стрелке.
  //=============================================================================
  final zx_DSP_JunctionInfo AddJunctionToList(Junction junction, int insertindex)
  {                                                                  
    if (insertindex < 0) insertindex = jncs.size();
    zx_DSP_JunctionInfo juncinfo = new zx_DSP_JunctionInfo();
    juncinfo.name = junction.GetLocalisedName();
    juncinfo.jnc = junction;
    juncinfo.defaultDirection = junction.GetDirection();
    CT_AdvancedJunction advjunc = cast<CT_AdvancedJunction>junction;                //Пытаемся привести стрелку к AdvancedJunction
    if (advjunc) {                                                                //Если стрелка AdvancedJunction
      juncinfo.type = (advjunc.GetJunctionType() & (CT_AdvancedJunction.BRANCH_LEFT | CT_AdvancedJunction.BRANCH_RIGHT)) >> 1;                                                
      if(advjunc.JunctionIsManual()) juncinfo.type = juncinfo.type | zx_DSP_JunctionInfo.JNC_MANUAL;         
      if(advjunc.JunctionIsAutoReturn()) juncinfo.type = juncinfo.type | zx_DSP_JunctionInfo.JNC_AUTOREVERT;         
      juncinfo.number = advjunc.GetJunctionNumber();
      juncinfo.multiplierLeft = advjunc.GetDirectionLeftMultiplier();                                      
      juncinfo.multiplierRight = advjunc.GetDirectionRightMultiplier();                                    
    } 
    juncinfo.index = insertindex;
    jncs[insertindex] = juncinfo;                                                                            //Добавляем стрелку в список стрелок
    return juncinfo;
  }

  //=============================================================================
  // Name: CreateJunctionList
  // Desc: Создаёт список всех стрелок маршрута с проверкой правильности и
  //       предзагрузкой параметров из AdvancedJunction
  // Note: Должна вызываться только из thread функции
  //=============================================================================
  final bool CreateJunctionList(void)
  {
    AsyncObjectSearchResult aresult = World.GetNamedObjectList(AssetCategory.Junction, "");
    if(!aresult.SynchronouslyWaitForResults()) {
      _lasterrormessage = _strtable.GetString("error-junction-findlistfailure");
      return false;
    }
    NamedObjectInfo[] list = aresult.GetResults();
    int i, count = list.size(); 
    jncs = new zx_DSP_JunctionInfo[count];                                                         
    for (i = 0; i < count; ++i) {
      Junction junc = cast<Junction>list[i].objectRef;
      if (!junc) {
        _lasterrormessage = "no junction: "+list[i].localisedUsername+"("+list[i].objectId.SerialiseToString()+")";
        return false;
      }
      if (!CheckCorrectJunction(junc)) return false;                                          //Проверка правильности установки стрелки
      /*
      zx_DSP_JunctionInfo juncinfo = new zx_DSP_JunctionInfo();
      juncinfo.name = junc.GetLocalisedName();
      juncinfo.jnc = junc;
      juncinfo.defaultDirection = junc.GetDirection();
      CT_AdvancedJunction advjunc = cast<CT_AdvancedJunction>junc;                //Пытаемся привести стрелку к AdvancedJunction
      if (advjunc) {                                                                //Если стрелка AdvancedJunction
        juncinfo.type = (advjunc.GetJunctionType() & (CT_AdvancedJunction.BRANCH_LEFT | CT_AdvancedJunction.BRANCH_RIGHT)) >> 1;                                                
        if(advjunc.JunctionIsManual()) juncinfo.type = juncinfo.type | zx_DSP_JunctionInfo.JNC_MANUAL;         
        if(advjunc.JunctionIsAutoReturn()) juncinfo.type = juncinfo.type | zx_DSP_JunctionInfo.JNC_AUTOREVERT;         
        juncinfo.number = advjunc.GetJunctionNumber();
        juncinfo.multiplierLeft = advjunc.GetDirectionLeftMultiplier();                                      
        juncinfo.multiplierRight = advjunc.GetDirectionRightMultiplier();                                    
      } 
      juncinfo.index = i;
      jncs[i] = juncinfo;                                                                            //Добавляем стрелку в список стрелок
      */
      AddJunctionToList(junc, i);
      CheckMustSleep();
    }
    return true;
  }

	//Определяет длину маршрута и флаги маркеров su по маршруту
  //variant — вариант маршрута, для которого выполняется определение длины маршрута
  //startSign — светофор начала маршрута
  //endSign — светофор конца маршрута 
  //Возвращает true, если расчёт выполнен удачно 
	final bool CalcPathLengthAndMarkers(zx_DSP_JunctionsHolder holder, zxSignal startSign, zxSignal endSign) {
		GSTrackSearch searcher = startSign.BeginTrackSearch(true);
    int i, length, marker, count = holder.jncs.size();
    bool alsncodebreake = false;
		for (i = 0; i <= count; ++i) {
			MapObject foundobject = searcher.SearchNext();
			float distance = searcher.GetDistance();
			while (foundobject and !foundobject.isclass(Junction) and foundobject != endSign) {
        zx_DSP_ALSNCodeBreaker alsncodebreakemarker = cast<zx_DSP_ALSNCodeBreaker>foundobject;
				if (foundobject.isclass(zxMarker)) marker = marker | (cast<zxMarker>foundobject).trmrk_flag;
        if (alsncodebreakemarker and (!alsncodebreakemarker.BreakOnlyOneSide() or searcher.GetFacingRelativeToSearchDirection())) alsncodebreake = true; 
				if (distance >= 5000.0 and foundobject.isclass(Trackside)) {
					searcher = (cast<Trackside>foundobject).BeginTrackSearch(searcher.GetFacingRelativeToSearchDirection());
					length = length + distance;
				}
        CheckMustSleepEx();      
				foundobject = searcher.SearchNext();
				distance = searcher.GetDistance();
			}
			if (!foundobject) return false; //Если вдруг конец пути, то возвращаем ошибку
			length = length + distance; //Добавляем расстояние
			if (i < holder.jncs.size()) { //Если есть дальнейшая стрелка
				if(holder.jncFace[i]) searcher = holder.jncs[i].jnc.BeginTrackSearch(holder.jncDirections[i]);
				else searcher = holder.jncs[i].jnc.BeginTrackSearch(JunctionBase.DIRECTION_BACKWARD);
			}
		}
		holder.length = length;
		zx_DSP_PathVariant variant = cast<zx_DSP_PathVariant>holder;
		if (variant) {
			variant.marker = marker;
			variant.alsncoding = !alsncodebreake;
		}

		return true;
	}
  
  //Расчитывает множитель веса маршрута
  //pjncs — массив стрелок по маршруту
  //directions — массив направлений по стрелкам
  //faces — массив указателей пошёрстности стрелки
  final int GetPathPriorityMultiplier(zx_DSP_JunctionInfo[] pjncs, int[] directions, bool[] faces)
  {
    int i, multiplier, count = pjncs.size();
    for (i = 0; i < count; ++i) {
      if (faces[i]) {  //Множитель учитывается только для проивошёрстных стрелок
        if (directions[i] == JunctionBase.DIRECTION_LEFT) multiplier = multiplier + pjncs[i].multiplierLeft;
        else if (directions[i] == JunctionBase.DIRECTION_RIGHT) multiplier = multiplier + pjncs[i].multiplierRight;
      }
    }
    return multiplier;
  }

  //Выполняет поиск маршрута на станции
  //station — станция, для которой необходимо выполнить поиск маршрута
  //startSignal — светофор начала маршрута
  //endSignal — светофор конца маршрута
  final zx_DSP_Path FindPatch(zx_DSP_Station station, zxSignal startSignal, zxSignal endSignal)
  {
    zx_DSP_SignalInfo signalInfo = GetSignalInfo(startSignal);
    if (!signalInfo or !signalInfo.pathsFrom) {
      return null;
    }
    int i, count = signalInfo.pathsFrom.size();
    for (i = 0; i < count; ++i) {
      if (signalInfo.pathsFrom[i].endSign == endSignal) {
        return signalInfo.pathsFrom[i];
      }
      CheckMustSleepEx();      
    }
    return null;
  }

  zx_DSP_JunctionLinkMarkerBase[] junctionLinks;
  //Добавляет маркер спареных стрелок в станцию
  //station — станция, в которую необходимо добавить маркер
  //junctionLink — добавялемый маркер спаренных стрелок
	final void AddJunctionLinkToStation(zx_DSP_Station station, zx_DSP_JunctionLinkMarkerBase junctionLink) {
		int i, count = junctionLinks.size();
		for (i = 0; i < count; ++i)
			if (junctionLink == junctionLinks[i]) return;
		junctionLinks[junctionLinks.size()] = junctionLink;
	}

  //Создание информации о соседе
  //jncinfo — информация о стрелке соседе
  //direction — направление по которому находится соседство (JunctionBase.DIRECTION_*)
  final zx_DSP_JunctionNeighbor CreateJunctionNeighbor(zx_DSP_JunctionInfo jncinfo, int direction)
  {
    zx_DSP_JunctionNeighbor neighbor = new zx_DSP_JunctionNeighbor();
    neighbor.junctionIndex = jncinfo.index;
    neighbor.direction = direction;
    return neighbor;
  }

  final void AppendSpanSignalBehindEntrance(GSTrackSearch ts, zxSignal[] signals) {
    while(ts.SearchNextObject()) {
      object obj = ts.GetObject();
      if (cast<JunctionBase>obj) {
        break;
      }
      zxSignal sig = cast<zxSignal>obj;
      if (!sig) {
        continue;
      }
      if (ts.GetFacingRelativeToSearchDirection()) {
        if ((sig.Type & zxSignal.ST_UNTYPED) and !(sig.Type & zxSignal.ST_UNLINKED)) {
          break;
        }
      }
      else {
        if (
          ((sig.Type & (zxSignal.ST_UNTYPED | zxSignal.ST_PERMOPENED)) == (zxSignal.ST_UNTYPED | zxSignal.ST_PERMOPENED))
          and !(sig.Type & (zxSignal.ST_UNLINKED | zxSignal.ST_FLOAT_BLOCK))
        ) {
          signals[signals.size()] = sig;
          break;
        }
      }
    }
  }

  //Выолняет рассчёт перегона и добавляет его в базу
  //entranceSignal - входной светофор, от которого необходимо выполнить расчёт перегона
  //Возвращает отсчитываемый от 0 индекс перегона в базе или значение -1 – если перегон отсутствует; значение -2 – если ошибка перегона (требуется завершить рассчёт).  
  final int CalcSpan(zxSignal entranceSignal)
  {
    zxSignal[] signals = new zxSignal[1];
    signals[0] = entranceSignal;
    zxSignal[] inverseSignals = new zxSignal[0];
    zx_DSP_SpanInfo spaninfomarker;
    bool multiinfomarker = false;
    bool facing = true;
    GSTrackSearch searcher = entranceSignal.BeginTrackSearch(false); //Ищём от входного светофора назад 
    MapObject foundobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
    float spanLength = 0.0;
    while (foundobject and !foundobject.isclass(Junction) and (!foundobject.isclass(zxSignal) or !ECLLogic.GetBitMask((cast<zxSignal>foundobject).Type, zxSignal.ST_UNTYPED | zxSignal.ST_IN))) {
      zxSignal signal = cast<zxSignal>foundobject;
      if (signal and ECLLogic.GetBitMask(signal.Type, zxSignal.ST_UNTYPED | zxSignal.ST_PERMOPENED) and !(signal.Type &  zxSignal.ST_UNLINKED)) {
        if (searcher.GetFacingRelativeToSearchDirection()) {
          inverseSignals[0,0] = new zxSignal[1];
          inverseSignals[0] = signal;
        }
        else signals[signals.size()] = signal; 
      } else if (foundobject.isclass(zx_DSP_SpanInfo)) {
        if (!spaninfomarker) {
          spaninfomarker = cast<zx_DSP_SpanInfo>((object)foundobject);
          facing = searcher.GetFacingRelativeToSearchDirection();
        }
        else multiinfomarker = true;
      }
      if (distance >= 5000.0 and foundobject.isclass(Trackside)) {
        spanLength = spanLength + distance;
        searcher = (cast<Trackside>foundobject).BeginTrackSearch(searcher.GetFacingRelativeToSearchDirection()); //Если пройдено 5 км, то начинаем поиск от последнего объекта (защита от потери поиска)
      }
      CheckMustSleepEx();      
      foundobject = searcher.SearchNext();
      distance = searcher.GetDistance();
    }
    if (foundobject and foundobject.isclass(zxSignal) and ECLLogic.GetBitMask((cast<zxSignal>foundobject).Type, zxSignal.ST_UNTYPED | zxSignal.ST_IN)) {
      zxSignal endspansignal = cast<zxSignal>foundobject;
      if (!searcher.GetFacingRelativeToSearchDirection()) {
        string onename = entranceSignal.privateName + "@" + entranceSignal.stationName;
        string twoname = endspansignal.privateName + "@" + endspansignal.stationName;
        _lasterrormessage = _strtable.GetString4("error-moreentrancesignal", onename, entranceSignal.GetLocalisedName(), twoname, endspansignal.GetLocalisedName());
        return -2;
      }
      spanLength = spanLength + distance;
      inverseSignals[0,0] = new zxSignal[1];
      inverseSignals[0] = endspansignal;
      if (multiinfomarker) {
        string onename = entranceSignal.privateName + "@" + entranceSignal.stationName; 
        string twoname = endspansignal.privateName + "@" + endspansignal.stationName; 
        _lasterrormessage = _strtable.GetString4("error-morespanmarker", onename, entranceSignal.GetLocalisedName(), twoname, endspansignal.GetLocalisedName());
        return -2;
      }
      AppendSpanSignalBehindEntrance(searcher, signals);
      AppendSpanSignalBehindEntrance(entranceSignal.BeginTrackSearch(true), inverseSignals);
      int spantype = zx_DSP_SpanInfo.TYPE_AUTOLOCK;
      if (spaninfomarker) spantype = spaninfomarker.GetSpanType();
      else if (spanLength < 25.0 and signals.size() == 1 and inverseSignals.size() == 1) spantype = zx_DSP_SpanInfo.TYPE_NULL;
      zx_DSP_SpanBase span = CreateSpanObject(spantype);
      zx_DSP_SpanBase inverseSpan = CreateSpanObject(spantype);
      span.inverse = inverseSpan;
      inverseSpan.inverse = span;
      span.index = spans.size();
      spans[span.index] = span;
      inverseSpan.index = spans.size();
      spans[inverseSpan.index] = inverseSpan;
      span.signals = signals;
      inverseSpan.signals = inverseSignals;
      span.Init(spaninfomarker, !facing);
      inverseSpan.Init(spaninfomarker, facing);

      int i, n;
      n = span.signals.size();
      for (i = 0; i < n; ++i) {
        zx_DSP_SignalInfo info = GetOrCreateSignalInfo(span.signals[i]);
        info.span = span;
      }
      n = inverseSpan.signals.size();
      for (i = 0; i < n; ++i) {
        zx_DSP_SignalInfo info = GetOrCreateSignalInfo(inverseSpan.signals[i]);
        info.span = inverseSpan;
      }
      span.SpanInitialization();
      inverseSpan.SpanInitialization();

      return span.index;
    }
    return -1;
  }
  
  //Выполняет поиск в базе перегона для указаного входного светофора.
  //entranceSignal — входной светофор, для которого требуется найти перегон.
  //canCalc — значение true, если при отсутвии перегона в базе требуется выполнить рассчёт пергона; в противном случае значение – false. 
  //Возвращает отсчитываемый от 0 индекс перегона в базе или значение -1 – если перегон отсутствует; значение -2 – если ошибка перегона (требуется завершить рассчёт).  
  public int FindSpan(zxSignal entranceSignal, bool canCalc)
  {
    if (!spans) {
      if (canCalc) return CalcSpan(entranceSignal);
      return -1; 
    }
    int i, count = spans.size();
    for (i = 0; i < count; ++i) {
      if (spans[i].signals[0] == entranceSignal) return i;
      CheckMustSleepEx();      
    }
    if (canCalc) return CalcSpan(entranceSignal);
    return -1;
  }

  final void MakePathInStation(zx_DSP_Station station, zx_DSP_JunctionInfo[] pjncs, int[] cdirection, int[] sdirection, zxSignal startSign, zxSignal endSign, zxSignal backwardEndSign, zxSignal[] forwardObjects, zxSignal[] backwardObjects) {
    // if (!pjncs.size()) return;    //временное исключение бесстрелочных маршрутов
    zx_DSP_PathVariant pvariant = new zx_DSP_PathVariant();
    pvariant.jncs.copy(pjncs);
    pvariant.jncDirections.copy(cdirection);
    pvariant.jncFace = GetJunctionsFace(sdirection);
    pvariant.forwardObjects.copy(forwardObjects);
    pvariant.backwardObjects.copy(backwardObjects);
    if (backwardEndSign and pvariant.backwardObjects.size()) {
      int i;
      for (i = pvariant.backwardObjects.size(); i; --i) {
        if (pvariant.backwardObjects[i - 1] == backwardEndSign) {
          pvariant.backwardObjects[i - 1, i] = null;
        }
      }
    }
    CalcPathLengthAndMarkers(pvariant, startSign, endSign);
    pvariant.priorityMultiplier = GetPathPriorityMultiplier(pvariant.jncs, pvariant.jncDirections, pvariant.jncFace);
    pvariant.name = "";
    zx_DSP_Path newpath = FindPatch(station, startSign, endSign);
    if (!newpath) {
      newpath = new zx_DSP_Path(); //Создаём новый маршрут
      newpath.startSign = startSign;
      newpath.endSign = endSign;
      newpath.backwardEndSign = backwardEndSign;

      zx_DSP_SignalInfo signalInfo = GetOrCreateSignalInfo(newpath.startSign);
      if (!signalInfo.pathsFrom) {
        signalInfo.pathsFrom = new zx_DSP_Path[0];
      }
      signalInfo.pathsFrom[signalInfo.pathsFrom.size()] = newpath;
      signalInfo = GetOrCreateSignalInfo(newpath.endSign);
      if (!signalInfo.pathsTo) {
        signalInfo.pathsTo = new zx_DSP_Path[0];
      }
      signalInfo.pathsTo[signalInfo.pathsTo.size()] = newpath;
      if (newpath.backwardEndSign) {
        signalInfo = GetOrCreateSignalInfo(newpath.backwardEndSign);
        if (!signalInfo.pathsBehind) {
          signalInfo.pathsBehind = new zx_DSP_Path[0];
        }
        signalInfo.pathsBehind[signalInfo.pathsBehind.size()] = newpath;
      }
      station.paths[station.paths.size()] = newpath;
    }
    newpath.variants[newpath.variants.size()] = pvariant;
  }

  final void MakeShuntInStation(zx_DSP_Station station, zx_DSP_JunctionInfo[] pjncs, int[] cdirection, int[] sdirection, zxSignal startSign, zxSignal endSign, zxSignal backwardEndSign, zxSignal[] forwardObjects, zxSignal[] backwardObjects) {
    // if (!pjncs.size()) return;    //временное исключение бесстрелочных маршрутов
    zx_DSP_ShuntPath path = new zx_DSP_ShuntPath();
    path.startSign = startSign;
    path.endSign = endSign;
    path.backwardEndSign = backwardEndSign;
    path.jncs.copy(pjncs);
    path.jncDirections.copy(cdirection);
    path.jncFace = GetJunctionsFace(sdirection);
    path.forwardObjects.copy(forwardObjects);
    path.backwardObjects.copy(backwardObjects);
    if (backwardEndSign and path.backwardObjects.size()) {
      int i;
      for (i = path.backwardObjects.size(); i; --i) {
        if (path.backwardObjects[i - 1] == backwardEndSign) {
          path.backwardObjects[i - 1, i] = null;
        }
      }
    }
    CalcPathLengthAndMarkers(path, startSign, endSign);
    path.priorityMultiplier = GetPathPriorityMultiplier(path.jncs, path.jncDirections, path.jncFace);
    path.name = "";

    zx_DSP_SignalInfo signalInfo = GetOrCreateSignalInfo(path.startSign);
    if (!signalInfo.shuntPathsFrom) {
      signalInfo.shuntPathsFrom = new zx_DSP_ShuntPath[0];
    }
    signalInfo.shuntPathsFrom[signalInfo.shuntPathsFrom.size()] = path;
    if (path.endSign) {
      signalInfo = GetOrCreateSignalInfo(path.endSign);
      if (!signalInfo.shuntPathsTo) {
        signalInfo.shuntPathsTo = new zx_DSP_ShuntPath[0];
      }
      signalInfo.shuntPathsTo[signalInfo.shuntPathsTo.size()] = path;
    }
    if (path.backwardEndSign) {
      signalInfo = GetOrCreateSignalInfo(path.backwardEndSign);
      if (!signalInfo.shuntPathsBehind) {
        signalInfo.shuntPathsBehind = new zx_DSP_ShuntPath[0];
      }
      signalInfo.shuntPathsBehind[signalInfo.shuntPathsBehind.size()] = path;
    }
    station.shuntPaths[station.shuntPaths.size()] = path;
  }

  zxSignal[] groupSignals;
  //Рассчёт всех возможных маршрутов от светофора
  //station — станция, для которой выполняется расчёт
  //sSignal — светофор начала маршрута
  final bool CalcPaths(zx_DSP_Station station, zxSignal sSignal)
  {
    bool searchPath = sSignal.Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER) and !(sSignal.Type & (/*zxSignal.ST_UNLINKED | */zxSignal.ST_SHUNT));
    bool searchShunt = !(sSignal.Type & zxSignal.ST_PERMOPENED) and sSignal.ex_sgn[zxIndication.STATE_W];
    if (!searchPath and !searchShunt) {
      return true;
    }
    bool processPath = searchPath;
    bool processShunt = true;       //глубина маневровых отслеживается в любом случае. На случай группового
    zx_DSP_JunctionInfo[] pjncs = new zx_DSP_JunctionInfo[0]; //Тут будут лежать стрелки маршрута
    int[] sdirection = new int[0]; //Коды sdirection: 1 - левое направление; 2 - правое направление; 4 - Стрелка противошерстная (иначе пошёрстная);
    int[] cdirection = new int[0]; //Направления для стрелки
    zxSignal forwardObject = null;
    zxSignal[] forwardObjects = new zxSignal[0];
    int[] forwardObjectsCounts = new int[0];
    bool backwardObject = false;
    zxSignal[] backwardObjects = new zxSignal[0];
    int[] backwardObjectsCounts = new int[0];

    zx_DSP_SpanBase spanFrom;
    if (processPath and sSignal.Type & zxSignal.ST_IN) {              //Для входного светофора нужно выполнить рассчёт перегона.
      int spanFromId = FindSpan(sSignal, true);
      if (spanFromId >= 0) {
        spanFrom = spans[spanFromId];
        spanFrom.station = station;
        station.spans[station.spans.size()] = spanFrom; 
      } else if (spanFromId == -2) return false;          //При ошибке поиска перегона завершаем рассчёт. 
    } 

    GSTrackSearch searcher = sSignal.BeginTrackSearch(true); //Начинаем поиск от светофора
    MapObject foundobject, tschecker;
    bool start = true;
    bool foundedStationBorder = false;
    Trackside prevObject = sSignal;
    zxSignal[] backPathSigs = new zxSignal[1];
    zxSignal backShuntSig;
    int lastPathDepth = -1;
    int foundedPathDepth = -1;
    int foundedShuntDepth = -1;
    bool foundedGroupMarker = false;
    zx_DSP_XTriggerCore xtrigger;//todo: ошибка, если триггер будет найден на пути (не в горловине)

    while (start or pjncs.size() > 0) { //Выполняем поиск, пока есть стрелки в списке маршрута
      bool endBranch = false;
      bool terminateShunt = false;
      object obj;
      if (searcher.SearchNextObject()) {
        obj = searcher.GetObject();
      }
      else {
        terminateShunt = true;
        endBranch = true;
      }

      Trackside foundobject = cast<Trackside>obj;
      Junction foundjunction = cast<Junction>foundobject;
      zxSignal sig = cast<zxSignal>foundobject;
      zx_DSP_PathBreaker breaker = cast<zx_DSP_PathBreaker>foundobject;
      zx_DSP_StationBorder border = cast<zx_DSP_StationBorder>foundobject;
      zx_DSP_JunctionLinkMarkerBase link = cast<zx_DSP_JunctionLinkMarkerBase>foundobject;
      if (link) {
        AddJunctionLinkToStation(station, link);
      }
      if (breaker) {
        if (processPath and breaker.PathBreaked(searcher.GetFacingRelativeToSearchDirection())) {
          foundedPathDepth = pjncs.size();
          processPath = false;
        }
        if (processShunt and breaker.ShuntPathBreaked(searcher.GetFacingRelativeToSearchDirection())) {
          foundedShuntDepth = pjncs.size();
          processShunt = false;
        }
        if (breaker.IsStaionBorder()) {
          foundedStationBorder = true;
          terminateShunt = true;
        }
        if (!processPath and !processShunt) {
          endBranch = true;
        }
      }
      if (border) {
        if (processPath and !border.CanPathPassage()) {
          foundedPathDepth = pjncs.size();
          processPath = false;
        }
        foundedStationBorder = true;
        terminateShunt = true;
      }
      if (searcher.GetFacingRelativeToSearchDirection() and cast<zx_DSP_GroupMarkerBase>foundobject) {
        foundedGroupMarker = true;
        // processShunt = foundedShuntDepth < 0 or pjncs.size() <= foundedShuntDepth;   // манйвры просчитываем в любом случае на случай, наличие маршрутов будет определяться групповым
      }

      if (sig) {
        if (sig.stationName != station.name and (sig.Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER | zxSignal.ST_SHUNT))) {
          terminateShunt = true;
        }
        if (searcher.GetFacingRelativeToSearchDirection()) {
          if (sig.Type & (zxSignal.ST_SHUNT)) {
            if (forwardObject) {
              forwardObjects[forwardObjects.size()] = forwardObject;
            }
            forwardObject = sig;
          }
          if (processPath and sig.Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER)) {
            zx_DSP_SpanBase spanTo = null;
            if (sig.Type & zxSignal.ST_IN) {
              int result = FindSpan(sig, true);
              if (result >= 0) spanTo = spans[result];
              else if (result == -2) return false;
            }
            int i = pjncs.size();
            while (i > 0 and !backPathSigs[i]) {
              --i;
            }
            if (lastPathDepth != i or !backPathSigs[i]) {
              //todo: i - количество стрелок в маршруте, остальные - это уже стрелки в пути
              zxSignal back = backPathSigs[i];
              if (pjncs.size() != i) {        //Если есть стрелки за обратным светофором, то временно уберём его. До полноценной реализации стрелки в пути
                back = null;
              }
              MakePathInStation(station, pjncs, cdirection, sdirection, sSignal, sig, back, forwardObjects, backwardObjects);
            }
            else {    // Такое возможно, если для одного обратного светофора есть несколько маршрутов (стрелка в пути). Предполагаем, что правильный - тот, что по плюсам, поэтому создаём только первый из таких. Или всё же ошибку?
            }
            lastPathDepth = i;
            foundedPathDepth = pjncs.size();
            processPath = false;
          }
          if (processShunt and sig.Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER | zxSignal.ST_SHUNT)) {
            if (searchShunt or foundedGroupMarker) {
              MakeShuntInStation(station, pjncs, cdirection, sdirection, sSignal, sig, backShuntSig, forwardObjects, backwardObjects);
            }
            foundedShuntDepth = pjncs.size();
            processShunt = false;
          }
          if (foundedGroupMarker) {
            if (sig.Type & zxSignal.ST_IN) {
              _lasterrormessage = "sig: " + sig.GetLocalisedName() + " in sig can not be group";
              return false;
            }
            int i = 0;
            while (i < groupSignals.size() and sig != groupSignals[i]) ++i;
            groupSignals[i] = sig;
            processPath = processShunt = false;
          }
          if (!processPath and !processShunt) {
            endBranch = true;
          }
        }
        else {
          if (sig.Type & (/*zxSignal.ST_IN | */zxSignal.ST_OUT | zxSignal.ST_ROUTER | zxSignal.ST_SHUNT) and !foundedStationBorder) {
            if (backwardObject) {
              backwardObjects[backwardObjects.size()] = sig;
            }
            backwardObject = true;
          }
          if (!backPathSigs[pjncs.size()] and sig.Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER)) {
            backPathSigs[pjncs.size()] = sig;
          }
          if (!backShuntSig and sig.Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER | zxSignal.ST_SHUNT)) {
            backShuntSig = sig;
          }
          if (sig.Type & zxSignal.ST_IN) {
            foundedStationBorder = true;
          }
          if (sig.Type & zxSignal.ST_IN and processShunt) {
            // terminateShunt = true;
            if (sig != backShuntSig) {
              sig = null;
            }
            if (searchShunt or foundedGroupMarker) {
              MakeShuntInStation(station, pjncs, cdirection, sdirection, sSignal, sig, backShuntSig, forwardObjects, backwardObjects);     //Входной является концом маневрового маршрута, хоть и стоит тылом. Надеюсь, это не приведёт к архитектурным сюрпризам
            }
            foundedShuntDepth = pjncs.size();
            processShunt = false;
          }
        }
      }

      if (foundedGroupMarker and foundjunction) {
        _lasterrormessage = "found junction " + foundjunction.GetLocalisedName() + " after group marker";
        return false;
      }

      if (foundedStationBorder and foundjunction) {       //стрелка на перегоне?
        foundjunction = null;
        endBranch = true;
        foundedPathDepth = foundedShuntDepth = pjncs.size();
      }

      if (foundjunction) { //Если объект является стрелкой, то
        zx_DSP_JunctionInfo prevjnc;
        zx_DSP_JunctionInfo currinfo = FindJunctionInfo(foundjunction, true, false); //Получаем информацию о стрелке
        if (!currinfo) { //Если стрелка отсутствует в подготовленном списке
          if(CheckCorrectJunction(foundjunction)) currinfo = AddJunctionToList(foundjunction, -1);
          else return false;
          //_lasterrormessage = _strtable.GetString1("error-junctionnull", (cast<Junction>foundobject).GetLocalisedName());
          //return false;
        }
        if (currinfo.station and currinfo.station != station) { //Если найденая стрелка принадлежит другой станции
          _lasterrormessage = _strtable.GetString4("error-junctionforanotherstationsig", sSignal.GetLocalisedName(), station.name, currinfo.jnc.GetLocalisedName(), currinfo.station.name);
          return false;
        }

        int prevdircode, prevdir;
        //MapObject checker = tschecker;
        if (pjncs.size() > 0) { //Если в маршруте есть хоть одна стрелка
          prevjnc = pjncs[pjncs.size() - 1];
          prevdircode = sdirection[sdirection.size() - 1];
          prevdir = cdirection[cdirection.size() - 1];
          //if (!checker) checker = prevjnc.jnc;
        } //else if (!checker) checker = sSignal;

        int rcode = GetJunctionArrivalDirection(currinfo.jnc, prevObject, true); //Определяем с какой стороны была найдена стрелка
        if (rcode == -1) {
          if (pjncs.size() > 0) _lasterrormessage = _strtable.GetString2("error-neighbortracksideplace-junction", currinfo.jnc.GetLocalisedName(), pjncs[pjncs.size() - 1].jnc.GetLocalisedName());
          else _lasterrormessage = _strtable.GetString3("error-neighbortracksideplace-signal", currinfo.jnc.GetLocalisedName(), sSignal.GetLocalisedName(), sSignal.privateName);
          return false; 
        }
        if (!(rcode & 7)) { //Если левер с ошибкой, то выдаём сообщения и покидаем расчёт                       !!!!!!!!!!!Данная конструкция под вопросом
          _lasterrormessage = _strtable.GetString1("error-leverwrongplace", currinfo.jnc.GetLocalisedName());
          return false;
        }
        if (!currinfo.station) { //Если стрелка не привязана к станции, привязываем её
          station.jncs[station.jncs.size()] = currinfo;
          currinfo.stationId = station.id;
          currinfo.station = station;
        }

        if (currinfo.searchDirState & zx_DSP_JunctionInfo.JNC_USED) {
          //Это может быть петля. А может, разворот поиска...
          endBranch = true;
        }
        else if (currinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL) {
          endBranch = true;
          terminateShunt = true;
        }
        else {
          if (zx_DSP_JunctionInfo.JNC_BACK != rcode and pjncs.size() > 1) {       //Детектор корыта
            int cdir;
            if (zx_DSP_JunctionInfo.JNC_LEFT == rcode) {
              cdir = JunctionBase.DIRECTION_RIGHT;
            }
            else if (zx_DSP_JunctionInfo.JNC_RIGHT == rcode) {
              cdir = JunctionBase.DIRECTION_LEFT;
            }
            GSTrackSearch ts = currinfo.jnc.BeginTrackSearch(cdir);
            while (ts.SearchNextObject()) {
              object obj = ts.GetObject();
              zxSignal s = cast<zxSignal>obj;
              if (s and (s.Type & zxSignal.ST_UNTYPED) and (s.Type & STATIONSIGNALTYPEMASK)) {
                break;
              }
              JunctionBase j = cast<JunctionBase>obj;
              if (j) {
                int i;
                int n = pjncs.size() - 1;
                for (i = 0; i < n; ++i) {
                  if (j == pjncs[i].jnc) {
                    endBranch = true;
                    break;
                  }
                }
                break;
              }
            }
          }
        }
        if (!endBranch) {
          bool lface = rcode == zx_DSP_JunctionInfo.JNC_BACK; //Определяем, что стрелка противошёрстная
          int cdir, dircode = ECLLogic.SetBitMask(0, 4, lface);
          if (rcode == zx_DSP_JunctionInfo.JNC_LEFT) { //Если стрелка пошёрстная c отклонение влево
            cdir = JunctionBase.DIRECTION_LEFT;
            dircode = dircode | 1;
          } else if(rcode == zx_DSP_JunctionInfo.JNC_RIGHT) { //Если стрелка пошёрстная c отклонение вправо
            cdir = JunctionBase.DIRECTION_RIGHT;
            dircode = dircode | 2;
          } else { //Если стрелка противошёрстная
            cdir = currinfo.defaultDirection;
            dircode = ECLLogic.SetBitMask(dircode, 1, cdir == JunctionBase.DIRECTION_LEFT);
            dircode = ECLLogic.SetBitMask(dircode, 2, cdir == JunctionBase.DIRECTION_RIGHT);
          }
          if(lface) searcher = currinfo.jnc.BeginTrackSearch(cdir);//Если стрелка противошёрстная, то начинаем от неё поиск по исходному направлению
          else searcher = currinfo.jnc.BeginTrackSearch(Junction.DIRECTION_BACKWARD); //Если стрелка пошёрстная, то начинаем от неё поиск назад

          currinfo.searchDirState = currinfo.searchDirState | zx_DSP_JunctionInfo.JNC_USED;
          forwardObject = null;
          forwardObjectsCounts[pjncs.size()] = forwardObjects.size();
          backwardObject = false;
          backwardObjectsCounts[pjncs.size()] = backwardObjects.size();
          pjncs[pjncs.size()] = currinfo; //Добавляем стрелку в текущий маршрут
          sdirection[sdirection.size()] = dircode; //Добавляем информацию о пройденных направлениях
          cdirection[cdirection.size()] = cdir; //Добавляем информацию о текущем направлении стрелки в маршруте
          backPathSigs[pjncs.size()] = null;

          if (prevjnc) { //Если есть предыдущая стрека, то
            int prevjncdir = prevdir;
            if (!(prevdircode & 4)) //Если предыдущая стрелка пошёрстная
              prevjncdir = Junction.DIRECTION_BACKWARD; //Ставим ей указатель назад

            if (prevdircode & 4 and prevdir == JunctionBase.DIRECTION_LEFT) { //Если предыдущая стрелка противошёрстная и отклонение влево
              prevjnc.searchDirState = prevjnc.searchDirState | zx_DSP_JunctionInfo.JNC_LEFT; //Сохраняем указатель, что налево уже произведён поиск
              prevjnc.neighborLeft = CreateJunctionNeighbor(currinfo, ConvertDirection(rcode)); //Сохраняем ссылку на соседа
            } else if (prevdircode & 4 and prevdir == JunctionBase.DIRECTION_RIGHT) { //Если стрелка противошёрстная и отклонение вправа
              prevjnc.searchDirState = prevjnc.searchDirState | zx_DSP_JunctionInfo.JNC_RIGHT; //Сохраняем указатель, что направо уже произведён поиск
              prevjnc.neighborRight = CreateJunctionNeighbor(currinfo, ConvertDirection(rcode)); //Сохраняем ссылку на соседа
            } else if (!(prevdircode & 4)) { //Если стрелка пошёрстная
              prevjnc.searchDirState = prevjnc.searchDirState | zx_DSP_JunctionInfo.JNC_BACK; //Сохраняем указатель, что назад уже произведён поиск
              prevjnc.neighborBack = CreateJunctionNeighbor(currinfo, ConvertDirection(rcode)); //Сохраняем ссылку на соседа
            }

            if (!(dircode & 4) and cdir == JunctionBase.DIRECTION_LEFT) { //Если стрелка пошёрстная и направление налево
              currinfo.searchDirState = currinfo.searchDirState | zx_DSP_JunctionInfo.JNC_LEFT; //Сохраняем указатель, что налево уже произведён поиск
              currinfo.neighborLeft = CreateJunctionNeighbor(prevjnc, prevjncdir); //Сохраняем ссылку на соседа
            } else if (!(dircode & 4) and cdir == JunctionBase.DIRECTION_RIGHT) {//Если стрелка пошёрстная и направление направо
              currinfo.searchDirState = currinfo.searchDirState | zx_DSP_JunctionInfo.JNC_RIGHT; //Сохраняем указатель, что направо уже произведён поиск
              currinfo.neighborRight = CreateJunctionNeighbor(prevjnc, prevjncdir); //Сохраняем ссылку на соседа
            } else if (dircode & 4) { //Если стрелка противошёрстная
              currinfo.searchDirState = currinfo.searchDirState | zx_DSP_JunctionInfo.JNC_BACK; //Сохраняем указатель, что назад уже произведён поиск
              currinfo.neighborBack = CreateJunctionNeighbor(prevjnc, prevjncdir); //Сохраняем ссылку на соседа
            }
          }
          backShuntSig = null;
        }
      }
      if (terminateShunt and processShunt) {
        if (backShuntSig and (searchShunt or foundedGroupMarker)) {
          MakeShuntInStation(station, pjncs, cdirection, sdirection, sSignal, null, backShuntSig, forwardObjects, backwardObjects);
        }
        foundedShuntDepth = pjncs.size();
        processShunt = false;
      }
      if (endBranch) { //Если объект не является стрелкой
        start = false;
        while (pjncs.size() > 0) { //Выполняем, пока есть стрелки в маршруте и нет указателя прерывания
          backPathSigs[pjncs.size()] = null;
          if (pjncs.size() <= lastPathDepth) {
            lastPathDepth = -1;
          }
          zx_DSP_JunctionInfo linfo = pjncs[pjncs.size() - 1]; //Получаем информацию о последней стрелке маршрута
          int ldircode = sdirection[sdirection.size() - 1]; //Получаем код направление последней стрелки маршрута
          int ldir = cdirection[cdirection.size() - 1]; //Получаем указатель направления последней стрелки маршрута

          if (!foundobject) { //Если был найден конец пути, то запоминаем пройденые направления
            if (ldircode & 4 and ldir == JunctionBase.DIRECTION_LEFT) linfo.searchDirState = linfo.searchDirState | zx_DSP_JunctionInfo.JNC_LEFT;
            else if (ldircode & 4 and ldir == JunctionBase.DIRECTION_RIGHT) linfo.searchDirState = linfo.searchDirState | zx_DSP_JunctionInfo.JNC_RIGHT;
            else if (!(ldircode & 4)) linfo.searchDirState = linfo.searchDirState | zx_DSP_JunctionInfo.JNC_BACK;
          }

          if (ldircode & 4 and !ECLLogic.GetBitMask(ldircode, 3)) { //Если стрелка противошёрстная и не все направления проверены
            if (ldircode & 1) {
              cdirection[cdirection.size() - 1] = JunctionBase.DIRECTION_RIGHT;
              sdirection[sdirection.size() - 1] = ldircode | 2;
            } else if(ldircode & 2) {
              cdirection[cdirection.size() - 1] = JunctionBase.DIRECTION_LEFT;
              sdirection[sdirection.size() - 1] = ldircode | 1;
            }
            searcher = linfo.jnc.BeginTrackSearch(cdirection[cdirection.size() - 1]);//Начинаем от неё поиск
            prevObject = linfo.jnc;
            break;
          } else {
            zx_DSP_JunctionInfo info = pjncs[pjncs.size() - 1];
            info.searchDirState = info.searchDirState & ~zx_DSP_JunctionInfo.JNC_USED;
            pjncs[pjncs.size() - 1, pjncs.size()] = null;
            sdirection[sdirection.size() - 1, sdirection.size()] = null;
            cdirection[cdirection.size() - 1, cdirection.size()] = null;
          }
        }
        int n = Math.Max(0, pjncs.size() - 1);
        forwardObject = null;
        if (n < forwardObjectsCounts.size()) {
          forwardObjects[forwardObjectsCounts[n], ] = null;
        }
        backwardObject = false;
        if (n < backwardObjectsCounts.size()) {
          backwardObjects[backwardObjectsCounts[n], ] = null;
        }
        processPath = searchPath and (foundedPathDepth < 0 or pjncs.size() <= foundedPathDepth);
        processShunt = foundedShuntDepth < 0 or pjncs.size() <= foundedShuntDepth;
        backShuntSig = null;
        foundedStationBorder = false;
        foundedGroupMarker = false;
      }
      else if (foundobject) {
        prevObject = foundobject;
        if (searcher.GetDistance() >= 5000.0) {
          searcher = foundobject.BeginTrackSearch(searcher.GetFacingRelativeToSearchDirection()); //Если пройдено 5 км, то начинаем поиск от последнего объекта (защита от потери поиска)
        }
      }
      CheckMustSleepEx();
    }
    return true;
  }

  //Поиск всех стрелок станции
  //station — станция, на которой необходимо выполнить поиск стрелок
  final bool FindAllStationJunctions(zx_DSP_Station station)
  {
    int i;
    for (i = 0; i < station.jncs.size(); ++i) {
      zx_DSP_JunctionInfo jncinf = station.jncs[i];
      int dir;
      for (dir = zx_DSP_JunctionInfo.JNC_LEFT; dir < zx_DSP_JunctionInfo.JNC_MANUAL; dir = dir << 1) {
        if (jncinf.searchDirState & dir) {
          continue;
        }
        GSTrackSearch searcher = jncinf.jnc.BeginTrackSearch(ConvertDirection(dir));
        Junction foundjunction;
        Trackside prevObject = jncinf.jnc;
        while (searcher.SearchNextObject()) {
          object obj = searcher.GetObject();
          Trackside foundobject = cast<Trackside>obj;
          if (foundjunction = cast<Junction>foundobject) {
            break;
          }
          zxSignal sig = cast<zxSignal>foundobject;
          zx_DSP_JunctionLinkMarkerBase link = cast<zx_DSP_JunctionLinkMarkerBase>foundobject;
          if (link) {
            AddJunctionLinkToStation(station, link);
          }
          zx_DSP_PathBreaker breaker = cast<zx_DSP_PathBreaker>foundobject;
          if (breaker and breaker.IsStaionBorder()) {
            break;
          }
          if (cast<zx_DSP_StationBorder>foundobject) {
            break;
          }
          if (sig and sig.stationName != station.name) {
            break;
          }
          if (foundobject) {
            prevObject = foundobject;
            if (searcher.GetDistance() >= 5000.0) {
              searcher = foundobject.BeginTrackSearch(searcher.GetFacingRelativeToSearchDirection()); //Если пройдено 5 км, то начинаем поиск от последнего объекта (защита от потери поиска)
            }
          }
          CheckMustSleepEx();
        }
        if (foundjunction) {
          zx_DSP_JunctionInfo currinfo = FindJunctionInfo(foundjunction, true, false); //Получаем информацию о стрелке
          if (!currinfo) { //Если стрелка отсутствует в подготовленном списке
            if (CheckCorrectJunction(foundjunction)) {
              currinfo = AddJunctionToList(foundjunction, -1);
            }
            else {
              return false;
            }
          }
          if (currinfo.station and currinfo.station != station) { //Если найденая стрелка принадлежит другой станции
            _lasterrormessage = _strtable.GetString4("error-junctionforanotherstationjnc", jncinf.jnc.GetLocalisedName(), station.name, currinfo.jnc.GetLocalisedName(), currinfo.station.name);
            return false;
          }

          int rcode = GetJunctionArrivalDirection(currinfo.jnc, prevObject, true); //Определяем с какой стороны была найдена стрелка
          if (rcode == -1) {
            _lasterrormessage = _strtable.GetString2("error-neighbortracksideplace-junction", currinfo.jnc.GetLocalisedName(), jncinf.jnc.GetLocalisedName());
            return false;
          }
          if (!(rcode & 7)) { //Если левер с ошибкой, то выдаём сообщения и покидаем расчёт                       !!!!!!!!!!!Данная конструкция под вопросом
            _lasterrormessage = _strtable.GetString1("error-leverwrongplace", currinfo.jnc.GetLocalisedName());
            return false;
          }
          if (!currinfo.station) { //Если стрелка не привязана к станции, привязываем её
            station.jncs[station.jncs.size()] = currinfo;
            currinfo.stationId = station.id;
            currinfo.station = station;
          }

          if (zx_DSP_JunctionInfo.JNC_LEFT == dir) { //Если поиск с предыдущей стрелки пошёл налево
            jncinf.searchDirState = jncinf.searchDirState | zx_DSP_JunctionInfo.JNC_LEFT; //Сохраняем указатель, что налево уже произведён поиск
            jncinf.neighborLeft = CreateJunctionNeighbor(currinfo, ConvertDirection(rcode)); //Сохраняем ссылку на соседа
          }
          else if (zx_DSP_JunctionInfo.JNC_RIGHT == dir) { //Если поиск с предыдущей стрелки пошёл направо
            jncinf.searchDirState = jncinf.searchDirState | zx_DSP_JunctionInfo.JNC_RIGHT; //Сохраняем указатель, что направо уже произведён поиск
            jncinf.neighborRight = CreateJunctionNeighbor(currinfo, ConvertDirection(rcode)); //Сохраняем ссылку на соседа
          }
          else if(zx_DSP_JunctionInfo.JNC_BACK == dir) { //Если поиск с предыдущей стрелки пошёл назад
            jncinf.searchDirState = jncinf.searchDirState | zx_DSP_JunctionInfo.JNC_BACK; //Сохраняем указатель, что назад уже произведён поиск
            jncinf.neighborBack = CreateJunctionNeighbor(currinfo, ConvertDirection(rcode)); //Сохраняем ссылку на соседа
          }
          if (rcode == zx_DSP_JunctionInfo.JNC_LEFT) { //Если стрелка пошёрстная и направление налево
            currinfo.searchDirState = currinfo.searchDirState | zx_DSP_JunctionInfo.JNC_LEFT; //Сохраняем указатель, что налево уже произведён поиск
            currinfo.neighborLeft = CreateJunctionNeighbor(jncinf, ConvertDirection(dir)); //Сохраняем ссылку на соседа
          }
          else if (rcode == zx_DSP_JunctionInfo.JNC_RIGHT) { //Если стрелка пошёрстная и направление направо
            currinfo.searchDirState = currinfo.searchDirState | zx_DSP_JunctionInfo.JNC_RIGHT; //Сохраняем указатель, что направо уже произведён поиск
            currinfo.neighborRight = CreateJunctionNeighbor(jncinf, ConvertDirection(dir)); //Сохраняем ссылку на соседа
          }
          else if (rcode == zx_DSP_JunctionInfo.JNC_BACK) { //Если стрелка противошёрстная
            currinfo.searchDirState = currinfo.searchDirState | zx_DSP_JunctionInfo.JNC_BACK; //Сохраняем указатель, что назад уже произведён поиск
            currinfo.neighborBack = CreateJunctionNeighbor(jncinf, ConvertDirection(dir)); //Сохраняем ссылку на соседа
          }
        }
        jncinf.searchDirState = jncinf.searchDirState | dir;
      }
    }

    return true;
  }

  final void RemovePathFromArray(zx_DSP_Path[] array, zx_DSP_Path path) {
    int i;
    for (i = array.size(); i > 0; --i) {
      if (path == array[i - 1]) {
        array[i - 1, i] = null;
      }
    }
  }

  final void RemovePathFromArray(zx_DSP_ShuntPath[] array, zx_DSP_ShuntPath path) {
    int i;
    for (i = array.size(); i > 0; --i) {
      if (path == array[i - 1]) {
        array[i - 1, i] = null;
      }
    }
  }

  final bool MergeGroupPaths(zx_DSP_Station station) {
    int i;
    int cnt = groupSignals.size();
    for (i = 0; i < cnt; ++i) {
      zxSignal sig = groupSignals[i];
      zx_DSP_SignalInfo signalInfo = GetSignalInfo(sig);
      if (!signalInfo) {
        continue;
      }
      if (signalInfo.pathsBehind and signalInfo.pathsBehind.size()) {
        _lasterrormessage = "sig: " + sig.GetLocalisedName() + ", has backward path: " + signalInfo.pathsBehind[0].GetNameOrGenerate(_strtable);
        return false;
      }
      if (signalInfo.shuntPathsBehind and signalInfo.shuntPathsBehind.size()) {
        _lasterrormessage = "sig: " + sig.GetLocalisedName() + ", has backward shunt path: " + signalInfo.shuntPathsBehind[0].GetNameOrGenerate(_strtable);
        return false;
      }
      int j, k;
      int m = 0;
      int n = 0;
      if (signalInfo.pathsTo) {
        m = signalInfo.pathsTo.size();
      }
      if (signalInfo.pathsFrom) {
        n = signalInfo.pathsFrom.size();
      }
      zx_DSP_SignalInfo info;
      for (j = 0; j < m; ++j) {
        zx_DSP_Path pathTo = signalInfo.pathsTo[j];
        if (pathTo.groupSign) {
          _lasterrormessage = "sig: " + sig.GetLocalisedName() + ", path: " + pathTo.GetNameOrGenerate(_strtable) + " already has group signal: " + pathTo.groupSign.GetLocalisedName();
          return false;
        }
        for (k = 0; k < n; ++k) {
          zx_DSP_Path pathFrom = signalInfo.pathsFrom[k];
          zx_DSP_Path newPath = new zx_DSP_Path();
          newPath.startSign = pathTo.startSign;
          newPath.groupSign = sig;
          newPath.backwardEndSign = pathFrom.backwardEndSign;
          newPath.endSign = pathFrom.endSign;
          int ii, jj;
          int mm = pathTo.variants.size();
          int nn = pathFrom.variants.size();
          for (ii = 0; ii < mm; ++ii) {
            for (jj = 0; jj < nn; ++jj) {
              zx_DSP_PathVariant variant = new zx_DSP_PathVariant();
              variant.jncs.copy(pathFrom.variants[jj].jncs);
              variant.jncs[0,0] = pathTo.variants[ii].jncs;
              variant.jncDirections.copy(pathFrom.variants[jj].jncDirections);
              variant.jncDirections[0,0] = pathTo.variants[ii].jncDirections;
              variant.jncFace.copy(pathFrom.variants[jj].jncFace);
              variant.jncFace[0,0] = pathTo.variants[ii].jncFace;
              variant.length = pathTo.variants[ii].length + pathFrom.variants[jj].length;
              variant.marker = pathTo.variants[ii].marker | pathFrom.variants[jj].marker;
              variant.alsncoding = pathTo.variants[ii].alsncoding and pathFrom.variants[jj].alsncoding;
              variant.priorityMultiplier = pathTo.variants[ii].priorityMultiplier + pathFrom.variants[jj].priorityMultiplier;
              newPath.variants[newPath.variants.size()] = variant;
            }
          }
          station.paths[station.paths.size()] = newPath;
          info = GetOrCreateSignalInfo(newPath.startSign);
          info.pathsFrom[info.pathsFrom.size()] = newPath;
          if (newPath.backwardEndSign) {
            info = GetOrCreateSignalInfo(newPath.backwardEndSign);
            info.pathsBehind[info.pathsBehind.size()] = newPath;
          }
          if (newPath.endSign) {
            info = GetOrCreateSignalInfo(newPath.endSign);
            info.pathsTo[info.pathsTo.size()] = newPath;
          }
        }
        RemovePathFromArray(station.paths, pathTo);
        if ((info = GetSignalInfo(pathTo.startSign)) and info.pathsFrom) {
          RemovePathFromArray(info.pathsFrom, pathTo);
        }
        if (pathTo.backwardEndSign and (info = GetSignalInfo(pathTo.backwardEndSign)) and info.pathsBehind) {
          RemovePathFromArray(info.pathsBehind, pathTo);
        }
      }
      for (j = 0; j < n; ++j) {
        zx_DSP_Path path = signalInfo.pathsFrom[j];
        if (path.groupSign) {
          _lasterrormessage = "sig: " + sig.GetLocalisedName() + ", path: " + path.GetNameOrGenerate(_strtable) + " already has group signal: " + path.groupSign.GetLocalisedName();
          return false;
        }
        RemovePathFromArray(station.paths, path);
        if (path.endSign and (info = GetSignalInfo(path.endSign)) and info.pathsFrom) {
          RemovePathFromArray(info.pathsFrom, path);
        }
        if (path.backwardEndSign and (info = GetSignalInfo(path.backwardEndSign)) and info.pathsBehind) {
          RemovePathFromArray(info.pathsBehind, path);
        }
      }
      signalInfo.pathsFrom = new zx_DSP_Path[0];
      signalInfo.pathsTo = new zx_DSP_Path[0];

      m = n = 0;
      if (signalInfo.shuntPathsTo) {
        m = signalInfo.shuntPathsTo.size();
      }
      if (signalInfo.shuntPathsFrom) {
        n = signalInfo.shuntPathsFrom.size();
      }
      for (j = 0; j < m; ++j) {
        zx_DSP_ShuntPath pathTo = signalInfo.shuntPathsTo[j];
        if (pathTo.groupSign) {
          _lasterrormessage = "sig: " + sig.GetLocalisedName() + ", path: " + pathTo.GetNameOrGenerate(_strtable) + " already has group signal: " + pathTo.groupSign.GetLocalisedName();
          return false;
        }
        for (k = 0; k < n; ++k) {
          zx_DSP_ShuntPath pathFrom = signalInfo.shuntPathsFrom[k];
          zx_DSP_ShuntPath newPath = new zx_DSP_ShuntPath();
          newPath.startSign = pathTo.startSign;
          newPath.groupSign = sig;
          newPath.backwardEndSign = pathFrom.backwardEndSign;
          newPath.endSign = pathFrom.endSign;
          newPath.jncs.copy(pathFrom.jncs);
          newPath.jncs[0,0] = pathTo.jncs;
          newPath.jncDirections.copy(pathFrom.jncDirections);
          newPath.jncDirections[0,0] = pathTo.jncDirections;
          newPath.jncFace.copy(pathFrom.jncFace);
          newPath.jncFace[0,0] = pathTo.jncFace;
          newPath.length = pathTo.length + pathFrom.length;
          newPath.priorityMultiplier = pathTo.priorityMultiplier + pathFrom.priorityMultiplier;
          station.shuntPaths[station.shuntPaths.size()] = newPath;
          info = GetOrCreateSignalInfo(newPath.startSign);
          info.shuntPathsFrom[info.shuntPathsFrom.size()] = newPath;
          if (newPath.backwardEndSign) {
            info = GetOrCreateSignalInfo(newPath.backwardEndSign);
            info.shuntPathsBehind[info.shuntPathsBehind.size()] = newPath;
          }
          if (newPath.endSign) {
            info = GetOrCreateSignalInfo(newPath.endSign);
            info.shuntPathsTo[info.shuntPathsTo.size()] = newPath;
          }
        }
        RemovePathFromArray(station.shuntPaths, pathTo);
        if ((info = GetSignalInfo(pathTo.startSign)) and info.shuntPathsFrom) {
          RemovePathFromArray(info.shuntPathsFrom, pathTo);
        }
        if (pathTo.backwardEndSign and (info = GetSignalInfo(pathTo.backwardEndSign)) and info.pathsBehind) {
          RemovePathFromArray(info.shuntPathsBehind, pathTo);
        }
      }
      for (j = 0; j < n; ++j) {
        zx_DSP_ShuntPath path = signalInfo.shuntPathsFrom[j];
        if (path.groupSign) {
          _lasterrormessage = "sig: " + sig.GetLocalisedName() + ", path: " + path.GetNameOrGenerate(_strtable) + " already has group signal: " + path.groupSign.GetLocalisedName();
          return false;
        }
        RemovePathFromArray(station.shuntPaths, path);
        if (path.endSign and (info = GetSignalInfo(path.endSign)) and info.shuntPathsFrom) {
          RemovePathFromArray(info.shuntPathsFrom, path);
        }
        if (path.backwardEndSign and (info = GetSignalInfo(path.backwardEndSign)) and info.pathsBehind) {
          RemovePathFromArray(info.shuntPathsBehind, path);
        }
      }
      signalInfo.shuntPathsFrom = new zx_DSP_ShuntPath[0];
      signalInfo.shuntPathsTo = new zx_DSP_ShuntPath[0];
    }

    return true;
  }

  //Выполняет полиск спареных стрелок
  //link — маркер спареных стрелок
  //direction — направление поиска от маркера
  //retJunction — массив найденых стрелок
	void FindJunctionsByLinkMarker(zx_DSP_JunctionLinkMarkerBase link, bool direction, zx_DSP_JunctionNeighbor[] retJunction) {
		zx_DSP_JunctionInfo[] tocheck = new zx_DSP_JunctionInfo[0];
		tocheck[0] = FindFirstJunctionInfo(link, direction, true, false);
    if (!tocheck[0]) return;
		int dir = GetJunctionArrivalDirection(tocheck[0].jnc, link, true);
		if (zx_DSP_JunctionInfo.JNC_BACK == dir) {
			while (tocheck.size() > 0) {
				if (tocheck[0].neighborLeft) {
					if (tocheck[0].neighborLeft.direction == JunctionBase.DIRECTION_BACKWARD) tocheck[tocheck.size()] = jncs[tocheck[0].neighborLeft.junctionIndex];
					else retJunction[retJunction.size()] = tocheck[0].neighborLeft;
				}
				if (tocheck[0].neighborRight) {
					if (tocheck[0].neighborRight.direction == JunctionBase.DIRECTION_BACKWARD) tocheck[tocheck.size()] = jncs[tocheck[0].neighborRight.junctionIndex];
					else retJunction[retJunction.size()] = tocheck[0].neighborRight;
				}
				tocheck[0, 1] = null;
			}
		} else {
			zx_DSP_JunctionNeighbor neighbor = new zx_DSP_JunctionNeighbor();
			neighbor.junctionIndex = tocheck[0].index;
			neighbor.direction = ConvertDirection(dir);
			retJunction[retJunction.size()] = neighbor;
		}
	}

  //Выполняет полный рассчёт станции
  //stationName - имя станции, расчёт которой выполняется
  final bool CalcStation(string stationName)
  {
    zx_DSP_Station station;
    zxSignal[] signlist = new zxSignal[0];                                                            //Создаём пустой список для светофоров
    BinarySortedElementS[] dbse = suCore.Signals.DBSE;                                                //Получаем базу светофоров sU
    int i, count = suCore.Signals.N;                                                                  //Получаем количество светофоров в базе
    groupSignals = new zxSignal[0];
    for (i = 0; i < count; ++i) {                                                                       //Перебираем все светофоры sU
      zxSignalLink slnk = cast<zxSignalLink>dbse[i].Object;                                           //Получаем ссылку на светофор из базы
      if (slnk and slnk.sign.stationName == stationName /*and slnk.sign.Type &
      (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER) and
      !(slnk.sign.Type & (zxSignal.ST_UNLINKED | zxSignal.ST_SHUNT))*/) {                                //Если ссылка действительна, светофор принадлежит обрабатываемой станции и является поездным
        if (!station) {                                                                                   //Если информация о станции ещё не создана, создаём её
          station = new zx_DSP_Station();                                                                 //Создаём объект станции
          station.name = stationName;                                                                     //Записываем наименование станции
          station.id = nextId++;                                                                          //Генерируем id станции
          station.activeShuntPaths = new zx_DSP_ShuntPath[0];
        }
        if (!CalcPaths(station, slnk.sign)) return false;                                              //Выполняем рассчёт всех маршрутов для светофора
        me.Sleep(0.001);                                                                                 //Защита от таймаута
      }
      else {
        CheckMustSleep();
      }
      if (slnk and slnk.sign.stationName == stationName and slnk.sign.Type &
      (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER | zxSignal.ST_SHUNT)) {                   //Если ссылка действительна, светофор принадлжеит обрабатываемой станции и является поездным или маневровым
        signlist[signlist.size()] = slnk.sign;                                                          //Добавляем светофор в информацию о станции
        zx_DSP_SignalInfo info = GetOrCreateSignalInfo(slnk.sign);
        info.station = station;
      }
    }
    if (station) {                                                                                      //Если информация о станции присутствует
      if (!FindAllStationJunctions(station)) return false;                                             //Выполняем поиск всех стрелок станции

      if (!MergeGroupPaths(station)) {
        return false;
      }

      StationJunctionSort(station);                                                                  //Выполняем сортировку списка стрелок станции
      PathSort(station);                                                                             //Сортировка всех маршрутов
      ShuntPathSort(station);

      station.signals = signlist;                                                                       //Сохраняем список светофоров в станцию
      StationSignalSort(station);                                                                    //Выполняем сортировку списка светофоров
      
      StationSpanSort(station);                                                                       //Выполняем сортировку списка перегонов

      if (!stations) stations = new zx_DSP_Station[0];                                                   //Если список станций отсуствует, создаём его
      stations[stations.size()] = station;                                                              //Добавляем станцию в список
    }
    return true;
  }

  final bool LinkJunctions() {
    int i;
    int count = junctionLinks.size();
    for (i = 0; i < count; ++i) {
      zx_DSP_JunctionNeighbor[] junctionstolink = new zx_DSP_JunctionNeighbor[0];
      FindJunctionsByLinkMarker(junctionLinks[i], true, junctionstolink);
      int jnccount = junctionstolink.size(), j, dir;
      if (jnccount == 0) {
        _lasterrormessage = _strtable.GetString1("error-junction-link-nullneighbor", junctionLinks[i].GetLocalisedName());
        return false;
      }
      FindJunctionsByLinkMarker(junctionLinks[i], false, junctionstolink);
      if (jnccount == junctionstolink.size()) {
        _lasterrormessage = _strtable.GetString1("error-junction-link-nullneighbor", junctionLinks[i].GetLocalisedName());
        return false;
      }
      jnccount = junctionstolink.size();
      zx_DSP_JunctionInfo[] junctions = new zx_DSP_JunctionInfo[jnccount];
      for (j = 0; j < jnccount; ++j) {
        zx_DSP_JunctionInfo jncinfo = jncs[junctionstolink[j].junctionIndex];
        if (jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL) {
          _lasterrormessage = _strtable.GetString1("error-junction-link-manual", jncinfo.jnc.GetLocalisedName());
          return false;
        }
        // if (station.id != jncinfo.stationId) {
        //   _lasterrormessage = _strtable.GetString1("error-junction-link-another-station", jncinfo.jnc.GetLocalisedName());
        //   return false;
        // }
        if (jncinfo.masterJunction or (jncinfo.slaveJunctions and jncinfo.slaveJunctions.size())) {
          int k;
          string newGroup = "";
          for (k = 0; k < jnccount; ++k) {
            if (k) {
              newGroup = newGroup + ", ";
            }
            newGroup = newGroup + jncs[junctionstolink[k].junctionIndex].jnc.GetLocalisedName();
          }
          zx_DSP_JunctionInfo ji = jncinfo;
          if (ji.masterJunction) {
            ji = ji.masterJunction;
          }
          string anotherGroup = ji.jnc.GetLocalisedName();
          for (k = 0; k < ji.slaveJunctions.size(); ++k) {
            anotherGroup = anotherGroup + ", " + ji.slaveJunctions[k].jnc.GetLocalisedName();
          }
          _lasterrormessage = _strtable.GetString3("error-junction-link-another-group", jncinfo.jnc.GetLocalisedName(), newGroup, anotherGroup);
          return false;
        }
        if (j == 0) dir = jncinfo.defaultDirection == junctionstolink[j].direction;
        else if (dir != (jncinfo.defaultDirection == junctionstolink[j].direction)) {
          _lasterrormessage = _strtable.GetString1("error-junction-link-invalid-direction", jncinfo.jnc.GetLocalisedName());
          return false;
        }
        junctions[j] = jncinfo;
      }
      SortJunction(junctions);
      zx_DSP_JunctionInfo masterjncinfo = junctions[0];
      masterjncinfo.slaveJunctions = new zx_DSP_JunctionInfo[jnccount - 1];
      for (j = 1; j < jnccount; ++j) {
        zx_DSP_JunctionInfo jncinfo = junctions[j];
        jncinfo.masterJunction = masterjncinfo;
        masterjncinfo.slaveJunctions[j - 1] = jncinfo;
      }
    }

    return true;
  }

  //Выполняет полный рассчёт маршрута
  final thread void FindAndCalcAllStation(void)
  {
    PostMessage(me, "ZXDSP-Event", "BeginCalc", 0.0);
    ++currentInfoVersion;
    _lasterrormessage = "";
    _calcstationname = null;
    stations = null; 
    jncs = null;
    spans = new zx_DSP_SpanBase[0];
    junctionLinks = new zx_DSP_JunctionLinkMarkerBase[0];
    if (CreateJunctionList()) { //Выполняем создание списка всех стрелок маршрута
      for (_calcprocess = 0; _calcprocess < _calcmaxprogress; ++_calcprocess){ //Перебираем все станции
        _calcstationname = suCore.Stations.SE[_calcprocess];
        PropertyBrowserRefresh(_propertybrowser);
        if (!CalcStation(_calcstationname)) break; //Расчитываем указанную станцию
      }
      if (!stations or stations.size() == 0 or _lasterrormessage.size()){
        if (!_lasterrormessage.size()) _lasterrormessage = _strtable.GetString("error-calcempty");
        stations = null;
        jncs = null; 
        spans = null;
      }
    }
    if (spans) {
      int i;
      for (i = 0; i < spans.size(); ++i) {
        if (!spans[i].station) {
          _lasterrormessage = _strtable.GetString2("error-no-station-for-span", spans[i].signals[0].privateName + "@" + spans[i].signals[0].stationName, spans[i].signals[0].GetLocalisedName());
          if (spans[i].inverse) {
            _lasterrormessage = _lasterrormessage + _strtable.GetString2("error-no-station-for-span-inv", spans[i].inverse.signals[0].privateName + "@" + spans[i].inverse.signals[0].stationName, spans[i].inverse.signals[0].GetLocalisedName());
          }
          stations = null;
          jncs = null;
          spans = null;
          break;
        }
      }
    }
    _calcstationname = null;
    PropertyBrowserRefresh(_propertybrowser);
    if (stations and LinkJunctions()) {
      StationSort();
    }
    else {
      jncs = null; 
      stations = null;
      spans = null;
    }
    _calcprocess = -1;
    PropertyBrowserRefresh(_propertybrowser);
    PostMessage(me, "ZXDSP-Event", "EndCalc", 0.0);
	  GenerateCache();
  }

  public string GetPropertyType(string propertyID)
  {
    if(propertyID == "calculate" or propertyID == "update" or propertyID == "stationlist" or propertyID[0, 14] == "controlstation" or propertyID[0, 12] == "selectobject" or 
    propertyID[0, 15] == "junctioncontrol" or propertyID[0, 24] == "junctiondefaultdirection" or propertyID[0, 18] == "junctionresettitle" or propertyID[0, 14] == "junctionmanual" or 
    propertyID[0, 14] == "junctionrevert" or propertyID[0, 14] == "junctionmoveup" or propertyID[0, 16] == "junctionmovedown" or propertyID[0, 12] == "junctionsort" or 
    propertyID[0, 12] == "selectsignal" or propertyID[0, 13] == "selectssignal" or propertyID[0, 8] == "pathmain" or propertyID[0, 13] == "pathinvisible" or propertyID[0, 14] == "spathinvisible" or propertyID[0, 11] == "pathcontrol" or propertyID[0, 12] == "spathcontrol" or 
    propertyID[0, 13] == "pathresetname" or propertyID[0, 14] == "spathresetname" or propertyID[0, 13] == "variantmoveup" or propertyID[0, 15] == "variantmovedown" or propertyID[0, 16] == "variantinvisible" or
    propertyID[0, 11] == "variantsort" or propertyID[0, 14] == "variantcontrol" or propertyID == "viewinvisible" or propertyID[0, 16] == "variantresetname" or "variantalsn" == propertyID[, 11]) return "link";
	  else if (propertyID[0, 12] == "junctiontype") return "list,0";
	  else if (propertyID[0, 13] == "junctiontitle") return "string,0,6";                                                   
	  else if (propertyID[0, 8] == "pathname") return "string,0,20";
	  else if (propertyID[0, 9] == "spathname") return "string,0,20";
	  else if (propertyID[0, 11] == "variantname") return "string,0,20";
    else if (propertyID[0, 14] == "junctionnumber") return "int,1,10000";
    return inherited(propertyID);
  }                                                                                                         

  string GetPropertyName(string propertyID)
  {
    if (propertyID[, 12] == "junctiontype") return _strtable.GetString("editvalue-junctiontype");
    else if (propertyID[, 13] == "junctiontitle") return _strtable.GetString("editvalue-junctiontitle");
    else if (propertyID[0, 14] == "junctionnumber") return _strtable.GetString("editvalue-junctionnumber");
    else if (propertyID[0, 8] == "pathname") return _strtable.GetString("editvalue-pathname");
    else if (propertyID[0, 9] == "spathname") return _strtable.GetString("editvalue-pathname");
    else if (propertyID[0, 11] == "variantname") return _strtable.GetString("editvalue-variantname");
    return inherited(propertyID);
  }

  string GetPropertyValue(string propertyID)
  {
    if (propertyID[, 12] == "junctiontype") {
      int[] indexes = GetIndexes(propertyID[13,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT) return _strtable.GetString("junctioncontrol-type-left");
      else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT) return _strtable.GetString("junctioncontrol-type-right");
      else return _strtable.GetString("junctioncontrol-type-symetry");
    } else if (propertyID[, 13] == "junctiontitle") {
      int[] indexes = GetIndexes(propertyID[13,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      if (jncinfo.title) return jncinfo.title;
      else ""; 
    } else if (propertyID[0, 14] == "junctionnumber") {
      int[] indexes = GetIndexes(propertyID[15,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      return (string)jncinfo.number; 
    } else if (propertyID[0, 8] == "pathname") {
      int[] indexes = GetIndexes(propertyID[9,]);
      zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
      if (path.name) return path.name;
      return "";
    } else if (propertyID[0, 9] == "spathname") {
      int[] indexes = GetIndexes(propertyID[10,]);
      zx_DSP_ShuntPath path = stations[indexes[0]].shuntPaths[indexes[1]];
      if (path.name) return path.name;
      return ""; 
    } else if (propertyID[0, 11] == "variantname") {
      int[] indexes = GetIndexes(propertyID[12,]);
      zx_DSP_PathVariant variant = stations[indexes[0]].paths[indexes[1]].variants[indexes[2]];
      if (variant.name) return variant.name;
      return ""; 
    }
    return inherited(propertyID);
  }

	public string[] GetPropertyElementList(string propertyID) {
    if (propertyID[, 12] == "junctiontype") {
			string[] retvalue = new string[3];
      retvalue[0] = _strtable.GetString("junctioncontrol-type-symetry");
      retvalue[1] = _strtable.GetString("junctioncontrol-type-left");
      retvalue[2] = _strtable.GetString("junctioncontrol-type-right");
      return retvalue;
    }
		return inherited(propertyID);
	}

  final void CreateJunctionListForVariantHTML(HTMLBuffer buffer, zx_DSP_JunctionsHolder variant)
  {
    int i, count = variant.jncs.size();
    for (i = 0; i < count; ++i) {
      zx_DSP_JunctionInfo jncinfo = variant.jncs[i];
      string image;
      if(variant.jncFace[i]) {
        if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and variant.jncDirections[i] == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-f-l-l-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and variant.jncDirections[i] == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-f-l-f-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and variant.jncDirections[i] == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-f-r-f-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and variant.jncDirections[i] == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-f-r-r-24-16", 24, 16);
        else if (variant.jncDirections[i] == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-f-s-l-24-16", 24, 16);
        else if (variant.jncDirections[i] == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-f-s-r-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT) image = MakeHTMLImage("jnc-f-l-u-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT) image = MakeHTMLImage("jnc-f-r-u-24-16", 24, 16);
        else image = MakeHTMLImage("jnc-f-s-u-24-16", 24, 16);
      } else {
        if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and variant.jncDirections[i] == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-r-l-l-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and variant.jncDirections[i] == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-r-l-f-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and variant.jncDirections[i] == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-r-r-f-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and variant.jncDirections[i] == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-r-r-r-24-16", 24, 16);
        else if (variant.jncDirections[i] == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-r-s-l-24-16", 24, 16);
        else if (variant.jncDirections[i] == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-r-s-r-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT) image = MakeHTMLImage("jnc-r-l-u-24-16", 24, 16);
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT) image = MakeHTMLImage("jnc-r-r-u-24-16", 24, 16);
        else image = MakeHTMLImage("jnc-r-s-u-24-16", 24, 16);
      }
      if (i > 0) buffer.Print(" ⮞ ");
      buffer.Print(image + jncinfo.jnc.GetLocalisedName());
      //buffer.Print("<nowrap>" + image + jncinfo.jnc.GetLocalisedName() + "</nowrap>");
    }
  }
  
  final void CreateVariationControlHTML(HTMLBuffer buffer)
  {
    zx_DSP_PathVariant variant = stations[_selectedstation].paths[_objectid].variants[_subobjectid];
    string title = variant.name;
    if (!title) title = "";
    buffer.Print("<br />");
    buffer.Print(HTMLWindow.StartTable());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-name") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + title + "</nowrap>"));
        if (!generateMutex.IsLocked()) buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[<font color=#" + StyleActionButtonColor + ">" + HTMLWindow.MakeLink("live://property/variantname." + _selectedstation + "." + _objectid + "." + _subobjectid, "…", _strtable.GetString("tooltip-pathname")) + "</font>]</b></nowrap>"));
        else buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[…]</b></nowrap>"));
        if (!generateMutex.IsLocked() and title.size()) buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[<font color=#" + StyleDeleteButtonColor + ">" + HTMLWindow.MakeLink("live://property/variantresetname." + _selectedstation + "." + _objectid + "." + _subobjectid, "X", _strtable.GetString("tooltip-variantresetname")) + "</font>]</b></nowrap>"));
        else buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[X]</b></nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-alsn") + ":</b></nowrap>"));
        if (!generateMutex.IsLocked()) {
          buffer.Print(HTMLWindow.MakeCell("<nowrap>" + MakeCheckBox("variantalsn." + _selectedstation + "." + _objectid + "." + _subobjectid, variant.alsncoding, _strtable.GetString("tooltip-pathmain")) + "</nowrap>"));
        }
        else {
          buffer.Print(HTMLWindow.MakeCell("<nowrap>" + MakeCheckBoxUnlink(variant.alsncoding) + "</nowrap>"));
        }
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-junctons") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + variant.jncs.size() + "</nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-multiplier") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + variant.priorityMultiplier + "</nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-length") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + _strtable.GetString1("string-distance", String.FloatToPercentRoundString(variant.length)) + "</nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-path") + ":</b></nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
    buffer.Print(HTMLWindow.EndTable());
    //buffer.Print("<br />");
    CreateJunctionListForVariantHTML(buffer, variant);
  }

  final void CreatePathControlHTML(HTMLBuffer buffer)
  {
    zx_DSP_Path path = stations[_selectedstation].paths[_objectid];
    string title = path.name;
    if (!title) title = "";
    buffer.Print("<br />");
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("pathcontrol-name") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + title + "</nowrap>"));
        if (!generateMutex.IsLocked()) buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[<font color=#" + StyleActionButtonColor + ">" + HTMLWindow.MakeLink("live://property/pathname." + _selectedstation + "." + _objectid, "…", _strtable.GetString("tooltip-pathname")) + "</font>]</b></nowrap>"));
        else buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[…]</b></nowrap>"));
        if (!generateMutex.IsLocked() and title.size()) buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[<font color=#" + StyleDeleteButtonColor + ">" + HTMLWindow.MakeLink("live://property/pathresetname." + _selectedstation + "." + _objectid, "X", _strtable.GetString("tooltip-pathresetname")) + "</font>]</b></nowrap>"));
        else buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[X]</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("", "width=100%"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("pathcontrol-maintrack") + ":</b></nowrap>"));
        if (!generateMutex.IsLocked()) buffer.Print(HTMLWindow.MakeCell(MakeCheckBox("pathmain." + _selectedstation + "." + _objectid, path.type & zx_DSP_Path.TYPE_AUTO, _strtable.GetString("tooltip-pathmain"))));
        else buffer.Print(HTMLWindow.MakeCell(MakeCheckBoxUnlink(path.type & zx_DSP_Path.TYPE_AUTO)));
      buffer.Print(HTMLWindow.EndRow());
      zx_DSP_SignalInfo signalInfo = GetOrCreateSignalInfo(path.startSign);
      if (signalInfo.span) {
        zx_DSP_SpanBase span = signalInfo.span.inverse;
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("pathcontrol-spanfrom") + ":</b></nowrap>"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>" + span.signals[0].privateName + "@" + HTMLWindow.MakeLink("live://property/controlstation." + span.station.index,  span.station.name) + "(" + _strtable.GetString("spantype." + span.GetType()) + ")</nowrap>"));
        buffer.Print(HTMLWindow.EndRow());
      }
      signalInfo = GetOrCreateSignalInfo(path.endSign);
      if (signalInfo.span) {
        zx_DSP_SpanBase span = signalInfo.span;
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("pathcontrol-spanto") + ":</b></nowrap>"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>" + span.signals[0].privateName + "@" + HTMLWindow.MakeLink("live://property/controlstation." + span.station.index,  span.station.name) + "(" + _strtable.GetString("spantype." + span.GetType()) + ")</nowrap>"));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        string sort;
        if (!generateMutex.IsLocked()) sort = HTMLWindow.MakeLink("live://property/variantsort." + _selectedstation + "." + _objectid, _strtable.GetString("sortlist"), _strtable.GetString("tooltip-variantsort"));
        else sort = _strtable.GetString("sortlist");
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + sort + "&nbsp;</nowrap>", "colspan=5 align=right"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("column-variant") + "</b>", "bgcolor=#" + StyleHeaderTableColor + " width=100%"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-alsn") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-junctioncount") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-multiplier") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-pathlength") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-action") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
      buffer.Print(HTMLWindow.EndRow());
      int i, count = path.variants.size(), viewindex, varindex;
      int visiblecount = count - path.invisbleVariationCount;
      bool canremove = count - path.invisbleVariationCount > 1;
      if (_viewinvisibled) visiblecount = count; 
      for (i = 0; i < count; ++i) {
        zx_DSP_PathVariant variant = path.variants[i];
        if (!_viewinvisibled and variant.invisible) continue;
        string title, action = "&nbsp";
        string alsnCoding;
        if (!generateMutex.IsLocked() and variant.name and variant.name.size()) title = HTMLWindow.MakeLink("live://property/variantname." + _selectedstation + "." + _objectid + "." + i, variant.name, _strtable.GetString("tooltip-variantname"));
        else if (!generateMutex.IsLocked()) title = HTMLWindow.MakeLink("live://property/variantname." + _selectedstation + "." + _objectid + "." + i, "<font color=#" + StyleEditButtonColor + ">…</font>", _strtable.GetString("tooltip-variantname"));
        else if (variant.name and variant.name.size()) title = variant.name;
        else title = "…";
        if (!variant.invisible and varindex == 0) title = _strtable.GetString("variantcontrol-main") + "&nbsp;[" + title +"]";
        else if (!variant.invisible) title = _strtable.GetString1("variantcontrol", varindex) + "&nbsp;[" + title +"]";
        else title = _strtable.GetString1("variantcontrol", "?") + "&nbsp;[" + title +"]";
        if (!generateMutex.IsLocked()) {
          alsnCoding = MakeCheckBox("variantalsn." + _selectedstation + "." + _objectid + "." + i, variant.alsncoding, _strtable.GetString("tooltip-variantalsn"));
        }
        else {
          alsnCoding = MakeCheckBoxUnlink(variant.alsncoding);
        }
        if (!generateMutex.IsLocked() and viewindex > 0) action = action + HTMLWindow.MakeLink("live://property/variantmoveup." + _selectedstation + "." + _objectid + "." + i, MakeHTMLImage("moveup-16", 16), _strtable.GetString("tooltip-variantmoveup"));
        else action = action + MakeHTMLImage("moveup-d-16", 16);             
        if (!generateMutex.IsLocked() and viewindex < visiblecount - 1) action = action + "&nbsp;" + HTMLWindow.MakeLink("live://property/variantmovedown." + _selectedstation + "." + _objectid + "." + i, MakeHTMLImage("movedown-16", 16), _strtable.GetString("tooltip-variantmovedown"));
        else action = action + "&nbsp;" + MakeHTMLImage("movedown-d-16", 16);             
        if (!generateMutex.IsLocked() and variant.invisible) action = action + "&nbsp;" + HTMLWindow.MakeLink("live://property/variantinvisible." + _selectedstation + "." + _objectid + "." + i, MakeHTMLImage("restore-16", 16), _strtable.GetString("tooltip-varianttovisible"));
        else if (!generateMutex.IsLocked() and canremove) action = action + "&nbsp;" + HTMLWindow.MakeLink("live://property/variantinvisible." + _selectedstation + "." + _objectid + "." + i, MakeHTMLImage("responsible-cancel-16", 16), _strtable.GetString("tooltip-varianttoinvisible"));
        else if (variant.invisible) action = action + "&nbsp;" + MakeHTMLImage("restore-16-d", 16);
        else  action = action + "&nbsp;" + MakeHTMLImage("responsible-cancel-d-16", 16);
        if (!generateMutex.IsLocked()) action = action + "&nbsp;" + HTMLWindow.MakeLink("live://property/variantcontrol." + _selectedstation + "." + _objectid + "." + i, MakeHTMLImage("junction-settings-16", 16), _strtable.GetString("tooltip-variantcontrol"));
        else action = action + "&nbsp;" + MakeHTMLImage("junction-settings-d-16", 16);             
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.StartCell("bgcolor=#" + StyleItemTableColor));
            buffer.Print(title + "<br />");
            CreateJunctionListForVariantHTML(buffer, variant);
          buffer.Print(HTMLWindow.EndCell());
          //buffer.Print(HTMLWindow.MakeCell(title, "bgcolor=#" + StyleItemTableColor + " width=100%"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + alsnCoding + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + variant.jncs.size() + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + variant.priorityMultiplier + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + String.FloatToPercentRoundString(variant.length) + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>" + action + "</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=left"));
        buffer.Print(HTMLWindow.EndRow());
        if (!variant.invisible) ++varindex;
        ++viewindex;
      }
    buffer.Print(HTMLWindow.EndTable());
  }  
  
  final void CreatePathListHTML(HTMLBuffer buffer)
  {
    zx_DSP_Station station = stations[_selectedstation];
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("column-path") + "</b>", "bgcolor=#" + StyleHeaderTableColor + " width=100%"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-mainpath") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-variantcount") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-action") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
      buffer.Print(HTMLWindow.EndRow());
      zxSignal signal = null;
      zx_DSP_SignalInfo signalInfo = null;
      int i;
      int count = station.paths.size();
      for (i = 0; i < count; ++i) {
        zx_DSP_Path path = station.paths[i];
        if (!_viewinvisibled and path.invisible) {
          continue;
        }
        if (signal != path.startSign) {
          signal = path.startSign;
          signalInfo = GetOrCreateSignalInfo(signal);
          buffer.Print(HTMLWindow.StartRow());
            buffer.Print(HTMLWindow.StartCell("bgcolor=#" + StyleItemTableColor + " width=100% colspan=4"));
              if (signal == _selectedsignal) buffer.Print(HTMLWindow.MakeLink("live://property/selectsignal." + _selectedstation + "." + i, MakeHTMLImage("node-close-16", 16),  _strtable.GetString("tooltip-unselectpathsigna")));
              else buffer.Print(HTMLWindow.MakeLink("live://property/selectsignal." + _selectedstation + "." + i, MakeHTMLImage("node-open-16", 16),  _strtable.GetString("tooltip-selectpathsignal")));
              buffer.Print("&nbsp;&nbsp;");
              if (signalInfo.span) {
                zx_DSP_Station inverseStation = signalInfo.span.inverse.station;
                buffer.Print(HTMLWindow.MakeLink("live://property/controlstation." + inverseStation.index, inverseStation.name, _strtable.GetString("tooltip-action-control-station")));
                buffer.Print("&nbsp;--&gt;&gt;&nbsp;");
              }
              buffer.Print(_strtable.GetString1("pathcontrol-from", signal.privateName));
            buffer.Print(HTMLWindow.StartCell("&nbsp;&nbsp;"));
          buffer.Print(HTMLWindow.EndRow());
        }
        if (path.startSign != _selectedsignal) {
          continue;
        }
        string title, mainpath, varcount, action = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        if (!generateMutex.IsLocked() and path.name and path.name.size()) title = HTMLWindow.MakeLink("live://property/pathname." + _selectedstation + "." + i, path.name, _strtable.GetString("tooltip-pathname"));
        else if (!generateMutex.IsLocked()) title = HTMLWindow.MakeLink("live://property/pathname." + _selectedstation + "." + i, "<font color=#" + StyleEditButtonColor + ">…</font>", _strtable.GetString("tooltip-pathname"));
        else if (path.name and path.name.size()) title = path.name;
        else title = "…";
        if (!generateMutex.IsLocked()) mainpath = MakeCheckBox("pathmain." + _selectedstation + "." + i, path.type & zx_DSP_Path.TYPE_AUTO, _strtable.GetString("tooltip-pathmain"));
        else mainpath = MakeCheckBoxUnlink(path.type & zx_DSP_Path.TYPE_AUTO);
        if(_viewinvisibled) varcount = path.variants.size();
        else varcount = path.variants.size() - path.invisbleVariationCount; 
        if (!generateMutex.IsLocked() and path.invisible) action = action + HTMLWindow.MakeLink("live://property/pathinvisible." + _selectedstation + "." + i, MakeHTMLImage("restore-16", 16), _strtable.GetString("tooltip-pathtovisible"));
        else if (!generateMutex.IsLocked()) action = action + HTMLWindow.MakeLink("live://property/pathinvisible." + _selectedstation + "." + i, MakeHTMLImage("responsible-cancel-16", 16), _strtable.GetString("tooltip-pathtoinvisible"));
        else if (path.invisible) action = action + MakeHTMLImage("restore-16-d", 16);
        else  action = action + MakeHTMLImage("responsible-cancel-d-16", 16);
        if (!generateMutex.IsLocked()) action = action + "&nbsp;" + HTMLWindow.MakeLink("live://property/pathcontrol." + _selectedstation + "." + i, MakeHTMLImage("junction-settings-16", 16), _strtable.GetString("tooltip-pathcontrol"));
        else action = action + "&nbsp;" + MakeHTMLImage("junction-settings-d-16", 16);
        title = "&nbsp;" + path.GenerateName(_strtable) + "&nbsp;[" + title + "]";
        zx_DSP_SignalInfo endSignalInfo = GetSignalInfo(path.endSign);
        if (endSignalInfo and endSignalInfo.span) {
          title = title + "&nbsp;--&gt;&gt;";
        }
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell(title, "bgcolor=#" + StyleItemTableColor + " width=100%"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + mainpath + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + varcount + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + action + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=left"));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
  }

  final void CreateShuntPathControlHTML(HTMLBuffer buffer) {
    zx_DSP_ShuntPath path = stations[_selectedstation].shuntPaths[_objectid];
    string title = path.name;
    if (!title) {
      title = "";
    }
    buffer.Print("<br />");
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("pathcontrol-name") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + title + "</nowrap>"));
        if (!generateMutex.IsLocked()) {
          buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[<font color=#" + StyleActionButtonColor + ">" + HTMLWindow.MakeLink("live://property/spathname." + _selectedstation + "." + _objectid, "…", _strtable.GetString("tooltip-pathname")) + "</font>]</b></nowrap>"));
        }
        else {
          buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[…]</b></nowrap>"));
        }
        if (!generateMutex.IsLocked() and title.size()) {
          buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[<font color=#" + StyleDeleteButtonColor + ">" + HTMLWindow.MakeLink("live://property/spathresetname." + _selectedstation + "." + _objectid, "X", _strtable.GetString("tooltip-pathresetname")) + "</font>]</b></nowrap>"));
        }
        else {
          buffer.Print(HTMLWindow.MakeCell("<nowrap><b>[X]</b></nowrap>"));
        }
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-junctons") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + path.jncs.size() + "</nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-multiplier") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + path.priorityMultiplier + "</nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-length") + ":</b></nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + _strtable.GetString1("string-distance", String.FloatToPercentRoundString(path.length)) + "</nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("variantcontrol-path") + ":</b></nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
    buffer.Print(HTMLWindow.EndTable());
    CreateJunctionListForVariantHTML(buffer, path);
  }

  final void CreateShuntPathListHTML(HTMLBuffer buffer) {
    zx_DSP_Station station = stations[_selectedstation];
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("column-path") + "</b>", "bgcolor=#" + StyleHeaderTableColor + " width=100%"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-junctioncount") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-multiplier") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-pathlength") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-action") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
      buffer.Print(HTMLWindow.EndRow());
      zxSignal prevSign = null;
      int i, count = station.shuntPaths.size();
      for (i = 0; i < count; ++i) {
        zx_DSP_ShuntPath path = station.shuntPaths[i];
        if (!_viewinvisibled and path.invisible) {
          continue;
        }
        if (prevSign != path.startSign) {
          prevSign = path.startSign;
          buffer.Print(HTMLWindow.StartRow());
            buffer.Print(HTMLWindow.StartCell("bgcolor=#" + StyleItemTableColor + " width=100% colspan=5"));
              if (path.startSign == _selectedsignal) {
                buffer.Print(HTMLWindow.MakeLink("live://property/selectssignal." + _selectedstation + "." + i, MakeHTMLImage("node-close-16", 16),  _strtable.GetString("tooltip-unselectpathsigna")));
              }
              else {
                buffer.Print(HTMLWindow.MakeLink("live://property/selectssignal." + _selectedstation + "." + i, MakeHTMLImage("node-open-16", 16),  _strtable.GetString("tooltip-selectpathsignal")));
              }
              buffer.Print("&nbsp;&nbsp;");
              buffer.Print(_strtable.GetString1("pathcontrol-from", path.startSign.privateName));
            buffer.Print(HTMLWindow.StartCell("&nbsp;&nbsp;"));
          buffer.Print(HTMLWindow.EndRow());
        }
        if (path.startSign != _selectedsignal) {
          continue;
        }
        string title = "…";
        if (path.name and path.name.size()) {
          title = path.name;
        }
        else if (!generateMutex.IsLocked()) {
          title = "<font color=#" + StyleEditButtonColor + ">…</font>";
        }
        if (!generateMutex.IsLocked()) {
          title = HTMLWindow.MakeLink("live://property/spathname." + _selectedstation + "." + i, title, _strtable.GetString("tooltip-pathname"));
        }
        title = "&nbsp;" + path.GenerateName(_strtable) + "&nbsp;[" + title + "]";
        string action = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        if (!generateMutex.IsLocked()) {
          if (path.invisible) {
            action = action + HTMLWindow.MakeLink("live://property/spathinvisible." + _selectedstation + "." + i, MakeHTMLImage("restore-16", 16), _strtable.GetString("tooltip-pathtovisible"));
          }
          else {
            action = action + HTMLWindow.MakeLink("live://property/spathinvisible." + _selectedstation + "." + i, MakeHTMLImage("responsible-cancel-16", 16), _strtable.GetString("tooltip-pathtoinvisible"));
          }
        }
        else {
          if (path.invisible) {
            action = action + MakeHTMLImage("restore-16-d", 16);
          }
          else {
            action = action + MakeHTMLImage("responsible-cancel-d-16", 16);
          }
        }
        if (!generateMutex.IsLocked()) {
          action = action + "&nbsp;" + HTMLWindow.MakeLink("live://property/spathcontrol." + _selectedstation + "." + i, MakeHTMLImage("junction-settings-16", 16), _strtable.GetString("tooltip-pathcontrol"));
        }
        else {
          action = action + "&nbsp;" + MakeHTMLImage("junction-settings-d-16", 16);
        }

        buffer.Print(HTMLWindow.StartRow());
          // buffer.Print(HTMLWindow.MakeCell(title, "bgcolor=#" + StyleItemTableColor + " width=100%"));
          // buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + mainpath + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          // buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + varcount + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          // buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + action + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=left"));
          buffer.Print(HTMLWindow.StartCell("bgcolor=#" + StyleItemTableColor));
            buffer.Print(title + "<br />");
            CreateJunctionListForVariantHTML(buffer, path);
          buffer.Print(HTMLWindow.EndCell());
          //buffer.Print(HTMLWindow.MakeCell(title, "bgcolor=#" + StyleItemTableColor + " width=100%"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + path.jncs.size() + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + path.priorityMultiplier + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + String.FloatToPercentRoundString(path.length) + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>" + action + "</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=left"));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
  }

  final void CreateJunctionControlHTML(HTMLBuffer buffer)
  {
    zx_DSP_JunctionInfo jncinfo = stations[_selectedstation].jncs[_objectid];
    string title = jncinfo.title, direction, image;
    bool autorevert = (jncinfo.masterJunction and jncinfo.masterJunction.type & zx_DSP_JunctionInfo.JNC_AUTOREVERT) or (jncinfo.type & zx_DSP_JunctionInfo.JNC_AUTOREVERT);
    if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-f-l-l-24-16", 24, 16);
    else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-f-l-f-24-16", 24, 16);
    else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-f-r-f-24-16", 24, 16);
    else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-f-r-r-24-16", 24, 16);
    else if (jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) image = MakeHTMLImage("jnc-f-s-l-24-16", 24, 16);
    else if (jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) image = MakeHTMLImage("jnc-f-s-r-24-16", 24, 16);
    else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT) image = MakeHTMLImage("jnc-f-l-u-24-16", 24, 16);
    else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT) image = MakeHTMLImage("jnc-f-r-u-24-16", 24, 16);
    else image = MakeHTMLImage("jnc-f-s-u-24-16", 24, 16);
    if ((jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) or
    (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT)) direction = _strtable.GetString("junctioncontrol-direction-forward");        
    else if ((jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) or
    (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT)) direction = _strtable.GetString("junctioncontrol-direction-deviation");        
    else if (jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) direction = _strtable.GetString("junctioncontrol-direction-left"); 
    else if (jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) direction = _strtable.GetString("junctioncontrol-direction-right"); 
    else direction = _strtable.GetString("string-defdir-unknown");
    if (!title) title = ""; 
    buffer.Print("<br />");
    buffer.Print(HTMLWindow.StartTable());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("junctioncontrol-state") + ":</b>"));
        buffer.Print(HTMLWindow.MakeCell(image));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("junctioncontrol-type") + ":</b>"));
        if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT) buffer.Print(HTMLWindow.MakeCell(_strtable.GetString("junctioncontrol-type-left")));
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT) buffer.Print(HTMLWindow.MakeCell(_strtable.GetString("junctioncontrol-type-right")));
        else buffer.Print(HTMLWindow.MakeCell(_strtable.GetString("junctioncontrol-type-symetry")));
        if (!generateMutex.IsLocked()) buffer.Print(HTMLWindow.MakeCell("<b>[<font color=#" + StyleActionButtonColor + ">" + HTMLWindow.MakeLink("live://property/junctiontype." + _selectedstation + "." + _objectid, "…", _strtable.GetString("tooltip-junctiontype")) + "</font>]</b>"));
        else buffer.Print(HTMLWindow.MakeCell("<b>[…]</b>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("junctioncontrol-direction") + ":</b>"));
        buffer.Print(HTMLWindow.MakeCell(direction));
        if (!generateMutex.IsLocked() and!jncinfo.masterJunction and (!jncinfo.slaveJunctions or !jncinfo.slaveJunctions.size())) 
          buffer.Print(HTMLWindow.MakeCell("<b>[<font color=#" + StyleActionButtonColor + ">" + HTMLWindow.MakeLink("live://property/junctiondefaultdirection." + _selectedstation + "." + _objectid, "⮌", _strtable.GetString("tooltip-junctiondefaultdirection")) + "</font>]</b>"));
        else buffer.Print(HTMLWindow.MakeCell("<b>[⮌]</b>"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("junctioncontrol-title") + ":</b>"));
        buffer.Print(HTMLWindow.MakeCell(title));
        if (!generateMutex.IsLocked()) buffer.Print(HTMLWindow.MakeCell("<b>[<font color=#" + StyleActionButtonColor + ">" + HTMLWindow.MakeLink("live://property/junctiontitle." + _selectedstation + "." + _objectid, "…", _strtable.GetString("tooltip-junctiontitle")) + "</font>]</b>"));
        else buffer.Print(HTMLWindow.MakeCell("<b>[…]</b>"));
        if (!generateMutex.IsLocked() and title.size()) buffer.Print(HTMLWindow.MakeCell("<b>[<font color=#" + StyleDeleteButtonColor + ">" + HTMLWindow.MakeLink("live://property/junctionresettitle." + _selectedstation + "." + _objectid, "X", _strtable.GetString("tooltip-junctiontitle-title")) + "</font>]</b>"));
        else buffer.Print(HTMLWindow.MakeCell("<b>[X]</b>"));
      buffer.Print(HTMLWindow.EndRow());                                                                      
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("junctioncontrol-number") + ":</b>"));
        buffer.Print(HTMLWindow.MakeCell(jncinfo.number));
        if (!generateMutex.IsLocked()) buffer.Print(HTMLWindow.MakeCell("<b>[<font color=#" + StyleActionButtonColor + ">" + HTMLWindow.MakeLink("live://property/junctionnumber." + _selectedstation + "." + _objectid, "…", _strtable.GetString("tooltip-junctionnumber")) + "</font>]</b>"));
        else buffer.Print(HTMLWindow.MakeCell("<b>[…]</b>"));
      buffer.Print(HTMLWindow.EndRow());
      if (!jncinfo.masterJunction and (!jncinfo.slaveJunctions or !jncinfo.slaveJunctions.size())) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("junctioncontrol-manual") + ":</b>"));
          if (!generateMutex.IsLocked()) buffer.Print(HTMLWindow.MakeCell(MakeCheckBox("junctionmanual." + _selectedstation + "." + _objectid, jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL, _strtable.GetString("tooltip-junctionmanual"))));
          else buffer.Print(HTMLWindow.MakeCell(MakeCheckBoxUnlink(jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL)));
        buffer.Print(HTMLWindow.EndRow());
      }
      if (!(jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("junctioncontrol-revert") + ":</b>"));
          if (!generateMutex.IsLocked()) buffer.Print(HTMLWindow.MakeCell(MakeCheckBox("junctionrevert." + _selectedstation + "." + _objectid, autorevert, _strtable.GetString("tooltip-junctionautorevert"))));
          else buffer.Print(HTMLWindow.MakeCell(MakeCheckBoxUnlink(autorevert)));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
    if (jncinfo.masterJunction or (jncinfo.slaveJunctions and jncinfo.slaveJunctions.size())) {
      zx_DSP_JunctionInfo[] junctions = jncinfo.slaveJunctions;
      buffer.Print(HTMLWindow.StartTable("width=100%"));
        buffer.Print(HTMLWindow.StartRow());                   
          buffer.Print(HTMLWindow.MakeSpacerCell(30));
          buffer.Print(HTMLWindow.MakeCell(""));
        buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.StartRow());                   
          buffer.Print(HTMLWindow.MakeCell("<nowrap><b>" + _strtable.GetString("junctioncontrol-linkjunctions") + ":</b></nowrap>", "colspan=2"));
        buffer.Print(HTMLWindow.EndRow());
        if (jncinfo.masterJunction) {
          string title = jncinfo.masterJunction.GetTitle();
          if (!title.size()) title = jncinfo.masterJunction.jnc.GetLocalisedName();
          else title = title + " [" + jncinfo.masterJunction.jnc.GetLocalisedName() + "]";  
          buffer.Print(HTMLWindow.StartRow());                   
            buffer.Print(HTMLWindow.MakeSpacerCell(30));
            buffer.Print(HTMLWindow.MakeCell(title));
          buffer.Print(HTMLWindow.EndRow());
          junctions = jncinfo.masterJunction.slaveJunctions;
        }
        int i, count = junctions.size(); //
        for (i = 0; i < count; ++i) {      
          zx_DSP_JunctionInfo slvjncinfo = junctions[i];
          if (slvjncinfo != jncinfo) {
            string title = slvjncinfo.GetTitle();
            if (!title.size()) title = slvjncinfo.jnc.GetLocalisedName(); 
            else title = title + " [" + slvjncinfo.jnc.GetLocalisedName() + "]";  
            buffer.Print(HTMLWindow.StartRow());                   
              buffer.Print(HTMLWindow.MakeSpacerCell(30));
              buffer.Print(HTMLWindow.MakeCell(title));
            buffer.Print(HTMLWindow.EndRow());
          }
        }
      buffer.Print(HTMLWindow.EndTable());
    }
  }
  
  final void CreateJunctionListHTML(HTMLBuffer buffer)
  {
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        string sort;
        if (!generateMutex.IsLocked()) sort = HTMLWindow.MakeLink("live://property/junctionsort." + _selectedstation, _strtable.GetString("sortlist"), _strtable.GetString("tooltip-junctionsort"));
        else sort = _strtable.GetString("sortlist");
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + sort + "&nbsp;</nowrap>", "colspan=6 align=right"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("column-junction") + "</b>", "bgcolor=#" + StyleHeaderTableColor + " width=100%"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-number") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-direct") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-manual") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-autoreturn") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-action") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
      buffer.Print(HTMLWindow.EndRow());
      int i, count = stations[_selectedstation].jncs.size();
      for (i = 0; i < count; ++i) {
        zx_DSP_JunctionInfo jncinfo = stations[_selectedstation].jncs[i];
        string namestring = "&nbsp;" + jncinfo.jnc.GetLocalisedName(), title, number, manual = "", revert = "", direct, action;
        bool autorevert = (jncinfo.masterJunction and jncinfo.masterJunction.type & zx_DSP_JunctionInfo.JNC_AUTOREVERT) or (jncinfo.type & zx_DSP_JunctionInfo.JNC_AUTOREVERT);
        if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) namestring = MakeHTMLImage("jnc-f-l-l-24-16", 24, 16) + namestring;
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) namestring = MakeHTMLImage("jnc-f-l-f-24-16", 24, 16) + namestring;
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) namestring = MakeHTMLImage("jnc-f-r-f-24-16", 24, 16) + namestring;
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) namestring = MakeHTMLImage("jnc-f-r-r-24-16", 24, 16) + namestring;
        else if (jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) namestring = MakeHTMLImage("jnc-f-s-l-24-16", 24, 16) + namestring;
        else if (jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) namestring = MakeHTMLImage("jnc-f-s-r-24-16", 24, 16) + namestring;
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT) namestring = MakeHTMLImage("jnc-f-l-u-24-16", 24, 16) + namestring;
        else if (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT) namestring = MakeHTMLImage("jnc-f-r-u-24-16", 24, 16) + namestring;
        else namestring = MakeHTMLImage("jnc-f-s-u-24-16", 24, 16) + namestring;
        if (!generateMutex.IsLocked() and jncinfo.title and jncinfo.title.size()) title = HTMLWindow.MakeLink("live://property/junctiontitle." + _selectedstation + "." + i, jncinfo.title, _strtable.GetString("tooltip-junctiontitle"));
        else if (!generateMutex.IsLocked()) title = HTMLWindow.MakeLink("live://property/junctiontitle." + _selectedstation + "." + i, "<font color=#" + StyleEditButtonColor + ">…</font>", _strtable.GetString("tooltip-junctiontitle"));
        else if (jncinfo.title and jncinfo.title.size()) title = jncinfo.title;
        else title = "…";
        if (!generateMutex.IsLocked()) number = HTMLWindow.MakeLink("live://property/junctionnumber." + _selectedstation + "." + i, jncinfo.number, _strtable.GetString("tooltip-junctionnumber"));
        else number = jncinfo.number;
        if (!generateMutex.IsLocked() and !jncinfo.masterJunction and (!jncinfo.slaveJunctions or !jncinfo.slaveJunctions.size())) 
          manual = MakeCheckBox("junctionmanual." + _selectedstation + "." + i, jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL, _strtable.GetString("tooltip-junctionmanual"));
        else if (!jncinfo.masterJunction and (!jncinfo.slaveJunctions or !jncinfo.slaveJunctions.size())) manual = MakeCheckBoxUnlink(jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL);
        if (!generateMutex.IsLocked() and !(jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL)) revert = MakeCheckBox("junctionrevert." + _selectedstation + "." + i, autorevert, _strtable.GetString("tooltip-junctionautorevert"));
        else if (!(jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL)) revert = MakeCheckBoxUnlink(autorevert);
        if ((jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) or
        (jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT)) direct = _strtable.GetString("string-defdir-forward");        
        else if ((jncinfo.type & zx_DSP_JunctionInfo.JNC_RIGHT and jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) or
        (jncinfo.type & zx_DSP_JunctionInfo.JNC_LEFT and jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT)) direct = _strtable.GetString("string-defdir-deviation");        
        else if (jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) direct = _strtable.GetString("string-defdir-left"); 
        else if (jncinfo.defaultDirection == JunctionBase.DIRECTION_RIGHT) direct = _strtable.GetString("string-defdir-right"); 
        else direct = _strtable.GetString("string-defdir-unknown");
        if (!generateMutex.IsLocked() and !jncinfo.masterJunction and (!jncinfo.slaveJunctions or !jncinfo.slaveJunctions.size())) 
          direct = HTMLWindow.MakeLink("live://property/junctiondefaultdirection." + _selectedstation + "." + i, direct, _strtable.GetString("tooltip-junctiondefaultdirection")); 
        if (!generateMutex.IsLocked() and i > 0) action = HTMLWindow.MakeLink("live://property/junctionmoveup." + _selectedstation + "." + i, MakeHTMLImage("moveup-16", 16), _strtable.GetString("tooltip-junctionmoveup"));
        else action = MakeHTMLImage("moveup-d-16", 16);             
        if (!generateMutex.IsLocked() and i < count - 1) action = action + "&nbsp;" + HTMLWindow.MakeLink("live://property/junctionmovedown." + _selectedstation + "." + i, MakeHTMLImage("movedown-16", 16), _strtable.GetString("tooltip-junctionmovedown"));
        else action = action + "&nbsp;" + MakeHTMLImage("movedown-d-16", 16);             
        if (!generateMutex.IsLocked()) action = action + "&nbsp;" + HTMLWindow.MakeLink("live://property/junctioncontrol." + _selectedstation + "." + i, MakeHTMLImage("junction-settings-16", 16), _strtable.GetString("tooltip-junctioncontrol"));
        else action = action + "&nbsp;" + MakeHTMLImage("junction-settings-d-16", 16);             
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell(namestring + "&nbsp;[" + title + "]", "bgcolor=#" + StyleItemTableColor + " width=100%"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + number + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + direct + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + manual + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + revert + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;&nbsp;&nbsp;&nbsp;" + action + "</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=left"));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
  }
  
  final void CreateSpanListHTML(HTMLBuffer buffer)
  {                                               
    zx_DSP_Station station = stations[_selectedstation];
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-signal") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("column-neighborsignal") + "</b>", "bgcolor=#" + StyleHeaderTableColor + " width=100%"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-type") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-action") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
      buffer.Print(HTMLWindow.EndRow());
      int i, count = station.spans.size();
      for (i = 0; i < count; ++i) {
        zx_DSP_SpanBase span = station.spans[i];
        string signalname, neighborsignalname;
        signalname = span.signals[0].privateName;
        neighborsignalname = span.inverse.signals[0].privateName + "@" + span.inverse.station.name;
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + signalname + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell(neighborsignalname, "bgcolor=#" + StyleItemTableColor + " width=100%"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + _strtable.GetString("spantype." + span.GetType()) + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + "" /*HTMLWindow.MakeLink("live://property/controlstation." + i,  MakeHTMLImage("junction-settings-16", 16), _strtable.GetString("tooltip-action-control-station"))*/ + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
  }
  
  final void CreateStationListHTML(HTMLBuffer buffer)
  {
    buffer.Print(HTMLWindow.StartTable("width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("<b>" + _strtable.GetString("column-stationname") + "</b>", "bgcolor=#" + StyleHeaderTableColor + " width=100%"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-junctioncount") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-signalcount") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-pathcount") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-spathcount") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;<b>" + _strtable.GetString("column-action") + "</b>&nbsp;</nowrap>", "bgcolor=#" + StyleHeaderTableColor));
      buffer.Print(HTMLWindow.EndRow());
      int i, count = stations.size();
      for (i = 0; i < count; ++i) {
        zx_DSP_Station station = stations[i];
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell(station.name, "bgcolor=#" + StyleItemTableColor + " width=100%"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + station.jncs.size() + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + station.signals.size() + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + station.paths.size() + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          string cntStr;
          if (station.activeShuntPaths) {
            cntStr = station.shuntPaths.size();
          }
          else {
            cntStr = "-";
          }
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + cntStr + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
          buffer.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + HTMLWindow.MakeLink("live://property/controlstation." + i,  MakeHTMLImage("junction-settings-16", 16), _strtable.GetString("tooltip-action-control-station")) + "&nbsp;</nowrap>", "bgcolor=#" + StyleItemTableColor + " align=center"));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
  }
  

  final string GetDescriptionHTML(void)
  {
    if (library.GetController() != me) {
      return "<font size=8 color=#FF0000>" + _strtable.GetString("error-alreadyexist") + "</font>";
    }
    HTMLBuffer retbuff = HTMLBufferStatic.Construct();
    retbuff.Print("<font size=6 color=#" + StyleTitleLabel + ">" + _strtable.GetString("string-title") + "</font><br /><br />");  
    retbuff.Print(HTMLWindow.StartTable("width=100%"));
      retbuff.Print(HTMLWindow.StartRow());
        retbuff.Print(HTMLWindow.MakeCell("", "width=50%"));
        retbuff.Print(HTMLWindow.StartCell("bgcolor=#" + StyleActionButtonColor));
          if (_calcprocess == -1 and !generateMutex.IsLocked() and !loadMutex.IsLocked()) 
            retbuff.Print("<nowrap>&nbsp;" + HTMLWindow.MakeLink("live://property/calculate", _strtable.GetString("calc"), _strtable.GetString("tooltip-calc")) + "&nbsp;</nowrap>");
          else retbuff.Print("<nowrap>&nbsp;" + HTMLWindow.MakeLink("live://property/update", _strtable.GetString("update"), _strtable.GetString("tooltip-update")) + "&nbsp;</nowrap>"); 
        retbuff.Print(HTMLWindow.EndCell());
        retbuff.Print(HTMLWindow.MakeCell("", "width=50%"));
      retbuff.Print(HTMLWindow.EndRow());
      retbuff.Print(HTMLWindow.StartRow());
        retbuff.Print(HTMLWindow.StartCell("colspan=3 align=center"));
          if (loadMutex.IsLocked()) retbuff.Print(_strtable.GetString("message-cacheloading"));
          else if (generateMutex.IsLocked()) retbuff.Print(_strtable.GetString("message-caching"));
          else if (_calcprocess == -1 and stations) retbuff.Print(_strtable.GetString("message-newcalc"));
          else if (_calcprocess == -1) retbuff.Print(_strtable.GetString("message-mustcalc"));
          else if (_calcstationname and _calcstationname.size()) retbuff.Print(_strtable.GetString4("message-calcing_station", _calcprocess + 1, _calcmaxprogress, (int)((float)_calcprocess / (float)_calcmaxprogress * 100), _calcstationname) + " " + _workindicator);
          else retbuff.Print(_strtable.GetString3("message-calcing", _calcprocess + 1, _calcmaxprogress, (int)((float)_calcprocess / (float)_calcmaxprogress * 100)));
        retbuff.Print(HTMLWindow.EndCell());
      retbuff.Print(HTMLWindow.EndRow());
      if (_calcprocess == -1 and _lasterrormessage and _lasterrormessage.size()) {
        retbuff.Print(HTMLWindow.StartRow());
          retbuff.Print(HTMLWindow.StartCell("colspan=3 align=center"));
            retbuff.Print(HTMLWindow.StartTable());
              retbuff.Print(HTMLWindow.StartRow());
                retbuff.Print(HTMLWindow.MakeCell(_lasterrormessage, "bgcolor=#f38b76 align=center"));
              retbuff.Print(HTMLWindow.EndRow());
            retbuff.Print(HTMLWindow.EndTable());
          retbuff.Print(HTMLWindow.EndCell());
        retbuff.Print(HTMLWindow.EndRow());
      }
    retbuff.Print(HTMLWindow.EndTable());

    if (_calcprocess == -1 and !loadMutex.IsLocked() and stations) {
      retbuff.Print("<br />");
      retbuff.Print(HTMLWindow.StartTable("width=100%"));
        retbuff.Print(HTMLWindow.StartRow());
          retbuff.Print(HTMLWindow.StartCell());
            retbuff.Print(MakeCheckBox("viewinvisible", _viewinvisibled, _strtable.GetString("tooltip-viewinvisibled")));
            retbuff.Print("&nbsp;");
            retbuff.Print(_strtable.GetString("viewinvisibled"));
          retbuff.Print(HTMLWindow.EndCell());
          retbuff.Print(HTMLWindow.MakeCell(_strtable.GetString("jnctioncount") + ":", "width=100"));
          retbuff.Print(HTMLWindow.MakeCell(jncs.size(), "width=35"));
        retbuff.Print(HTMLWindow.EndRow());
        retbuff.Print(HTMLWindow.StartRow());
          retbuff.Print(HTMLWindow.MakeCell(""));
          retbuff.Print(HTMLWindow.MakeCell(_strtable.GetString("stationcount") + ":", "width=100"));
          retbuff.Print(HTMLWindow.MakeCell(stations.size(), "width=35"));
        retbuff.Print(HTMLWindow.EndRow());
      retbuff.Print(HTMLWindow.EndTable());
      if (_selectedstation >= 0) {
        zx_DSP_Station station = stations[_selectedstation];
        retbuff.Print(HTMLWindow.MakeLink("live://property/stationlist", _strtable.GetString("stationlist"), _strtable.GetString("tooltip-stationlist")));
        retbuff.Print("&nbsp;●&nbsp;");
        if (_objectid >= 0) retbuff.Print(HTMLWindow.MakeLink("live://property/controlstation." + _selectedstation, stations[_selectedstation].name, _strtable.GetString("tooltip-stationlist")));
        else retbuff.Print(station.name);
        if (_objectid < 0) {
          retbuff.Print("<br />");
          retbuff.Print(HTMLWindow.StartTable("width=100%"));
            retbuff.Print(HTMLWindow.StartRow());
              retbuff.Print(HTMLWindow.MakeCell("", "width=50%"));
              if (_objecttype == 0) retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + _strtable.GetString1("junctionlist", station.jncs.size()) + "&nbsp;</nowrap>", "bgcolor=#" + StyleEditButtonColor)); 
              else retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + HTMLWindow.MakeLink("live://property/selectobject.0", _strtable.GetString1("junctionlist", station.jncs.size()), _strtable.GetString("tooltip-junctionlist")) + "&nbsp;</nowrap>", "bgcolor=#" + StyleActionButtonColor));
              retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;&nbsp;</nowrap>"));
              if (_objecttype == 1) retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + _strtable.GetString1("pathlist", station.paths.size()) + "&nbsp;</nowrap>", "bgcolor=#" + StyleEditButtonColor));
              else retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + HTMLWindow.MakeLink("live://property/selectobject.1", _strtable.GetString1("pathlist", station.paths.size()), _strtable.GetString("tooltip-pathlist")) + "&nbsp;</nowrap>", "bgcolor=#" + StyleActionButtonColor));
              if (station.activeShuntPaths) {
                retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;&nbsp;</nowrap>"));
                if (4 == _objecttype) {
                  retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + _strtable.GetString1("spathlist", station.shuntPaths.size()) + "&nbsp;</nowrap>", "bgcolor=#" + StyleEditButtonColor));
                }
                else {
                  retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + HTMLWindow.MakeLink("live://property/selectobject.4", _strtable.GetString1("spathlist", station.shuntPaths.size()), _strtable.GetString("tooltip-spathlist")) + "&nbsp;</nowrap>", "bgcolor=#" + StyleActionButtonColor));
                }
              }
              retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;&nbsp;</nowrap>"));
              if (_objecttype == 2) retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + _strtable.GetString1("signallist", station.signals.size()) + "&nbsp;</nowrap>", "bgcolor=#" + StyleEditButtonColor));
              else retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + HTMLWindow.MakeLink("live://property/selectobject.2", _strtable.GetString1("signallist", station.signals.size()), _strtable.GetString("tooltip-signallist")) + "&nbsp;</nowrap>", "bgcolor=#" + StyleActionButtonColor));
              if (spans) {
                retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;&nbsp;</nowrap>"));
                if (_objecttype == 3) retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + _strtable.GetString1("spanlist", station.spans.size()) + "&nbsp;</nowrap>", "bgcolor=#" + StyleEditButtonColor));
                else retbuff.Print(HTMLWindow.MakeCell("<nowrap>&nbsp;" + HTMLWindow.MakeLink("live://property/selectobject.3", _strtable.GetString1("spanlist", station.spans.size()), _strtable.GetString("tooltip-signallist")) + "&nbsp;</nowrap>", "bgcolor=#" + StyleActionButtonColor));
              }
              retbuff.Print(HTMLWindow.MakeCell("", "width=50%"));
            retbuff.Print(HTMLWindow.EndRow());
          retbuff.Print(HTMLWindow.EndTable());
          if (_objecttype == 2) {
            int i, count = station.signals.size();
            for (i = 0; i < count; ++i) {
              if (i != 0) retbuff.Print("&nbsp;");
              retbuff.Print("<nowrap>");
              retbuff.Print(MakeHTMLImage("signal", 16));
              retbuff.Print("<b>");
              retbuff.Print(station.signals[i].privateName);
              retbuff.Print("</b>;</nowrap>");
            }
          }
          else if (_objecttype == 3) CreateSpanListHTML(retbuff);
          else if (_objecttype == 1) CreatePathListHTML(retbuff);
          else if (4 == _objecttype) CreateShuntPathListHTML(retbuff);
          else CreateJunctionListHTML(retbuff);
        } else if (!_objecttype) {
          zx_DSP_JunctionInfo jncinfo = station.jncs[_objectid];
          string title = jncinfo.GetTitle();
          if (title.size()) title = title + " [" + jncinfo.jnc.GetLocalisedName() + "]";
          else title = jncinfo.jnc.GetLocalisedName();  
          retbuff.Print("&nbsp;●&nbsp;" + title);
          CreateJunctionControlHTML(retbuff);
        } else if (_objecttype == 1) {
          zx_DSP_Path path = station.paths[_objectid];
          retbuff.Print("&nbsp;●&nbsp;");
          if (_subobjectid >= 0) { 
            string variantname;
            int index, viewindex;
            while (index <= _subobjectid) {
              zx_DSP_PathVariant variant = path.variants[index];
              if (index == _subobjectid) {
                if (!variant.invisible and !viewindex) variantname = _strtable.GetString("variantcontrol-main"); 
                else if (!variant.invisible) variantname = _strtable.GetString1("variantcontrol", viewindex);
                else variantname = _strtable.GetString1("variantcontrol", "?");
                if (variant.name and variant.name.size()) variantname = variantname + "&nbsp;[" + variant.name + "]";
              }
              if (!variant.invisible) ++viewindex;
              ++index;
            }
            retbuff.Print(HTMLWindow.MakeLink("live://property/pathcontrol." + _selectedstation + "." + _objectid, path.GenerateName(_strtable), _strtable.GetString("tooltip-pathcontrol")));
            retbuff.Print("&nbsp;●&nbsp;" + variantname);
            CreateVariationControlHTML(retbuff);
          } else {
            retbuff.Print(path.GenerateName(_strtable));
            CreatePathControlHTML(retbuff);
          }
        }
        else if (4 == _objecttype) {
          zx_DSP_ShuntPath path = station.shuntPaths[_objectid];
          retbuff.Print("&nbsp;●&nbsp;");
          retbuff.Print(path.GenerateName(_strtable));
          CreateShuntPathControlHTML(retbuff);
        }
      } else CreateStationListHTML(retbuff);  
    }     
    return retbuff.AsString();
  }

  public void PropertyBrowserRefresh(Browser browser)
  {
    _propertybrowser = browser;
    inherited(browser);
  }

  void LinkPropertyValue(string propertyID)
  {
    if(propertyID == "calculate"){
      int count = Str.ToInt(suCore.LibraryCall("station_count", null, null));
      if(count > 0){
        _calcmaxprogress = count;
        _calcprocess = 0;
        FindAndCalcAllStation();
      } else _lasterrormessage = _strtable.GetString("error-calcempty"); 
      _selectedstation = -1;
    }else if (propertyID == "viewinvisible"){
      _viewinvisibled = !_viewinvisibled;
    } else if (propertyID == "stationlist") {
      _selectedstation = -1;
    } else if (propertyID[0, 14] == "controlstation") {
      int index = Str.ToInt(propertyID[15,]);
      _selectedstation = index;
      _objectid = -1;
      _selectedsignal = null;
      _subobjectid = -1;
    } else if (propertyID[0, 12] == "selectobject") {
      int index = Str.ToInt(propertyID[13,]);
      _objecttype = index;
      _subobjectid = -1;
    } else if (propertyID[0, 15] == "junctioncontrol") {
		  int[] indexes = GetIndexes(propertyID[16,]);
      _selectedstation = indexes[0];
      _objectid = indexes[1];
      _objecttype = 0;
    } else if (propertyID[0, 24] == "junctiondefaultdirection") {
		  int[] indexes = GetIndexes(propertyID[25,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      if (jncinfo.defaultDirection == JunctionBase.DIRECTION_LEFT) 
        jncinfo.defaultDirection = JunctionBase.DIRECTION_RIGHT;
      else jncinfo.defaultDirection = JunctionBase.DIRECTION_LEFT;  
      GenerateCache();
    } else if (propertyID[0, 18] == "junctionresettitle") {
		  int[] indexes = GetIndexes(propertyID[19,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      jncinfo.title = null;
      GenerateCache();
    } else if (propertyID[0, 14] == "junctionmanual") {
		  int[] indexes = GetIndexes(propertyID[15,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      jncinfo.type = ECLLogic.SetBitMask(jncinfo.type, zx_DSP_JunctionInfo.JNC_MANUAL, !(jncinfo.type & zx_DSP_JunctionInfo.JNC_MANUAL));
      GenerateCache();
    } else if (propertyID[0, 14] == "junctionrevert") {
		  int[] indexes = GetIndexes(propertyID[15,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      if (jncinfo.masterJunction) jncinfo.masterJunction.type = ECLLogic.SetBitMask(jncinfo.masterJunction.type, zx_DSP_JunctionInfo.JNC_AUTOREVERT, !(jncinfo.masterJunction.type & zx_DSP_JunctionInfo.JNC_AUTOREVERT));
      else jncinfo.type = ECLLogic.SetBitMask(jncinfo.type, zx_DSP_JunctionInfo.JNC_AUTOREVERT, !(jncinfo.type & zx_DSP_JunctionInfo.JNC_AUTOREVERT));
      GenerateCache();
    } else if (propertyID[0, 14] == "junctionmoveup") {
		  int[] indexes = GetIndexes(propertyID[15,]);
      if (indexes[1] > 0) {
        zx_DSP_Station station = stations[indexes[0]];
        zx_DSP_JunctionInfo jncinfo = station.jncs[indexes[1] - 1];
        station.jncs[indexes[1] - 1] = station.jncs[indexes[1]];
        station.jncs[indexes[1]] = jncinfo;  
      }
      GenerateCache();
    } else if (propertyID[0, 16] == "junctionmovedown") {
		  int[] indexes = GetIndexes(propertyID[17,]);
      zx_DSP_Station station = stations[indexes[0]];
      if (indexes[1] < station.jncs.size() - 1) {
        zx_DSP_JunctionInfo jncinfo = station.jncs[indexes[1] + 1];
        station.jncs[indexes[1] + 1] = station.jncs[indexes[1]];
        station.jncs[indexes[1]] = jncinfo;  
      }
      GenerateCache();
    } else if (propertyID[0, 12] == "junctionsort") {
      int index = Str.ToInt(propertyID[13,]);
      zx_DSP_Station station = stations[index];
      CacheSleeper sleeper = generateMutex.TryLock();
      if (sleeper) {      //лучше ничего не делать, чем нарваться на таймаут
        SortStationJunctionAsync(station, sleeper);
      }
    } else if (propertyID[0, 12] == "selectsignal") {
		  int[] indexes = GetIndexes(propertyID[13,]);
      zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
      if (_selectedsignal == path.startSign) _selectedsignal = null;
      else _selectedsignal = path.startSign;
    } else if (propertyID[0, 13] == "selectssignal") {
      int[] indexes = GetIndexes(propertyID[14,]);
      zx_DSP_ShuntPath path = stations[indexes[0]].shuntPaths[indexes[1]];
      if (_selectedsignal == path.startSign) _selectedsignal = null;
      else _selectedsignal = path.startSign;  
    } else if (propertyID[0, 8] == "pathmain") {
		  int[] indexes = GetIndexes(propertyID[9,]);
      zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
      path.type = path.type ^ zx_DSP_Path.TYPE_AUTO;
      GenerateCache();
    } else if (propertyID[0, 13] == "pathinvisible") {
		  int[] indexes = GetIndexes(propertyID[14,]);
      zx_DSP_Station station = stations[indexes[0]];
      zx_DSP_Path path = station.paths[indexes[1]];
      path.invisible = !path.invisible;
      if (path.invisible) ++station.invisiblePathCount;
      else --station.invisiblePathCount;
      GenerateCache();
    } else if (propertyID[0, 14] == "spathinvisible") {
      int[] indexes = GetIndexes(propertyID[14,]);
      zx_DSP_Station station = stations[indexes[0]];
      zx_DSP_ShuntPath path = station.shuntPaths[indexes[1]];
      path.invisible = !path.invisible;
      // if (path.invisible) ++station.invisiblePathCount;
      // else --station.invisiblePathCount;
      GenerateCache();
    } else if (propertyID[0, 13] == "pathresetname") {
		  int[] indexes = GetIndexes(propertyID[14,]);
      zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
      path.name = null;
      GenerateCache();
    } else if (propertyID[0, 14] == "spathresetname") {
      int[] indexes = GetIndexes(propertyID[14,]);
      zx_DSP_ShuntPath path = stations[indexes[0]].shuntPaths[indexes[1]];
      path.name = null;
      GenerateCache();
    } else if (propertyID[0, 11] == "pathcontrol") {
		  int[] indexes = GetIndexes(propertyID[12,]);
      _selectedstation = indexes[0];
      _objectid = indexes[1];
      _objecttype = 1;
      _subobjectid = -1;
    } else if (propertyID[0, 12] == "spathcontrol") {
      int[] indexes = GetIndexes(propertyID[12,]);
      _selectedstation = indexes[0];
      _objectid = indexes[1];
      _objecttype = 4;
      _subobjectid = -1;
    } else if (propertyID[0, 13] == "variantmoveup") {
		  int[] indexes = GetIndexes(propertyID[14,]);
      if (indexes[2] > 0) {
        zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
        zx_DSP_PathVariant variant = path.variants[indexes[2] - 1];
        path.variants[indexes[2] - 1] = path.variants[indexes[2]];
        path.variants[indexes[2]] = variant; 
        GenerateCache();
      } 
    } else if (propertyID[0, 15] == "variantmovedown") {
		  int[] indexes = GetIndexes(propertyID[16,]);
      zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
      if (indexes[2] < path.variants.size() - 1) {
        zx_DSP_PathVariant variant = path.variants[indexes[2] + 1];
        path.variants[indexes[2] + 1] = path.variants[indexes[2]];
        path.variants[indexes[2]] = variant; 
        GenerateCache();
      } 
    } else if (propertyID[0, 16] == "variantinvisible") {
		  int[] indexes = GetIndexes(propertyID[17,]);
      zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
      zx_DSP_PathVariant variant = path.variants[indexes[2]];
      variant.invisible = !variant.invisible;
      if (variant.invisible) ++path.invisbleVariationCount;
      else --path.invisbleVariationCount;    
      GenerateCache();
    } else if (propertyID[0, 11] == "variantsort") {
		  int[] indexes = GetIndexes(propertyID[12,]);
      zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
      SortPathVariant(path, false);
      GenerateCache();
    } else if (propertyID[0, 14] == "variantcontrol") {
		  int[] indexes = GetIndexes(propertyID[15,]);
      _selectedstation = indexes[0];
      _objectid = indexes[1];
      _objecttype = 1;
      _subobjectid = indexes[2];
    } else if (propertyID[0, 16] == "variantresetname") {
      int[] indexes = GetIndexes(propertyID[17,]);
      zx_DSP_PathVariant variant = stations[indexes[0]].paths[indexes[1]].variants[indexes[2]];
      variant.name = null;
      GenerateCache();  
    }
    else if ("variantalsn" == propertyID[, 11]) {
      int[] indexes = GetIndexes(propertyID[12,]);
      zx_DSP_PathVariant variant = stations[indexes[0]].paths[indexes[1]].variants[indexes[2]];
      variant.alsncoding = !variant.alsncoding;
      GenerateCache();
    }
		else inherited(propertyID);    
  }

  void SetPropertyValue(string propertyID, int value)
  {
    if (propertyID[0, 14] == "junctionnumber") {
      int[] indexes = GetIndexes(propertyID[15,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      jncinfo.number = value;
      GenerateCache();  
    }  
		else inherited(propertyID, value);
  }

  void SetPropertyValue(string propertyID, string value)
  {
    if (propertyID[, 13] == "junctiontitle") {
      int[] indexes = GetIndexes(propertyID[13,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      Str.TrimLeft(value, " ");
      Str.TrimRight(value, " ");
      jncinfo.title = value;
      GenerateCache();  
    } else if (propertyID[0, 8] == "pathname") {
      int[] indexes = GetIndexes(propertyID[9,]);
      zx_DSP_Path path = stations[indexes[0]].paths[indexes[1]];
      Str.TrimLeft(value, " ");
      Str.TrimRight(value, " ");
      path.name = value;
      GenerateCache();  
    } else if (propertyID[0, 9] == "spathname") {
      int[] indexes = GetIndexes(propertyID[10,]);
      zx_DSP_ShuntPath path = stations[indexes[0]].shuntPaths[indexes[1]];
      Str.TrimLeft(value, " ");
      Str.TrimRight(value, " ");
      path.name = value;
      GenerateCache();
    } else if (propertyID[0, 11] == "variantname") {
      int[] indexes = GetIndexes(propertyID[12,]);
      zx_DSP_PathVariant variant = stations[indexes[0]].paths[indexes[1]].variants[indexes[2]];
      Str.TrimLeft(value, " ");
      Str.TrimRight(value, " ");
      variant.name = value;
      GenerateCache();  
    }                    
		else inherited(propertyID, value);
  }

	void SetPropertyValue(string propertyID, string value, int index) {
		if (propertyID[, 12] == "junctiontype") {
      int[] indexes = GetIndexes(propertyID[13,]);
      zx_DSP_JunctionInfo jncinfo = stations[indexes[0]].jncs[indexes[1]];
      jncinfo.type = jncinfo.type & ~(zx_DSP_JunctionInfo.JNC_LEFT | zx_DSP_JunctionInfo.JNC_RIGHT);
      if (index == 1) jncinfo.type = jncinfo.type | zx_DSP_JunctionInfo.JNC_LEFT;  
      else if (index == 2) jncinfo.type = jncinfo.type | zx_DSP_JunctionInfo.JNC_RIGHT;  
      GenerateCache();
    }
		else inherited(propertyID, value, index);
	}

  void OnUpdateCacheProgress()
  {
     if(World.GetCurrentModule() != World.DRIVER_MODULE and _propertybrowser)
     	  PropertyBrowserRefresh(_propertybrowser);
  }

  public void SetProperties(Soup soup) {
    inherited(soup);
    _selectedstation = -1;
  }

};