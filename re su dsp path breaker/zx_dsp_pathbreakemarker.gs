//=============================================================================
// File: zx_dsp_pathbreaker.gs
// Desc: Реализация маркера прерывания маршрута
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_pathbreaker.gs"
include "ecl_bitlogic.gs"  
include "zx_dsp_ui.gs"

//=============================================================================
// Name: zx_DSP_PathBreaker
// Desc: Класс, реализующий маркер прерывания маршрутов 
//=============================================================================
final class zx_DSP_PathBreakerMarker isclass zx_DSP_PathBreaker, zx_DSP_UI
{
  
  final string GetShuntPathBreakerString(StringTable strTable, int direction)
  {
    if(ECLLogic.GetBitMask(direction, TYPE_FORWARD_SHUNT | TYPE_BACKWARD_SHUNT)) return strTable.GetString("direction-both");
    else if(direction & TYPE_FORWARD_SHUNT) return strTable.GetString("direction-forward");
    else if(direction & TYPE_BACKWARD_SHUNT) return strTable.GetString("direction-backward");
    return strTable.GetString("direction-none");
  }

  final string GetPathBreakerString(StringTable strTable, int direction)
  {
    if(ECLLogic.GetBitMask(direction, TYPE_FORWARD_PATH | TYPE_BACKWARD_PATH)) return strTable.GetString("direction-both");
    else if(direction & TYPE_FORWARD_PATH) return strTable.GetString("direction-forward");
    else if(direction & TYPE_BACKWARD_PATH) return strTable.GetString("direction-backward");
    return strTable.GetString("direction-none");
  }

  string GetPropertyType(string propertyID)
  {
    if(propertyID[0, 5] == "shunt" or propertyID[0, 4] == "path" or "border" == propertyID) return "link";
    return inherited(propertyID);
  }

  string GetDescriptionHTML(void){
    StringTable strtable = GetAsset().GetStringTable();
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    int i;
    
    buffer.Print("<font size=10 color=#" + GetUIPropertyValue("labelcolor-title") +"><b>");
    buffer.Print(strtable.GetString("title"));
    buffer.Print("</b></font><br><br>");

    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("settings")), "colspan=3"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("shuntpath") + ":"));
        buffer.Print(HTMLWindow.StartCell());
          for (i = 0; i <= 3; ++i) MakeMultiActionItem(buffer, "live://property/shunt/" + i, GetShuntPathBreakerString(strtable, i), strtable.GetString("tooltip-shuntpath"), true, (action & 3) == i, i == 0, "");    
        buffer.Print(HTMLWindow.EndCell());
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("path") + ":"));
        buffer.Print(HTMLWindow.StartCell());
          for (i = 0; i <= 3; ++i) MakeMultiActionItem(buffer, "live://property/path/" + (i << 2), GetPathBreakerString(strtable, i << 2), strtable.GetString("tooltip-path"), true, ((action >> 2) & 3) == i, i == 0, "");    
        buffer.Print(HTMLWindow.EndCell());
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.CheckBox("", action & TYPE_STATION_BORDER) + "&nbsp;" + HTMLWindow.MakeLink("live://property/border", strtable.GetString("border"), strtable.GetString("tooltip-border")), "colspan=2"));
      buffer.Print(HTMLWindow.EndRow());
    buffer.Print(HTMLWindow.EndTable());
    buffer.Print(HTMLWindow.StartFontColor(GetUIPropertyValue("labelcolor-description")));    
    buffer.Print("<br /><br />");
    buffer.Print(strtable.GetString("description"));
    buffer.Print("<br /><br />");
    buffer.Print(HTMLWindow.EndFontColor());    
    return buffer.AsString();
  }

  void LinkPropertyValue(string propertyID)
  {
    if(propertyID[0, 5] == "shunt") {
      int valmask = TYPE_FORWARD_SHUNT | TYPE_BACKWARD_SHUNT;
      int setvalue = Str.ToInt(propertyID[6, ]) & valmask;
      action = action & ~valmask | setvalue;  
    } else if(propertyID[0, 4] == "path") {
      int valmask = TYPE_FORWARD_PATH | TYPE_BACKWARD_PATH;
      int setvalue = Str.ToInt(propertyID[5, ]) & valmask;
      action = action & ~valmask | setvalue;  
    }
    else if ("border" == propertyID) {
      action = action ^ TYPE_STATION_BORDER;
    }
    else inherited(propertyID);
  }

  public void SetProperties(Soup soup)
  {
    inherited(soup);
    me.SetMeshVisible("forward-shunt", action & TYPE_FORWARD_SHUNT, 0.0);
    me.SetMeshVisible("backward-shunt", action & TYPE_BACKWARD_SHUNT, 0.0);
    me.SetMeshVisible("forward-path", action & TYPE_FORWARD_PATH, 0.0);
    me.SetMeshVisible("backward-path", action & TYPE_BACKWARD_PATH, 0.0);
    SetMeshVisible("station-border", action & TYPE_STATION_BORDER, 0.0);
  }

  void Init(Asset asset) 
  {
    inherited(asset);
    LoadUIProperties(asset);
  }

};