//=============================================================================
// File: zx_dsp_alsncodebreakermarker.gs
// Desc: Реализация маркера прерывания кода АЛСН
// Auth: Алексей 'Эрендир' Зверев 2021 © Trainz Dev Team
//       Камиль Ахметзянов aka kemal
//       Licence: GNU GPL
//=============================================================================

include "zx_dsp_alsncodebreaker.gs"
include "zx_dsp_ui.gs"

//=============================================================================
// Name: zx_DSP_ALSNCodeBreakerMarker
// Desc: Класс, реализующий маркер прерывания кода АЛСН
//=============================================================================
final class zx_DSP_ALSNCodeBreakerMarker isclass zx_DSP_ALSNCodeBreaker, zx_DSP_UI
{

  string GetPropertyType(string propertyID)
  {
    if(propertyID == "onlyoneside") return "link";
    return inherited(propertyID);
  }

  string GetDescriptionHTML(void){
    StringTable strtable = GetAsset().GetStringTable();
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    
    buffer.Print("<font size=10 color=#" + GetUIPropertyValue("labelcolor-title") + "><b>");
    buffer.Print(strtable.GetString("title"));
    buffer.Print("</b></font><br><br>");

    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("settings")), "colspan=3"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("onlyoneside") + ":"));
        buffer.Print(HTMLWindow.MakeCell(MakeCheckBox("onlyoneside", BreakOnlyOneSide(), strtable.GetString("tooltip"))));
      buffer.Print(HTMLWindow.EndRow());
    buffer.Print(HTMLWindow.EndTable());
    buffer.Print("<br /><br />");
    buffer.Print(HTMLWindow.StartTable("border=0 width=100%"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.StartCell());
          buffer.Print(HTMLWindow.StartFontColor(GetUIPropertyValue("labelcolor-description")));    
          buffer.Print(strtable.GetString("description"));
          buffer.Print(HTMLWindow.EndFontColor());    
        buffer.Print(HTMLWindow.EndCell());
      buffer.Print(HTMLWindow.EndRow());
    buffer.Print(HTMLWindow.EndTable());
    return buffer.AsString();
  }

  void LinkPropertyValue(string propertyID)
  {
    if(propertyID == "onlyoneside") SetBreakOnlyOneSide(!BreakOnlyOneSide());
    else inherited(propertyID);
  }

  void OnBreakOnlyOneSideChnaged() 
  {
    SetMeshVisible("arrow", !BreakOnlyOneSide(), 0.0);
  } 
  
  public void Init(Asset asset)
  {
    inherited(asset);
    LoadUIProperties(asset);
  }

};